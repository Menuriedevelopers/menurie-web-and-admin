<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $hidden = [
        'updated_at'];
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function chef()
    {
        return $this->belongsTo('App\Chef');
    }

    public function detail()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function address()
    {
        return $this->hasOne('App\ShippingAddress');
    }
}
