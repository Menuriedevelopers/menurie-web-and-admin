<?php 
if ( ! function_exists( 'get_city_list' ))
{
    function get_state_list( $old_data='' )
    {
		if( $old_data == "" )
			echo file_get_contents( app_path().'/Helpers/states.txt' );
		else{
			$pattern='<option value="'.$old_data.'">';
			$replace='<option value="'.$old_data.'" selected="selected">';
			$state_list=file_get_contents( app_path().'/Helpers/states.txt' );
			$state_list=str_replace($pattern,$replace,$state_list);
			echo $state_list;
		}
    }
}