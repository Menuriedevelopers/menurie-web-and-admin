<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function addons()
    {
        return $this->hasMany('App\CartAddons');
    }
}
