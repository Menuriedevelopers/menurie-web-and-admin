<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemRequest extends Model
{
    public function chef()
    {
        return $this->belongsTo('App\Chef');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
}
