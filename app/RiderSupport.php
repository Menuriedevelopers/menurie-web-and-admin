<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiderSupport extends Model
{
    public function rider()
    {
        return $this->belongsTo('App\Rider');
    }
}
