<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Order;

class OrderStatusChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $order;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
          'order-track.'.$this->order->id,
          'chef-order-update.'.$this->order->chef_id,
          'order-alert'
        ];
    }

    public function broadcastWith()
    {
        $status = 0;
        if($this->order->order_status == 'Pending')
        $status = 1;
        if($this->order->order_status == 'Preparing')
        $status = 2;
        if($this->order->order_status == 'Picked')
        $status = 3;
        if($this->order->order_status == 'Delivered')
        $status = 4;
        $extra = [
            'current'=> $status
        ];
        return array_merge($this->order->toArray(),$extra);
    }
}
