<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Validator;
use Twilio\Exceptions\TwilioException;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Mail\CustomerUpdateEmail;
use Illuminate\Http\Request;
use App\HotelRoomService;
use App\ShippingAddress;
use App\CustomerSupport;
use App\CustomerAddress;
use Twilio\Rest\Client;
use App\CustomerDevice;
use App\SurveyAnswer;
use App\OrderAddons;
use App\ItemRequest;
use App\ChefTiming;
use App\RiderOrder;
use App\AddonList;
use App\Category;
use App\Favorite;
use App\Customer;
use App\Rider;
use App\Order;
use App\Chef;
use App\Item;
use App\Cart;
use App\Chat;
use App\Ticket;
use DateTime;
use Illuminate\Support\Facades\Mail;
use stdClass;

class CustomerController extends Controller
{
    public function register(Request $request)
    {
        if ($request->has(['loginEmail', 'loginPassword'])) {
            $email = $request->loginEmail;
            $password = $request->loginPassword;
            $token = $request->token;
            $device = $request->device;
            $deviceName = $request->device_name;
            $customer = Customer::where('email', $email)->first();
            if (!$customer)
                return response()->json(['status' => false, 'data' => (object) array(), 'action' => 'No customer found regarding this email'], 200);
            else {
                if (Hash::check($password, $customer->password)) {
                    $this->updateToken($customer->id, $device, $deviceName, $token);
                    $customer->timezone = $request->timezone;
                    $customer->save();
                    return response()->json(['status' => true, 'data' => $customer, 'action' => 'Customer logged in'], 200);
                } else
                    return response()->json(['status' => false, 'data' => (object) array(), 'action' => 'Please enter correct password'], 200);
            }
        } else {
            $firstName = $request->first_name;
            $lastName = $request->last_name;
            $email = $request->email;
            $token = $request->token;
            $device = $request->device;
            $deviceName = $request->device_name;
            $platform = '';
            if ($request->platform)
                $platform = $request->platform;
            if ($request->filled('password'))
                $password = bcrypt($request->password);
            else
                $password = '';
            $phone =  $request->phone;

            if (!$email && !$request->apple_token)
                return response()->json(['status' => false, 'data' => new stdClass(), 'action' => 'Customer email not added'], 200);
            if (!$request->first_name || !$request->last_name)
                return response()->json(['status' => false, 'data' => new stdClass(), 'action' => 'Customer name not added'], 200);
            $customer = new Customer;
            $customer->first_name = $firstName;
            $customer->last_name = $lastName;
            $customer->timezone = $request->timezone;
            if ($request->apple_token) {
                $arr = explode('.', $request->apple_token);
                $id_token = base64_decode($arr[1]);
                $token_data = json_decode($id_token);
                $apple = 'apple.com';
                $customer->apple_id = $token_data->firebase->identities->$apple[0] ?? $request->token_data->sub;
            }
            if ($email)
                $customer->email = $email;
            $customer->password = $password;
            $customer->phone = $phone;
            $customer->platform = $platform;
            $customer->save();
            $this->updateToken($customer->id, $device, $deviceName, $token);
            return response()->json(['status' => true, 'data' => $customer, 'action' => 'Customer registered'], 200);
        }
    }

    public function updateToken($id, $device, $deviceName, $token)
    {
        $update = CustomerDevice::where('customer_id', $id)->where('device_id', $device)->where('name', $deviceName)->first();
        if ($update) {
            $update->fcm_token = $token;
            $update->save();
        } else {
            $device1 = new CustomerDevice;
            $device1->customer_id = $id;
            $device1->device_id = $device;
            $device1->name = $deviceName;
            $device1->fcm_token = $token;
            $device1->save();
        }
    }

    public function verifySignup(Request $request)
    {
        $email = $request->email;
        $platform = $request->platform;
        $token = $request->token;
        $device = $request->device;
        $deviceName = $request->device_name;
        if ($request->apple_token) {
            $arr = explode('.', $request->apple_token);
            $id_token = base64_decode($arr[1]);
            $token_data = json_decode($id_token);
            $apple = 'apple.com';
            $id = $token_data->firebase->identities->$apple[0] ?? $request->token_data->sub;
            $check = Customer::where([['apple_id', $id], ['platform', 'apple']])->first();
            if ($check) {
                $this->updateToken($check->id, $device, $deviceName, $token);
                return response()->json(['status' => true, 'data' => $check], 200);
            } else
                return response()->json(['status' => false, 'data' => new stdClass()], 200);
        } else {
            $check = Customer::where([['email', $email], ['platform', $platform]])->first();
            if ($check == null) {
                $customer = (object) array();
                return response()->json(['status' => false, 'data' => $customer], 200);
            } else {
                $find = Customer::find($check->id);
                $this->updateToken($check->id, $device, $deviceName, $token);
                return response()->json(['status' => true, 'data' => $find], 200);
            }
        }
    }

    public function verifySignup1(Request $request)
    {
        $email = '';
        $phone = '';
        if ($request->email)
            $email = $request->email;
        if ($request->phone)
            $phone = $request->phone;
        $platform = '';
        if ($request->platform)
            $platform = $request->platform;
        $token = $request->token;
        $check = null;
        $check1 = null;
        if ($request->email)
            $check = Customer::where([['email', $email], ['platform', $platform]])->first();
        if ($request->phone)
            $check1 = Customer::where('phone', $phone)->first();
        $error = '';
        if (!$check and $check1)
            $error = 'Phone';
        elseif ($check1 && $check)
            $error = 'Email and Phone';
        elseif ($check && !$check1)
            $error = 'Email';
        if ($error != '')
            return response()->json(['status' => false, 'action' => $error . ' already exists'], 200);
        else {
            $otp = app('App\Http\Controllers\API\ChefController')->sendOtp($phone);
            return response()->json(['status' => true, 'action' => $otp], 200);
        }
        return response()->json(['status' => false, 'action' => 'No data entered'], 200);
    }

    public function logout(Request $request)
    {
        $check = Customer::find($request->customerId);
        if ($check == null)
            return response()->json(['status' => true, 'action' => 'Customer not found'], 200);
        else {
            $checkDevice = CustomerDevice::where('customer_id', $check->id)->where('device_id', $request->deviceId)->first();
            if ($checkDevice)
                $checkDevice->delete();
            return response()->json(['status' => true, 'action' => 'Customer logged out'], 200);
        }
    }

    public function delete($id)
    {
        $check = Customer::find($id);
        if ($check) {
            $tokens = CustomerDevice::where('customer_id', $check->id)->delete();
            $items = Cart::where('customer_id', $check->id)->get();
            foreach ($items as $item)
                $item->delete();

            $items = Order::where('customer_id', $check->id)->get();
            foreach ($items as $item) {
                $del = RiderOrder::where('order_id', $item->id)->delete();
                $item->delete();
            }
            $tickets = Ticket::where('customer_id', $check->id)->delete();
            $supports = CustomerSupport::where('customer_id', $check->id)->delete();
            $check->delete();
            return response()->json(['status' => true, 'action' => 'Customer account deleted'], 200);
        } else
            return response()->json(['status' => true, 'action' => 'Customer not found'], 200);
    }

    public function verify(Request $request)
    {
        $phone =  $request->phone;
        $code = $request->code;
        $error = '';
        if ($phone == '' && !is_numeric($phone) && $code != '' && is_numeric($code))
            $error = 'Phone must be a valid and numeric';
        elseif ($phone != '' && is_numeric($phone) && $code == '' && !is_numeric($code))
            $error = 'Code must be in numeric';
        elseif ($phone == '' && !is_numeric($phone) && $code == '' && !is_numeric($code))
            $error = 'Phone must be a valid number And Code must be in numeric';
        if ($error == '') {
            /* Get credentials from .env */
            $token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_sid = getenv("TWILIO_SID");
            $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");

            $twilio = new Client($twilio_sid, $token);
            try {
                $verification = $twilio->verify->v2->services($twilio_verify_sid)
                    ->verificationChecks
                    ->create($code, array('to' => $phone));

                if ($verification->valid) {
                    return response()->json(['status' => true, 'action' => 'Phone number verified'], 200);
                } else
                    return response()->json(['status' => false, 'action' => 'Entered code is invalid'], 200);
            } catch (TwilioException $e) {
                return response()->json(['status' => false, 'action' => $e->getMessage()], 200);
            }
        } else
            return response()->json(['status' => false, 'action' => $error], 200);
    }

    public function newEmailVerify(Request $request)
    {
        $check = Customer::where('email', $request->email)->first();
        if ($check)
            return response()->json(['status' => false, 'data' => 0, 'action' => 'Email already exists']);
        else {
            $code = rand(100000, 999999);
            Mail::to($request->email)->send(new CustomerUpdateEmail($code));
            return response()->json(['status' => true, 'data' => $code, 'action' => 'Email otp sent']);
        }
    }

    public function profile($id)
    {
        $user = Customer::find($id);
        if ($user)
            return response()->json(['status' => true, 'data' => $user, 'action' => 'Profile of user'], 200);

        return response()->json(['status' => false, 'data' => [], 'action' => 'No user found'], 200);
    }

    public function notifyMe($id, $status)
    {
        $user = Customer::find($id);
        if ($user) {
            $user->notify = $status;
            $user->save();
            return response()->json(['status' => true, 'action' => ($status == 0) ? 'Notifications turned off' : 'Notifications turned on'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'No user found'], 200);
    }

    public function editProfile(Request $request)
    {
        $id = $request->customerId;
        $firstName = $request->first_name;
        $lastName = $request->last_name;
        $email = $request->email;
        $phone = $request->phone;
        $image = '';
        $check = Customer::find($id);
        if ($check) {
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $filename = $check->id . '-' . time() . '.' . $extension;
                if ($file->move('uploads/customer/', $filename))
                    $image = env('APP_URL') . '/uploads/customer/' . $filename;
                $check->image = $image;
            }
            if ($firstName)
                $check->first_name = $firstName;
            if ($lastName)
                $check->last_name = $lastName;
            if ($email) {
                $check1 = Customer::where('email', $email)->where('platform', '')->whereNotIn('id', [$check->id])->first();
                if ($check1)
                    return response()->json(['status' => false, 'action' => 'Email already exists'], 200);
                else
                    $check->email = $email;
            }
            if ($phone) {
                $check1 = Customer::where('phone', $phone)->whereNotIn('id', [$check->id])->first();
                if ($check1)
                    return response()->json(['status' => false, 'action' => 'Phone number already exists'], 200);
                else
                    $check->phone = $phone;
            }
            $check->save();
            $get = Customer::find($check->id);
            return response()->json(['status' => true, 'data' => $get, 'action' => 'Customer profile updated'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'No customer found'], 200);
    }

    public function forgotPassword(Request $request)
    {
        if (!$request->has('phone') && $request->phone == '')
            return response()->json(['status' => false, 'action' => 'Phone is required and should not be empty'], 200);
        else {
            $phone = $request->phone;
            $check = Customer::where('phone', $request->phone)->where('platform', '')->first();
            if (!$check)
                return response()->json(['status' => false, 'action' => 'No customer found of this phone number'], 200);
            else {
                $otp = app('App\Http\Controllers\API\ChefController')->sendOtp($phone);
                return response()->json(['status' => true, 'action' => 'An OTP is sent to your phone number to verify for reset password request'], 200);
            }
        }
    }

    public function resetPassword(Request $request)
    {
        $phone = $request->phone;
        $password = bcrypt($request->password);
        $error = '';
        if ($phone == '' && !is_numeric($phone) && $password != '')
            $error = 'Phone must be a valid and numeric';
        elseif ($phone != '' && is_numeric($phone) && $password == '')
            $error = 'Password should not be empty';
        elseif ($phone == '' && !is_numeric($phone) && $password == '')
            $error = 'Phone must be a valid number And password should not be empty';

        if ($error == '') {
            $check = Customer::where('phone', $request->phone)->first();
            if (!$check)
                return response()->json(['status' => false, 'action' => 'No customer found by this number'], 200);
            else {
                $check->password = $password;
                $check->save();
                return response()->json(['status' => true, 'action' => 'Password updated'], 200);
            }
        } else
            return response()->json(['status' => false, 'action' => $error], 200);
    }

    public function specialItems($user, $id)
    {
        $ids = Item::where([['approved', 1], ['special', 1], ['is_active', 1], ['chef_id', $id]])->latest()->get();
        $inactive = [];
        foreach ($ids as $item) {
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $id)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    // if ($check->status == 0)
                    //     $inactive[] = $item->id;
                    // else {
                    if ($check->status == 1 && $check->type == 0)
                        $inactive[] = $item->id;
                    // }
                }
            }
        }
        $arr = array();
        $items = Item::with('chef', 'category')->where([['approved', 1], ['special', 1], ['is_active', 1], ['chef_id', $id]])->whereNotIn('id', $inactive)->latest()->get()->take(10);
        foreach ($items as $item) {
            if ($item->category) {
                $categoryId = $item->category->id;
                $categoryName = $item->category->name;
            } else {
                $categoryId = 0;
                $categoryName = '';
            }
            $isFav = false;

            if (Favorite::where('item_id', $item->id)->where('user_id', $user->id)->first())
                $isFav = true;
            else
                $isFav = false;

            $arr1 = array(
                'id' => $item->id, 'categoryName' => $categoryName, 'item_name' => $item->item_name, 'item_image' => $item->item_image, 'price' => $item->price, 'is_favorite' => $isFav, 'rating' => '5.0'
            );
            array_push($arr, $arr1);
        }
        return $arr;
    }

    public function allSpecialItems($user, $id)
    {
        $items = Item::with('chef', 'category')->where([['approved', 1], ['special', 1], ['is_active', 1], ['chef_id', $id]])->latest()->get();
        $arr = array();
        foreach ($items as $item) {
            if ($item->category) {
                $categoryId = $item->category->id;
                $categoryName = $item->category->name;
            } else {
                $categoryId = 0;
                $categoryName = '';
            }
            $isFav = false;

            if (Favorite::where('item_id', $item->id)->where('user_id', $user)->first())
                $isFav = true;
            else
                $isFav = false;

            $arr[] = array(
                'id' => $item->id, 'categoryName' => $categoryName, 'item_name' => $item->item_name, 'item_image' => $item->item_image, 'price' => $item->price, 'is_favorite' => $isFav, 'rating' => '5.0'
            );
        }
        return response()->json(['status' => true, 'data' => $arr, 'action' => 'All special items by chef'], 200);
    }

    public function items($user, $category, $chefId)
    {
        $ids = Item::where('special', 0)->where('is_active', 1)->latest()->get();
        $inactive = [];
        foreach ($ids as $item) {
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $chefId)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    // if ($check->status == 0)
                    //     $inactive[] = $item->id;
                    // else {
                    if ($check->status == 1 && $check->type == 0)
                        $inactive[] = $item->id;
                    // }
                }
            }
        }
        if ($category == 0) {
            $cat = Category::where('name', 'Breakfast')->first();
            $items = Item::with('chef', 'category')->where('special', 0)->where('is_active', 1)->where('category_id', $cat->id)->whereNotIn('id', $inactive)->latest()->get()->take(10);
        } elseif ($category == 1) {
            $cat = Category::where('name', 'Lunch')->first();
            $items = Item::with('chef', 'category')->where('special', 0)->where('is_active', 1)->where('category_id', $cat->id)->whereNotIn('id', $inactive)->latest()->get()->take(10);
        } elseif ($category == 2) {
            $cat = Category::where('name', 'Dinner')->first();
            $items = Item::with('chef', 'category')->where('special', 0)->where('is_active', 1)->where('category_id', $cat->id)->whereNotIn('id', $inactive)->latest()->get()->take(10);
        } else
            $items = Item::with('chef', 'category')->where('category_id', $category)->where('special', 0)->where('is_active', 1)->whereNotIn('id', $inactive)->latest()->get()->take(10);
        $arr = array();
        foreach ($items as $item) {
            if ($item->category) {
                $categoryId = $item->category->id;
                $categoryName = $item->category->name;
            } else {
                $categoryId = 0;
                $categoryName = '';
            }
            if ($item->chef_id == 0) {
                $chefName = 'Menurie Chef';
                $chefImg = '';
            } else {
                $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                $chefImg = $item->chef->image;
            }
            $isFav = false;
            if (Favorite::where('item_id', $item->id)->where('user_id', $user->id)->first())
                $isFav = true;
            else
                $isFav = false;
            $arr1 = array(
                'id' => $item->id, 'categoryName' => $categoryName, 'item_name' => $item->item_name, 'item_image' => $item->item_image, 'price' => $item->price, 'is_favorite' => $isFav, 'rating' => '5.0'
            );
            array_push($arr, $arr1);
        }
        return $arr;
    }

    public function itemsSeeAll($user, $category, $chefId)
    {
        $ids = Item::where('special', 0)->where('is_active', 1)->latest()->get();
        $inactive = [];
        foreach ($ids as $item) {
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $chefId)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    // if ($check->status == 0)
                    //     $inactive[] = $item->id;
                    // else {
                    if ($check->status == 1 && $check->type == 0)
                        $inactive[] = $item->id;
                    // }
                }
            }
        }
        if ($category == 0) {
            $cat = Category::where('name', 'Breakfast')->first();
            $items = Item::with('chef', 'category')->where('special', 0)->where('is_active', 1)->where('category_id', $cat->id)->whereNotIn('id', $inactive)->latest()->Paginate(10);
        } elseif ($category == 1) {
            $cat = Category::where('name', 'Lunch')->first();
            $items = Item::with('chef', 'category')->where('special', 0)->where('is_active', 1)->where('category_id', $cat->id)->whereNotIn('id', $inactive)->latest()->Paginate(10);
        } elseif ($category == 2) {
            $cat = Category::where('name', 'Dinner')->first();
            $items = Item::with('chef', 'category')->where('special', 0)->where('is_active', 1)->where('category_id', $cat->id)->whereNotIn('id', $inactive)->latest()->Paginate(10);
        } else
            $items = Item::with('chef', 'category')->where('category_id', $category)->where('special', 0)->where('is_active', 1)->whereNotIn('id', $inactive)->latest()->Paginate(10);
        $arr = array();
        foreach ($items as $item) {
            if ($item->category) {
                $categoryId = $item->category->id;
                $categoryName = $item->category->name;
            } else {
                $categoryId = 0;
                $categoryName = '';
            }
            if ($item->chef_id == 0) {
                $chefName = 'Menurie Chef';
                $chefImg = '';
            } else {
                $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                $chefImg = $item->chef->image;
            }
            $isFav = false;
            if (Favorite::where('item_id', $item->id)->where('user_id', $user->id)->first())
                $isFav = true;
            else
                $isFav = false;
            $arr1 = array(
                'id' => $item->id, 'categoryName' => $categoryName, 'item_name' => $item->item_name, 'item_image' => $item->item_image, 'price' => $item->price, 'is_favorite' => $isFav, 'rating' => '5.0'
            );
            array_push($arr, $arr1);
        }
        return response()->json(['status' => true, 'data' => $arr, 'action' => 'Items by lunch breakfast dinner'], 200);
    }

    public function recommended($user, $chefId)
    {
        $ids = Item::where('special', 0)->where('is_active', 1)->latest()->get();
        $inactive = [];
        foreach ($ids as $item) {
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $chefId)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    if ($check->status == 1 && $check->type == 0)
                        $inactive[] = $item->id;
                }
            }
        }
        $items = Item::with('chef', 'category')->where('special', 0)->where('is_active', 1)->whereNotIn('id', $inactive)->latest()->get()->take(10);
        $arr = array();
        foreach ($items as $item) {
            if ($item->category) {
                $categoryId = $item->category->id;
                $categoryName = $item->category->name;
            } else {
                $categoryId = 0;
                $categoryName = '';
            }
            if ($item->chef_id == 0) {
                $chefName = 'Menurie Chef';
                $chefImg = '';
            } else {
                $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                $chefImg = $item->chef->image;
            }
            $isFav = false;
            if (Favorite::where('item_id', $item->id)->where('user_id', $user->id)->first())
                $isFav = true;
            else
                $isFav = false;
            $arr1 = array(
                'id' => $item->id, 'categoryName' => $categoryName, 'item_name' => $item->item_name, 'item_image' => $item->item_image, 'price' => $item->price, 'is_favorite' => $isFav, 'rating' => '5.0'
            );
            array_push($arr, $arr1);
        }
        return $arr;
    }

    public function allItems($user, $category, $chefId)
    {
        $user = Customer::find($user);
        $ids = Item::where('category_id', $category)->where('special', 0)->where('is_active', 1)->latest()->get();
        $inactive = [];
        foreach ($ids as $item) {
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $chefId)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    // if ($check->status == 0)
                    //     $inactive[] = $item->id;
                    // else {
                    if ($check->status == 1 && $check->type == 0)
                        $inactive[] = $item->id;
                    // }
                }
            } else
                $inactive[] = $item->id;
        }
        $items = Item::with('chef', 'category')->where('category_id', $category)->where('special', 0)->where('is_active', 1)->whereNotIn('id', $inactive)->latest()->get();
        $arr = array();
        foreach ($items as $item) {
            if ($item->category) {
                $categoryId = $item->category->id;
                $categoryName = $item->category->name;
            } else {
                $categoryId = 0;
                $categoryName = '';
            }
            if ($item->chef_id == 0) {
                $chefName = 'Menurie Chef';
                $chefImg = '';
            } else {
                $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                $chefImg = $item->chef->image;
            }
            $isFav = false;
            if (Favorite::where('item_id', $item->id)->where('user_id', $user->id)->first())
                $isFav = true;
            else
                $isFav = false;
            $arr[] = array(
                'id' => $item->id, 'categoryName' => $categoryName, 'item_name' => $item->item_name, 'item_image' => $item->item_image, 'price' => $item->price, 'is_favorite' => $isFav, 'rating' => '5.0'
            );
        }
        return response()->json(['status' => true, 'data' => $arr, 'action' => 'All items by category'], 200);
    }

    public function chefs($lat, $lng)
    {
        $getChefs = Chef::select('id', 'first_name', 'last_name', 'email', 'image', 'lat_long', 'order_range')->latest()->get();
        $distanceArr = [];
        foreach ($getChefs as $item) {
            if ($item->lat_long != '') {
                $arr = explode(',', $item->lat_long);
                if ($lat == $arr[0] && $lng == $arr[1])
                    array_push($distanceArr, array("distance" => 0.00, "chef" => $item->id));
                else {
                    $theta = $lng - $arr[1];
                    $dist = sin(deg2rad($lat)) * sin(deg2rad($arr[0])) +  cos(deg2rad($lat)) * cos(deg2rad($arr[0])) * cos(deg2rad($theta));
                    $dist = acos($dist);
                    $dist = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    // $kilometers = ($miles * 1.609344);
                    // $km = round($kilometers, 2);
                    if ($miles <= $item->order_range)
                        $distanceArr[] = array("distance" => $miles, "chef" => $item->id);
                }
            }
        }
        $arr = collect($distanceArr)->sortBy('distance');
        $main = [];
        foreach ($arr as $item)
            $main[] = $item;
        $chef = null;
        $soon = 0;
        foreach ($main as $item) {
            $check = Chef::find($item['chef']);
            date_default_timezone_set($check->timezone);
            $yesterday = date('l', strtotime("-1 days"));
            $time = date('H:i');
            $day = date('l');
            $getTime = ChefTiming::where('chef_id', $check->id)->where('day', $day)->first();
            $yesterdayTime = ChefTiming::where('chef_id', $check->id)->where('day', $yesterday)->first();
            if ($yesterdayTime) {
                if ($yesterdayTime->time != 'Closed') {
                    $breakTime = explode('-', $yesterdayTime->time);
                    $startTime = date_format(date_create($breakTime[0]), 'H:i');
                    $startTimeHour = date_format(date_create($breakTime[0]), 'H');
                    $endTime = date_format(date_create($breakTime[1]), 'H:i');
                    $endTimeHour = date_format(date_create($breakTime[1]), 'H');
                    if ($endTime < $startTime) {
                        $check->open = $yesterdayTime->time;
                        $chef = $check;
                        break;
                    }
                }
            }
            if (!$getTime)
                $check->open = 'Closed';
            else {
                if ($getTime->time == 'Closed')
                    $check->open = 'Closed';
                else {
                    $breakTime = explode('-', $getTime->time);
                    $startTime = date_format(date_create($breakTime[0]), 'H:i');
                    $endTime = date_format(date_create($breakTime[1]), 'H:i');
                    $dateA = date('Y-m-d H:m:s');
                    $d1 = new DateTime($startTime);
                    $d2 = new DateTime();
                    $interval = $d1->diff($d2);
                    // dd($interval->invert . ' - ' . $interval->i);
                    if ($interval->invert == 1 && $interval->i <= 30)
                        $soon = 1;
                    if ($startTime > $time)
                        $check->open = 'Closed';
                    elseif ($endTime < $startTime)
                        $check->open = $getTime->time;
                    elseif ($startTime <= $time && $endTime > $time) {
                        $check->open = $getTime->time;
                        $chef = $check;
                        break;
                    } elseif ($endTime < $time)
                        $check->open = 'Closed';
                }
            }
        }
        return ['chefs' => $getChefs, 'chef' => $chef, 'distances' => $distanceArr, 'soon' => $soon];
    }

    public function home($userId, $category = 0, Request $request)
    {
        $user = Customer::find($userId);
        if ($user) {
            if (isset($request->show) && $request->show == 0)
                return response()->json(['status' => true, 'show' => 0, 'chef_id' => 0, 'data' => array('chefs' => [], 'items' => [], 'special_items' => [], 'order_id' => 0)], 200);
            else {
                $chefs = $this->chefs($request->lat, $request->lng);
                if ($chefs['chef']) {
                    $sitems = $this->specialItems($user, $chefs['chef']->id);
                    $items = $this->items($user, $category, $chefs['chef']->id);
                    $recommended = $this->recommended($user, $chefs['chef']->id);
                    $chef = $chefs['chef'];
                    $order = Order::where('customer_id', $userId)->where('survey', 0)->where('order_status', 'Delivered')->latest()->first()->id ?? 0;
                    return response()->json(['status' => true, 'show' => 1, 'soon' => $chefs['soon'], 'chef_id' => $chef->id, 'data' => array('chefs' => $chefs['chefs'], 'items' => $items, 'time' => $items, 'special_items' => $sitems, 'recommended' => $recommended, 'order_id' => $order)], 200);
                } else {
                    if (count($chefs['distances']) == 0)
                        return response()->json(['status' => true, 'show' => 0, 'soon' => $chefs['soon'], 'chef_id' => 0, 'data' => array('chefs' => [], 'items' => [], 'special_items' => [], 'order_id' => 0)], 200);
                    else
                        return response()->json(['status' => true, 'show' => 2, 'soon' => $chefs['soon'], 'chef_id' => 0, 'data' => array('chefs' => $chefs['chefs'], 'items' => [], 'special_items' => [], 'order_id' => 0)], 200);
                }
            }
        } else
            return response()->json(['status' => true, 'show' => 0, 'soon' => 0, 'chef_id' => 0, 'data' => array('chefs' => [], 'items' => [], 'special_items' => [], 'order_id' => 0), 'action' => 'Customer not found'], 200);
    }

    public function orderByStatus($status, $customer)
    {
        if ($status == 'preparing')
            $orders = Order::with('address', 'detail')->where('customer_id', $customer)->where('order_status', 'Placed')->orWhere('order_status', $status)->where('customer_id', $customer)->orWhere('order_status', 'picked')->where('customer_id', $customer)->latest()->get();
        else
            $orders = Order::with('address', 'detail')->where('customer_id', $customer)->where('order_status', $status)->latest()->get();
        $main = [];
        foreach ($orders as $order) {
            $detail = array();
            foreach ($order->detail as $item) {
                $find = Item::find($item->item_id);
                $get = OrderAddons::where('order_detail_id', $item->id)->get();
                $addons = array();
                foreach ($get as $addon) {
                    $arr = array(
                        'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                    );
                    array_push($addons, $arr);
                }
                $arr1 = array(
                    'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
                );
                array_push($detail, $arr1);
            }
            $rider = (object) [];
            if ($order->order_status == 'Picked') {
                $log = RiderOrder::where('order_id', $order->id)->first();
                if ($log)
                    $rider = Rider::find($log->rider_id);
            }
            $main[] = array(
                'orderId' => $order->id, 'orderStatus' => $order->order_status, 'orderSurvey' => $order->survey, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to ?? '', 'deliveryAddress' => $order->address->address ?? '', 'deliveryNote' => $order->address->delivery_note ?? '', 'instruction' => $order->address->instruction ?? '', 'orderDetail' => $detail, 'rider' => $rider
            );
            // array_push($all, $main);
        }
        // foreach ($orders as $order) {
        //     $get = ShippingAddress::where('order_id', $order->id)->first();
        //     $order->deliver_to = $get->delivery_to;
        //     $order->address = $get->address;
        //     $order->note = $get->delivery_note;
        //     if ($get->delivery_to == 'hotel') {
        //         $hotel = HotelRoomService::where('order_id', $order->id)->first();
        //         $order->deliver_to = 'Hotel ' . $hotel->hotel_name . ' room no: ' . $hotel->room_no . ' on Floor no: ' . $hotel->floor . ' for: ' . $hotel->person_name;
        //     }
        //     foreach ($order->detail as $detail) {
        //         $item = Item::find($detail->item_id);
        //         if ($item) {
        //             $detail->item_name = $item->item_name;
        //             $detail->image = $item->item_image;
        //         } else {
        //             $detail->item_name = '';
        //             $detail->image = '';
        //         }
        //         $addons = OrderAddons::select('addon_id', 'quantity', 'price')->where('order_detail_id', $detail->id)->get();
        //         foreach ($addons as $item) {
        //             $get = AddonList::find($item->addon_id);
        //             $item->name = $get->name;
        //         }
        //         $detail->addons = $addons;
        //     }
        // }
        return response()->json(['status' => true, 'data' => $main], 200);
    }

    public function counts($id)
    {
        $cart = Cart::where('customer_id', $id)->count();
        $orders = Order::where('customer_id', $id)->where('order_status', '!=', 'Delivered')->where('order_status', '!=', 'Cancelled')->count();
        $ordersArr = Order::where('customer_id', $id)->where('order_status', '!=', 'Delivered')->where('order_status', '!=', 'Cancelled')->latest()->pluck('id');
        return response()->json(['status' => true, 'data' => array('cart' => $cart, 'orders' => $orders, 'list' => $ordersArr)]);
    }

    public function favorite($id, $userId)
    {
        $item = Item::find($id);
        $user = Customer::find($userId);
        if ($item) {
            $check = Favorite::where('item_id', $item->id)->where('user_id', $user->id)->first();
            if ($check) {
                $check->delete();
                return response()->json(['status' => true, 'action' => 'Item unfavorited'], 200);
            } else {
                $view = new Favorite();
                $view->item_id = $item->id;
                $view->user_id = $user->id;
                $view->save();
            }
            return response()->json(['status' => true, 'action' => 'Item favorited'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'Item not found'], 200);
    }

    public function favoritedUsers($loginId)
    {
        $find = Customer::find($loginId);
        if ($find) {
            $ids = Favorite::where('user_id', $loginId)->pluck('item_id');
            $items = Item::whereIn('id', $ids)->Paginate(10);
            $arr = array();
            foreach ($items as $item) {
                if ($item->category) {
                    $categoryId = $item->category->id;
                    $categoryName = $item->category->name;
                } else {
                    $categoryId = 0;
                    $categoryName = '';
                }
                $isFav = true;
                $arr[] = array(
                    'id' => $item->id, 'categoryName' => $categoryName, 'item_name' => $item->item_name, 'item_image' => $item->item_image, 'price' => $item->price, 'is_favorite' => $isFav, 'rating' => '5.0'
                );
            }
            return response()->json(['status' => true, 'data' => $arr, 'action' => 'User Item favorited'], 200);
        } else
            return response()->json(['status' => false, 'data' => [], 'action' => 'User not found'], 200);
    }

    public function all($userId, $chefId)
    {
        $user = Customer::find($userId);
        $ids = Item::where('special', 0)->where('is_active', 1)->get();
        $inactive = [];
        foreach ($ids as $item) {
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $chefId)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    // if ($check->status == 0)
                    //     $inactive[] = $item->id;
                    // else {
                    if ($check->status == 1 && $check->type == 0)
                        $inactive[] = $item->id;
                    // }
                }
            }
        }
        // dd($arr);
        $categories = Category::whereHas('items')->get();
        foreach ($categories as $item) {
            $items = Item::with('chef')->where('special', 0)->where('is_active', 1)->where('category_id', $item->id)->whereNotIn('id', $inactive)->latest()->get()->take(10);
            $arr = array();
            foreach ($items as $item1) {
                if ($item1->category) {
                    $categoryId = $item1->category->id;
                    $categoryName = $item1->category->name;
                } else {
                    $categoryId = 0;
                    $categoryName = '';
                }
                if ($item1->chef_id == 0) {
                    $chefName = 'Menurie Chef';
                    $chefImg = '';
                } else {
                    $chefName = $item1->chef->first_name . ' ' . $item1->chef->last_name;
                    $chefImg = $item1->chef->image;
                }
                $isFav = false;
                if (Favorite::where('item_id', $item1->id)->where('user_id', $user->id)->first())
                    $isFav = true;
                else
                    $isFav = false;
                $arr[] = array(
                    'id' => $item1->id, 'categoryName' => $categoryName, 'item_name' => $item1->item_name, 'item_image' => $item1->item_image, 'price' => $item1->price, 'is_favorite' => $isFav, 'rating' => '5.0'
                );
            }
            $item->items = $arr;
        }
        return response()->json(['status' => true, 'data' => $categories, 'action' => 'List of all categories and its items'], 200);
    }

    public function detail($id, $userId)
    {
        $user = Customer::find($userId);
        $item = Item::with('addons')->find($id);
        if ($item) {
            $category = Category::find($item->category_id);
            if ($category) {
                $item->categoryName = $category->name;
            } else {
                $item->categoryName = '';
            }
            if ($item->chef_id == 0) {
                $item->chefName = 'Menurie Chef';
                $item->chefImg = '';
            } else {
                $item->chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                $item->chefImg = $item->chef->image;
            }
            if (Favorite::where('item_id', $item->id)->where('user_id', $user->id)->first())
                $item->isFav = true;
            else
                $item->isFav = false;
            $item->rating = '5.0';
            return response()->json(['status' => true, 'data' => $item, 'action' => 'Item detail'], 200);
        } else
            return response()->json(['status' => false, 'data' => (object) [], 'action' => 'Item not found'], 200);
    }

    public function filterItem($userId, Request $request)
    {
        $user = Customer::find($userId);
        $ids = Item::where('chef_id', $request->chef_id)->where('special', 0)->where('is_active', 1)->where('item_name', 'LIKE', "%" . $request->keyword . "%")->get();
        $inactive = [];
        foreach ($ids as $item) {
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $request->chef_id)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    // if ($check->status == 0)
                    //     $inactive[] = $item->id;
                    // else {
                    if ($check->status == 1 && $check->type == 0)
                        $inactive[] = $item->id;
                    // }
                }
            }
        }
        $items = Item::with('chef', 'category')->where('special', 0)->where('is_active', 1)->whereNotIn('id', $inactive)->where('item_name', 'LIKE', "%" . $request->keyword . "%")->get();
        $arr = [];
        foreach ($items as $item1) {
            if ($item1->category) {
                $categoryId = $item1->category->id;
                $categoryName = $item1->category->name;
            } else {
                $categoryId = 0;
                $categoryName = '';
            }
            if ($item1->chef_id == 0) {
                $chefName = 'Menurie Chef';
                $chefImg = '';
            } else {
                $chefName = $item1->chef->first_name . ' ' . $item1->chef->last_name;
                $chefImg = $item1->chef->image;
            }
            $isFav = false;
            if (Favorite::where('item_id', $item1->id)->where('user_id', $user->id)->first())
                $isFav = true;
            else
                $isFav = false;
            $arr[] = array(
                'id' => $item1->id, 'categoryName' => $categoryName, 'item_name' => $item1->item_name, 'item_image' => $item1->item_image, 'price' => $item1->price, 'is_favorite' => $isFav, 'rating' => '5.0'
            );
        }
        return response()->json(['status' => true, 'data' => $arr, 'action' => 'List of all items by search'], 200);
    }

    public function support(Request $request)
    {
        $customer = Customer::find($request->customer_id);
        if ($customer) {
            if ($request->message) {
                $support = new CustomerSupport();
                $support->customer_id = $customer->id;
                $support->message = $request->message;
                $support->save();
                return response()->json(['status' => true, 'action' => 'Support message sent!']);
            } else
                return response()->json(['status' => false, 'action' => 'Support message should not be empty']);
        } else
            return response()->json(['status' => false, 'action' => 'Customer not found']);
    }

    public function dismissSurvey($id)
    {
        $order = Order::find($id);
        if ($order) {
            $order->survey = 2;
            $order->save();
            return response()->json(['status' => true, 'action' => 'Order survey dismissed']);
        } else
            return response()->json(['status' => false, 'action' => 'Order not found']);
    }

    public function saveSurvey(Request $request)
    {
        $order = Order::find($request->order);
        if ($order) {
            $customer = Customer::find($request->customer);
            if ($customer) {
                $options = explode(',', $request->options);
                $descriptions = explode(',', $request->descriptions);
                // dd($descriptions[3]);
                foreach ($descriptions as $index => $item) {
                    $survey = new SurveyAnswer();
                    $survey->customer_id = $customer->id;
                    $survey->order_id = $order->id;
                    $survey->question_id = $index;
                    if ($index == 5)
                        $survey->option = 6;
                    else
                        $survey->option = $options[$index];
                    $survey->description = $item;
                    $survey->save();
                }
                $order->survey = 1;
                $order->save();
                return response()->json(['status' => true, 'action' => 'Survey added']);
            } else
                return response()->json(['status' => false, 'action' => 'Customer not found']);
        } else
            return response()->json(['status' => false, 'action' => 'Order not found']);
    }

    public function addAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'location' => 'required|string',
            'name' => 'required|string',
            'customer_id' => 'required|exists:customers,id',
        ]);
        $errors = $validator->errors();
        if ($validator->fails())
            return response()->json(['status' => false, 'action' => $errors->first()]);
        else {
            if (CustomerAddress::where('customer_id', $request->customer_id)->where('title', $request->title)->first())
                return response()->json(['status' => false, 'action' => 'Address already exists']);
            else {
                $address = new CustomerAddress();
                $address->customer_id = $request->customer_id;
                $address->location = $request->location;
                $address->title = $request->title;
                $address->name = $request->name;
                $address->save();
                return response()->json(['status' => true, 'action' => 'Address saved']);
            }
        }
    }

    public function addresses($id)
    {
        $customer = Customer::find($id);
        if ($customer) {
            $list = CustomerAddress::where('customer_id', $id)->get();
            return response()->json(['status' => true, 'data' => $list, 'action' => 'List of address']);
        } else
            return response()->json(['status' => false, 'data' => [], 'action' => 'Customer not found']);
    }

    public function deleteAddress($id)
    {
        $address = CustomerAddress::find($id);
        if ($address) {
            $address->delete();
            return response()->json(['status' => true, 'action' => 'Customer address deleted']);
        } else
            return response()->json(['status' => false, 'action' => 'Customer address not found']);
    }

    public function addTicket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'description' => 'required|string',
            'customer_id' => 'required|exists:customers,id',
        ]);
        $errors = $validator->errors();
        if ($validator->fails())
            return response()->json(['status' => false, 'action' => $errors->first()]);
        else {
            $customer = Customer::find($request->customer_id);
            $ticket = new Ticket();
            $ticket->customer_id = $request->customer_id;
            $ticket->type = $request->type;
            $ticket->description = $request->description;
            if ($request->order_id)
                $ticket->order_id = $request->order_id;
            $ticket->date_time = date('Y-m-d H:i:s');
            $ticket->save();
            $chat_message = new Chat();
            $chat_message->from = $request->customer_id;
            $chat_message->to = 0;
            $chat_message->from_to = $request->customer_id . '_0';
            $chat_message->message = $request->description;
            $chat_message->time = strtotime(date('Y-m-d H:i:s'));
            $chat_message->ticket_id = $ticket->id;
            $chat_message->save();
            $find = Ticket::find($ticket->id);
            return response()->json(['status' => true, 'data' => $find, 'action' => 'Ticket saved']);
        }
    }

    public function tickets($id, $type)
    {
        $customer = Customer::find($id);
        if ($customer) {
            $list = Ticket::where('status', $type)->where('customer_id', $id)->get();
            return response()->json(['status' => true, 'data' => $list, 'action' => 'List of tickets']);
        } else
            return response()->json(['status' => false, 'data' => [], 'action' => 'Customer not found']);
    }

    public function ticket($id)
    {
        $messages = Chat::select('id', 'from', 'to', 'from_to', 'message', 'is_read', 'time', 'created_at')->where('ticket_id', $id)->latest()->get();
        foreach ($messages as $message) {
            $receiver = new stdClass();
            if ($message->from == 0)
                $message->name = 'Admin';
            else {
                $customer = Customer::find($message->from);
                $message->name = $customer->first_name . ' ' . $customer->last_name;
            }
        }
        return response()->json(['status' => true, 'data' => $messages, 'action' => 'List of ticket messages'], 200);
    }

    public function closeTicket($id)
    {
        $address = Ticket::find($id);
        if ($address) {
            $address->status = 1;
            $address->save();
            return response()->json(['status' => true, 'action' => 'Customer ticket closed']);
        } else
            return response()->json(['status' => false, 'action' => 'Customer ticket not found']);
    }
}
