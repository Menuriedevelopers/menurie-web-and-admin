<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CartAddons;
use App\AddonList;
use App\Cart;
use App\Item;

class CartController extends Controller
{
    public function index($customer)
    {
        $list = Cart::with('item', 'addons')->where('customer_id', $customer)->get();
        $cart = array();
        foreach ($list as $item) {
            $addons = array();
            foreach ($item->addons as $addon) {
                $getAddon = AddonList::where('id', $addon->addon_id)->first();
                $arr1 = array(
                    'addonName' => $getAddon->name, 'addonPrice' => $addon->price, 'addonQuantity' => $addon->quantity
                );
                array_push($addons, $arr1);
            }
            $arr = array(
                'cart_id' => $item->id, 'itemId' => $item->item->id, 'itemName' => $item->item->item_name, 'itemImage' => $item->item->item_image, 'itemPrice' => $item->item->price, 'quantity' => $item->quantity, 'total' => $item->price, 'addon' => $addons
            );
            array_push($cart, $arr);
        }
        return response()->json(['status' => true, 'data' => $cart, 'action' => 'List of user\'s cart'], 200);
    }

    public function store(Request $request, $customer)
    {
        $itemId = $request->itemId;
        $addons = $request->addons;
        $quantity = $request->itemQuantity;
        $item = Item::find($itemId);
        $cartId = 0;
        if ($request->addonId) {
            $check = Cart::whereHas('addons')->where('customer_id', $customer)->where('item_id', $itemId)->latest()->first();
            if ($check) {
                $countAddons = count($check->addons);
                if ($countAddons != count($request->addonId))
                    $check = Cart::whereHas('addons')->whereNotIn('id', [$check->id])->where('customer_id', $customer)->where('item_id', $itemId)->latest()->first();
            }
        } else
            $check = Cart::where('customer_id', $customer)->where('item_id', $itemId)->latest()->first();
        $addons = array();
        if ($check) {
            if ($request->addonId) {
                $addonsList = CartAddons::where('cart_id', $check->id)->latest()->pluck('addon_id')->toArray();
                if (count($addonsList) != 0) {
                    if (array_diff($addonsList, $request->addonId) == array_diff($request->addonId, $addonsList)) {
                        for ($i = 0; $i < count($request->addonId); $i++) {
                            $addon = CartAddons::where('cart_id', $check->id)->where('addon_id', $request->addonId[$i])->first();
                            $addon->quantity = $addon->quantity + $quantity;
                            $addon->save();
                        }
                        $check->quantity = $quantity + $check->quantity;
                        $check->save();
                        $cartId = $check->id;
                    } else {
                        $save = new Cart;
                        $save->customer_id = $customer;
                        $save->item_id = $itemId;
                        $save->quantity = $quantity;
                        $save->price = $item->price;
                        $save->save();
                        $cartId = $save->id;
                        for ($i = 0; $i < count($request->addonId); $i++) {
                            $find = AddonList::find($request->addonId[$i]);
                            if ($find) {
                                $cartAddon = new CartAddons;
                                $cartAddon->cart_id = $cartId;
                                $cartAddon->addon_id = $request->addonId[$i];
                                $cartAddon->quantity = $quantity;
                                $cartAddon->price = $find->price;
                                $cartAddon->save();
                                $check->price = $check->price + $find->price;
                                $check->save();
                                $arr = array(
                                    'addonId' => $find->name, 'addonQuantity' => (int) $quantity, 'addonPrice' => floatval($find->price)
                                );
                                array_push($addons, $arr);
                            }
                        }
                    }
                } else {
                    $save = new Cart;
                    $save->customer_id = $customer;
                    $save->item_id = $itemId;
                    $save->quantity = $quantity;
                    $save->price = $item->price;
                    $save->save();
                    $cartId = $save->id;
                    for ($i = 0; $i < count($request->addonId); $i++) {
                        $find = AddonList::find($request->addonId[$i]);
                        if ($find) {
                            $cartAddon = new CartAddons;
                            $cartAddon->cart_id = $cartId;
                            $cartAddon->addon_id = $request->addonId[$i];
                            $cartAddon->quantity = $quantity;
                            $cartAddon->price = $find->price;
                            $cartAddon->save();
                            $check->price = $check->price + $find->price;
                            $check->save();
                            $arr = array(
                                'addonId' => $find->name, 'addonQuantity' => (int) $quantity, 'addonPrice' => floatval($find->price)
                            );
                            array_push($addons, $arr);
                        }
                    }
                }
            } else {
                $without = Cart::doesntHave('addons')->where('customer_id', $customer)->where('item_id', $itemId)->latest()->first();
                if ($without) {
                    $without->quantity = $quantity + $without->quantity;
                    $without->save();
                } else {
                    $save = new Cart;
                    $save->customer_id = $customer;
                    $save->item_id = $itemId;
                    $save->quantity = $quantity;
                    $save->price = $item->price;
                    $save->save();
                }
            }
        } else {
            $save = new Cart;
            $save->customer_id = $customer;
            $save->item_id = $itemId;
            $save->quantity = $quantity;
            $save->price = $item->price;
            $save->save();
            $cartId = $save->id;
            if ($request->addonId) {
                for ($i = 0; $i < count($request->addonId); $i++) {
                    $find = AddonList::find($request->addonId[$i]);
                    if ($find) {
                        $cartAddon = new CartAddons;
                        $cartAddon->cart_id = $cartId;
                        $cartAddon->addon_id = $request->addonId[$i];
                        $cartAddon->quantity = $quantity;
                        $cartAddon->price = $find->price;
                        $cartAddon->save();
                        $save->price = $save->price + $find->price;
                        $save->save();
                        $arr = array(
                            'addonId' => $find->name, 'addonQuantity' => (int) 1, 'addonPrice' => floatval($find->price)
                        );
                        array_push($addons, $arr);
                    }
                }
            }
            if ($request->addons) {
                $get = explode(',', $request->addons);
                foreach ($get as $index => $item) {
                    $find = AddonList::find($item);
                    if ($find) {
                        $cartAddon = new CartAddons;
                        $cartAddon->cart_id = $cartId;
                        $cartAddon->addon_id = $item;
                        $cartAddon->quantity = $quantity;
                        $cartAddon->price = $find->price;
                        $cartAddon->save();
                        $save->price = $save->price + $find->price;
                        $save->save();
                        $arr = array(
                            'addonId' => $find->name, 'addonQuantity' => (int) 1, 'addonPrice' => floatval($find->price)
                        );
                        array_push($addons, $arr);
                    }
                }
            }
        }
        return response()->json(['status' => true, 'action' => 'Item added to cart'], 200);
    }

    public function update(Request $request, $cartId)
    {
        $itemId = $request->itemId;
        $addons = $request->addons;
        $quantity = $request->itemQuantity;
        $price = $request->totalPrice;
        $find = Cart::find($cartId);
        if ($find) {
            $find->quantity = $quantity;
            $find->save();
            $data = Cart::find($cartId);
            return response()->json(['status' => true, 'data' => $data, 'action' => 'Cart updated'], 200);
        } else
            return response()->json(['status' => false, 'data' => (object) [], 'action' => 'Cart not updated'], 200);
    }

    public function softDelete($cartId)
    {
        $delete = Cart::destroy($cartId);
        if ($delete)
            return response()->json(['status' => true, 'action' => 'Cart item deleted'], 200);
        else
            return response()->json(['status' => false, 'action' => 'Cart item not deleted'], 200);
    }

    public function deleteAll($customerId)
    {
        $check = Cart::where('customer_id', $customerId)->get();
        if ($check) {
            $delete = Cart::where('customer_id', $customerId)->delete();
            if ($delete)
                return response()->json(['status' => true, 'action' => 'Cart empty'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'No item is added in user\'s cart'], 200);
    }
}
