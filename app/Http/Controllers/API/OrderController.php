<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\HotelRoomService;
use App\ShippingAddress;
use App\CustomerDevice;
use App\OrderAddons;
use App\RiderOrder;
use Carbon\Carbon;
use App\AddonList;
use App\Chat;
use App\Customer;
use App\Order;
use App\Rider;
use App\Chef;
use App\Item;
use Pusher\Pusher;

class OrderController extends Controller
{
    public $pusher;

    public function __construct()
    {
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => false
        );
        $this->pusher = new Pusher(
            "68f9e094a21250f4ccdc",
            "674c8a47c4552b15ca15",
            "1282636",
            $options
        );
    }

    public function orderCounts($id)
    {
        $chef = Chef::find($id);
        if ($chef) {
            $placedOrders = Order::where('chef_id', $chef->id)->where('order_status', 'Placed')->count('id');
            $confirmedOrders = Order::where('chef_id', $chef->id)->where('order_status', 'Preparing')->count('id');
            $cancelledOrders = Order::where('chef_id', $chef->id)->where('order_status', 'Cancelled')->count('id');
            $pickedOrders = Order::where('chef_id', $chef->id)->where('order_status', 'Picked')->count('id');
            $deliveredOrders = Order::where('chef_id', $chef->id)->where('order_status', 'Delivered')->count('id');
            $date = date('Y-m-d');
            $specialItems = Order::where('chef_id', $chef->id)->whereDate('created_at', $date)->where('order_status', 'Delivered')->count('id');
            $main = (object) array();
            $main->special = (int) $specialItems;
            $main->placed = (int) $placedOrders;
            $main->confirmed = (int) $confirmedOrders;
            $main->picked = (int) $pickedOrders;
            $main->delivered = (int) $deliveredOrders;
            $main->cancelled = (int) $cancelledOrders;
            return response()->json(['status' => true, 'data' => $main, 'action' => 'Orders count'], 200);
        } else
            return response()->json(['status' => false, 'data' => [], 'action' => 'No chef found'], 200);
    }

    public function ordersList($type, $chef)
    {
        $main = (object) array();
        if ($type == 'today') {
            $date = Carbon::today();
            $confirmedOrders = Order::where('chef_id', $chef)->where('order_status', 'Preparing')->whereDate('created_at', $date)->count('id');
            $cancelledOrders = Order::where('chef_id', $chef)->where('order_status', 'Cancelled')->whereDate('created_at', $date)->count('id');
            $pickedOrders = Order::where('chef_id', $chef)->where('order_status', 'Picked')->whereDate('created_at', $date)->count('id');
            $deliveredOrders = Order::where('chef_id', $chef)->where('order_status', 'Delivered')->whereDate('created_at', $date)->count('id');
            $main->confirmed = (int) $confirmedOrders;
            $main->picked = (int) $pickedOrders;
            $main->delivered = (int) $deliveredOrders;
            $main->cancelled = (int) $cancelledOrders;
            $orders = Order::with('customer', 'detail')->where('chef_id', $chef)->whereDate('created_at', $date)->latest()->get();
            foreach ($orders as $order) {
                $get = ShippingAddress::where('order_id', $order->id)->first();
                $order->deliver_to = $get->delivery_to;
                $order->address = $get->address;
                $order->note = $get->delivery_note;
                if ($get->delivery_to == 'hotel') {
                    $hotel = HotelRoomService::where('order_id', $order->id)->first();
                    $order->deliver_to = 'Hotel ' . $hotel->hotel_name . ' room no: ' . $hotel->room_no . ' on Floor no: ' . $hotel->floor . ' for: ' . $hotel->person_name;
                }
                foreach ($order->detail as $detail) {
                    $item = Item::find($detail->item_id);
                    if ($item) {
                        $detail->item_name = $item->item_name;
                        $detail->image = $item->item_image;
                    } else {
                        $detail->item_name = '';
                        $detail->image = '';
                    }
                    $addons = OrderAddons::select('addon_id', 'quantity', 'price')->where('order_detail_id', $detail->id)->get();
                    foreach ($addons as $item) {
                        $get1 = AddonList::find($item->addon_id);
                        $item->name = $get1->name;
                    }
                    $detail->addons = $addons;
                }
            }
            $main->orders = $orders;
        } elseif ($type == 'week') {
            $date = Carbon::today()->subDay(7);
            $confirmedOrders = Order::where('chef_id', $chef)->where('order_status', 'Preparing')->whereDate('created_at', '>=', $date)->count('id');
            $cancelledOrders = Order::where('chef_id', $chef)->where('order_status', 'Cancelled')->whereDate('created_at', '>=', $date)->count('id');
            $pickedOrders = Order::where('chef_id', $chef)->where('order_status', 'Picked')->whereDate('created_at', '>=', $date)->count('id');
            $deliveredOrders = Order::where('chef_id', $chef)->where('order_status', 'Delivered')->whereDate('created_at', '>=', $date)->count('id');
            // $main->placed = (int) $placedOrders;
            $main->confirmed = (int) $confirmedOrders;
            $main->picked = (int) $pickedOrders;
            $main->delivered = (int) $deliveredOrders;
            $main->cancelled = (int) $cancelledOrders;
            $orders = Order::with('customer', 'detail')->where('chef_id', $chef)->whereDate('created_at', '>=', $date)->latest()->get();
            foreach ($orders as $order) {
                $get = ShippingAddress::where('order_id', $order->id)->first();
                $order->deliver_to = $get->delivery_to;
                $order->address = $get->address;
                $order->note = $get->delivery_note;
                if ($get->delivery_to == 'hotel') {
                    $hotel = HotelRoomService::where('order_id', $order->id)->first();
                    $order->deliver_to = 'Hotel ' . $hotel->hotel_name . ' room no: ' . $hotel->room_no . ' on Floor no: ' . $hotel->floor . ' for: ' . $hotel->person_name;
                }
                foreach ($order->detail as $detail) {
                    $item = Item::find($detail->item_id);
                    if ($item) {
                        $detail->item_name = $item->item_name;
                        $detail->image = $item->item_image;
                    } else {
                        $detail->item_name = '';
                        $detail->image = '';
                    }
                    $addons = OrderAddons::select('addon_id', 'quantity', 'price')->where('order_detail_id', $detail->id)->get();
                    foreach ($addons as $item) {
                        $get1 = AddonList::find($item->addon_id);
                        $item->name = $get1->name;
                    }
                    $detail->addons = $addons;
                }
            }
            $main->orders = $orders;
        } elseif ($type == 'month') {
            $date = Carbon::today()->subDay(30);
            $confirmedOrders = Order::where('chef_id', $chef)->where('order_status', 'Preparing')->whereDate('created_at', '>=', $date)->count('id');
            $cancelledOrders = Order::where('chef_id', $chef)->where('order_status', 'Cancelled')->whereDate('created_at', '>=', $date)->count('id');
            $pickedOrders = Order::where('chef_id', $chef)->where('order_status', 'Picked')->whereDate('created_at', '>=', $date)->count('id');
            $deliveredOrders = Order::where('chef_id', $chef)->where('order_status', 'Delivered')->whereDate('created_at', '>=', $date)->count('id');
            // $main->placed = (int) $placedOrders;
            $main->confirmed = (int) $confirmedOrders;
            $main->picked = (int) $pickedOrders;
            $main->delivered = (int) $deliveredOrders;
            $main->cancelled = (int) $cancelledOrders;
            $orders = Order::with('customer', 'detail')->where('chef_id', $chef)->whereDate('created_at', '>=', $date)->latest()->get();
            foreach ($orders as $order) {
                $get = ShippingAddress::where('order_id', $order->id)->first();
                $order->deliver_to = $get->delivery_to;
                $order->address = $get->address;
                $order->note = $get->delivery_note;
                if ($get->delivery_to == 'hotel') {
                    $hotel = HotelRoomService::where('order_id', $order->id)->first();
                    $order->deliver_to = 'Hotel ' . $hotel->hotel_name . ' room no: ' . $hotel->room_no . ' on Floor no: ' . $hotel->floor . ' for: ' . $hotel->person_name;
                }
                foreach ($order->detail as $detail) {
                    $item = Item::find($detail->item_id);
                    if ($item) {
                        $detail->item_name = $item->item_name;
                        $detail->image = $item->item_image;
                    } else {
                        $detail->item_name = '';
                        $detail->image = '';
                    }
                    $addons = OrderAddons::select('addon_id', 'quantity', 'price')->where('order_detail_id', $detail->id)->get();
                    foreach ($addons as $item) {
                        $get1 = AddonList::find($item->addon_id);
                        $item->name = $get1->name;
                    }
                    $detail->addons = $addons;
                }
            }
            $main->orders = $orders;
        }
        return response()->json(['status' => true, 'data' => $main], 200);
    }

    public function orderByStatus1($status, $chef)
    {
        $orders = Order::with('customer', 'detail')->where('chef_id', $chef)->where('order_status', $status)->get();
        foreach ($orders as $order) {
            $get = ShippingAddress::where('order_id', $order->id)->first();
            $order->deliver_to = $get->delivery_to;
            $order->address = $get->address;
            $order->note = $get->delivery_note;
            if ($get->delivery_to == 'hotel') {
                $hotel = HotelRoomService::where('order_id', $order->id)->first();
                $order->deliver_to = 'Hotel ' . $hotel->hotel_name . ' room no: ' . $hotel->room_no . ' on Floor no: ' . $hotel->floor . ' for: ' . $hotel->person_name;
            }
            foreach ($order->detail as $detail) {
                $item = Item::find($detail->item_id);
                if ($item) {
                    $detail->item_name = $item->item_name;
                    $detail->image = $item->item_image;
                } else {
                    $detail->item_name = '';
                    $detail->image = '';
                }
                $addons = OrderAddons::select('addon_id', 'quantity', 'price')->where('order_detail_id', $detail->id)->get();
                foreach ($addons as $item) {
                    $get1 = AddonList::find($item->addon_id);
                    $item->name = $get1->name;
                }
                $detail->addons = $addons;
            }
        }
        return response()->json(['status' => true, 'data' => $orders], 200);
    }

    public function orderByStatus($status, $chef)
    {
        $check = Chef::find($chef);
        if ($check) {
            if ($status == 'all')
                $orders = Order::with('detail')->where('chef_id', $chef)->where('order_status', 'Placed')->orWhere('order_status', 'Preparing')->where('chef_id', $chef)->orWhere('order_status', 'picked')->where('chef_id', $chef)->latest()->Paginate(12);
            elseif ($status == 'today')
                $orders = Order::with('detail')->where('chef_id', $chef)->where('order_status', 'Delivered')->whereDate('created_at', date('Y-m-d'))->latest()->Paginate(12);
            else
                $orders = Order::with('detail')->where('chef_id', $chef)->where('order_status', $status)->latest()->Paginate(12);
            $main = [];
            foreach ($orders as $order) {
                foreach ($order->detail as $item) {
                    $find = Item::find($item->item_id);
                    $get = OrderAddons::where('order_detail_id', $item->id)->get();
                    $addons = array();
                    foreach ($get as $addon) {
                        $arr = array(
                            'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                        );
                        array_push($addons, $arr);
                    }
                    /*$arr1 = array(
                    'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
                );
                array_push($detail, $arr1);*/
                    $item->orderItemName = $find->item_name;
                    $item->orderItemImage = $find->item_image;
                    $item->orderItemQuantity = $item->quantity;
                    $item->orderItemPrice = $item->price;
                    $item->addons = $addons;
                }
                $rider = (object) [];
                if ($order->order_status == 'Picked') {
                    $log = RiderOrder::where('order_id', $order->id)->first();
                    if ($log)
                        $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email', 'lat_lng')->find($log->rider_id);
                    if ($rider) {
                        $latLng = explode(',', $rider->lat_lng);
                        $rider->lat = $latLng[0];
                        $rider->lng = $latLng[1];
                    }
                }
                /*$main[] = array(
                'orderId' => $order->id, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to ?? '', 'deliveryAddress' => $order->address->address ?? '', 'deliveryNote' => $order->address->delivery_note ?? '', 'instruction' => $order->address->instruction ?? '', 'orderDetail' => $detail, 'rider' => $rider
            );*/
                $address = ShippingAddress::where('order_id', $order->id)->first();
                $order->deliveryTo = $address->delivery_to ?? '';
                $order->deliveryAddress = $address->address ?? '';
                $order->deliveryNote = $address->delivery_note ?? '';
                $order->instruction = $address->instruction ?? '';
                // $order->orderDetail = $detail;
                $order->rider = $rider;
            }
            return response()->json(['status' => true, 'suspended' => $check->suspended, 'data' => $orders], 200);
        } else
            return response()->json(['status' => false, 'suspended' => 0, 'data' => []], 200);
    }

    public function orderByStatusArr($id)
    {
        $order = Order::with('detail')->find($id);
        foreach ($order->detail as $item) {
            $find = Item::find($item->item_id);
            $get = OrderAddons::where('order_detail_id', $item->id)->get();
            $addons = array();
            foreach ($get as $addon) {
                $arr = array(
                    'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                );
                array_push($addons, $arr);
            }
            $item->orderItemName = $find->item_name;
            $item->orderItemImage = $find->item_image;
            $item->orderItemQuantity = $item->quantity;
            $item->orderItemPrice = $item->price;
            $item->addons = $addons;
        }
        $rider = (object) [];
        if ($order->order_status == 'Picked') {
            $log = RiderOrder::where('order_id', $order->id)->first();
            if ($log)
                $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email', 'lat_lng')->find($log->rider_id);
            if ($rider) {
                $latLng = explode(',', $rider->lat_lng);
                $rider->lat = $latLng[0];
                $rider->lng = $latLng[1];
            }
        }
        $address = ShippingAddress::where('order_id', $order->id)->first();
        $order->deliveryTo = $address->delivery_to ?? '';
        $order->deliveryAddress = $address->address ?? '';
        $order->deliveryNote = $address->delivery_note ?? '';
        $order->instruction = $address->instruction ?? '';
        $order->rider = $rider;
        return $order;
    }

    public function orderAssignedRider($id)
    {
        $order = Order::find($id);
        if ($order) {
            $find = RiderOrder::where('order_id', $id)->first();
            if ($find) {
                $rider = Rider::find($find->rider_id);
                $get = app('App\Http\Controllers\API\CheckoutController')->orderObj($order->id);
                $this->pusher->trigger('order-' . $get['orderId'], 'change-event', $get);
                return response()->json(['status' => true, 'data' => $rider, 'action' => 'Assigned rider detail'], 200);
            } else
                return response()->json(['status' => false, 'data' => (object) [], 'action' => 'Rider not found'], 200);
        } else
            return response()->json(['status' => false, 'data' => (object) [], 'action' => 'Order not found'], 200);
    }

    public function orderDetail($order)
    {
        $order = Order::with('address', 'detail')->where('id', $order)->first();
        $chef = Chef::find($order->chef_id);
        if ($chef->lat_long == '')
            $chefll = ['', ''];
        else
            $chefll = explode(',', $chef->lat_long);
        $detail = array();
        $customer = Customer::find($order->customer_id);
        foreach ($order->detail as $item) {
            $find = Item::find($item->item_id);
            $get = OrderAddons::where('order_detail_id', $item->id)->get();
            $addons = array();
            foreach ($get as $addon) {
                $arr = array(
                    'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                );
                array_push($addons, $arr);
            }
            $arr1 = array(
                'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
            );
            array_push($detail, $arr1);
        }
        $rider = (object) [];
        if ($order->order_status == 'Picked') {
            $log = RiderOrder::where('order_id', $order->id)->first();
            if ($log)
                $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email', 'lat_lng')->find($log->rider_id);
            $latLng = explode(',', $rider->lat_lng);
            $rider->lat = $latLng[0];
            $rider->lng = $latLng[1];
        }
        $arr = array(
            'orderId' => $order->id, 'orderLat' => $order->lat, 'orderLng' => $order->lng, 'chefLat' => $chefll[0], 'chefLng' => $chefll[1], 'customerId' => $customer->id, 'customerFirstName' => $customer->first_name, 'customerLastName' => $customer->last_name, 'customerImage' => $customer->image, 'customerCountryCode' => $customer->country_code, 'customerPhone' => $customer->phone, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to, 'deliveryAddress' => $order->address->address, 'deliveryNote' => $order->address->delivery_note, 'instruction' => $order->address->instruction, 'orderDetail' => $detail, 'rider' => $rider
        );
        return response()->json(['status' => true, 'data' => $arr, 'action' => 'Order detail'], 200);
    }

    public function updateOrderStatus($id, $chef, Request $request)
    {
        $findOrder = Order::find($id);
        if ($findOrder) {
            $getChef = Chef::find($chef);
            if ($findOrder->chef_id == $getChef->id) {
                $title = '';
                $body = '';
                if ($request->status == 1) {
                    $findOrder->order_status = 'Preparing';
                    $findOrder->save();
                    $title = 'Your Order now preparing by chef';
                    $body = 'Tap to order more';
                } else {
                    $findOrder->order_status = 'Cancelled';
                    $findOrder->save();
                    $title = 'Your Order is cancelled by chef';
                    $body = 'Tap to see new other dishes';
                }
                $customerObj = Customer::find($findOrder->customer_id);
                $get = app('App\Http\Controllers\API\CheckoutController')->orderObj($findOrder->id);
                $this->pusher->trigger('order-' . $get['orderId'], 'change-event', $get);
                if ($customerObj->notify == 1) {
                    $devices = CustomerDevice::where('customer_id', $customerObj->id)->distinct('fcm_token')->pluck('fcm_token');
                    foreach ($devices as $item)
                        app('App\Http\Controllers\API\CheckoutController')->notification($item, $title, $body, ['status' => 'order', 'order_id' => $findOrder->id]);
                }
                return response()->json(['status' => true, 'action' => 'Chef accepted order'], 200);
            } else
                return response()->json(['status' => false, 'action' => 'This order was not assigned to this chef'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'Order not found'], 200);
    }
}
