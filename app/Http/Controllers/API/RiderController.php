<?php

namespace App\Http\Controllers\API;

use Twilio\Exceptions\TwilioException;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\CompleteOrder;
use App\HotelRoomService;
use App\ShippingAddress;
use Twilio\Rest\Client;
use App\CustomerDevice;
use App\RiderSupport;
use App\OrderAddons;
use App\RiderDevice;
use App\ChefDevice;
use App\RiderOrder;
use App\AddonList;
use App\Customer;
use App\Order;
use App\Rider;
use App\Chat;
use App\Chef;
use App\Item;
use App\Mail\RiderUpdateEmail;
use App\Ticket;
use Illuminate\Support\Facades\Validator;
use Pusher\Pusher;
use stdClass;

class RiderController extends Controller
{
    public $pusher;

    public function __construct()
    {
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => false
        );
        $this->pusher = new Pusher(
            "68f9e094a21250f4ccdc",
            "674c8a47c4552b15ca15",
            "1282636",
            $options
        );
    }

    public function updateToken($id, $device, $deviceName, $token)
    {
        $update = RiderDevice::where('rider_id', $id)->where('device_id', $device)->where('name', $deviceName)->first();
        if ($update) {
            $update->fcm_token = $token;
            $update->save();
        } else {
            $device1 = new RiderDevice;
            $device1->rider_id = $id;
            $device1->device_id = $device;
            $device1->name = $deviceName;
            $device1->fcm_token = $token;
            $device1->save();
        }
    }

    public function register(Request $request)
    {
        if ($request->has(['loginEmail', 'loginPassword'])) {
            $email = $request->loginEmail;
            $password = $request->loginPassword;
            $token = $request->token;
            $device = $request->device;
            $deviceName = $request->device_name;
            $rider = Rider::where('email', $email)->first();
            if (!$rider)
                return response()->json(['status' => false, 'data' => (object) array(), 'action' => 'No rider found regarding this email'], 200);
            else {
                if (Hash::check($password, $rider->password)) {
                    $this->updateToken($rider->id, $device, $deviceName, $token);
                    $rider->timezone = $request->timezone;
                    $rider->save();
                    return response()->json(['status' => true, 'data' => $rider, 'action' => 'Rider logged in'], 200);
                } else
                    return response()->json(['status' => false, 'data' => (object) array(), 'action' => 'Please enter correct password'], 200);
            }
        } else {
            $firstName = $request->first_name;
            $lastName = $request->last_name;
            $email = $request->email;
            $token = $request->token;
            $device = $request->device;
            $deviceName = $request->device_name;
            $password = bcrypt($request->password);
            $phone = $request->phone;
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $trimmed = str_replace(' ', '', $firstName);
                $filename = $trimmed . '.' . $extension;
                if ($file->move('uploads/rider/', $filename))
                    $image = env('APP_URL') . '/uploads/rider/' . $filename;
            } else
                $image = '';
            $checkUser = Rider::where('email', $email)->first();
            $checkUser1 = Rider::where('phone', $phone)->first();
            //Validating Rider
            $error = '';
            if ($checkUser == null and $checkUser1 != null)
                $error = 'Phone';
            elseif ($checkUser1 != null && $checkUser != null)
                $error = 'Email and Phone';
            elseif ($checkUser != null && $checkUser1 == null)
                $error = 'Email';

            if ($error == '') {
                $rider = new Rider;
                $rider->first_name = $firstName;
                $rider->last_name = $lastName;
                $rider->image = $image;
                $rider->email = $email;
                $rider->password = $password;
                $rider->phone = $phone;
                $rider->timezone = $request->timezone;
                $rider->save();
                $this->updateToken($rider->id, $device, $deviceName, $token);
                $find = Rider::find($rider->id);
                if ($find)
                    return response()->json(['status' => true, 'data' => $find, 'action' => 'Rider registered'], 200);
            } else
                return response()->json(['status' => false, 'data' => (object) array(), 'action' => $error . ' already exists'], 200);
        }
    }

    public function verifySignup(Request $request)
    {
        $email = $request->email;
        $phone = $request->phone;
        // $token = $request->token;
        $check = Rider::where('email', $email)->first();
        $check1 = Rider::where('phone', $phone)->first();
        $error = '';
        if (!$check and $check1)
            $error = 'Phone';
        elseif ($check1 && $check)
            $error = 'Email and Phone';
        elseif ($check && !$check1)
            $error = 'Email';
        if ($error != '')
            return response()->json(['status' => false, 'action' => $error . ' already exists'], 200);
        else {
            $otp = app('App\Http\Controllers\API\ChefController')->sendOtp($phone);
            return response()->json(['status' => true, 'action' => $otp], 200);
        }
    }

    public function verify(Request $request)
    {
        $phone = $request->phone;
        $code = $request->code;
        /* Get credentials from .env */
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");

        $twilio = new Client($twilio_sid, $token);
        try {
            $verification = $twilio->verify->v2->services($twilio_verify_sid)->verificationChecks->create($code, array('to' => $phone));
            if ($verification->valid) {
                return response()->json(['status' => true, 'action' => 'Phone number verified']);
            } else
                return response()->json(['status' => false, 'action' => 'Entered code is invalid']);
        } catch (TwilioException $e) {
            if ($e->getStatusCode()) {
                if ($e->getStatusCode() == 404)
                    return response()->json(['status' => false, 'code' => $e->getStatusCode(), 'action' => 'Send otp request again']);
            }
        }
    }

    public function newEmailVerify(Request $request)
    {
        $check = Rider::where('email', $request->email)->first();
        if ($check)
            return response()->json(['status' => false, 'data' => 0, 'action' => 'Email already exists']);
        else {
            $code = rand(100000, 999999);
            Mail::to($request->email)->send(new RiderUpdateEmail($code));
            return response()->json(['status' => true, 'data' => $code, 'action' => 'Email otp sent']);
        }
    }

    public function logout(Request $request)
    {
        $check = Rider::find($request->riderId);
        if ($check == null)
            return response()->json(['status' => true, 'action' => 'Rider not found'], 200);
        else {
            $check->online = 0;
            $check->save();
            $checkDevice = RiderDevice::where('rider_id', $check->id)->where('device_id', $request->deviceId)->first();
            if ($checkDevice)
                $checkDevice->delete();
            return response()->json(['status' => true, 'action' => 'Rider logged out'], 200);
        }
    }

    public function delete($id)
    {
        $check = Rider::find($id);
        if ($check == null)
            return response()->json(['status' => true, 'action' => 'Rider not found'], 200);
        else {
            $tokens = RiderDevice::where('rider_id', $check->id)->delete();
            $tickets = Ticket::where('rider_id', $check->id)->delete();
            $orders = RiderOrder::where('rider_id', $check->id)->delete();
            $sp = RiderSupport::where('rider_id', $check->id)->delete();
            $check->delete();
            return response()->json(['status' => true, 'action' => 'Rider account deleted'], 200);
        }
    }

    public function profile($id)
    {
        $rider = Rider::find($id);
        if ($rider)
            return response()->json(['status' => true, 'data' => $rider, 'action' => 'Rider profile'], 200);

        return response()->json(['status' => false, 'data' => $rider, 'action' => 'No rider found'], 200);
    }

    public function notifyMe($id, $status)
    {
        $user = Rider::find($id);
        if ($user) {
            $user->notify = $status;
            $user->save();
            return response()->json(['status' => true, 'action' => ($status == 0) ? 'Notifications turned off' : 'Notifications turned on'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'No rider found'], 200);
    }

    public function editProfile(Request $request)
    {
        $id = $request->riderId;
        $fname = $request->first_name;
        $lname = $request->last_name;
        $email = $request->email;
        $phone = $request->phone;
        $city = $request->city;
        $area = $request->area;
        $postcode = $request->postcode;
        $image = '';
        $check = Rider::find($id);
        if ($check != null) {
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $trimmed = str_replace(' ', '', $fname);
                $filename = $trimmed . '-' . $check->id . uniqid() . '.' . $extension;
                if ($file->move('uploads/rider/', $filename))
                    $image = env('APP_URL') . '/uploads/rider/' . $filename;
            }
            if ($fname)
                $check->first_name = $fname;
            if ($lname)
                $check->last_name = $lname;
            if ($image)
                $check->image = $image;
            if ($email)
                $check->email = $email;
            if ($phone)
                $check->phone = $phone;
            if ($city)
                $check->city = $city;
            if ($area)
                $check->area = $area;
            if ($postcode)
                $check->post_code = $postcode;
            $check->save();
            $rider = Rider::find($check->id);
            return response()->json(['status' => true, 'data' => $rider, 'action' => 'Rider profile updated'], 200);
        } else
            return response()->json(['status' => false, 'data' => new stdClass(), 'action' => 'No rider found'], 200);
    }

    public function forgotPassword(Request $request)
    {
        if (!$request->has('phone') && $request->phone == '')
            return response()->json(['status' => false, 'action' => 'Phone is required and should not be empty'], 200);
        else {
            $phone = $request->phone;
            $check = Rider::where('phone', $phone)->first();
            if (!$check)
                return response()->json(['status' => false, 'action' => 'No rider found of this phone number'], 200);
            else {
                $otp = app('App\Http\Controllers\API\ChefController')->sendOtp($phone);
                return response()->json(['status' => true, 'action' => $otp], 200);
            }
        }
    }

    public function resetPassword(Request $request)
    {
        $phone = $request->phone;
        $password = bcrypt($request->password);
        $error = '';
        if ($phone == '' && !is_numeric($phone) && $password != '')
            $error = 'Phone must be a valid and numeric';
        elseif ($phone != '' && is_numeric($phone) && $password == '')
            $error = 'Password should not be empty';
        elseif ($phone == '' && !is_numeric($phone) && $password == '')
            $error = 'Phone must be a valid number And password should not be empty';

        if ($error == '') {
            $check = Rider::where('phone', $phone)->first();
            if ($check == null)
                return response()->json(['status' => false, 'action' => 'No rider found by this number'], 200);
            else {
                $update = Rider::where('phone', $phone)->update(['password' => $password]);
                if ($update)
                    return response()->json(['status' => true, 'action' => 'Password updated'], 200);
            }
        } else
            return response()->json(['status' => false, 'action' => $error], 200);
    }

    public function uploadDocuments(Request $request)
    {
        $riderId = 0;
        $vehicle = $request->vehicle;
        if ($request->has(['riderId']))
            $riderId = $request->riderId;
        else
            return response()->json(['status' => false, 'action' => 'Rider id should not be empty'], 200);

        $rider = Rider::find($riderId);
        if ($rider == null)
            return response()->json(['status' => false, 'action' => 'No rider found'], 200);
        if ($request->hasfile('idFront') && $request->hasfile('idBack')) {
            $file = $request->file('idFront');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $trimmed = str_replace(' ', '', $riderId);
            $filename = $trimmed . '.' . $extension;
            if ($file->move('uploads/rider/', 'front' . $filename))
                $image = env('APP_URL') . '/uploads/rider/front' . $filename;

            $file1 = $request->file('idBack');
            $extension = $file1->getClientOriginalExtension(); // getting image extension
            $trimmed1 = str_replace(' ', '', $riderId);
            $filename1 = $trimmed1 . '.' . $extension;
            if ($file1->move('uploads/rider/', 'back' . $filename1))
                $image1 = env('APP_URL') . '/uploads/rider/back' . $filename1;

            $update = Rider::where('id', $riderId)->update(['id_front' => $image, 'id_back' => $image1, 'vehicle_no' => $vehicle]);
            if ($update)
                return response()->json(['status' => true, 'action' => 'Rider documents saved'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'Picture of id front and back should be added'], 200);
    }

    public function onlineRiders($id)
    {
        $chef = Chef::find($id);
        $chefArr = explode(',', $chef->lat_long);
        $lat = $chefArr[0];
        $lng = $chefArr[1];
        $distanceArr = array();
        $riders = Rider::select('id', 'lat_lng')->where('lat_lng', '!=', '')->where('online', 1)->get();
        foreach ($riders as $item) {
            $arr = explode(',', $item->lat_lng);
            if ($lat == $arr[0] && $lng == $arr[1])
                $distanceArr[] = $item->id;
            else {
                $theta = $lng - $arr[1];
                $dist = sin(deg2rad($lat)) * sin(deg2rad($arr[0])) +  cos(deg2rad($lat)) * cos(deg2rad($arr[0])) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                // $kilometers = ($miles * 1.609344);
                // $km = round($kilometers, 2);
                if ($miles <= 2)
                    $distanceArr[] = $item->id;
            }
        }
        $riders = Rider::whereIn('id', $distanceArr)->get();
        return response()->json(['status' => true, 'data' => $riders, 'action' => 'List of online riders'], 200);
    }

    public function acceptRequest($id, $rider)
    {
        $findOrder = Order::find($id);
        if ($findOrder) {
            $findRider = Rider::find($rider);
            if ($findRider) {
                $new = new RiderOrder;
                $new->order_id = $findOrder->id;
                $new->rider_id = $findRider->id;
                $new->save();
                $findOrder->order_status = 'Picked';
                $findOrder->save();
                $findRider->online = 0;
                $findRider->save();
                $customerObj = Customer::find($findOrder->customer_id);
                $get1 = app('App\Http\Controllers\API\CheckoutController')->orderObj($findOrder->id);
                $this->pusher->trigger('order-' . $get1['orderId'], 'change-event', $get1);
                $get = $this->riderOrderObj($findOrder->id);
                $this->pusher->trigger('rider-order-' . $findRider->id, 'new-order-event', $get);
                if ($customerObj->notify == 1) {
                    $devices = CustomerDevice::where('customer_id', $customerObj->id)->distinct('fcm_token')->pluck('fcm_token');
                    foreach ($devices as $item)
                        app('App\Http\Controllers\Api\CheckoutController')->notification($item, 'Your Order is Picked up by our Rider', 'Tap to order more', ['status' => 'order', 'order_id' => $findOrder->id]);
                }
                if ($findRider->notify == 1) {
                    $devices = RiderDevice::where('rider_id', $findRider->id)->distinct('fcm_token')->pluck('fcm_token');
                    foreach ($devices as $item)
                        app('App\Http\Controllers\Api\CheckoutController')->notification($item, 'You have a new order to deliver', 'Tap to order more', ['status' => 'order', 'order_id' => $findOrder->id]);
                }
                return response()->json(['status' => true, 'action' => 'Rider order accepted'], 200);
            } else
                return response()->json(['status' => false, 'action' => 'Rider not found'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'Order not found'], 200);
    }

    public function completeOrder($id)
    {
        $findOrder = Order::find($id);
        if ($findOrder) {
            $findOrder->order_status = 'Delivered';
            $findOrder->save();
            $riderOrder = RiderOrder::where('order_id', $id)->update(['status' => 1]);
            $getRider = RiderOrder::where('order_id', $id)->first();
            $customerObj = Customer::find($findOrder->customer_id);
            $chef = Chef::find($findOrder->chef_id);
            $get = app('App\Http\Controllers\API\CheckoutController')->orderObj($findOrder->id);
            $this->pusher->trigger('order-' . $get['orderId'], 'change-event', $get);
            if ($customerObj->notify == 1) {
                $devices = CustomerDevice::where('customer_id', $customerObj->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices as $item)
                    app('App\Http\Controllers\Api\CheckoutController')->notification($item, 'Your Order is Completed', 'Tap to order more', ['status' => 'order', 'order_id' => $findOrder->id]);
            }
            if ($chef->notify == 1) {
                $devices1 = ChefDevice::where('chef_id', $chef->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices1 as $item1)
                    app('App\Http\Controllers\Api\CheckoutController')->notification($item1, 'Order #' . $id . ' has been delivered', 'Tap to see the detail of the order', ['status' => 'order', 'order_id' => $findOrder->id]);
            }
            Mail::to($customerObj->email)->send(new CompleteOrder($customerObj->first_name . ' ' . $customerObj->last_name));
            return response()->json(['status' => true, 'action' => 'Order completed'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'Order not found'], 200);
    }

    public function orders($status, $rider)
    {
        $ids = RiderOrder::where('rider_id', $rider)->pluck('order_id');
        $orders = Order::with('detail')->whereIn('id', $ids)->where('order_status', $status)->latest()->get();
        /*foreach ($orders as $order) {
            $get = ShippingAddress::where('order_id', $order->id)->first();
            $order->deliver_to = $get->delivery_to;
            $order->address = $get->address;
            $order->note = $get->delivery_note;
            if ($get->delivery_to == 'hotel') {
                $hotel = HotelRoomService::where('order_id', $order->id)->first();
                $order->deliver_to = 'Hotel ' . $hotel->hotel_name . ' room no: ' . $hotel->room_no . ' on Floor no: ' . $hotel->floor . ' for: ' . $hotel->person_name;
            }
            foreach ($order->detail as $detail) {
                $item = Item::find($detail->item_id);
                if ($item) {
                    $detail->item_name = $item->item_name;
                    $detail->image = $item->item_image;
                } else {
                    $detail->item_name = '';
                    $detail->image = '';
                }
                $addons = OrderAddons::select('addon_id', 'quantity', 'price')->where('order_detail_id', $detail->id)->get();
                foreach ($addons as $item) {
                    $get = AddonList::find($item->addon_id);
                    $item->name = $get->name;
                }
                $detail->addons = $addons;
            }
        }*/
        $main = [];
        foreach ($orders as $order) {
            $detail = array();
            $customer = Customer::find($order->customer_id);
            foreach ($order->detail as $item) {
                $find = Item::find($item->item_id);
                $get = OrderAddons::where('order_detail_id', $item->id)->get();
                $addons = array();
                foreach ($get as $addon) {
                    $arr = array(
                        'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                    );
                    array_push($addons, $arr);
                }
                $arr1 = array(
                    'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
                );
                array_push($detail, $arr1);
            }
            $main[] = array(
                'orderId' => $order->id, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to, 'deliveryAddress' => $order->address->address, 'deliveryNote' => $order->address->delivery_note, 'instruction' => $order->address->instruction, 'orderDetail' => $detail, 'customer' => $customer
            );
        }
        return response()->json(['status' => true, 'data' => $main, 'action' => 'Orders by status'], 200);
    }

    public function online($id, $status = 'offline', Request $request)
    {
        $rider = Rider::find($id);
        if ($rider) {
            if ($status == 'online')
                $rider->online = 1;
            elseif ($status == 'offline')
                $rider->online = 0;
            if ($request->lat && $request->lng) {
                $rider->lat_lng = $request->lat . ',' . $request->lng;
                $rider->save();
            }
            $rider1 = Rider::find($id);
            return response()->json(['status' => true, 'data' => $rider1, 'action' => 'Rider ' . $status], 200);
        }
    }

    public function currentOrder($id)
    {
        $rider = Rider::find($id);
        if ($rider) {
            $riderOrder = RiderOrder::where('rider_id', $id)->where('status', 0)->first();
            if ($riderOrder) {
                $order = Order::with('customer', 'detail')->find($riderOrder->order_id);
                $detail = array();
                $chef = Chef::find($order->chef_id);
                if ($chef) {
                    $chefLL = explode(',', $chef->lat_long);
                } else {
                    $chef = new stdClass();
                    $chef->id = 0;
                    $chef->first_name = 'Menurie Chef';
                    $chef->last_name = '';
                    $chef->phone = '';
                    $chef->image = '';
                    $chefLL = ['', ''];
                }
                $customer = Customer::find($order->customer_id);
                foreach ($order->detail as $item) {
                    $find = Item::find($item->item_id);
                    $get = OrderAddons::where('order_detail_id', $item->id)->get();
                    $addons = array();
                    foreach ($get as $addon) {
                        $arr = array(
                            'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                        );
                        array_push($addons, $arr);
                    }
                    $arr1 = array(
                        'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
                    );
                    array_push($detail, $arr1);
                }
                $rider = (object) [];
                $unreadMessages = 0;
                if ($order->order_status == 'Picked') {
                    $log = RiderOrder::where('order_id', $order->id)->first();
                    if ($log)
                        $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email')->find($log->rider_id);
                    $unreadMessages = Chat::where('from_to', $order->id)->where('to', 'rider-' . $rider->id)->where('is_read', 0)->count();
                }
                $arr = array(
                    'orderId' => $order->id, 'unreadMessages' => $unreadMessages, 'orderLat' => $order->lat, 'orderLng' => $order->lng, 'chefId' => $chef->id, 'chefImage' => $chef->image, 'chefFirstName' => $chef->first_name, 'chefLastName' => $chef->last_name, 'chefPhone' => $chef->phone, 'chefLat' => $chefLL[0], 'chefLng' => $chefLL[1], 'customerId' => $customer->id, 'customerFirstName' => $customer->first_name, 'customerLastName' => $customer->last_name, 'customerImage' => $customer->image, 'customerCountryCode' => $customer->country_code, 'customerPhone' => $customer->phone, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to, 'deliveryAddress' => $order->address->address, 'deliveryNote' => $order->address->delivery_note, 'instruction' => $order->address->instruction, 'orderDetail' => $detail
                );
                return response()->json(['status' => true, 'data' => $arr, 'type' => 1, 'action' => 'Rider order detail'], 200);
            } else
                return response()->json(['status' => true, 'data' => (object) [], 'type' => 0, 'action' => 'Rider order detail'], 200);
        } else
            return response()->json(['status' => false, 'data' => (object) [], 'type' => 0, 'action' => 'Rider not found'], 200);
    }

    public function riderOrderObj($id)
    {
        $order = Order::with('address', 'detail')->where('id', $id)->first();
        $detail = array();
        $chef = Chef::find($order->chef_id);
        if ($chef) {
            if ($chef->lat_long == '')
                $chefll = ['', ''];
            else
                $chefll = explode(',', $chef->lat_long);
        } else
            $chefll = ['', ''];
        $customer = Customer::find($order->customer_id);
        foreach ($order->detail as $item) {
            $find = Item::find($item->item_id);
            $get = OrderAddons::where('order_detail_id', $item->id)->get();
            $addons = array();
            foreach ($get as $addon) {
                $arr = array(
                    'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                );
                array_push($addons, $arr);
            }
            $arr1 = array(
                'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
            );
            array_push($detail, $arr1);
        }
        $rider = (object) [];
        $unreadMessages = 0;
        if ($order->order_status == 'Picked') {
            $log = RiderOrder::where('order_id', $order->id)->first();
            if ($log)
                $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email')->find($log->rider_id);
            $unreadMessages = Chat::where('from_to', $order->id)->where('to', 'rider-' . $rider->id)->where('is_read', 0)->count();
        }
        $arr = array(
            'orderId' => $order->id, 'unreadMessages' => $unreadMessages, 'chefId' => $chef->id, 'chefImage' => $chef->image, 'chefFirstName' => $chef->first_name, 'chefLastName' => $chef->last_name, 'chefPhone' => $chef->phone, 'chefLat' => $chefll[0], 'chefLng' => $chefll[1], 'orderLat' => $order->lat, 'orderLng' => $order->lng, 'customerId' => $customer->id, 'customerFirstName' => $customer->first_name, 'customerLastName' => $customer->last_name, 'customerImage' => $customer->image, 'customerCountryCode' => $customer->country_code, 'customerPhone' => $customer->phone, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to, 'deliveryAddress' => $order->address->address, 'deliveryNote' => $order->address->delivery_note, 'instruction' => $order->address->instruction, 'orderDetail' => $detail
        );
        return $arr;
    }

    public function orderDetail($id)
    {
        $order = Order::with('address', 'detail')->where('id', $id)->first();
        $detail = array();
        $customer = Customer::find($order->customer_id);
        foreach ($order->detail as $item) {
            $find = Item::find($item->item_id);
            $get = OrderAddons::where('order_detail_id', $item->id)->get();
            $addons = array();
            foreach ($get as $addon) {
                $arr = array(
                    'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                );
                array_push($addons, $arr);
            }
            $arr1 = array(
                'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
            );
            array_push($detail, $arr1);
        }
        $rider = (object) [];
        $unreadMessages = 0;
        if ($order->order_status == 'Picked') {
            $log = RiderOrder::where('order_id', $order->id)->first();
            if ($log)
                $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email')->find($log->rider_id);
            $unreadMessages = Chat::where('from_to', $order->id)->where('to', 'rider-' . $rider->id)->where('is_read', 0)->count();
        }
        $arr = array(
            'orderId' => $order->id, 'unreadMessages' => $unreadMessages, 'orderLat' => $order->lat, 'orderLng' => $order->lng, 'customerId' => $customer->id, 'customerFirstName' => $customer->first_name, 'customerLastName' => $customer->last_name, 'customerImage' => $customer->image, 'customerCountryCode' => $customer->country_code, 'customerPhone' => $customer->phone, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to, 'deliveryAddress' => $order->address->address, 'deliveryNote' => $order->address->delivery_note, 'instruction' => $order->address->instruction, 'orderDetail' => $detail
        );
        return response()->json(['status' => true, 'data' => $arr, 'action' => 'Order detail'], 200);
    }

    public function support(Request $request)
    {
        $rider = Rider::find($request->rider_id);
        if ($rider) {
            if ($request->message) {
                $support = new RiderSupport();
                $support->rider_id = $rider->id;
                $support->message = $request->message;
                $support->save();
                return response()->json(['status' => true, 'action' => 'Support message sent!']);
            } else
                return response()->json(['status' => false, 'action' => 'Support message should not be empty']);
        } else
            return response()->json(['status' => false, 'action' => 'Chef not found']);
    }

    public function addTicket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'description' => 'required|string',
            'rider_id' => 'required|exists:riders,id',
        ]);
        $errors = $validator->errors();
        if ($validator->fails())
            return response()->json(['status' => false, 'action' => $errors->first()]);
        else {
            $rider = Rider::find($request->rider_id);
            $ticket = new Ticket();
            $ticket->rider_id = $request->rider_id;
            $ticket->type = $request->type;
            $ticket->description = $request->description;
            $ticket->date_time = date('Y-m-d H:i:s');
            if ($request->order_id)
                $ticket->order_id = $request->order_id;
            $ticket->save();
            $chat_message = new Chat();
            $chat_message->from = $request->rider_id;
            $chat_message->to = 0;
            $chat_message->from_to = $request->rider_id . '_0';
            $chat_message->message = $request->description;
            $chat_message->time = strtotime(date('Y-m-d H:i:s'));
            $chat_message->ticket_id = $ticket->id;
            $chat_message->save();
            $find = Ticket::find($ticket->id);
            return response()->json(['status' => true, 'data' => $find, 'action' => 'Ticket saved']);
        }
    }

    public function tickets($id, $type)
    {
        $rider = Rider::find($id);
        if ($rider) {
            $list = Ticket::where('status', $type)->where('rider_id', $id)->get();
            return response()->json(['status' => true, 'data' => $list, 'action' => 'List of tickets']);
        } else
            return response()->json(['status' => false, 'data' => [], 'action' => 'Rider not found']);
    }

    public function ticket($id)
    {
        $messages = Chat::select('id', 'from', 'to', 'from_to', 'message', 'is_read', 'time', 'created_at')->where('ticket_id', $id)->latest()->get();
        foreach ($messages as $message) {
            $receiver = new stdClass();
            if ($message->from == 0)
                $message->name = 'Admin';
            else {
                $rider = Rider::find($message->from);
                $message->name = $rider->first_name . ' ' . $rider->last_name;
            }
        }
        return response()->json(['status' => true, 'data' => $messages, 'action' => 'List of ticket messages'], 200);
    }

    public function closeTicket($id)
    {
        $address = Ticket::find($id);
        if ($address) {
            $address->status = 1;
            $address->save();
            return response()->json(['status' => true, 'action' => 'Rider ticket closed']);
        } else
            return response()->json(['status' => false, 'action' => 'Rider ticket not found']);
    }

    public function notification($token, $title, $body, $order, $badge = 1)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' => $title,
            'body' => $body,
            'sound' => true,
            'badge' => $badge,
        ];

        $extraNotificationData = $order;

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AAAA3hwon80:APA91bG5gnTrqXVcPB-2hUZR0LbHQ8cfiwtf5U9gpyafiJNZmj46k2pgGzzwzQMYbENXKvwV502ZIw0dnbkC4G9cAyiZrtUqFxHr2u4zXF9Zn6SREnCLeDKAjFIjXNMugnm67MIRH6pV',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
