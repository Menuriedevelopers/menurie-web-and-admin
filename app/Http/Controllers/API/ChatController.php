<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomerDevice;
use App\RiderDevice;
use App\OrderAddons;
use App\RiderOrder;
use Pusher\Pusher;
use App\Customer;
use App\Order;
use App\Rider;
use App\Chat;
use App\Chef;
use App\Item;
use App\Ticket;
use stdClass;

class ChatController extends Controller
{

    public $pusher;

    public function __construct()
    {
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => false
        );
        $this->pusher = new Pusher(
            "68f9e094a21250f4ccdc",
            "674c8a47c4552b15ca15",
            "1282636",
            $options
        );
    }

    public function orderDetail($id)
    {
        $order = Order::with('address', 'detail')->where('id', $id)->first();
        $chef = Chef::find($order->chef_id);
        if ($chef) {
            if ($chef->lat_long == '')
                $chefll = ['', ''];
            else
                $chefll = explode(',', $chef->lat_long);
        } else
            $chefll = ['', ''];
        $detail = array();
        $customer = Customer::find($order->customer_id);
        foreach ($order->detail as $item) {
            $find = Item::find($item->item_id);
            $get = OrderAddons::where('order_detail_id', $item->id)->get();
            $addons = array();
            foreach ($get as $addon) {
                $arr = array(
                    'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                );
                array_push($addons, $arr);
            }
            $arr1 = array(
                'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
            );
            array_push($detail, $arr1);
        }
        $rider = (object) [];
        $unreadMessages = 0;
        if ($order->order_status == 'Picked') {
            $log = RiderOrder::where('order_id', $order->id)->first();
            if ($log)
                $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email', 'lat_lng')->find($log->rider_id);
            $latLng = explode(',', $rider->lat_lng);
            $rider->lat = $latLng[0];
            $rider->lng = $latLng[1];
            $unreadMessages = Chat::where('from_to', $order->id)->where('to', 'rider-' . $rider->id)->where('is_read', 0)->count();
        }
        $arr = array(
            'orderId' => $order->id, 'chefLat' => $chefll[0], 'chefLng' => $chefll[1], 'chefImage' => $chef->image, 'chefFirstName' => $chef->first_name, 'chefLastName' => $chef->last_name, 'chefPhone' => $chef->phone, 'unreadMessages' => $unreadMessages, 'orderLat' => $order->lat, 'orderLng' => $order->lng, 'customerId' => $customer->id, 'customerFirstName' => $customer->first_name, 'customerLastName' => $customer->last_name, 'customerImage' => $customer->image, 'customerCountryCode' => $customer->country_code, 'customerPhone' => $customer->phone, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to, 'deliveryAddress' => $order->address->address, 'deliveryNote' => $order->address->delivery_note, 'instruction' => $order->address->instruction, 'orderDetail' => $detail
        );
        return $arr;
    }

    public function support_message(Request $request)
    {
        $ticket = Ticket::find($request->ticket);
        if ($ticket) {
            $chat_message = new Chat();
            $chat_message->from = $request->from;
            $chat_message->to = $request->to;
            $from = $request->from;
            $to = $request->to;
            $chat_message->from_to = $request->from . '_' . $request->to;
            $chat_message->message = $request->message;
            $chat_message->time = strtotime(date('Y-m-d H:i:s'));
            if ($request->ticket)
                $chat_message->ticket_id = $request->ticket;
            $chat_message->save();

            $message = Chat::select('from', 'to', 'from_to', 'message', 'is_read', 'time')->find($chat_message->id);
            // $readAll = Chat::where('from', 0)->where('to', $chat_message->to)->update(['is_read' => 1]);
            $item = Chat::find($chat_message->id);
            if ($item->from == 0)
                $item->name = 'Admin';
            if ($ticket->customer_id != 0) {
                $customer = Customer::find($item->from);
                $item->name = $customer->first_name . ' ' . $customer->last_name;
            }
            if ($ticket->rider_id != 0) {
                $rider = Rider::find($item->from);
                $item->name = $rider->first_name . ' ' . $rider->last_name;
            }
            if ($ticket->chef_id != 0) {
                $chef = Chef::find($item->from);
                $item->name = $chef->first_name . ' ' . $chef->last_name;
            }
            $this->pusher->trigger('chat-channel', 'message-event', $item);
            $this->pusher->trigger('chat-' . $item->ticket_id, 'message-event', $item);
            return response()->json(['result' => true, 'data' => $item, 'action' => 'Message sent!'], 200);
        } else
            return response()->json(['result' => false, 'data' => new stdClass(), 'action' => 'No ticket found!'], 200);
    }

    public function send_message(Request $request)
    {
        $chat_message = new Chat();
        $chat_message->from = $request->from;
        $chat_message->to = $request->to;
        $from = explode('-', $request->from);
        $to = explode('-', $request->to);
        $find = Chat::where('from_to', $request->order_id)->first();
        $channel = '';
        if ($find) {
            $channel = $find->from_to;
            $chat_message->from_to = $request->order_id;
            $read = Chat::where('from_to', $channel)->where('to', $request->from)->where('is_read', 0)->update(['is_read' => 1]);
        } else {
            $channel = '';
            $chat_message->from_to = $request->order_id;
        }
        $chat_message->message = $request->message;

        $chat_message->time = strtotime(date('Y-m-d H:i:s'));
        $chat_message->save();

        $message = Chat::select('from', 'to', 'from_to', 'message', 'is_read', 'time')->find($chat_message->id);
        if ($from[0] == 'rider')
            $sender = Rider::select('id', 'first_name', 'last_name', 'image')->find($from[1]);
        if ($from[0] == 'customer')
            $sender = Customer::select('id', 'first_name', 'last_name', 'image')->find($from[1]);
        $message->sender = $sender;
        if ($to[0] == 'rider')
            $receiver = Rider::select('id', 'first_name', 'last_name', 'image', 'notify')->find($to[1]);
        if ($to[0] == 'customer')
            $receiver = Customer::select('id', 'first_name', 'last_name', 'image', 'notify')->find($to[1]);
        $message->receiver = $receiver;
        if ($channel == '')
            $this->pusher->trigger('order-' . $request->order_id, 'message-event', $message);
        else
            $this->pusher->trigger('order-' . $chat_message->from_to, 'message-event', $message);

        if ($to[0] == 'rider')
            $get = $this->orderDetail($request->order_id);
        else
            $get = app('App\Http\Controllers\API\CheckoutController')->orderObj($request->order_id);
        $this->pusher->trigger('order-' . $get['orderId'], 'change-event', $get);
        $arr = [];
        if ($to[0] == 'customer') {
            if ($receiver->notify == 1) {
                $tokens = CustomerDevice::where('customer_id', $receiver->id)->pluck('fcm_token');
                foreach ($tokens as $item)
                    app('App\Http\Controllers\API\CheckoutController')->notification($item, 'Rider ' . $sender->first_name . ' has sent you a new message', 'Tap to reply back', ['status' => 'message', 'order_id' => $request->order_id]);
            }
        }
        if ($to[0] == 'rider') {
            if ($receiver->notify == 1) {
                $tokens = RiderDevice::where('rider_id', $receiver->id)->pluck('fcm_token');
                foreach ($tokens as $item)
                    app('App\Http\Controllers\API\CheckoutController')->notification($item, 'Customer ' . $sender->first_name . ' has sent you a new message', 'Tap to reply back', ['status' => 'message', 'order_id' => $request->order_id]);
            }
        }
        return response()->json(['result' => true, 'data' => $message, 'action' => 'Message sent!'], 200);
    }

    public function listMessages(Request $request)
    {
        if ($request->order_id)
            $messages = Chat::select('id', 'from', 'to', 'from_to', 'message', 'is_read', 'time', 'created_at')->where('ticket_id', 0)->where('from_to', $request->order_id)->latest()->get();
        else
            $messages = Chat::select('id', 'from', 'to', 'from_to', 'message', 'is_read', 'time', 'created_at')->where('ticket_id', 0)->where('from_to', $request->from . '_' . $request->to)->latest()->get();
        $receiver = new stdClass();
        foreach ($messages as $message) {
            if (!$request->order_id) {
                $receiver = new stdClass();
                if ($message->from == 0)
                    $message->name = 'Admin';
                else {
                    $customer = Customer::find($message->from);
                    $message->name = $customer->first_name . ' ' . $customer->last_name;
                }
            }
            if ($request->order_id) {
                $from = explode('-', $message->from);
                $to = explode('-', $message->to);
                if ($from[0] == 'rider')
                    $sender = Rider::select('id', 'first_name', 'last_name', 'image')->find($from[1]);
                if ($from[0] == 'customer')
                    $sender = Customer::select('id', 'first_name', 'last_name', 'image')->find($from[1]);
                $message->sender = $sender;
                if ($to[0] == 'rider')
                    $receiver = Rider::select('id', 'first_name', 'last_name', 'image')->find($to[1]);
                if ($to[0] == 'customer')
                    $receiver = Customer::select('id', 'first_name', 'last_name', 'image')->find($to[1]);
                $message->receiver = $receiver;
            }
        }
        return response()->json(['status' => true, 'data' => $messages, 'info' => $receiver, 'action' => 'List of messages'], 200);
    }

    public function messageRead($id, $group)
    {
        $to = explode('-', $id);
        if ($to[0] == 'rider')
            $user = Rider::select('id', 'first_name', 'last_name', 'image')->find($to[1]);
        if ($to[0] == 'customer')
            $user = Customer::select('id', 'first_name', 'last_name', 'image')->find($to[1]);
        if ($user) {
            $count = Chat::where('from_to', $group)->where('to', $id)->where('is_read', 0)->update(['is_read' => 1]);
            $get = Chat::where('from_to', $group)->latest()->first();
            $read = 0;
            if ($get->from == $id)
                $read = 0;
            else
                $read = 1;
            $this->pusher->trigger($group, 'read-event', (object) array('is_read' => $read));
            return response()->json(['status' => true, 'action' => 'Messages read'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'User not found'], 200);
    }
}
