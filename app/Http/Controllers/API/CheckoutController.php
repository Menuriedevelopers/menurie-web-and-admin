<?php

namespace App\Http\Controllers\API;

use Cartalyst\Stripe\Exception\CardErrorException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Stripe;
use App\HotelRoomService;
use App\ShippingAddress;
use App\CustomerDevice;
use App\OrderAddons;
use App\OrderDetail;
use App\ChefDevice;
use App\RiderOrder;
use App\ChefTiming;
use App\AddonList;
use Pusher\Pusher;
use App\Customer;
use App\Order;
use App\Rider;
use App\Item;
use App\Cart;
use App\Chat;
use App\Chef;
use stdClass;

class CheckoutController extends Controller
{
    public $pusher;

    public function __construct()
    {
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => false
        );
        $this->pusher = new Pusher(
            "68f9e094a21250f4ccdc",
            "674c8a47c4552b15ca15",
            "1282636",
            $options
        );
    }

    public function ordersList($customer)
    {
        $orders = Order::with('address', 'detail')->where('customer_id', $customer)->latest()->get();
        $list = array();
        foreach ($orders as $item) {
            $arr = array(
                'id' => $item->id, 'totalPrice' => $item->total_price, 'orderStatus' => $item->order_status, 'paymentMethod' => $item->payment_method, 'subTotal' => $item->sub_total, 'tax' => $item->tax, 'serviceCharges' => $item->service_charges, 'totalPrice' => $item->total_price
            );
            array_push($list, $arr);
        }
        return response()->json(['status' => true, 'data' => $list, 'action' => 'List of orders'], 200);
    }

    public function orderDetail($order)
    {
        $order = Order::with('address', 'detail')->where('id', $order)->first();
        $chef = Chef::find($order->chef_id);
        if ($chef) {
            if ($chef->lat_long == '')
                $chefll = ['', ''];
            else
                $chefll = explode(',', $chef->lat_long);
        } else
            $chefll = ['', ''];
        $detail = array();
        foreach ($order->detail as $item) {
            $find = Item::find($item->item_id);
            $get = OrderAddons::where('order_detail_id', $item->id)->get();
            $addons = array();
            foreach ($get as $addon) {
                $arr = array(
                    'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                );
                array_push($addons, $arr);
            }
            $arr1 = array(
                'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
            );
            array_push($detail, $arr1);
        }
        $rider = (object) [];
        if ($order->order_status == 'Picked') {
            $log = RiderOrder::where('order_id', $order->id)->first();
            if ($log)
                $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email', 'lat_lng')->find($log->rider_id);
            $latLng = explode(',', $rider->lat_lng);
            $rider->lat = $latLng[0];
            $rider->lng = $latLng[1];
            $rider->unreadMessages = Chat::where('from_to', $order->id)->where('to', 'customer-' . $order->customer_id)->where('is_read', 0)->count();
        }
        $arr = array(
            'orderId' => $order->id, 'chefLat' => $chefll[0], 'chefLng' => $chefll[1], 'orderLat' => $order->lat, 'orderLng' => $order->lng, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to, 'deliveryAddress' => $order->address->address, 'deliveryNote' => $order->address->delivery_note, 'instruction' => $order->address->instruction, 'orderDetail' => $detail, 'rider' => $rider
        );
        return response()->json(['status' => true, 'data' => $arr, 'action' => 'Order detail'], 200);
    }

    public function orderObj($id)
    {
        $order = Order::with('address', 'detail')->where('id', $id)->first();
        $chef = Chef::find($order->chef_id);
        if ($chef) {
            if ($chef->lat_long == '')
                $chefll = ['', ''];
            else
                $chefll = explode(',', $chef->lat_long);
        } else
            $chefll = ['', ''];
        $detail = array();
        $customer = Customer::find($order->customer_id);
        foreach ($order->detail as $item) {
            $find = Item::find($item->item_id);
            $get = OrderAddons::where('order_detail_id', $item->id)->get();
            $addons = array();
            foreach ($get as $addon) {
                $arr = array(
                    'orderAddonId' => $addon->id, 'orderAddonName' => $addon->addon->name, 'quantity' => $addon->quantity, 'price' => $addon->price
                );
                array_push($addons, $arr);
            }
            $arr1 = array(
                'orderItemId' => $find->id, 'orderItemName' => $find->item_name, 'orderItemImage' => $find->item_image, 'orderItemQuantity' => $item->quantity, 'orderItemPrice' => $item->price, 'addons' => $addons
            );
            array_push($detail, $arr1);
        }
        $rider = (object) [];
        if ($order->order_status == 'Picked') {
            $log = RiderOrder::where('order_id', $order->id)->first();
            if ($log)
                $rider = Rider::select('id', 'first_name', 'last_name', 'image', 'phone', 'email', 'lat_lng')->find($log->rider_id);
            $latLng = explode(',', $rider->lat_lng);
            $rider->lat = $latLng[0];
            $rider->lng = $latLng[1];
            $rider->unreadMessages = Chat::where('from_to', $order->id)->where('to', 'customer-' . $order->customer_id)->where('is_read', 0)->count();
        }
        $arr = array(
            'orderId' => $order->id, 'chefLat' => $chefll[0], 'chefLng' => $chefll[1], 'chefImage' => $chef->image, 'chefFirstName' => $chef->first_name, 'chefLastName' => $chef->last_name, 'chefPhone' => $chef->phone, 'orderLat' => $order->lat, 'orderLng' => $order->lng, 'customerFirstName' => $customer->first_name, 'customerLastName' => $customer->last_name, 'customerImage' => $customer->image, 'customerCountryCode' => $customer->country_code, 'customerPhone' => $customer->phone, 'orderStatus' => $order->order_status, 'orderDate' => $order->date, 'orderTime' => $order->time, 'paymentMethod' => $order->payment_method, 'subTotal' => $order->sub_total, 'tax' => $order->tax, 'serviceCharges' => $order->service_charges, 'totalPrice' => $order->total_price, 'brand' => $order->brand, 'last4' => $order->last4, 'deliveryTo' => $order->address->delivery_to, 'deliveryAddress' => $order->address->address, 'deliveryNote' => $order->address->delivery_note, 'instruction' => $order->address->instruction, 'orderDetail' => $detail, 'rider' => $rider
        );
        return $arr;
    }

    public function createOrder(Request $request, $customer)
    {
        $deliveryTo = $request->delivery_to;
        $deliveryAddress = $request->delivery_address;
        $deliveryNote = $request->delivery_note;
        $paymentMethod = $request->payment_method;

        $getCart = Cart::with('addons')->where('customer_id', $customer)->get();
        $customerObj = Customer::find($customer);
        if (!$getCart->isEmpty()) {
            $totalPrice = 0;
            foreach ($getCart as $item) {
                $addonTotal = 0;
                foreach ($item->addons as $addon)
                    $addonTotal += $addon->price;
                $totalPrice += (float) ($item->price + $addonTotal) * $item->quantity;
            }

            $location = session()->get('location');
            $chefs = Chef::where('lat_long', '!=', '')->get();
            $distanceArr = array();
            foreach ($chefs as $item) {
                $arr = explode(',', $item->lat_long);
                if ($request->lat == $arr[0] && $request->lng == $arr[1])
                    array_push($distanceArr, array("distance" => 0.00, "chef" => $item->id));
                else {
                    $theta = $request->lng - $arr[1];
                    $dist = sin(deg2rad($request->lat)) * sin(deg2rad($arr[0])) +  cos(deg2rad($request->lat)) * cos(deg2rad($arr[0])) * cos(deg2rad($theta));
                    $dist = acos($dist);
                    $dist = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    // $kilometers = ($miles * 1.609344);
                    // $km = round($kilometers, 2);
                    if ($miles <= $item->order_range) {
                        $distanceArr[] = array("distance" => $miles, "chef" => $item->id);
                    }
                    // $arr = array("distance" => round($kilometers, 2), "chef" => $item->id);
                    // array_push($distanceArr, $arr);
                }
            }
            // $min = min(array_column($distanceArr, 'distance'));
            // $min = min($distanceArr);

            $arr = collect($distanceArr)->sortBy('distance');
            $min = [];
            foreach ($arr as $item)
                $min[] = $item;
            $chef = null;
            foreach ($min as $item) {
                $check = Chef::find($item['chef']);
                date_default_timezone_set($check->timezone);
                $yesterday = date('l', strtotime("-1 days"));
                $time = date('H:i');
                $day = date('l');
                $getTime = ChefTiming::where('chef_id', $check->id)->where('day', $day)->first();
                $yesterdayTime = ChefTiming::where('chef_id', $check->id)->where('day', $yesterday)->first();
                if ($yesterdayTime) {
                    if ($yesterdayTime->time != 'Closed') {
                        $breakTime = explode('-', $yesterdayTime->time);
                        $startTime = date_format(date_create($breakTime[0]), 'H:i');
                        $endTime = date_format(date_create($breakTime[1]), 'H:i');
                        if ($endTime < $startTime) {
                            // if ($endTime > $time) {
                            $check->open = $yesterdayTime->time;
                            $chef = $check;
                            break;
                            // }
                        }
                    }
                }
                if (!$getTime)
                    $check->open = 'Closed';
                else {
                    if ($getTime->time == 'Closed')
                        $check->open = 'Closed';
                    else {
                        $breakTime = explode('-', $getTime->time);
                        $startTime = date_format(date_create($breakTime[0]), 'H:i');
                        $endTime = date_format(date_create($breakTime[1]), 'H:i');
                        if ($startTime > $time)
                            $check->open = 'Closed';
                        elseif ($endTime < $startTime)
                            $check->open = $getTime->time;
                        elseif ($startTime <= $time && $endTime > $time) {
                            $check->open = $getTime->time;
                            $chef = $check;
                            break;
                        } elseif ($endTime < $time)
                            $check->open = 'Closed';
                    }
                }
            }
            if (!$chef) {
                if (count($distanceArr) == 0)
                    return response()->json(['status' => false, 'data' => (object) [], 'action' => 'Kitchen found at your location'], 200);
                else
                    return response()->json(['status' => false, 'data' => (object) [], 'action' => 'Kitchen at your location are closed now'], 200);
            }
            $total = ($totalPrice * 0.089) + $totalPrice;

            if ($paymentMethod == 'stripe') {
                $stripe = Stripe::make(env('STRIPE_SECRET_KEY'), '2020-03-02');
                if ($request->card_id) {
                    $stripe->paymentIntents()->create([
                        'amount' => $total,
                        'currency' => 'USD',
                        'customer' => $customerObj->stripe_id,
                        'payment_method_types' => ['card'],
                        'payment_method' => $request->card_id,
                        'confirm' => true
                    ]);
                } else {
                    $charge = $stripe->charges()->create([
                        'amount'   => $total,
                        'currency' => 'USD',
                        'source' => $request->stripeToken,
                        'receipt_email' => $customerObj->email,
                    ]);
                }
            }
            $order = new Order;
            $order->date = date('d F, Y');
            $order->time = date('H:i:s');
            $order->customer_id = $customerObj->id;
            $order->chef_id = $chef->id;
            $order->lat = $request->lat;
            $order->lng = $request->lng;
            $order->sub_total = $totalPrice;
            $order->tax = ($totalPrice * 0.089);
            $order->delivery_charges = "0.00";
            $order->service_charges = "0.00";
            $order->total_price = $total;
            $order->payment_method = $paymentMethod;
            $order->order_status = 'Placed';
            $order->brand = $request->brand ?? '';
            $order->last4 = $request->last4 ?? '';
            $order->save();
            $shipping = new ShippingAddress;
            $shipping->order_id = $order->id;
            $shipping->customer_id = $customerObj->id;
            $shipping->delivery_to = $deliveryTo;
            $shipping->address = $deliveryAddress;
            $shipping->delivery_note = $deliveryNote;
            $shipping->instruction = ($request->instruction) ? $request->instruction : '';
            $shipping->save();
            /*if ($deliveryTo == 'hotel') {
                $hotel = new HotelRoomService;
                $hotel->order_id = $order->id;
                $hotel->hotel_name = $request->hotel_name;
                $hotel->person_name = $request->person_name;
                $hotel->floor = $request->floor;
                $hotel->room_no = $request->room_no;
                $hotel->delivery_time = $request->delivery_time;
                $hotel->save();
            }*/
            foreach ($getCart as $cart) {
                $orderDetail = new OrderDetail;
                $orderDetail->order_id = $order->id;
                $orderDetail->item_id = $cart->item_id;
                $orderDetail->quantity = $cart->quantity;
                $orderDetail->price = $cart->price;
                $orderDetail->save();
                foreach ($cart->addons as $addon) {
                    $orderAddon = new OrderAddons;
                    $orderAddon->order_detail_id = $orderDetail->id;
                    $orderAddon->addon_id = $addon->addon_id;
                    $orderAddon->quantity = $addon->quantity;
                    $orderAddon->price = $addon->price;
                    $orderAddon->save();
                }
            }
            $find = Cart::where('customer_id', $customerObj->id)->delete();
            $get = $this->orderObj($order->id);
            $this->pusher->trigger('order-' . $get['orderId'], 'change-event', $get);
            if ($customerObj->notify == 1) {
                $devices = CustomerDevice::where('customer_id', $customerObj->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices as $item)
                    $this->notification($item, 'Your Order is placed successfully', 'Tap to see the detail of your order', ['status' => 'order', 'order_id' => $order->id]);
            }
            if ($chef->notify == 1) {
                $devices1 = ChefDevice::where('chef_id', $chef->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices1 as $item1)
                    $this->notification($item1, 'New order added', 'Tap to see the detail of the order', ['status' => 'order', 'order_id' => $order->id]);
            }
            $this->pusher->trigger('chef-' . $chef->id, 'new-order', app('App\Http\Controllers\API\OrderController')->orderByStatusArr($order->id));
            return response()->json(['status' => true, 'data' => $get, 'action' => 'Order added'], 200);
        } else
            return response()->json(['status' => false, 'data' => (object) [], 'action' => 'No items found in cart'], 200);
    }

    public function softDelete($orderId)
    {
        $order = Order::where('id', $orderId)->where('order_status', 'Pending')->delete();
        if ($order)
            return response()->json(['status' => true, 'action' => 'Order Cancelled'], 200);
        else
            return response()->json(['status' => false, 'action' => 'No order found'], 200);
    }

    public function stripeTokenGenerate(Request $request)
    {
        $customer = Customer::find($request->id);
        $stripe = Stripe::make(env('STRIPE_SECRET_KEY'), '2020-03-02');
        try {
            $token = $stripe->tokens()->create([
                'card' => [
                    'number'    => $request->card_no,
                    'exp_month' => $request->month,
                    'exp_year'  => $request->year,
                    'cvc'       => $request->cvc,
                ],
            ]);
            if ($request->save) {
                $already = false;
                $fingerprint = $token['card']['fingerprint'];
                $cardToken = $token['id'];
                if ($customer->stripe_id != '' && $request->save == true) {
                    $cards = $stripe->cards()->all($customer->stripe_id);
                    foreach ($cards['data'] as $card) {
                        if ($card['fingerprint'] == $fingerprint) {
                            $already = true;
                            $cardToken = $card['id'];
                        }
                    }
                    if (!$already)
                        $stripe->cards()->create($customer->stripe_id, $token['id']);
                } else {
                    $person = $stripe->customers()->create([
                        'email' => $customer->email,
                        'phone' => $customer->phone,
                        'name' => $customer->first_name . ' ' . $customer->last_name,
                        'description' => 'Paying to menurie'
                    ]);
                    $customer->stripe_id = $person['id'];
                    $customer->save();
                    if ($request->save == true) {
                        $cards = $cards = $stripe->cards()->all($person['id']);
                        foreach ($cards['data'] as $card) {
                            if ($card['fingerprint'] == $fingerprint) {
                                $already = true;
                                $cardToken = $card['id'];
                            }
                        }
                        if (!$already)
                            $stripe->cards()->create($customer->stripe_id, $token['id']);
                    }
                }
            }
            if (!$already)
                return response()->json(['status' => true, 'token' => $cardToken, 'brand' => $token['card']['brand'], 'last4' => $token['card']['last4'], 'action' => ''], 200);
            else
                return response()->json(['status' => false, 'token' => $cardToken, 'brand' => $token['card']['brand'], 'last4' => $token['card']['last4'], 'action' => 'Card already exists'], 200);
        } catch (CardErrorException $e) {
            // Get the status code
            $code = $e->getCode();

            // Get the error message returned by Stripe
            $message = $e->getMessage();

            // Get the error type returned by Stripe
            $type = $e->getErrorType();
            return response()->json(['status' => false, 'token' => '', 'brand' => '', 'last4' => 0, 'action' => $message], 200);
        }
    }

    public function myCards($id)
    {
        $customer = Customer::find($id);
        if ($customer) {
            $stripe = Stripe::make(env('STRIPE_SECRET_KEY'), '2020-03-02');
            if ($customer->stripe_id != '') {
                // $cards = $stripe->cards()->all($customer->stripe_id);
                $cards = $stripe->paymentMethods()->all([
                    'limit' => 90,
                    'customer' => $customer->stripe_id,
                    'type' => 'card',
                ]);
                $arr = [];
                $count = count($cards['data']) - 1;
                foreach ($cards['data'] as $item) {
                    $arr[] = [
                        'id' => $cards['data'][$count]['id'],
                        'brand' => $cards['data'][$count]['card']['brand'],
                        'last4' => $cards['data'][$count]['card']['last4']
                    ];
                    $count--;
                }
                return response()->json(['status' => true, 'data' => $arr, 'action' => 'List of cards']);
            } else
                return response()->json(['status' => true, 'data' => [], 'action' => 'List of cards']);
        } else
            return response()->json(['status' => false, 'data' => [], 'action' => 'No customer found']);
    }

    public function deleteCard($id, Request $request)
    {
        $customer = Customer::find($id);
        if ($customer) {
            $stripe = Stripe::make(env('STRIPE_SECRET_KEY'), '2020-03-02');
            $card = $stripe->cards()->delete($customer->stripe_id, $request->card_id);
            return response()->json(['status' => true, 'action' => 'Card deleted']);
        } else
            return response()->json(['status' => false, 'action' => 'Customer not found']);
    }

    public function notification($token, $title, $body, $order, $badge = 1)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' => $title,
            'body' => $body,
            'sound' => true,
            'badge' => $badge,
        ];

        $extraNotificationData = $order;

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AAAAUTbtIys:APA91bG28l6Ps42vEoA0Ptsq0x7HG4qGK55hDYstQBiKVBudSswKhV_XHCtlaZhKq6wMrbxkDmKtvTW7Oj5GqR-FRyNFu0ETdnd-DO35KvfRaTpz2i87_HnY6uS3t5i2jQijoiipTJiL',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
