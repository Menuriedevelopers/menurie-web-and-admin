<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Validator;
use Twilio\Exceptions\TwilioException;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\Mail\NewChef;
use App\ItemRequest;
use App\ChefSupport;
use App\ChefTiming;
use App\ChefDevice;
use App\AddonList;
use App\Category;
use App\Chat;
use App\Chef;
use App\Customer;
use App\Item;
use App\Mail\ChefUpdateEmail;
use App\Ticket;
use stdClass;

class ChefController extends Controller
{

    public function sendOtp($phone)
    {
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        try {
            $twilio->verify->v2->services($twilio_verify_sid)->verifications->create($phone, "sms");
            return 'OTP sent for phone number verification';
        } catch (TwilioException $e) {
            $error = '';
            if ($e->getCode() == 60200)
                $error = 'Phone number is invalid';
            if ($e->getCode() == 404)
                $error = 'Send otp request again';
            return $error;
        }
    }

    public function updateToken($id, $device, $deviceName, $token)
    {
        $update = ChefDevice::where('chef_id', $id)->where('device_id', $device)->where('name', $deviceName)->first();
        if ($update) {
            $update->fcm_token = $token;
            $update->save();
        } else {
            $device1 = new ChefDevice;
            $device1->chef_id = $id;
            $device1->device_id = $device;
            $device1->name = $deviceName;
            $device1->fcm_token = $token;
            $device1->save();
        }
    }

    public function register(Request $request)
    {
        if ($request->has(['loginEmail'])) {
            $email = $request->loginEmail;
            $password = $request->loginPassword;
            $token = $request->token;
            $device = $request->device;
            $deviceName = $request->device_name;
            $chef = Chef::where('email', $email)->first();
            if (!$chef)
                return response()->json(['status' => false, 'data' => (object) array(), 'action' => 'No chef found regarding this email'], 200);
            else {
                $checkPass = $chef->password;
                if (Hash::check($password, $chef->password)) {
                    $this->updateToken($chef->id, $device, $deviceName, $token);
                    $chef->timezone = $request->timezone;
                    $chef->save();
                    return response()->json(['status' => true, 'data' => $chef, 'action' => 'Chef logged in'], 200);
                } else
                    return response()->json(['status' => false, 'data' => (object) array(), 'action' => 'Please enter correct password'], 200);
            }
        } else {
            $firstName = $request->first_name;
            $lastName = $request->last_name;
            $email = $request->email;
            $token = $request->token;
            $device = $request->device;
            $deviceName = $request->device_name;
            $phone = $request->phone;
            $latlong = $request->latlong;
            $address = $request->address;
            $range = $request->range;

            if ($request->filled('password'))
                $password = bcrypt($request->password);
            else
                $password = '';
            $checkUser = Chef::where('email', $email)->first();
            $checkUser1 = Chef::where('phone', $phone)->first();
            //Validating Chef
            $error = '';
            if ($checkUser == null and $checkUser1 != null)
                $error = 'Phone';
            elseif ($checkUser1 != null && $checkUser != null)
                $error = 'Email and Phone';
            elseif ($checkUser != null && $checkUser1 == null)
                $error = 'Email';

            if ($error == '') {
                $chef = new Chef;
                $chef->first_name = $firstName;
                $chef->last_name = $lastName;
                $chef->email = $email;
                $chef->password = $password;
                $chef->phone = $phone;
                $chef->address = $address;
                $chef->lat_long = $latlong;
                $chef->order_range = $range;
                $chef->timezone = $request->timezone;
                $chef->save();
                $find = Chef::find($chef->id);
                Mail::to('support@menurie.com')->send(new NewChef());
                $this->updateToken($chef->id, $device, $deviceName, $token);
                if ($request->days) {
                    $del = ChefTiming::where('chef_id', $chef->id)->delete();
                    foreach ($request->days as $index => $item) {
                        $timing = new ChefTiming;
                        $timing->chef_id = $chef->id;
                        $timing->day = $item;
                        $timing->old_time = $request->time[$index];
                        $timing->time = $request->time[$index];
                        $timing->save();
                    }
                }
                return response()->json(['status' => true, 'data' => $find, 'action' => 'Chef registered'], 200);
            } else
                return response()->json(['status' => false, 'data' => (object) array(), 'action' => $error . ' already exists'], 200);
        }
    }

    public function verifySignup(Request $request)
    {
        $email = $request->email;
        $phone = $request->phone;
        $check = Chef::where('email', $email)->first();
        $check1 = Chef::where('phone', $phone)->first();
        $error = '';
        if (!$check and $check1)
            $error = 'Phone';
        elseif ($check1 && $check)
            $error = 'Email and Phone';
        elseif ($check && !$check1)
            $error = 'Email';
        if ($error != '') {
            return response()->json(['status' => false, 'action' => $error . ' already exists'], 200);
        } else {
            $code = rand(100000, 999999);
            $otp = $this->sendOtp($phone);
            return response()->json(['status' => true, 'action' => $otp], 200);
        }
    }

    public function logout(Request $request)
    {
        $check = Chef::find($request->chefId);
        if ($check == null)
            return response()->json(['status' => true, 'action' => 'Chef not found'], 200);
        else {
            $checkDevice = ChefDevice::where('chef_id', $check->id)->where('device_id', $request->deviceId)->first();
            if ($checkDevice)
                $checkDevice->delete();
            return response()->json(['status' => true, 'action' => 'Chef logged out'], 200);
        }
    }

    public function delete($id)
    {
        $check = Chef::find($id);
        if ($check) {
            $items = Item::where('chef_id', $check->id)->get();
            foreach ($items as $item)
                $item->delete();
            $tokens = ChefDevice::where('chef_id', $check->id)->delete();
            $tickets = Ticket::where('chef_id', $check->id)->delete();
            $reqs = ItemRequest::where('chef_id', $check->id)->delete();
            $supports = ChefSupport::where('chef_id', $check->id)->delete();
            $check->delete();
            return response()->json(['status' => true, 'action' => 'Chef account deleted'], 200);
        } else
            return response()->json(['status' => true, 'action' => 'Chef not found'], 200);
    }

    public function verify(Request $request)
    {
        $phone = $request->phone;
        $code = $request->code;
        // $error ='';
        // if( $phone == '' && !is_numeric($phone) && $code != '' && is_numeric($code) )
        //     $error = 'Phone must be a valid and numeric';
        // elseif( $phone != '' && is_numeric($phone) && $code == '' && !is_numeric($code) )
        //     $error = 'Code must be in numeric';
        // elseif($phone == '' && !is_numeric($phone) && $code == '' && !is_numeric($code))
        //     $error = 'Phone must be a valid number And Code must be in numeric';
        // if($error == '')
        // {
        // $getChef = Chef::where('phone',$phone)->first();
        // if($getChef)
        // {
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");

        $twilio = new Client($twilio_sid, $token);
        // try
        // {
        $verification = $twilio->verify->v2->services($twilio_verify_sid)->verificationChecks->create($code, array('to' => $phone));

        if ($verification->valid)
        // if($code == 123456)
        {
            // $getChef->verified = 1;
            // $getChef->save();
            return response()->json(['status' => true, 'action' => 'Phone number verified'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'Entered code is invalid'], 200);
        // else
        //     return response()->json(['status'=>false,'data'=>(object) array(),'action'=>'Entered code is invalid'],200);
        // }
        // catch(TwilioException $e)
        // {
        //     return response()->json(['status'=>false,'data'=>(object) array(),'action'=>$e->getMessage()],200);
        // }
        // }
        // else
        //     return response()->json(['status' => false,'data'=>(object) array(), 'action' => 'No Phone number found!'],200);
        // }
        // else
        //     return response()->json(['status'=>false,'data'=>(object) array(),'action'=>$error],200);
    }

    public function profile($id)
    {
        $chef = Chef::find($id);
        if ($chef) {
            date_default_timezone_set($chef->timezone);
            $yesterday = date('l', strtotime("-1 days"));
            $time = date('H:i');
            $day = date('l');
            $done = 0;
            $getTime = ChefTiming::where('chef_id', $chef->id)->where('day', $day)->first();
            $yesterdayTime = ChefTiming::where('chef_id', $chef->id)->where('day', $yesterday)->first();
            if ($yesterdayTime) {
                if ($yesterdayTime->time != 'Closed') {
                    $breakTime = explode('-', $yesterdayTime->time);
                    $startTime = date_format(date_create($breakTime[0]), 'H:i');
                    $endTime = date_format(date_create($breakTime[1]), 'H:i');
                    if ($endTime < $startTime) {
                        $chef->open = $yesterdayTime->time;
                        $chef->day = $yesterday;
                        $done = 1;
                    }
                }
            }
            if ($done == 0) {
                if (!$getTime)
                    $chef->open = 'Closed';
                else {
                    if ($getTime->time == 'Closed')
                        $chef->open = 'Closed';
                    else {
                        $breakTime = explode('-', $getTime->time);
                        $startTime = date_format(date_create($breakTime[0]), 'H:i');
                        $endTime = date_format(date_create($breakTime[1]), 'H:i');
                        if ($startTime > $time)
                            $chef->open = 'Closed';
                        elseif ($endTime < $startTime)
                            $chef->open = $getTime->time;
                        elseif ($startTime <= $time && $endTime > $time) {
                            $chef->open = $getTime->time;
                            $chef->day = $day;
                        } elseif ($endTime < $time)
                            $chef->open = 'Closed';
                    }
                }
            }
            return response()->json(['status' => true, 'data' => $chef, 'action' => 'Chef profile'], 200);
        }
        return response()->json(['status' => false, 'data' => $chef, 'action' => 'No chef found'], 200);
    }

    public function notifyMe($id, $status)
    {
        $user = Chef::find($id);
        if ($user) {
            $user->notify = $status;
            $user->save();
            $chef = Chef::find($user->id);
            return response()->json(['status' => true, 'data' => $chef, 'action' => ($status == 0) ? 'Notifications turned off' : 'Notifications turned on'], 200);
        } else
            return response()->json(['status' => false, 'data' => new stdClass(), 'action' => 'No chef found'], 200);
    }

    public function times($id)
    {
        $chef = Chef::find($id);
        if ($chef) {
            $times = ChefTiming::where('chef_id', $chef->id)->get();
            return response()->json(['status' => true, 'data' => $times, 'action' => 'Chef timings'], 200);
        } else
            return response()->json(['status' => false, 'data' => array(), 'action' => 'No chef found'], 200);
    }

    public function editProfile(Request $request)
    {
        $id = $request->chefId;
        $fname = $request->first_name;
        $lname = $request->last_name;
        $email = $request->email;
        $phone = $request->phone;
        $latlong = $request->latlong;
        $address = $request->address;
        $city = $request->city;
        $area = $request->area;
        $range = $request->range;
        $postcode = $request->postcode;
        $cuisines = $request->cuisines;
        $image = '';
        $check = Chef::find($id);
        if ($check) {
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $trimmed = str_replace(' ', '', $fname);
                $filename = $trimmed . '-' . $check->id . uniqid() . '.' . $extension;
                if ($file->move('uploads/chef/', $filename))
                    $image = env('APP_URL') . '/uploads/chef/' . $filename;
                $check->image = $image;
            }
            if ($fname)
                $check->first_name = $fname;
            if ($lname)
                $check->last_name = $lname;
            if ($email)
                $check->email = $email;
            if ($phone)
                $check->phone = $phone;
            if ($address)
                $check->address = $address;
            if ($latlong)
                $check->lat_long = $latlong;
            if ($city)
                $check->city = $city;
            if ($area)
                $check->area = $area;
            if ($range)
                $check->order_range = $range;
            if ($postcode)
                $check->post_code = $postcode;
            if ($cuisines)
                $check->cuisines = $cuisines;
            $check->save();
            $chef = Chef::find($check->id);
            if ($request->days) {
                foreach ($request->days as $index => $item) {
                    $timing = ChefTiming::where('chef_id', $chef->id)->where('day', $item)->first();
                    if ($request->time[$index] == 'Closed')
                        $timing->old_time = $timing->time;
                    $timing->time = $request->time[$index];
                    $timing->save();
                }
            }
            return response()->json(['status' => true, 'data' => $chef, 'action' => 'Chef profile updated'], 200);
        } else
            return response()->json(['status' => false, 'data' => (object)array(), 'action' => 'No chef found'], 200);
    }

    public function newEmailVerify(Request $request)
    {
        $check = Chef::where('email', $request->email)->first();
        if ($check)
            return response()->json(['status' => false, 'data' => 0, 'action' => 'Email already exists']);
        else {
            $code = rand(100000, 999999);
            Mail::to($request->email)->send(new ChefUpdateEmail($code));
            return response()->json(['status' => true, 'data' => $code, 'action' => 'Email otp sent']);
        }
    }

    public function forgotPassword(Request $request)
    {
        if (!$request->has('phone') && $request->phone == '')
            return response()->json(['status' => false, 'action' => 'Phone is required and should not be empty'], 200);
        else {
            $phone = $request->phone;
            $check = Chef::where('phone', $phone)->first();
            if (!$check)
                return response()->json(['status' => false, 'action' => 'No chef found of this phone number'], 200);
            else {
                $otp = app('App\Http\Controllers\API\ChefController')->sendOtp($phone);
                return response()->json(['status' => true, 'action' => $otp], 200);
            }
        }
    }

    public function resetPassword(Request $request)
    {
        $phone = $request->phone;
        $password = bcrypt($request->password);
        $error = '';
        if ($phone == '' && !is_numeric($phone) && $password != '')
            $error = 'Phone must be a valid and numeric';
        elseif ($phone != '' && is_numeric($phone) && $password == '')
            $error = 'Password should not be empty';
        elseif ($phone == '' && !is_numeric($phone) && $password == '')
            $error = 'Phone must be a valid number And password should not be empty';

        if ($error == '') {
            $check = Chef::where('phone', $phone)->first();
            if ($check == null)
                return response()->json(['status' => false, 'action' => 'No chef found by this number'], 200);
            else {
                $update = Chef::where('phone', $phone)->update(['password' => $password]);
                if ($update)
                    return response()->json(['status' => true, 'action' => 'Password updated'], 200);
            }
        } else
            return response()->json(['status' => false, 'action' => $error], 200);
    }

    public function categories()
    {
        $categories = Category::orderBy('id', 'DESC')->get()->toArray();
        return response()->json(['status' => true, 'data' => $categories, 'action' => 'List of categories'], 200);
    }

    public function addCategory(Request $request)
    {
        if (!$request->has('chefId') && $request->chefId == '')
            return response()->json(['status' => false, 'action' => 'Chef id is required and should not be empty'], 200);

        if ($request->has('name')) {
            $name = $request->name;
            $chefId = $request->chefId;
            if ($name == '')
                return response()->json(['status' => false, 'action' => 'Category name should not be empty'], 200);
            else {
                $check = Chef::where('id', $chefId)->first();
                if ($check == null)
                    return response()->json(['status' => false, 'action' => 'No chef found'], 200);
                else {
                    $check1 = Category::where('chef_id', $chefId)->where('name', $name)->first();
                    if ($check1)
                        return response()->json(['status' => false, 'action' => 'Cannot save duplicate category'], 200);
                    // foreach($restaurantId as $item)
                    // {
                    $category = new Category;
                    $category->chef_id = $chefId;
                    $category->name = $name;
                    $category->save();
                    // }
                    if ($category)
                        return response()->json(['status' => true, 'action' => 'New category saved'], 200);
                }
            }
        } else
            return response()->json(['status' => false, 'action' => 'Enter name inorder to add category'], 200);
    }

    public function editCategory(Request $request)
    {
        if (!$request->has('chefId') && $request->chefId == '')
            return response()->json(['status' => false, 'action' => 'Chef id is required and should not be empty'], 200);
        if (!$request->has('categoryId') && $request->categoryId == '')
            return response()->json(['status' => false, 'action' => 'Category id is required and should not be empty'], 200);
        if ($request->has('name')) {
            $name = $request->name;
            $chefId = $request->chefId;
            $categoryId = $request->categoryId;
            if ($name == '')
                return response()->json(['status' => false, 'action' => 'Category name should not be empty'], 200);
            else {
                // $check = Category::where([['chef_id',$chefId],['id',$categoryId]])->first();
                // if($check == null)
                //     return response()->json(['status'=>false, 'action'=>'No category found by regarding chef'],200);
                // else
                // {
                $update = Category::where('id', $categoryId)->update(['name' => $name]);
                if ($update)
                    return response()->json(['status' => true, 'data' => $update, 'action' => 'Category updated'], 200);
                // }
            }
        }
    }

    public function deleteCategory(Request $request)
    {
        if (!$request->has('category') && $request->category == '')
            return response()->json(['status' => false, 'action' => 'Category id is required and should not be empty'], 200);
        $find = Category::find($request->category);
        if ($find) {
            $category = Category::find($request->category);
            if ($category) {
                $items = Item::where('category_id', $category->id)->get();
                foreach ($items as $item)
                    $item->delete();
                $category->delete();
                return response()->json(['status' => true, 'action' => "Category deleted"], 200);
            }
        } else
            return response()->json(['status' => true, 'action' => "Category not found"], 200);
    }

    public function categoryActiveStatus(Request $request)
    {
        if ($request->has('category')) {
            $categoryId = $request->category;
            if (is_numeric($categoryId)) {
                if ($request->has('active')) {
                    $status = $request->active;
                    if (is_numeric($status)) {
                        $check = Category::find($categoryId);
                        if ($check == null)
                            return response()->json(['status' => false, 'action' => 'No category found of given id'], 200);
                        else {
                            $category = Category::where('id', $categoryId)->update(['is_active' => $status]);
                            if ($category)
                                return response()->json(['status' => true, 'action' => 'Category active status changed'], 200);
                        }
                    } else
                        return response()->json(['status' => false, 'action' => 'Enter active status value in numbers 0,1'], 200);
                } else
                    return response()->json(['status' => false, 'action' => 'Enter active value to change category status'], 200);
            } else
                return response()->json(['status' => false, 'action' => 'Category id should be a number'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'Enter category id to change active status'], 200);
    }

    public function items()
    {
        $items = Item::where('special', 0)->latest()->Paginate(12);
        foreach ($items as $item) {
            $addons = AddonList::where('item_id', $item->id)->get()->toArray();
            $item->category_name = Category::find($item->category_id)->name ?? '';
            $item->addons = $addons;
        }
        return response()->json(['status' => true, 'data' => $items, 'action' => 'List of items'], 200);
    }

    public function addItem(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'categoryId' => 'required|integer', 'name' => 'required|string', 'description' => 'required|string', 'price' => 'required', 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $errors = $validate->errors();
        if ($errors->isEmpty()) {
            $categoryId = $request->categoryId;
            $name = $request->name;
            $description = $request->description;
            $price = $request->price;
            $image = '';
            // $check2 = Item::where('special', 0)->where('item_name', $name)->first();
            // if ($check2)
            //     return response()->json(['status' => false, 'action' => ['Cannot save duplicate item']], 200);
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $trimmed = str_replace(' ', '', $name);
                $filename = $trimmed . '.' . $extension;
                if ($file->move('uploads/items/', $filename))
                    $image = env('APP_URL') . '/uploads/items/' . $filename;
            }
            $item = new Item;
            $item->category_id = $categoryId;
            $item->item_name = $name;
            $item->description = $description;
            $item->price = $price;
            $item->item_image = $image;
            $item->save();
            if ($item)
                return response()->json(['status' => true, 'action' => ['Item saved']], 200);
        } else
            return response()->json(['status' => false, 'action' => $errors->all()], 200);
    }

    public function editItem(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'itemId' => 'required|integer', 'categoryId' => 'required|integer', 'name' => 'required|string', 'description' => 'required|string', 'price' => 'required'
        ]);
        $errors = $validate->errors();
        if ($errors->isEmpty()) {
            $itemId = $request->itemId;
            $categoryId = $request->categoryId;
            $name = $request->name;
            $description = $request->description;
            $price = $request->price;
            $image = '';
            $check = Item::where('id', $itemId)->first();
            if ($check == null)
                return response()->json(['status' => false, 'action' => ['Item not found']], 200);
            else {
                // $check2 = Item::where('special', 0)->where('item_name', $name)->whereNotIn('id', [$itemId])->first();
                // if ($check2)
                //     return response()->json(['status' => false, 'action' => ['Cannot save duplicate item']], 200);
                // else {
                $item = Item::find($itemId);
                if ($categoryId)
                    $item->category_id = $categoryId;
                if ($name)
                    $item->item_name = $name;
                if ($description)
                    $item->description = $description;
                if ($price)
                    $item->price = $price;
                if ($request->hasfile('image')) {
                    $file = $request->file('image');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $trimmed = str_replace(' ', '', $name);
                    $filename = $trimmed . '.' . $extension;
                    if ($file->move('uploads/items/', $filename))
                        $image = env('APP_URL') . '/uploads/items/' . $filename;
                    $item->item_image = $image;
                }
                $item->save();
                return response()->json(['status' => true, 'data' => $item, 'action' => 'Item updated'], 200);
                // }
            }
        } else
            return response()->json(['status' => false, 'action' => $errors->all()], 200);
    }

    public function deleteItem(Request $request)
    {
        if (!$request->has('item') && $request->item == '')
            return response()->json(['status' => false, 'action' => 'item id is required and should not be empty'], 200);
        else {
            $itemId = $request->item;
            $find = Item::find($itemId);
            if ($find) {
                $addons = AddonList::where('item_id', $find->id)->delete();
                $requests = ItemRequest::where('item_id', $find->id)->delete();
                $find->delete();
                return response()->json(['status' => true, 'action' => 'Item deleted'], 200);
            } else
                return response()->json(['status' => false, 'action' => 'Item not found'], 200);
        }
    }

    public function itemActiveStatus(Request $request)
    {
        if ($request->has('item')) {
            $itemId = $request->item;
            if (is_numeric($itemId)) {
                if ($request->has('active')) {
                    $status = $request->active;
                    if (is_numeric($status)) {
                        $check = Item::find($itemId);
                        if ($check == null)
                            return response()->json(['status' => false, 'action' => 'No item of given id found'], 200);
                        else {
                            $item = Item::where('id', $itemId)->update(['is_active' => $status]);
                            if ($item)
                                return response()->json(['status' => true, 'action' => 'Item active status changed'], 200);
                        }
                    } else
                        return response()->json(['status' => false, 'action' => 'Enter active status value in numbers 0,1'], 200);
                } else
                    return response()->json(['status' => false, 'action' => 'Enter active value to change item status'], 200);
            } else
                return response()->json(['status' => false, 'action' => 'Item id should be a number'], 200);
        } else
            return response()->json(['status' => false, 'action' => 'Enter item id to change active status'], 200);
    }

    public function addOn(Request $request)
    {
        $itemId = $request->itemId;
        if ($request->addonName) {
            $count = count($request->addonName);
            if ($count != 0) {
                for ($i = 0; $i < $count; $i++) {
                    $addonList = new AddonList;
                    $addonList->item_id = $itemId;
                    $addonList->name = $request->addonName[$i];
                    $addonList->price = $request->price[$i];
                    $addonList->save();
                }
            }
        } else {
            $addons = explode(',', $request->addons);
            $prices = explode(',', $request->prices);
            foreach ($addons as $index => $item) {
                $addonList = new AddonList;
                $addonList->item_id = $itemId;
                $addonList->name = $item;
                $addonList->price = $prices[$index];
                $addonList->save();
            }
        }
        return response()->json(['status' => true, 'action' => 'Addon saved'], 200);
    }

    public function editAddon(Request $request, $id)
    {
        $itemId = $request->itemId;
        $count = count($request->addonListId);
        $count1 = count($request->addonName);
        for ($i = 0; $i < $count; $i++) {
            $addonList = AddonList::find($request->addonListId[$i]);
            $addonList->name = $request->addonName[$i];
            $addonList->price = $request->price[$i];
            $addonList->save();
        }
        for ($i = $count; $i < $count1; $i++) {
            $addonList = new AddonList;
            $addonList->item_id = $itemId;
            $addonList->name = $request->addonName[$i];
            $addonList->price = $request->price[$i];
            $addonList->save();
        }

        return response()->json(['status' => true, 'action' => 'Addon updated'], 200);
    }

    public function addOnDelete($id)
    {
        $find = AddonList::find($id);
        if ($find) {
            $find->delete();
            return response()->json(['status' => true, 'action' => "Addon deleted"], 200);
        }
    }

    public function allItems($chefId, $category = 0)
    {
        if ($category == 0)
            $items = Item::where('special', 0)->latest()->Paginate(12);
        else
            $items = Item::where('category_id', $category)->where('special', 0)->latest()->Paginate(12);
        foreach ($items as $item) {
            $addons = AddonList::where('item_id', $item->id)->get()->toArray();
            $item->category_name = Category::find($item->category_id)->name ?? '';
            $item->addons = $addons;
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $chefId)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    if ($check->status == 0)
                        $item->is_active = 2;
                    else {
                        if ($check->type == 0)
                            $item->is_active = 0;
                        else
                            $item->is_active = 1;
                    }
                }
            }
        }
        return response()->json(['status' => true, 'data' => $items, 'action' => 'List of all items'], 200);
    }

    public function specialItems($chefId)
    {
        $items = Item::where('chef_id', $chefId)->where('special', 1)->latest()->Paginate(12);
        foreach ($items as $item) {
            $addons = AddonList::where('item_id', $item->id)->get()->toArray();
            $item->category_name = Category::find($item->category_id)->name ?? '';
            $item->addons = $addons;
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $chefId)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    if ($check->status == 0)
                        $item->is_active = 2;
                    else {
                        if ($check->type == 0)
                            $item->is_active = 0;
                        else
                            $item->is_active = 1;
                    }
                }
            }
        }
        return response()->json(['status' => true, 'data' => $items, 'action' => 'List of special items'], 200);
    }

    public function addSpecialItem(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'chefId' => 'required|integer', 'categoryId' => 'required|integer', 'name' => 'required|string', 'description' => 'required|string', 'price' => 'required', 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $errors = $validate->errors();
        if ($errors->isEmpty()) {
            $chefId = $request->chefId;
            $categoryId = $request->categoryId;
            $name = $request->name;
            $description = $request->description;
            $price = $request->price;
            $check1 = Chef::where('id', $chefId)->first();
            if ($check1 == null)
                return response()->json(['status' => false, 'action' => 'No chef found'], 200);
            else {
                if ($request->hasfile('image')) {
                    $file = $request->file('image');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $trimmed = str_replace(' ', '', $name);
                    $filename = $trimmed . '.' . $extension;
                    if ($file->move('uploads/items/', $filename))
                        $image = env('APP_URL') . '/uploads/items/' . $filename;
                }
                $item = new Item;
                $item->special = 1;
                $item->chef_id = $chefId;
                $item->category_id = $categoryId;
                $item->item_name = $name;
                $item->description = $description;
                $item->price = $price;
                $item->item_image = $image;
                $item->save();
                if ($item) {
                    $get = Item::find($item->id);
                    $get->addons = AddonList::where('item_id', $get->id)->get()->toArray();
                    $data = [];
                    $data['item'] = $get->item_name;
                    $data['chef'] = $check1->first_name . ' ' . $check1->last_name;
                    Mail::send('emails.approval', $data, function ($message) {
                        $message->from('automatedEmail@menurie.com', 'Menurie System');
                        $message->sender('automatedEmail@menurie.com', 'Menurie System');
                        $message->to('support@menurie.com', 'Menurie Support');
                        $message->subject('New special item approved');
                    });
                    return response()->json(['status' => true, 'data' => $get, 'action' => ['Special item saved']], 200);
                }
            }
        } else
            return response()->json(['status' => false, 'action' => $errors->all()], 200);
    }

    public function editSpecialItem(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'itemId' => 'required|integer', 'chefId' => 'required|integer', 'categoryId' => 'required|integer', 'name' => 'required|string', 'description' => 'required|string', 'price' => 'required'
        ]);
        $errors = $validate->errors();
        if ($errors->isEmpty()) {
            $itemId = $request->itemId;
            $chefId = $request->chefId;
            $categoryId = $request->categoryId;
            $name = $request->name;
            $description = $request->description;
            $price = $request->price;
            $image = '';
            $check = Item::where('id', $itemId)->first();
            if ($check == null)
                return response()->json(['status' => false, 'action' => ['Item not found']], 200);
            else {
                $check1 = Chef::where('id', $chefId)->first();
                if (!$check1)
                    return response()->json(['status' => false, 'action' => 'No chef found'], 200);
                // $check2 = Item::where('special', 1)->where('item_name', $name)->whereNotIn('id', [$itemId])->first();
                // if ($check2)
                //     return response()->json(['status' => false, 'action' => ['Cannot save duplicate item']], 200);
                // else {
                if ($request->hasfile('image')) {
                    $file = $request->file('image');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $trimmed = str_replace(' ', '', $name);
                    $filename = $trimmed . '.' . $extension;
                    if ($file->move('uploads/items/', $filename))
                        $image = env('APP_URL') . '/uploads/items/' . $filename;
                }
                if ($image == '') {
                    $update = Item::where('id', $itemId)
                        ->update(['category_id' => $categoryId, 'item_name' => $name, 'description' => $description, 'price' => $price]);
                    if ($update)
                        return response()->json(['status' => true, 'data' => $update, 'action' => 'Item updated'], 200);
                } else {
                    $update = Item::where('id', $itemId)
                        ->update(['category_id' => $categoryId, 'item_name' => $name, 'description' => $description, 'price' => $price, 'item_image' => $image]);
                    if ($update)
                        return response()->json(['status' => true, 'data' => $update, 'action' => 'Item updated'], 200);
                }
                // }
            }
        } else
            return response()->json(['status' => false, 'action' => $errors->all()], 200);
    }

    public function support(Request $request)
    {
        $chef = Chef::find($request->chef_id);
        if ($chef) {
            if ($request->message) {
                $support = new ChefSupport();
                $support->chef_id = $chef->id;
                $support->message = $request->message;
                $support->save();
                return response()->json(['status' => true, 'action' => 'Support message sent!']);
            } else
                return response()->json(['status' => false, 'action' => 'Support message should not be empty']);
        } else
            return response()->json(['status' => false, 'action' => 'Chef not found']);
    }

    public function request(Request $request)
    {
        $chef = Chef::find($request->chef_id);
        if ($chef) {
            $item = Item::find($request->item_id);
            if ($item) {
                $support = new ItemRequest();
                $support->item_id = $item->id;
                $support->chef_id = $chef->id;
                $support->type = $request->type;
                $support->save();
                return response()->json(['status' => true, 'action' => 'Item request sent!']);
            } else
                return response()->json(['status' => false, 'action' => 'Item not found']);
        } else
            return response()->json(['status' => false, 'action' => 'Chef not found']);
    }

    public function addTicket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'description' => 'required|string',
            'chef_id' => 'required|exists:chefs,id',
        ]);
        $errors = $validator->errors();
        if ($validator->fails())
            return response()->json(['status' => false, 'action' => $errors->first()]);
        else {
            $chef = Chef::find($request->chef_id);
            $ticket = new Ticket();
            $ticket->chef_id = $request->chef_id;
            $ticket->type = $request->type;
            $ticket->description = $request->description;
            $ticket->date_time = date('Y-m-d H:i:s');
            if ($request->order_id)
                $ticket->order_id = $request->order_id;
            $ticket->save();
            $chat_message = new Chat();
            $chat_message->from = $request->chef_id;
            $chat_message->to = 0;
            $chat_message->from_to = $request->chef_id . '_0';
            $chat_message->message = $request->description;
            $chat_message->time = strtotime(date('Y-m-d H:i:s'));
            $chat_message->ticket_id = $ticket->id;
            $chat_message->save();
            $find = Ticket::find($ticket->id);
            return response()->json(['status' => true, 'data' => $find, 'action' => 'Ticket saved']);
        }
    }

    public function tickets($id, $type)
    {
        $chef = Chef::find($id);
        if ($chef) {
            $list = Ticket::where('status', $type)->where('chef_id', $id)->get();
            return response()->json(['status' => true, 'data' => $list, 'action' => 'List of tickets']);
        } else
            return response()->json(['status' => false, 'data' => [], 'action' => 'Chef not found']);
    }

    public function ticket($id)
    {
        $messages = Chat::select('id', 'from', 'to', 'from_to', 'message', 'is_read', 'time', 'created_at')->where('ticket_id', $id)->latest()->get();
        foreach ($messages as $message) {
            $receiver = new stdClass();
            if ($message->from == 0)
                $message->name = 'Admin';
            else {
                $chef = Chef::find($message->from);
                $message->name = $chef->first_name . ' ' . $chef->last_name;
            }
        }
        return response()->json(['status' => true, 'data' => $messages, 'action' => 'List of ticket messages'], 200);
    }

    public function closeTicket($id)
    {
        $address = Ticket::find($id);
        if ($address) {
            $address->status = 1;
            $address->save();
            return response()->json(['status' => true, 'action' => 'Chef ticket closed']);
        } else
            return response()->json(['status' => false, 'action' => 'Chef ticket not found']);
    }
}
