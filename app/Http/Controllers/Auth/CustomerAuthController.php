<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;

class CustomerAuthController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/index';

    public function showRegisterForm()
    {
        if (!Auth::guard('customer')->check())
            return view('site.registration');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'integer'],
        ]);
        if ($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
        else {
            DB::beginTransaction();
            $check = Customer::where('email', $request->email)->where('guest', 0)->first();
            if ($check) {
                if ($check->password)
                    return redirect()->back()->withErrors('User already registered')->withInput();
                else
                    return redirect()->route('forget');
            }
            $customer = new Customer;
            $customer->first_name = $request->input('first_name');
            $customer->last_name = $request->input('last_name');
            $customer->email = $request->input('email');
            $customer->phone = $request->input('full_number');
            $customer->password = bcrypt($request->input('password'));
            $customer->save();
            DB::commit();
            return redirect()->route('customerLogin');
        }
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            $check = Customer::where('email', $user->email)->where('guest', 0)->first();
            if ($check) {
                Auth::guard('customer')->login($check);
                return redirect('/');
            } else {
                $arr = explode(' ', $user->name);
                $user1 = new Customer;
                $user1->first_name = $arr[0];
                $user1->last_name = $arr[1];
                $user1->email = $user->email;
                $user1->platform = 'facebook';
                $user1->phone = '';
                $user1->save();
                Auth::guard('customer')->login($user1);
                return redirect('/');
            }
        } catch (Exception $e) {
            return redirect('auth/facebook');
        }
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
            $finduser = Customer::where('email', $user->email)->where('guest', 0)->first();
            if ($finduser) {
                Auth::guard('customer')->login($finduser);
                return redirect('/');
            } else {
                $arr = explode(' ', $user->name);
                $user1 = new Customer;
                $user1->first_name = $arr[0];
                $user1->last_name = $arr[1];
                $user1->email = $user->email;
                $user1->platform = 'google';
                $user1->phone = '';
                $user1->save();
                Auth::guard('customer')->login($user1);
                return redirect('/');
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function handleAppleCallback(Request $request)
    {
        if ($request->user) {
            $token_array = explode('.', $request->id_token);
            $id_token = base64_decode($token_array[1]);
            $token_data = json_decode($id_token);
            $user = json_decode($request->user);
            $user1 = new Customer;
            $user1->apple_id = $token_data->sub;
            $user1->first_name = $user->name->firstName;
            $user1->last_name = $user->name->firstName;
            $user1->platform = 'apple';
            $user1->phone = '';
            $user1->save();
            Auth::guard('customer')->login($user1);
            return redirect('/');
        } elseif ($request->id_token) {
            $token_array = explode('.', $request->id_token);
            $id_token = base64_decode($token_array[1]);
            $token_data = json_decode($id_token);
            $finduser = Customer::where('apple_id', $token_data->sub)->first();
            if ($finduser) {
                Auth::guard('customer')->login($finduser);
                return redirect('/');
            } else
                return redirect('/');
        } else
            return redirect()->back();
    }

    public function showLoginForm()
    {
        if (Auth::guard('customer')->check())
            return redirect()->route('index');
        else
            return view('site.loginPage');
    }

    public function login(Request $request)
    {
        $auth = Auth::guard('customer')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
        if ($auth) {
            if ($request->input('redirect') == 'checkout')
                return redirect()->route('checkout');

            return redirect()->route('index');
        } else
            return redirect()->back()->with('error', 'Incorrect email or password entered!')->withInput();
    }

    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect()->route('customerLogin');
    }
}
