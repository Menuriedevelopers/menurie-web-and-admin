<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rider;
use Validator;
use Auth;
use DB;

class RiderAuthController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/rider/login';

    public function showRegisterForm()
    {
        if(Auth::guard('rider')->check())
            return redirect()->route('rider.welcome');
        else
            return view('site.rider.riderRegistration');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'max:15','string'],
        ]);
        if ($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
        else
        {
            DB::beginTransaction();
            $rider = new Rider;
            $rider->first_name = $request->input('first_name');
            $rider->last_name = $request->input('last_name');
            $rider->email = $request->input('email');
            $rider->phone = $request->input('full_number');
            $rider->password = bcrypt($request->input('password'));
            $rider->save();
            DB::commit();
            return redirect()->route('rider.login');
        }
    }

    public function showLoginForm()
    {
        if(Auth::guard('rider')->check())
        return redirect()->route('rider.welcome');
        else
        return view('site.rider.riderLogin');
    }

    public function login(Request $request)
    {
        $auth = Auth::guard('rider')->attempt(['email'=>$request->input('email'),'password'=>$request->input('password')]);
        if($auth)
        return redirect()->route('rider.welcome');
    }

    public function logout()
    {
        Auth::guard('rider')->logout();
        return redirect()->route('rider.login');
    }
}
