<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Chef;
use App\Restaurant;
use Validator;
use Auth;
use DB;

class ChefAuthController extends Controller
{
    use AuthenticatesUsers;
    public function showRegisterForm()
    {
        if (Auth::guard('chef')->check())
            return redirect()->route('chef.index');
        else
            return view('site.chef.registration');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:chefs,email'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'max:15', 'string']
        ]);
        if ($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
        else {
            DB::beginTransaction();
            $chef = new Chef;
            $chef->first_name = $request->input('first_name');
            $chef->last_name = $request->input('last_name');
            $chef->email = $request->input('email');
            $chef->phone = $request->input('full_number');
            $chef->password = bcrypt($request->input('password'));
            $chef->address = ($request->input('address')) ? $request->input('address') : '';
            $chef->lat_long = ($request->input('latlong')) ? $request->input('latlong') : '';
            $chef->save();
            DB::commit();
            return redirect()->route('chef.login');
        }
    }

    public function showLoginForm()
    {
        if (Auth::guard('chef')->check())
            return redirect()->route('chef.index');
        else
            return view('site.chef.login');
    }

    public function login(Request $request)
    {
        $auth = Auth::guard('chef')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
        if ($auth)
            return redirect()->route('chef.index');
        else
            return redirect()->back()->with('error', 'Incorrect email or password entered!')->withInput();
    }

    public function logout()
    {
        Auth::guard('chef')->logout();
        return redirect()->route('chef.login');
    }
}
