<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Events\OrderStatusChanged;
use Illuminate\Support\Facades\DB;
use App\Events\OrderUpdateChef;
use Illuminate\Http\Request;
use App\CustomerDevice;
use Carbon\Carbon;
use App\Customer;
use App\Order;
use App\Chef;
use App\OrderDetail;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if ($request->value) {
                if ($request->value == 1) {
                    if ($request->item) {
                        $ids = OrderDetail::select('order_id')->where('item_id', $request->item)->distinct('order_id')->pluck('order_id');
                        $breakfast = Order::whereIn('id', $ids)->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $lunch = Order::whereIn('id', $ids)->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $dinner = Order::whereIn('id', $ids)->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $midnight = Order::whereIn('id', $ids)->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $orders = Order::with('customer', 'detail')->whereIn('id', $ids)->where('created_at', date('Y-m-d'))->latest()->get();
                    } else {
                        $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $orders = Order::with('customer', 'detail')->where('created_at', date('Y-m-d'))->latest()->get();
                    }
                } elseif ($request->value == 2) {
                    if ($request->item) {
                        $ids = OrderDetail::select('order_id')->where('item_id', $request->item)->distinct('order_id')->pluck('order_id');
                        $breakfast = Order::whereIn('id', $ids)->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $lunch = Order::whereIn('id', $ids)->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $dinner = Order::whereIn('id', $ids)->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $midnight = Order::whereIn('id', $ids)->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $orders = Order::with('customer', 'detail')->whereIn('id', $ids)->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->latest()->get();
                    } else {
                        $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $orders = Order::with('customer', 'detail')->whereBetween('created_at', [
                            Carbon::now()->subdays(7)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->latest()->get();
                    }
                } elseif ($request->value == 3) {
                    if ($request->item) {
                        $ids = OrderDetail::select('order_id')->where('item_id', $request->item)->distinct('order_id')->pluck('order_id');
                        $breakfast = Order::whereIn('id', $ids)->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $lunch = Order::whereIn('id', $ids)->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $dinner = Order::whereIn('id', $ids)->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $midnight = Order::whereIn('id', $ids)->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $orders = Order::with('customer', 'detail')->whereIn('id', $ids)->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->latest()->get();
                    } else {
                        $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $orders = Order::with('customer', 'detail')->whereBetween('created_at', [
                            Carbon::now()->subdays(30)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->latest()->get();
                    }
                } elseif ($request->value == 4) {
                    if ($request->item) {
                        $ids = OrderDetail::select('order_id')->where('item_id', $request->item)->distinct('order_id')->pluck('order_id');
                        $breakfast = Order::whereIn('id', $ids)->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $lunch = Order::whereIn('id', $ids)->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $dinner = Order::whereIn('id', $ids)->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $midnight = Order::whereIn('id', $ids)->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $orders = Order::with('customer', 'detail')->whereIn('id', $ids)->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->latest()->get();
                    } else {
                        $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->count();
                        $orders = Order::with('customer', 'detail')->whereBetween('created_at', [
                            Carbon::now()->subdays(365)->format('Y-m-d'),
                            Carbon::now()->subday()->format('Y-m-d')
                        ])->latest()->get();
                    }
                } else {
                    if ($request->item) {
                        $ids = OrderDetail::select('order_id')->where('item_id', $request->item)->distinct('order_id')->pluck('order_id');
                        $breakfast = Order::whereIn('id', $ids)->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $lunch = Order::whereIn('id', $ids)->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $dinner = Order::whereIn('id', $ids)->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $midnight = Order::whereIn('id', $ids)->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $orders = Order::with('customer', 'detail')->whereIn('id', $ids)->where('created_at', date('Y-m-d'))->latest()->get();
                    } else {
                        $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                        $orders = Order::with('customer', 'detail')->where('created_at', date('Y-m-d'))->latest()->get();
                    }
                }
            } elseif ($request->start && $request->end) {
                if ($request->item) {
                    $ids = OrderDetail::select('order_id')->where('item_id', $request->item)->distinct('order_id')->pluck('order_id');
                    $breakfast = Order::whereIn('id', $ids)->whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->count();
                    $lunch = Order::whereIn('id', $ids)->whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->count();
                    $dinner = Order::whereIn('id', $ids)->whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->count();
                    $midnight = Order::whereIn('id', $ids)->whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->count();
                    $orders = Order::with('customer', 'detail')->whereIn('id', $ids)->whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->latest()->get();
                } else {
                    $breakfast = Order::whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->count();
                    $lunch = Order::whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->count();
                    $dinner = Order::whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->count();
                    $midnight = Order::whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->count();
                    $orders = Order::with('customer', 'detail')->whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->latest()->get();
                }
            } else {
                if ($request->item) {
                    $ids = OrderDetail::select('order_id')->where('item_id', $request->item)->distinct('order_id')->pluck('order_id');
                    $breakfast = Order::whereIn('id', $ids)->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                    $lunch = Order::whereIn('id', $ids)->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                    $dinner = Order::whereIn('id', $ids)->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                    $midnight = Order::whereIn('id', $ids)->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                    $orders = Order::with('customer', 'detail')->whereIn('id', $ids)->where('created_at', date('Y-m-d'))->latest()->get();
                } else {
                    $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                    $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                    $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                    $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereDate('created_at', date('Y-m-d'))->count();
                    $orders = Order::with('customer', 'detail')->where('created_at', date('Y-m-d'))->latest()->get();
                }
            }
            return view('admin.order.orders-load', compact('orders', 'breakfast', 'lunch', 'dinner', 'midnight'))->render();
        }
        $query = '?';
        foreach ($request->query() as $index => $param)
            $query .= $index . '=' . $param . '&';
        if ($request->item) {
            $ids = OrderDetail::select('order_id')->where('item_id', $request->item)->distinct('order_id')->pluck('order_id');
            $breakfast = Order::whereIn('id', $ids)->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereDate('created_at', date('Y-m-d'))->count();
            $lunch = Order::whereIn('id', $ids)->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereDate('created_at', date('Y-m-d'))->count();
            $dinner = Order::whereIn('id', $ids)->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereDate('created_at', date('Y-m-d'))->count();
            $midnight = Order::whereIn('id', $ids)->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereDate('created_at', date('Y-m-d'))->count();
            $orders = Order::with('customer', 'detail')->whereIn('id', $ids)->whereDate('created_at', date('Y-m-d'))->latest()->get();
        } else {
            $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereDate('created_at', date('Y-m-d'))->count();
            $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereDate('created_at', date('Y-m-d'))->count();
            $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereDate('created_at', date('Y-m-d'))->count();
            $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereDate('created_at', date('Y-m-d'))->count();
            $orders = Order::with('customer', 'detail')->whereDate('created_at', date('Y-m-d'))->latest()->get();
        }
        return view('admin.order.orders', compact('query', 'orders', 'breakfast', 'lunch', 'dinner', 'midnight'));
    }

    public function getChefs($restaurant)
    {
        $arr = DB::select(DB::raw("SELECT id, CONCAT(first_name,' ',last_name) as text FROM chefs WHERE restaurant_id = '$restaurant'"));
        return $arr;
    }

    public function show($id)
    {
        $order = Order::with('customer', 'chef', 'detail', 'address')->where('id', $id)->first();
        // $chefs = Chef::where('restaurant_id',$order->restaurant_id)->get();
        $chefs = [];
        return view('admin.order.order', compact('order', 'chefs'));
    }

    public function assignOrder($id, Request $request)
    {
        $find = Order::with('chef', 'customer', 'detail')->find($id);
        if ($find) {
            $get = app('App\Http\Controllers\API\CheckoutController')->orderObj($find->id);
            $find->chef_id = $request->chef;
            $find->order_status = 'Preparing';
            $find->save();
            $chef = Chef::find($request->chef);
            event(new OrderStatusChanged($find));
            event(new OrderUpdateChef($find));
            $get = Customer::find($find->customer_id);
            if ($get->notify == 1) {
                $devices = CustomerDevice::where('customer_id', $get->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices as $item)
                    $notify = app('App\Http\Controllers\API\CheckoutController')->notification($item, 'Your Order is Confirmed & Preparing', 'Tap to see the detail of your order', ['status' => 'order', 'order_id' => $find->id]);
            }
            return true;
        }
    }
}
