<?php

namespace App\Http\Controllers\Admin;

use App\Chef;
use App\Mail\CustomerSupport as MailCustomerSupport;
use App\Mail\RiderSupport as MailRiderSupport;
use App\Mail\ChefSupport as MailChefSupport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\CustomerSupport;
use App\RiderSupport;
use App\OrderDetail;
use App\ItemRequest;
use App\ChefSupport;
use Carbon\Carbon;
use App\Order;
use App\Item;
use App\User;
use stdClass;

class IndexController extends Controller
{
    public function index()
    {
        $sales = Order::count();
        $income = Order::where('order_status', 'Delivered')->sum('total_price');
        $completed = Order::where('order_status', 'Delivered')->count();
        $cancelled = Order::where('order_status', 'Cancelled')->count();
        $preparing = Order::where('order_status', 'Preparing')->count();
        $placed = Order::where('order_status', 'Preparing')->count();
        $picked = Order::where('order_status', 'Picked')->count();
        $chefs = Chef::select('id', 'first_name', 'last_name')->get();
        foreach ($chefs as $item) {
            $item->sales = Order::where('chef_id', $item->id)->count();
            $item->income = Order::where('chef_id', $item->id)->where('order_status', 'Delivered')->sum('total_price');
            $item->completed = Order::where('chef_id', $item->id)->where('order_status', 'Delivered')->count();
            $item->placed = Order::where('chef_id', $item->id)->where('order_status', 'Placed')->count();
            $item->picked = Order::where('chef_id', $item->id)->where('order_status', 'Picked')->count();
            $item->preparing = Order::where('chef_id', $item->id)->where('order_status', 'Preparing')->count();
            $item->cancelled = Order::where('chef_id', $item->id)->where('order_status', 'Cancelled')->count();
        }
        return view('admin.index', compact('chefs', 'sales', 'income', 'completed', 'preparing', 'placed', 'picked', 'cancelled'));
    }

    public function overall($type, $chef = 0)
    {
        $data = $this->orderFilter($type, $chef);
        $index = $data['index'];
        $counts = $data['counts'];
        $text = $data['text'];
        if ($chef != 0) {
            $earnings = Order::where('chef_id', $chef)->where('order_status', 'Delivered')->sum('total_price');
            $month = Order::where('chef_id', $chef)->where('order_status', 'Delivered')->whereMonth('created_at', date('Y-m-d'))->sum('total_price');
            $completed = Order::where('chef_id', $chef)->where('order_status', 'Delivered')->count();
            if ($completed != 0)
                $average = $earnings / $completed;
            else
                $average = 0;
        } else {
            $earnings = Order::where('order_status', 'Delivered')->sum('total_price');
            $month = Order::where('order_status', 'Delivered')->whereMonth('created_at', date('Y-m-d'))->sum('total_price');
            $completed = Order::where('order_status', 'Delivered')->count();
            if ($completed != 0)
                $average = $earnings / $completed;
            else
                $average = 0;
        }
        return view('admin.overall', compact('type', 'chef', 'index', 'counts', 'text', 'earnings', 'completed', 'month', 'average'));
    }

    public function orderFilter($type = 0, $chef = 0)
    {
        $text = '';
        if ($type == 0) {
            $text = 'Orders of last 30 days';
            if ($chef != 0) {
                $filter = Order::where('chef_id', $chef)->whereBetween('created_at', [
                    Carbon::now()->subdays(30)->format('Y-m-d'),
                    Carbon::now()->subday()->format('Y-m-d')
                ])->get()->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('d'); // grouping by months
                });
            } else {
                $filter = Order::whereBetween('created_at', [
                    Carbon::now()->subdays(30)->format('Y-m-d'),
                    Carbon::now()->subday()->format('Y-m-d')
                ])->get()->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('d'); // grouping by months
                });
            }
        }
        if ($type == 2) {
            $text = 'Orders yearly';
            if ($chef != 0) {
                $filter = Order::where('chef_id', $chef)->select('id', 'created_at')->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                    });
            } else {
                $filter = Order::select('id', 'created_at')->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                    });
            }
        }
        if ($type == 1) {
            $text = 'Orders monthly';
            if ($chef != 0) {
                $filter = Order::where('chef_id', $chef)->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('m'); // grouping by months
                    });
            } else {
                $filter = Order::get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('m'); // grouping by months
                    });
            }
        }
        $main = [];
        $filterOrders = [];
        foreach ($filter as $index => $item) {
            $main[] = $index;
            $filterOrders[] = $filter[$index]->count();
        }
        return ['index' => $main, 'counts' => $filterOrders, 'text' => $text];
    }

    public function sales(Request $request)
    {
        if ($request->ajax()) {
            if ($request->value) {
                if ($request->value == 1) {
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereDate('created_at', date('Y-m-d'))->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format($item->price * $id, 2);
                        $arr[] = $obj;
                    }
                    if ($request->q && $request->q = 1) {
                        $items = collect($arr)->sortByDesc('quantity');
                        $q = 1;
                    } else {
                        $items = collect($arr)->sortBy('price');
                        $q = 0;
                    }
                } elseif ($request->value == 2) {
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereBetween('created_at', [
                        Carbon::now()->subdays(8)->format('Y-m-d'),
                        Carbon::now()->format('Y-m-d')
                    ])->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format($item->price * $id, 2);
                        $arr[] = $obj;
                    }
                    if ($request->q && $request->q = 1) {
                        $items = collect($arr)->sortByDesc('quantity');
                        $q = 1;
                    } else {
                        $items = collect($arr)->sortBy('price');
                        $q = 0;
                    }
                } elseif ($request->value == 3) {
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereBetween('created_at', [
                        Carbon::now()->subdays(30)->format('Y-m-d'),
                        Carbon::now()->format('Y-m-d')
                    ])->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format($item->price * $id, 2);
                        $arr[] = $obj;
                    }
                    if ($request->q && $request->q = 1) {
                        $items = collect($arr)->sortByDesc('quantity');
                        $q = 1;
                    } else {
                        $items = collect($arr)->sortBy('price');
                        $q = 0;
                    }
                } elseif ($request->value == 4) {
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereBetween('created_at', [
                        Carbon::now()->subdays(366)->format('Y-m-d'),
                        Carbon::now()->format('Y-m-d')
                    ])->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format(number_format($item->price * $id, 2), 2);
                        $arr[] = $obj;
                    }
                    if ($request->q && $request->q = 1) {
                        $items = collect($arr)->sortByDesc('quantity');
                        $q = 1;
                    } else {
                        $items = collect($arr)->sortBy('price');
                        $q = 0;
                    }
                } else {
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereDate('created_at', date('Y-m-d'))->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format(number_format($item->price * $id, 2), 2);
                        $arr[] = $obj;
                    }
                    if ($request->q && $request->q = 1) {
                        $items = collect($arr)->sortByDesc('quantity');
                        $q = 1;
                    } else {
                        $items = collect($arr)->sortBy('price');
                        $q = 0;
                    }
                }
            } elseif ($request->start && $request->end) {
                $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->orderBy('q')->groupBy('item_id')->pluck('q');
                $arr = [];
                foreach ($ids as $index => $id) {
                    $item = Item::find($ids[$index]);
                    if (!$item)
                        continue;
                    $obj = new stdClass();
                    $obj->item_name = $item->item_name;
                    $obj->quantity = $id;
                    $obj->price = number_format(number_format($item->price * $id, 2), 2);
                    $arr[] = $obj;
                }
                // $obj1 = new stdClass();
                // $obj1->item_name = 'dum';
                // $obj1->quantity = 22;
                // $obj1->price = 220;
                // $arr[] = $obj1;
                if ($request->q && $request->q = 1) {
                    $items = collect($arr)->sortByDesc('quantity');
                    $q = 1;
                } else {
                    $items = collect($arr)->sortBy('price');
                    $q = 0;
                }
            } else {
                $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereDate('created_at', date('Y-m-d'))->orderBy('q')->groupBy('item_id')->pluck('q');
                $arr = [];
                foreach ($ids as $index => $id) {
                    $item = Item::find($ids[$index]);
                    if (!$item)
                        continue;
                    $obj = new stdClass();
                    $obj->item_name = $item->item_name;
                    $obj->quantity = $id;
                    $obj->price = number_format(number_format($item->price * $id, 2), 2);
                    $arr[] = $obj;
                }
                if ($request->q && $request->q = 1) {
                    $items = collect($arr)->sortByDesc('quantity');
                    $q = 1;
                } else {
                    $items = collect($arr)->sortBy('price');
                    $q = 0;
                }
            }
            return view('admin.sale.index-load', compact('items', 'q'))->render();
        } else {
            $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereDate('created_at', date('Y-m-d'))->orderBy('q')->groupBy('item_id')->pluck('q');
            $arr = [];
            foreach ($ids as $index => $id) {
                $item = Item::find($ids[$index]);
                if (!$item)
                    continue;
                $obj = new stdClass();
                $obj->item_name = $item->item_name;
                $obj->quantity = $id;
                $obj->price = number_format(number_format($item->price * $id, 2), 2);
                $arr[] = $obj;
            }
            if ($request->q && $request->q = 1) {
                $items = collect($arr)->sortByDesc('quantity');
                $q = 1;
            } else {
                $items = collect($arr)->sortBy('price');
                $q = 0;
            }
            return view('admin.sale.index', compact('items', 'q'));
        }
    }

    public function topSales(Request $request)
    {
        if ($request->ajax()) {
            if ($request->value) {
                if ($request->value == 1) {
                    $ids1 = Order::where('order_status', 'Delivered')->whereDate('created_at', date('Y-m-d'))->pluck('id');
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereIn('order_id', $ids1)->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format(number_format($item->price * $id, 2), 2);
                        $arr[] = $obj;
                    }
                    $items = collect($arr)->sortByDesc('quantity');
                    return view('admin.sale.dishes-load', compact('items'))->render();
                } elseif ($request->value == 2) {
                    $ids1 = Order::where('order_status', 'Delivered')->whereBetween('created_at', [
                        Carbon::now()->subdays(8)->format('Y-m-d'),
                        Carbon::now()->format('Y-m-d')
                    ])->pluck('id');
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereIn('order_id', $ids1)->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format(number_format($item->price * $id, 2), 2);
                        $arr[] = $obj;
                    }
                    $items = collect($arr)->sortByDesc('quantity');
                    return view('admin.sale.dishes-load', compact('items'))->render();
                } elseif ($request->value == 3) {
                    $ids1 = Order::where('order_status', 'Delivered')->whereBetween('created_at', [
                        Carbon::now()->subdays(31)->format('Y-m-d'),
                        Carbon::now()->format('Y-m-d')
                    ])->pluck('id');
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereIn('order_id', $ids1)->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format(number_format($item->price * $id, 2), 2);
                        $arr[] = $obj;
                    }
                    $items = collect($arr)->sortByDesc('quantity');
                    return view('admin.sale.dishes-load', compact('items'))->render();
                } elseif ($request->value == 4) {
                    $ids1 = Order::where('order_status', 'Delivered')->whereBetween('created_at', [
                        Carbon::now()->subdays(366)->format('Y-m-d'),
                        Carbon::now()->format('Y-m-d')
                    ])->pluck('id');
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereIn('order_id', $ids1)->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format(number_format($item->price * $id, 2), 2);
                        $arr[] = $obj;
                    }
                    $items = collect($arr)->sortByDesc('quantity');
                    return view('admin.sale.dishes-load', compact('items'))->render();
                } else {
                    $ids1 = Order::where('order_status', 'Delivered')->whereDate('created_at', date('Y-m-d'))->pluck('id');
                    $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereIn('order_id', $ids1)->orderBy('q')->groupBy('item_id')->pluck('q');
                    $arr = [];
                    foreach ($ids as $index => $id) {
                        $item = Item::find($ids[$index]);
                        if (!$item)
                            continue;
                        $obj = new stdClass();
                        $obj->item_name = $item->item_name;
                        $obj->quantity = $id;
                        $obj->price = number_format(number_format($item->price * $id, 2), 2);
                        $arr[] = $obj;
                    }
                    $items = collect($arr)->sortByDesc('quantity');
                    return view('admin.sale.dishes-load', compact('items'))->render();
                }
            } elseif ($request->start && $request->end) {
                $ids1 = Order::where('order_status', 'Delivered')->whereDate('created_at', '>=', Carbon::parse($request->start)->format('Y-m-d'))->whereDate('created_at', '<=', Carbon::parse($request->end)->format('Y-m-d'))->pluck('id');
                $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereIn('order_id', $ids1)->orderBy('q')->groupBy('item_id')->pluck('q');
                $arr = [];
                foreach ($ids as $index => $id) {
                    $item = Item::find($ids[$index]);
                    if (!$item)
                        continue;
                    $obj = new stdClass();
                    $obj->item_name = $item->item_name;
                    $obj->quantity = $id;
                    $obj->price = number_format(number_format($item->price * $id, 2), 2);
                    $arr[] = $obj;
                }
                $items = collect($arr)->sortByDesc('quantity');
                return view('admin.sale.dishes-load', compact('items'))->render();
            } else {
                $ids1 = Order::where('order_status', 'Delivered')->whereDate('created_at', date('Y-m-d'))->pluck('id');
                $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereIn('order_id', $ids1)->orderBy('q')->groupBy('item_id')->pluck('q');
                $arr = [];
                foreach ($ids as $index => $id) {
                    $item = Item::find($ids[$index]);
                    if (!$item)
                        continue;
                    $obj = new stdClass();
                    $obj->item_name = $item->item_name;
                    $obj->quantity = $id;
                    $obj->price = number_format(number_format($item->price * $id, 2), 2);
                    $arr[] = $obj;
                }
                $items = collect($arr)->sortByDesc('quantity');
                return view('admin.sale.dishes-load', compact('items'))->render();
            }
        } else {
            $ids1 = Order::where('order_status', 'Delivered')->whereDate('created_at', date('Y-m-d'))->pluck('id');
            $ids = OrderDetail::selectRaw('sum(quantity) as q')->whereIn('order_id', $ids1)->orderBy('q')->groupBy('item_id')->pluck('q');
            $arr = [];
            foreach ($ids as $index => $id) {
                $item = Item::find($ids[$index]);
                if (!$item)
                    continue;
                $obj = new stdClass();
                $obj->item_name = $item->item_name;
                $obj->quantity = $id;
                $obj->price = number_format(number_format($item->price * $id, 2), 2);
                $arr[] = $obj;
            }
            $items = collect($arr)->sortByDesc('quantity');
            return view('admin.sale.dishes', compact('items'));
        }
    }

    public function saveData(Request $request)
    {
        $request->validate([
            'name'           => 'required',
            'email'          => 'required|email'
        ]);
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        return response()->json(['status' => true, 'message' => 'Data successfully updated!']);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password'  => 'required',
            'password' => 'nullable|between:6,25|confirmed',
        ]);
        $user = User::find(Auth::user()->id);
        if (Hash::check($request->old_password, $user->password)) {
            $user->password = bcrypt($request->password);
            $user->save();
            return response()->json(['status' => true, 'message' => 'Password updated!']);
        } else
            return response()->json(['status' => false, 'message' => 'Old password is not correct!']);
    }

    public function chefSupports()
    {
        $supports = ChefSupport::whereHas('chef')->latest()->Paginate(25);
        return view('admin.support.chef', compact('supports'));
    }

    public function customerSupports()
    {
        $supports = CustomerSupport::latest()->Paginate(25);
        return view('admin.support.customer', compact('supports'));
    }

    public function riderSupports()
    {
        $supports = RiderSupport::latest()->Paginate(25);
        return view('admin.support.rider', compact('supports'));
    }

    public function reply($person, $id)
    {
        if ($person == 'chef') {
            $support = ChefSupport::find($id);
            $support->name = $support->chef->first_name . ' ' . $support->chef->last_name;
        } elseif ($person == 'customer') {
            $support = CustomerSupport::find($id);
            $support->name = $support->customer->first_name . ' ' . $support->customer->last_name;
        } elseif ($person == 'rider') {
            $support = RiderSupport::find($id);
            $support->name = $support->rider->first_name . ' ' . $support->rider->last_name;
        }
        return view('admin.support.reply', compact('person', 'id', 'support'));
    }

    public function sendReply(Request $request)
    {
        if ($request->type == 'chef') {
            $support = ChefSupport::find($request->id);
            Mail::to($support->chef->email)->send(new MailChefSupport($support->chef->first_name . ' ' . $support->chef->last_name, $request->message));
            return redirect()->route('chefSupports')->with('message', 'Replied to chef support message');
        } elseif ($request->type == 'customer') {
            $support = CustomerSupport::find($request->id);
            Mail::to($support->customer->email)->send(new MailCustomerSupport($support->customer->first_name . ' ' . $support->customer->last_name, $request->message));
            return redirect()->route('customerSupports')->with('message', 'Replied to customer support message');
        } elseif ($request->type == 'rider') {
            $support = RiderSupport::find($request->id);
            Mail::to($support->rider->email)->send(new MailRiderSupport($support->rider->first_name . ' ' . $support->rider->last_name, $request->message));
            return redirect()->route('riderSupports')->with('message', 'Replied to rider support message');
        }
    }

    public function chefRequests(Request $request)
    {
        if ($request->chef)
            $requests = ItemRequest::whereHas('chef')->where('chef_id', $request->chef)->latest()->Paginate(25);
        else
            $requests = ItemRequest::whereHas('chef')->latest()->Paginate(25);
        return view('admin.request.index', compact('requests'));
    }

    public function chefRequestApprove($id)
    {
        $find = ItemRequest::find($id);
        if ($find) {
            $find->status = 1;
            $find->save();
            return redirect()->back()->with('message', 'Request of chef approved');
        }
        return redirect()->back()->with('error', 'No request found');
    }

    public function approveSpecial($id)
    {
        $find = Item::find($id);
        if ($find) {
            $find->approved = 1;
            $find->save();
            return redirect()->back()->with('message', 'Item approved');
        }
        return redirect()->back()->with('error', 'No request found');
    }
}
