<?php

namespace App\Http\Controllers\Admin;

use App\Chat;
use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use App\Rider;
use App\RiderOrder;
use App\Ticket;

class RiderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->type == '')
            $riders = Rider::latest()->get();
        if ($request->type == 'suspended')
            $riders = Rider::where('suspended', 1)->latest()->get();
        if ($request->type == 'available')
            $riders = Rider::where('online', 1)->latest()->get();
        if ($request->type == 'unavailable')
            $riders = Rider::where('online', 0)->latest()->get();
        return view('admin.rider.riders', compact('riders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Rider $rider)
    {
        $ids = RiderOrder::where('rider_id', $rider->id)->pluck('order_id');
        $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->whereIn('id', $ids)->count();
        $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->whereIn('id', $ids)->count();
        $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->whereIn('id', $ids)->count();
        $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->whereIn('id', $ids)->count();
        return view('admin.rider.rider', compact('rider', 'breakfast', 'lunch', 'dinner', 'midnight'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function softDelete($id)
    {
        $rider = Rider::find($id);
        if ($rider) {
            if ($rider->suspended == 0)
                $rider->suspended = 1;
            elseif ($rider->suspended == 1)
                $rider->suspended = 0;
            $rider->save();
            return redirect()->back()->with('message', ($rider->suspended == 1) ? 'Rider suspended' : 'Rider unsuspended');
        } else
            return back()->with('error', 'No rider found');
    }

    public function verify($id)
    {
        $find = Rider::find($id);
        if ($find != null) {
            $update = Rider::where('id', $id)->update(['approved' => 1]);
            if ($update)
                return back()->with('message', 'Rider verified');
        } else
            return back()->with('error', 'No rider found');
    }

    public function tickets($type)
    {
        $tickets = Ticket::where('status', $type)->where('rider_id', '!=', 0)->latest()->Paginate(25);
        return view('admin.ticket.rider.index', compact('tickets', 'type'));
    }

    public function ticket($id)
    {
        $ticket = Ticket::find($id);
        if ($ticket) {
            $messages = Chat::select('id', 'from', 'to', 'from_to', 'message', 'is_read', 'time', 'created_at')->where('ticket_id', $id)->get();
            return view('admin.ticket.rider.show', compact('ticket', 'messages'));
        } else
            return redirect()->back()->with('error', 'No ticket found');
    }
}
