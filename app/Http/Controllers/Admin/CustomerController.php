<?php

namespace App\Http\Controllers\Admin;

use App\Chat;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use App\Order;
use App\Ticket;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->type == '')
            $customers = Customer::latest()->Paginate(15);
        elseif ($request->type == 'new')
            $customers = Customer::whereDate('created_at', date('Y-m-d'))->latest()->Paginate(15);
        elseif ($request->type == 'realtime') {
            $ids = Order::where('created_at', date('Y-m-d'))->where('order_status', '!=', 'Delivered')->orWhere('created_at', date('Y-m-d'))->where('order_status', '!=', 'Cancelled')->pluck('customer_id');
            $customers = Customer::whereIn('id', $ids)->latest()->Paginate(15);
        } elseif ($request->type == 'active') {
            $ids = Order::where('created_at', '>=', date('Y-m-d', strtotime("-7 days")))->pluck('customer_id');
            $customers = Customer::whereIn('id', $ids)->latest()->Paginate(15);
        } elseif ($request->type == 'inactive') {
            $ids = Order::where('created_at', '<', date('Y-m-d', strtotime("-7 days")))->pluck('customer_id');
            $customers = Customer::whereIn('id', $ids)->latest()->Paginate(15);
        }
        $type = $request->type;
        return view('admin.customer.customers', compact('customers', 'type'));
    }

    public function tickets($type)
    {
        $tickets = Ticket::where('status', $type)->where('customer_id', '!=', 0)->latest()->Paginate(25);
        return view('admin.ticket.index', compact('tickets', 'type'));
    }

    public function ticket($id)
    {
        $ticket = Ticket::find($id);
        if ($ticket) {
            $messages = Chat::select('id', 'from', 'to', 'from_to', 'message', 'is_read', 'time', 'created_at')->where('ticket_id', $id)->get();
            return view('admin.ticket.show', compact('ticket', 'messages'));
        } else
            return redirect()->back()->with('error', 'No ticket found');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $customer = Customer::find($id);
        $breakfast = Order::whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->where('customer_id', $id)->count();
        $lunch = Order::whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->where('customer_id', $id)->count();
        $dinner = Order::whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->where('customer_id', $id)->count();
        $midnight = Order::whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->where('customer_id', $id)->count();
        return view('admin.customer.customer', compact('customer', 'breakfast', 'lunch', 'dinner', 'midnight'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }
}
