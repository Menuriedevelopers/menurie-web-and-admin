<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Restaurant;
use Validator;

class RestaurantController extends Controller
{
    public function index()
    {
        $restaurants = Restaurant::latest('created_at')->get();
        return view('admin.restaurant.restaurants',compact('restaurants'));
    }

    public function saveRestaurant(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'branchno'=>'required'
            ,'location'=>'required|string'
        ]);
        if($validator->fails())
        return back()->withErrors($validator->errors()->all())->withInput();

        $branch = $request->branchno;
        $location = $request->location;
        $latlong = $request->latlong;
        $save = new Restaurant;
        $save->branch_no = $branch;
        $save->location = $location;
        $save->lat_long = $latlong;
        $save->save();
        if($save)
            return redirect()->route('restaurantsListPage')->with('message', 'Restaurant Successfully Added');
    }

    public function showList()
    {
        $restaurants = Restaurant::latest('created_at')->get()->toArray();
        return response()->json(['status'=>true,'data'=>$restaurants,'action'=>'List of restaurants'],200);
    }

    public function softDelete($id)
    {
        $find = Restaurant::destroy($id);
        if ($find)
            return back()->with('message', 'Restaurant location deleted');
        else 
            return back()->with('error', 'Error while deleting restaurant location');
    }
}
