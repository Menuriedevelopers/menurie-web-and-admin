<?php

namespace App\Http\Controllers\Admin;

use App\Chat;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Chef;
use App\ChefTiming;
use App\Mail\ApproveChef;
use App\Order;
use App\Ticket;
use DateTime;
use Illuminate\Support\Facades\Mail;

class ChefController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->type == '')
            $chefs = Chef::get();
        if ($request->type == 'suspended')
            $chefs = Chef::where('suspended', 1)->get();
        if ($request->type == 'active') {
            $get = Chef::get();
            $ids = [];
            foreach ($get as $item) {
                date_default_timezone_set($item->timezone);
                $yesterday = date('l', strtotime("-1 days"));
                $time = date('H:i');
                $day = date('l');
                $getTime = ChefTiming::where('chef_id', $item->id)->where('day', $day)->first();
                $yesterdayTime = ChefTiming::where('chef_id', $item->id)->where('day', $yesterday)->first();
                if ($yesterdayTime) {
                    if ($yesterdayTime->time != 'Closed') {
                        $breakTime = explode('-', $yesterdayTime->time);
                        $startTime = date_format(date_create($breakTime[0]), 'H:i');
                        $startTimeHour = date_format(date_create($breakTime[0]), 'H');
                        $endTime = date_format(date_create($breakTime[1]), 'H:i');
                        $endTimeHour = date_format(date_create($breakTime[1]), 'H');
                        if ($endTime < $startTime)
                            $ids[] = $item->id;
                    }
                }
                if (!$getTime)
                    continue;
                else {
                    if ($getTime->time == 'Closed')
                        continue;
                    else {
                        $breakTime = explode('-', $getTime->time);
                        $startTime = date_format(date_create($breakTime[0]), 'H:i');
                        $endTime = date_format(date_create($breakTime[1]), 'H:i');
                        $dateA = date('Y-m-d H:m:s');
                        $d1 = new DateTime($startTime);
                        $d2 = new DateTime();
                        $interval = $d1->diff($d2);
                        if ($interval->invert == 1 && $interval->i <= 30)
                            continue;
                        if ($startTime > $time)
                            continue;
                        elseif ($endTime < $startTime)
                            $ids[] = $item->id;
                        elseif ($startTime <= $time && $endTime > $time) {
                            $ids[] = $item->id;
                        } elseif ($endTime < $time)
                            continue;
                    }
                }
            }
            $chefs = Chef::whereIn('id', $ids)->get();
        }
        if ($request->type == 'inactive') {
            $get = Chef::get();
            $ids1 = [];
            foreach ($get as $check) {
                date_default_timezone_set($check->timezone);
                $yesterday = date('l', strtotime("-1 days"));
                $time = date('H:i');
                $day = date('l');
                $getTime = ChefTiming::where('chef_id', $check->id)->where('day', $day)->first();
                if (!$getTime)
                    $ids1[] = $check->id;
                else {
                    if ($getTime->time == 'Closed')
                        $ids1[] = $check->id;
                    else {
                        $breakTime = explode('-', $getTime->time);
                        $startTime = date_format(date_create($breakTime[0]), 'H:i');
                        $endTime = date_format(date_create($breakTime[1]), 'H:i');
                        $dateA = date('Y-m-d H:m:s');
                        $d1 = new DateTime($startTime);
                        $d2 = new DateTime();
                        $interval = $d1->diff($d2);
                        if ($interval->invert == 1 && $interval->i <= 30)
                            $ids1[] = $check->id;
                        if ($startTime > $time)
                            $ids1[] = $check->id;
                        elseif ($endTime < $startTime)
                            continue;
                        elseif ($startTime <= $time && $endTime > $time) {
                            continue;
                        } elseif ($endTime < $time)
                            $ids1[] = $check->id;
                    }
                }
            }
            $chefs = Chef::whereIn('id', $ids1)->get();
        }
        return view('admin.chef.chefs', compact('chefs'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Chef $chef)
    {
        $breakfast = Order::where('chef_id', $chef->id)->whereTime('created_at', '>', '06:00:00')->whereTime('created_at', '<', '12:00:00')->count();
        $lunch = Order::where('chef_id', $chef->id)->whereTime('created_at', '>', '12:00:00')->whereTime('created_at', '<', '18:00:00')->count();
        $dinner = Order::where('chef_id', $chef->id)->whereTime('created_at', '>', '18:00:00')->whereTime('created_at', '<', '24:00:00')->count();
        $midnight = Order::where('chef_id', $chef->id)->whereTime('created_at', '>', '24:00:00')->whereTime('created_at', '<', '06:00:00')->count();
        return view('admin.chef.chef', compact('chef', 'breakfast', 'lunch', 'dinner', 'midnight'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function softDelete($id)
    {
        $chef = Chef::find($id);
        if ($chef) {
            if ($chef->suspended == 0)
                $chef->suspended = 1;
            elseif ($chef->suspended == 1)
                $chef->suspended = 0;
            $chef->save();
            return redirect()->back()->with('message', ($chef->suspended == 1) ? 'Chef suspended' : 'Chef unsuspended');
        } else
            return back()->with('error', 'No chef found');
    }

    public function updateRange($id, Request $request)
    {
        $chef = Chef::find($id);
        if ($chef) {
            $chef->order_range = $request->range;
            $chef->save();
            return redirect()->back()->with('message', 'Chef range updated');
        }
    }

    public function verify($id)
    {
        $find = Chef::find($id);
        if ($find != null) {
            Mail::to($find->email)->send(new ApproveChef($find->first_name . ' ' . $find->last_name));
            $update = Chef::where('id', $id)->update(['approved' => 1]);
            if ($update)
                return back()->with('message', 'Chef verified');
        } else
            return back()->with('error', 'No chef found');
    }

    public function tickets($type)
    {
        $tickets = Ticket::where('status', $type)->where('chef_id', '!=', 0)->latest()->Paginate(25);
        return view('admin.ticket.chef.index', compact('tickets', 'type'));
    }

    public function ticket($id)
    {
        $ticket = Ticket::find($id);
        if ($ticket) {
            $messages = Chat::select('id', 'from', 'to', 'from_to', 'message', 'is_read', 'time', 'created_at')->where('ticket_id', $id)->get();
            return view('admin.ticket.chef.show', compact('ticket', 'messages'));
        } else
            return redirect()->back()->with('error', 'No ticket found');
    }
}
