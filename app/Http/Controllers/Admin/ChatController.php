<?php

namespace App\Http\Controllers\Admin;

use App\Chat;
use App\Chef;
use App\ChefDevice;
use App\Customer;
use App\CustomerDevice;
use App\Http\Controllers\Controller;
use App\Rider;
use App\RiderDevice;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Pusher\Pusher;

class ChatController extends Controller
{

    public $pusher;

    public function __construct()
    {
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => false
        );
        $this->pusher = new Pusher(
            "68f9e094a21250f4ccdc",
            "674c8a47c4552b15ca15",
            "1282636",
            $options
        );
    }

    public function auth(Request $request)
    {
        echo $this->pusher->presence_auth($request->channel_name, $request->socket_id, Auth::user()->id, Auth::user());
    }

    public function get_messages($user_id, $limit = 30, $offset = 0)
    {
        DB::beginTransaction();
        $messages = DB::select("SELECT * FROM `chats` WHERE (chats.from = $user_id AND chats.to = 0) OR (chats.from = 0 AND chats.to = $user_id) ORDER BY id DESC LIMIT $limit OFFSET $offset");
        DB::commit();
        foreach ($messages as $item) {
            if ($item->from == 0)
                $item->name = 'Admin';
            else {
                $customer = Customer::find($item->from);
                $item->name = $customer->first_name . ' ' . $customer->last_name;
            }
        }
        echo json_encode($messages);
    }

    //Send User to User/Client Message
    public function send_message(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'to' => "required",
            'message' => "required",
        ]);

        if ($validator->fails()) {
            echo json_encode(['result' => false, 'message' => $validator->errors()->all()]);
            die();
        }
        $chat_message = new Chat();
        $chat_message->from = 0;
        $chat_message->from_to = $request->to . '_0';
        $chat_message->to = intval($request->to);
        if ($request->ticket)
            $chat_message->ticket_id = $request->ticket;
        //If user Directly enter a tag
        if (strpos($request->message, '<a') !== false) {
            echo json_encode(['result' => false, 'message' => array('msg' => 'illegal operation')]);
            die();
        }
        //Remove inline Style
        $chat_message->message = preg_replace('/(<[^>]*) style=("[^"]+"|\'[^\']+\')([^>]*>)/i', '$1$3', $request->message);
        $chat_message->time = strtotime(date('Y-m-d H:i:s'));
        $chat_message->save();
        // $readAll = Chat::where('from', $chat_message->to)->where('to', 0)->orWhere('from', 0)->where('to', $chat_message->to)->update(['is_read' => 1]);
        $item = Chat::find($chat_message->id);
        if ($item->from == 0)
            $item->name = 'Admin';

        $ticket = Ticket::find($item->ticket_id);
        if ($ticket->customer_id != 0) {

            $customer = Customer::find($item->to);
            $item->name = $customer->first_name . ' ' . $customer->last_name;
            if ($customer->notify == 1) {
                $tokens = CustomerDevice::where('customer_id', $item->to)->pluck('fcm_token');
                foreach ($tokens as $item1)
                    app('App\Http\Controllers\API\CheckoutController')->notification($item1, 'New message against your ticket', 'Tap to reply', ['status' => 'support', 'ticket_id' => $item->ticket_id, 'ticket_status' => $ticket->status, 'ticket_title' => $ticket->type]);
            }
        }
        if ($ticket->chef_id != 0) {
            $customer = Chef::find($item->to);
            $item->name = $customer->first_name . ' ' . $customer->last_name;
            if ($customer->notify == 1) {
                $tokens = ChefDevice::where('chef_id', $item->to)->pluck('fcm_token');
                foreach ($tokens as $item1)
                    app('App\Http\Controllers\API\CheckoutController')->notification($item1, 'New message against your ticket', 'Tap to reply', ['status' => 'support', 'ticket_id' => $item->ticket_id, 'ticket_status' => $ticket->status, 'ticket_title' => $ticket->type]);
            }
        }
        if ($ticket->rider_id != 0) {
            $customer = Rider::find($item->to);
            $item->name = $customer->first_name . ' ' . $customer->last_name;
            if ($customer->notify == 1) {
                $tokens = RiderDevice::where('rider_id', $item->to)->pluck('fcm_token');
                foreach ($tokens as $item1)
                    app('App\Http\Controllers\API\CheckoutController')->notification($item1, 'New message against your ticket', 'Tap to reply', ['status' => 'support', 'ticket_id' => $item->ticket_id, 'ticket_status' => $ticket->status, 'ticket_title' => $ticket->type]);
            }
        }
        $this->pusher->trigger('chat-' . $item->ticket_id, 'message-event', $item);
        echo json_encode(['result' => true, 'data' => $item]);
    }
}
