<?php

namespace App\Http\Controllers\Site;

use Illuminate\Support\Facades\Validator;
use Twilio\Exceptions\TwilioException;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\ItemRequest;
use App\Favorite;
use App\Category;
use App\Customer;
use App\Order;
use App\Chef;
use App\Item;

class IndexController extends Controller
{

    public function specialItems($user, $id)
    {
        $ids = Item::where([['approved', 1], ['special', 1], ['is_active', 1], ['chef_id', $id]])->latest()->get();
        $inactive = [];
        foreach ($ids as $item) {
            if ($item->is_active == 1) {
                $check = ItemRequest::where('chef_id', $id)->where('item_id', $item->id)->latest()->first();
                if ($check) {
                    // if ($check->status == 0)
                    //     $inactive[] = $item->id;
                    // else {
                    if ($check->status == 1 && $check->type == 0)
                        $inactive[] = $item->id;
                    // }
                }
            }
        }
        $arr = array();
        $items = Item::with('chef', 'category')->where([['approved', 1], ['special', 1], ['is_active', 1], ['chef_id', $id]])->whereNotIn('id', $inactive)->latest()->get()->take(10);
        foreach ($items as $item) {
            if ($user) {
                if (Favorite::where('item_id', $item->id)->where('user_id', $user->id)->first())
                    $item->isFav = true;
                else
                    $item->isFav = false;
            }
        }
        return $items;
    }

    public function index()
    {
        if (Auth::guard('customer')->check())
            app('App\Http\Controllers\Site\CartController')->checkDbCart(Auth::guard('customer')->user()->id);
        $items = Item::with('chef')->where('special', 0)->latest()->take(6)->get();
        $sItems = null;
        $location = session()->get('location');
        if ($location) {
            $chefs = app('App\Http\Controllers\API\CustomerController')->chefs($location['lat'], $location['lng']);
            if (Auth::check())
                $user = Auth::user();
            else
                $user = null;
            if ($chefs['chef'])
                $sItems = $this->specialItems($user, $chefs['chef']->id);
        }
        $breakfast = Category::with('items')->where('name', 'Breakfast')->first();
        $pizza = Category::with('items')->where('name', 'Pizza')->first();
        $salads = Category::with('items')->where('name', 'Salads')->first();
        $lunch = Category::with('items')->where('name', 'Lunch')->first();
        $chefs = Chef::latest()->get();
        return view('site.index', compact('chefs', 'pizza', 'lunch', 'salads', 'breakfast', 'items', 'sItems'));
    }

    public function forgot(Request $request)
    {
        $phone = $request->full_number;
        $find = Customer::where('phone', $request->full_number)->where('guest', 0)->first();
        if ($find) {
            $token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_sid = getenv("TWILIO_SID");
            $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
            $twilio = new Client($twilio_sid, $token);
            try {
                $twilio->verify->v2->services($twilio_verify_sid)->verifications->create($phone, "sms");
                return view('site.rider.riderOtp', compact('phone'));
            } catch (TwilioException $e) {
                $error = 'An error occured try again later';
                if ($e->getCode() == 60200)
                    $error = 'Phone number is invalid';
                if ($e->getCode() == 404)
                    $error = 'Send otp request again';
                return redirect()->back()->with('error', $error);
            }
        } else
            return redirect()->back()->with('error', 'No user found of this phone number');
    }

    public function forgotVerify(Request $request)
    {
        $code = $request->input('p-1') . $request->input('p-2') . $request->input('p-3') . $request->input('p-4') . $request->input('p-5') . $request->input('p-6');
        $phone = $request->phone;
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $verification = $twilio->verify->v2->services($twilio_verify_sid)->verificationChecks->create($code, array('to' => $phone));

        if ($verification->valid)
            return view('site.reset', compact('phone'));
        else
            return redirect()->back()->with('error', 'Wrong OTP password entered');
    }

    public function reset(Request $request)
    {
        $phone = $request->phone;
        $password = $request->password;
        $user = Customer::where('phone', $phone)->where('guest', 0)->first();
        $user->password = bcrypt($password);
        $user->save();
        return redirect()->route('customer.login')->with('message', 'Password reset successful login now!');
    }

    public function list($type = 'NA')
    {
        $categories = Category::whereHas('items')->with('items')->orderBy('name')->get();
        return view('site.foodDetail', compact('categories', 'type'));
    }

    public function chefDetail($id)
    {
        $chef = Chef::find($id);
        if ($chef) {
            $items = Item::where('chef_id', $chef->id)->orderBy('item_name')->get();
            return view('site.chef-detail', compact('items', 'chef'));
        } else
            return redirect()->back();
    }

    public function itemDetail(Request $request)
    {
        $item = Item::with('chef', 'addons')->where('is_active', 1)->where('id', $request->id)->first();
        if ($item)
            return view('site.modals.addToCartModal', compact('item'))->render();
    }

    public function saveLocation($place, $lat, $lng)
    {
        $location = session()->get('location');
        $location = ['lat' => $lat, 'lng' => $lng, 'name' => $place];
        session()->put('location', $location);
        return response()->json(['status' => true], 200);
    }

    public function user()
    {
        $orders = Order::where('customer_id', Auth::guard('customer')->user()->id)->take(4)->get();
        return view('site.userProfile', compact('orders'));
    }

    public function user_orders()
    {
        $pendingOrders = Order::with('detail')->where('order_status', 'Placed')->where('customer_id', Auth::guard('customer')->user()->id)->get();
        $completedOrders = Order::with('detail')->where('order_status', 'Delivered')->where('customer_id', Auth::guard('customer')->user()->id)->get();
        $cancelledOrders = Order::with('detail')->where('order_status', 'Cancelled')->where('customer_id', Auth::guard('customer')->user()->id)->get();
        return view('site.user-orders', compact('completedOrders', 'pendingOrders', 'cancelledOrders'));
    }

    public function autocomplete(Request $request)
    {
        $data = Item::where('special', 0)->select('item_name')->where('item_name', 'LIKE', '%' . $request->terms . '%')->pluck('item_name');
        $data1 = Category::select('name')->where('name', 'LIKE', '%' . $request->terms . '%')->pluck('name');
        $main = $data->merge($data1);
        return response()->json($main);
    }

    public function search_result($name)
    {
        $product = $name;
        $ids = Category::select('id')->where('name', 'LIKE', '%' . $product . '%')->pluck('id');
        $items = Item::whereIn('category_id', $ids)->where('special', 0)->where('item_name', 'LIKE', '%' . $product . '%')->get();
        $count = count($items);
        if ($items)
            return view('site.product-search', compact('items', 'product', 'count'));
        else
            return view('site.no-product-search');
    }

    public function searching_result(Request $request)
    {
        $product = $request->record;
        $items = Item::where('item_name', 'LIKE', '%' . $request->record . '%')->get();
        $count = count($items);
        if ($items) {
            return view('site.product-search', compact('items', 'product', 'count'));
        } else {
            return view('site.no-product-search');
        }
    }

    public function edit_profile(Request $request)
    {
        $rule  =  array(
            'file'       => 'required',
            'first_name' => 'required',
            'last_name'  => 'required',
            'email' => 'required'
        );
        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return $validator->errors();
        } else {
            $user = Customer::where('id', $request->id)->first();
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =  $user->id . uniqid() . '.' . $extension;
            $destinationPath = '/uploads/customer/';
            $extension = '.' . $request->file->getClientOriginalExtension();
            $filename = $user->id . '-' . time() . $extension;
            $file->move($destinationPath, $filename);
            $user->image = env('APP_URL') . $destinationPath . $filename;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->update();
            $orders = Order::where('customer_id', Auth::guard('customer')->user()->id)->take(4)->get();
            return view('site.userProfile', compact('orders'));
        }
    }

    public function edit_image(Request $request)
    {
        $user = Customer::where('id', $request->id)->first();
        $file = $request->file('user_image');
        $destinationPath = 'uploads/';
        $extension = '.' . $request->user_image->getClientOriginalExtension();
        $filename = time() . $extension;
        $file->move($destinationPath, $filename);
        $user->image = $destinationPath . $filename;
        $user->update();
        return redirect('user/profile');
    }

    public function edit_name(Request $request)
    {
        $user = Customer::where('id', $request->id)->first();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->update();
        return redirect('user/profile');
    }

    public function edit_email(Request $request)
    {
        $user = Customer::where('id', $request->id)->first();
        $user->email = $request->email;
        $user->update();
        return redirect('user/profile');
    }

    public function edit_phone(Request $request)
    {
        $user = Customer::where('id', $request->id)->first();
        $user->phone = $request->phone;
        $user->update();
        return redirect('user/profile');
    }

    public function orders()
    {
        $orders = Order::where('customer_id', Auth::guard('customer')->user()->id)->Paginate(10);
        return view('site.orders', compact('orders'));
    }

    public function order($id)
    {
        $order = Order::with('detail')->find($id);
        if ($order)
            return view('site.order', compact('order'));
    }
}
