<?php

namespace App\Http\Controllers\Site;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\CustomerDevice;
use App\OrderAddons;
use App\RiderOrder;
use App\AddonList;
use Pusher\Pusher;
use Carbon\Carbon;
use App\Category;
use App\Customer;
use App\Order;
use App\Rider;
use App\Item;
use App\Chef;

class ChefController extends Controller
{
    public $pusher;

    public function __construct()
    {
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => false
        );
        $this->pusher = new Pusher(
            "68f9e094a21250f4ccdc",
            "674c8a47c4552b15ca15",
            "1282636",
            $options
        );
    }

    public function index()
    {
        if (Auth::guard('chef')->check()) {
            $last7 = Carbon::today()->subDays(7);
            $dates1 = [];
            for ($i = 7; $i >= 0; $i--)
                $dates1[] = date('Y-m-d', strtotime("-$i day"));
            $salesArr = [];
            $best = ['sale' => 0, 'date' => $dates1[0]];
            $low = ['sale' => 0, 'date' => $dates1[0]];
            $dates = [];
            foreach ($dates1 as $item) {
                $dates[] = date_format(date_create($item), 'm-d');
                $sale = Order::where('chef_id', Auth::guard('chef')->user()->id)->whereDate('created_at', $item)->count();
                $salesArr[] = $sale;
                if ($sale <= $low['sale']) {
                    $low['sale'] = $sale;
                    $low['date'] = $item;
                }
                if ($sale > $best['sale']) {
                    $best['sale'] = $sale;
                    $best['date'] = $item;
                }
            }

            $sales = Order::where('chef_id', Auth::guard('chef')->user()->id)->count();
            $income = Order::where('chef_id', Auth::guard('chef')->user()->id)->where('order_status', 'completed')->sum('total_price');
            $completed = Order::where('chef_id', Auth::guard('chef')->user()->id)->where('order_status', 'completed')->count();
            $preparing = Order::where('chef_id', Auth::guard('chef')->user()->id)->where('order_status', 'preparing')->count();
            return view('site.chef.index', compact('sales', 'income', 'completed', 'preparing', 'dates', 'salesArr', 'best', 'low'));
        } else
            return redirect()->route('chef.login');
    }

    public function orders()
    {
        $orders = Order::where('chef_id', Auth::guard('chef')->user()->id)->latest()->Paginate(15);
        return view('site.chef.order.orders', compact('orders'));
    }

    public function showOrder($id)
    {
        $order = Order::with('customer', 'chef', 'detail', 'address')->find($id);
        $riders = Rider::where('online', 1)->orderBy('first_name')->get();
        return view('site.chef.order.order', compact('order', 'riders'));
    }

    public function categories()
    {
        $categories = Category::latest()->Paginate(25);
        return view('admin.category.index', compact('categories'));
    }

    public function category($id)
    {
        $category = Category::find($id);
        $items = Item::where('category_id', $category->id)->get();
        return view('admin.category.show', compact('category', 'items'));
    }

    public function addCategory()
    {
        return view('admin.category.create');
    }

    public function storeCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
        ]);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator->errors()->all());
        DB::beginTransaction();
        $category = new Category();
        $category->name = $request->input('name');
        $category->save();
        DB::commit();
        return redirect()->route('categories')->with(['message' => 'Category Successfully Saved']);
    }

    public function editCategory($id)
    {
        $category = Category::find($id);
        return view('admin.category.edit', compact('category', 'id'));
    }

    public function updateCategory(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3'
        ]);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator->errors()->all());
        DB::beginTransaction();
        $item = Category::find($id);
        $item->name = $request->input('name');
        $item->save();
        DB::commit();
        return redirect()->route('category', $item->id)->with(['message' => 'Category Successfully Updated']);
    }

    public function deleteCategory($id)
    {
        $find = Category::find($id);
        if ($find) {
            if ($find->is_active == 1)
                $find->is_active = 0;
            else
                $find->is_active = 1;
            $find->save();
            if ($find)
                return redirect()->back()->with(['message' => 'Category active status updated']);
        }
    }

    public function special()
    {
        $items = Item::where('special', 1)->where('approved', 1)->latest()->Paginate(25);
        $type = 'special';
        return view('admin.items.items', compact('items', 'type'));
    }

    public function unapprovedSpecial()
    {
        $items = Item::where('special', 1)->where('approved', 0)->latest()->Paginate(25);
        $type = 'special';
        return view('admin.items.items', compact('items', 'type'));
    }

    public function items()
    {
        $items = Item::where('special', 0)->latest()->Paginate(25);
        $type = 'item';
        return view('admin.items.items', compact('items', 'type'));
    }

    public function item($id)
    {
        $item = Item::with('chef', 'addons')->find($id);
        return view('admin.items.show', compact('item'));
    }

    public function addItem()
    {
        $categories = Category::orderBy('name')->where('is_active', 1)->get();
        return view('admin.items.create', compact('categories'));
    }

    public function storeItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'item_name' => 'required|min:3',
            'category' => 'required',
            'item_price' => 'required|numeric|between:0,9999.99',
            'item_description' => 'required',
            'item_image' => 'mimes:jpeg,jpg,png|required|max:10000'
            // ,'addons.*.addon_name'=>'required|string'
            // ,'addons.*.addon_price'=>'required'
        ]);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator->errors()->all());
        DB::beginTransaction();
        $item = new Item;
        if ($request->has('specialItem')) {
            $item->chef_id = Auth::guard('chef')->user()->id;
            $item->special = 1;
        }
        $item->item_name = $request->input('item_name');
        $item->category_id = $request->input('category');
        $item->description = $request->input('item_description');
        $item->price = $request->input('item_price');
        $item->count = 0;
        if ($request->hasfile('item_image')) {
            $file = $request->file('item_image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = uniqid() . Auth::user()->id . '.' . $extension;
            if ($file->move('uploads/items/', $filename))
                $image = env('APP_URL') . '/uploads/items/' . $filename;
            $item->item_image = $image;
        }
        $item->save();
        foreach ($request->addons as $addon) {
            if (isset($addon['addon_name']) && isset($addon['addon_price'])) {
                $addon1 = new AddonList;
                $addon1->item_id = $item->id;
                $addon1->name = $addon['addon_name'];
                $addon1->price = $addon['addon_price'];
                $addon1->save();
            }
        }
        DB::commit();
        return redirect()->route('items')->with(['message' => 'Item Successfully Saved']);
    }

    public function edit($id)
    {
        $categories = Category::orderBy('name')->where('is_active', 1)->get();
        $item = Item::with('addons')->find($id);
        return view('admin.items.edit', compact('item', 'id', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'item_name' => 'required|min:3|max:18',
            'category' => 'required',
            'item_price' => 'required|numeric|between:0,9999.99',
            'item_description' => 'required',
            'item_image' => 'mimes:jpeg,jpg,png|nullable|max:10000'
            // ,'addons.*.addon_name'=>'string'
            // ,'addons.*.addon_price'=>'required'
        ]);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator->errors()->all());
        DB::beginTransaction();
        $item = Item::find($id);
        $item->item_name = $request->input('item_name');
        $item->category_id = $request->input('category');
        $item->description = $request->input('item_description');
        $item->price = $request->input('item_price');
        $item->count = 0;
        if ($request->hasfile('item_image')) {
            $file = $request->file('item_image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = Auth::guard('chef')->user()->id . '.' . $extension;
            if ($file->move('uploads/items/', $filename))
                $image = env('APP_URL') . '/uploads/items/' . $filename;
            $item->item_image = $image;
        }
        $item->save();
        $del = AddonList::where('item_id', $item->id)->delete();
        if ($request->addons) {
            foreach ($request->addons as $addon) {
                $addon1 = new AddonList;
                $addon1->item_id = $item->id;
                $addon1->name = $addon['addon_name'];
                $addon1->price = $addon['addon_price'];
                $addon1->save();
            }
        }
        DB::commit();
        return redirect()->route('item', $item->id)->with(['message' => 'Item Successfully Updated']);
    }

    public function softDelete($id)
    {
        $find = Item::find($id);
        if ($find) {
            if ($find->is_active == 1)
                $find->is_active = 0;
            else
                $find->is_active = 1;
            $find->save();
            if ($find)
                return redirect()->back()->with(['message' => 'Item status updated']);
        }
    }

    public function settingsForm($id)
    {
        $chef = Chef::find($id);
        return view('site.chef.settings', compact('chef'));
    }

    public function setting($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);
        if ($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
        else {
            $check = Chef::find($id);
            if ($check != null) {
                $find = Chef::where('email', $request->input('email'))->whereNotIn('id', [$check->id])->first();
                if ($find)
                    return redirect()->back()->with('error', 'Email is already taken');
                if ($request->hasfile('image')) {
                    $file = $request->file('image');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $trimmed = str_replace(' ', '', $request->input('first_name'));
                    $filename = $trimmed . '.' . $extension;
                    if ($file->move('uploads/chef/', $filename))
                        $image = env('APP_URL') . '/uploads/chef/' . $filename;
                    $check->image = $image;
                }
                $check->first_name = $request->input('first_name');
                $check->last_name = $request->input('last_name');
                $check->email = $request->input('email');
                $check->address = ($request->input('address')) ? $request->input('address') : '';
                $check->city = ($request->input('city')) ? $request->input('city') : '';
                $check->area = ($request->input('area')) ? $request->input('area') : '';
                $check->post_code = ($request->input('postcode')) ? $request->input('postcode') : '';
                if ($request->filled('latlong'))
                    $check->lat_long = $request->input('latlong');
                $check->save();
                if ($check)
                    return redirect()->back()->with('message', 'Chef profile updated!');
            }
        }
    }

    public function acceptOrder($id)
    {
        $order = Order::find($id);
        if ($order) {
            $order->order_status = 'Preparing';
            $order->save();
            $customerObj = Customer::find($order->customer_id);
            $get = app('App\Http\Controllers\API\CheckoutController')->orderObj($order->id);
            if ($customerObj->notify == 1) {
                $devices = CustomerDevice::where('customer_id', $customerObj->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices as $item)
                    $notify = app('App\Http\Controllers\API\CheckoutController')->notification($item, 'Your Order is accepted and preparing', 'Tap to order more', ['status' => 'order', 'order_id' => $order->id]);
            }
            return redirect('/chef/orders/' . $order->id)->with('message', 'Order accepted by chef');
        } else
            return redirect()->back()->with('message', 'Order not found');
    }

    public function assignRider(Request $request)
    {
        $order = Order::find($request->order);
        $rider = Rider::find($request->rider);
        if ($order && $rider) {
            $new = new RiderOrder();
            $new->order_id = $order->id;
            $new->rider_id = $rider->id;
            $new->save();
            $order->order_status = 'Picked';
            $order->save();
            $customerObj = Customer::find($order->customer_id);
            $get = app('App\Http\Controllers\API\CheckoutController')->orderObj($order->id);
            $this->pusher->trigger('order-' . $get['orderId'], 'change-event', $get);
            if ($customerObj->notify == 1) {
                $devices = CustomerDevice::where('customer_id', $customerObj->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices as $item)
                    $notify = app('App\Http\Controllers\API\CheckoutController')->notification($item, 'Your Order is on the way', 'Tap to order more', ['status' => 'order', 'order_id' => $order->id]);
            }
            return redirect('/chef/orders/' . $order->id)->with('message', 'Rider assigned to order');
        } else
            return redirect()->back()->with('message', 'Rider/Order not found');
    }
}
