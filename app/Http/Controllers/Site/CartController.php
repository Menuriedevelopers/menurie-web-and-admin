<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\CartAddons;
use App\AddonList;
use App\Customer;
use App\Item;
use App\Cart;

class CartController extends Controller
{
    public function checkDbCart($id)
    {
        $addonsArr = [];
        $user = Customer::find($id);
        $cart = Cart::where('customer_id', $user->id)->get();
        $arr = [];
        foreach ($cart as $item) {
            $find = Item::find($item->item_id);
            $quantity = $item->quantity;
            $itemPrice = $find->price;
            $total = $quantity * $find->price;
            $addons = CartAddons::where('cart_id', $item->id)->get();
            foreach ($addons as $item1) {
                $find1 = AddonList::find($item1->addon_id);
                if ($find1) {
                    $total += $find1->price * $item1->quantity;
                    $addonsArr1 = [
                        'addonId' => $find1->id, 'addonName' => $find1->name, 'addonPrice' => $find1->price, 'addonTotal' => $find1->price * $item1->quantity, 'addonQuantity' => $item1->quantity
                    ];
                    $addonsArr[$find1->id] = $addonsArr1;
                }
            }
            $cart1 = session()->get('cart');
            if ($cart1) {
                $cart1[$item->item_id] = [
                    "id" => $find->id,
                    "name" => $find->item_name,
                    "chef" => $find->chef->first_name ?? 'Menurie Chef',
                    "quantity" => $quantity,
                    "price" => $find->price,
                    "photo" => $find->item_image,
                    "subTotal" => $quantity * $find->price,
                    "total" => $total,
                    "addons" => $addonsArr
                ];
            } else {
                $cart1[$item->item_id] = [
                    "id" => $find->id,
                    "name" => $find->item_name,
                    "chef" => $find->chef->first_name ?? 'Menurie Chef',
                    "quantity" => $quantity,
                    "price" => $find->price,
                    "photo" => $find->item_image,
                    "subTotal" => $quantity * $find->price,
                    "total" => $total,
                    "addons" => $addonsArr
                ];
            }
            session()->put('cart', $cart1);
        }
    }

    public function addToCart(Request $request)
    {
        $itemId = $request->item_id;
        $addons = $request->addon_id;
        $addonQuantity = 1;
        $quantity = $request->item_quantity;
        $find = Item::with('chef')->find($itemId);
        $itemPrice = $find->price;
        $addonsArr = [];
        $total = 0;
        $cartId = 0;
        if (Auth::guard('customer')->check()) {
            if ($addons)
                $check = Cart::whereHas('addons')->where('customer_id', Auth::guard('customer')->user()->id)->where('item_id', $find->id)->latest()->first();
            else
                $check = Cart::where('customer_id', Auth::guard('customer')->user()->id)->where('item_id', $find->id)->latest()->first();
            if ($check) {
                if ($addons) {
                    $addonsList = CartAddons::where('cart_id', $check->id)->latest()->pluck('addon_id')->toArray();
                    if (count($addonsList) != 0) {
                        if (array_diff($addonsList, $addons) == array_diff($addons, $addonsList)) {
                            for ($i = 0; $i < count($addons); $i++) {
                                $addon = CartAddons::where('cart_id', $check->id)->where('addon_id', $addons[$i])->first();
                                $addon->quantity = $addon->quantity + $quantity;
                                $addon->save();
                            }
                            $check->quantity = $quantity + $check->quantity;
                            $check->save();
                            $cartId = $check->id;
                        } else {
                            $save = new Cart;
                            $save->customer_id = Auth::guard('customer')->user()->id;
                            $save->item_id = $find->id;
                            $save->quantity = $quantity;
                            $save->price = $itemPrice;
                            $save->save();
                            $cartId = $save->id;
                            for ($i = 0; $i < count($addons); $i++) {
                                $find1 = AddonList::find($addons[$i]);
                                if ($find1) {
                                    if (Auth::guard('customer')->check()) {
                                        $cartAddon = new CartAddons;
                                        $cartAddon->cart_id = $cartId;
                                        $cartAddon->addon_id = $addons[$i];
                                        $cartAddon->quantity = $addonQuantity;
                                        $cartAddon->price = $find1->price;
                                        $cartAddon->save();
                                    }
                                    $total += $find1->price * $addonQuantity;
                                    $addonsArr1 = [
                                        'addonId' => $find1->id, 'addonName' => $find1->name, 'addonPrice' => $find1->price, 'addonTotal' => $find1->price * $addonQuantity, 'addonQuantity' => $addonQuantity
                                    ];
                                    $addonsArr[$find1->id] = $addonsArr1;
                                }
                            }
                        }
                    } else {
                        $save = new Cart;
                        $save->customer_id = Auth::guard('customer')->user()->id;
                        $save->item_id = $find->id;
                        $save->quantity = $quantity;
                        $save->price = $itemPrice;
                        $save->save();
                        $total = $quantity * $find->price;
                        $cartId = $save->id;
                        for ($i = 0; $i < count($addons); $i++) {
                            $find1 = AddonList::find($addons[$i]);
                            if ($find1) {
                                if (Auth::guard('customer')->check()) {
                                    $cartAddon = new CartAddons;
                                    $cartAddon->cart_id = $cartId;
                                    $cartAddon->addon_id = $addons[$i];
                                    $cartAddon->quantity = $addonQuantity;
                                    $cartAddon->price = $find1->price;
                                    $cartAddon->save();
                                }
                                $total += $find1->price * $addonQuantity;
                                $addonsArr1 = [
                                    'addonId' => $find1->id, 'addonName' => $find1->name, 'addonPrice' => $find1->price, 'addonTotal' => $find1->price * $addonQuantity, 'addonQuantity' => $addonQuantity
                                ];
                                $addonsArr[$find1->id] = $addonsArr1;
                            }
                        }
                    }
                } else {
                    $without = Cart::doesntHave('addons')->where('customer_id', Auth::guard('customer')->user()->id)->where('item_id', $find->id)->latest()->first();
                    if ($without) {
                        $without->quantity = $quantity + $without->quantity;
                        $without->save();
                    } else {
                        $save = new Cart;
                        $save->customer_id = Auth::guard('customer')->user()->id;
                        $save->item_id = $find->id;
                        $save->quantity = $quantity;
                        $save->price = $find->price;
                        $save->save();
                    }
                }
            } else {
                $save = new Cart;
                $save->customer_id = Auth::guard('customer')->user()->id;
                $save->item_id = $find->id;
                $save->quantity = $quantity;
                $save->price = $itemPrice;
                $save->save();
                $total = $quantity * $find->price;
                $cartId = $save->id;
                if ($addons) {
                    for ($i = 0; $i < count($addons); $i++) {
                        $find1 = AddonList::find($addons[$i]);
                        if ($find1) {
                            if (Auth::guard('customer')->check()) {
                                $cartAddon = new CartAddons;
                                $cartAddon->cart_id = $cartId;
                                $cartAddon->addon_id = $addons[$i];
                                $cartAddon->quantity = $addonQuantity;
                                $cartAddon->price = $find1->price;
                                $cartAddon->save();
                            }
                            $total += $find1->price * $addonQuantity;
                            $addonsArr1 = [
                                'addonId' => $find1->id, 'addonName' => $find1->name, 'addonPrice' => $find1->price, 'addonTotal' => $find1->price * $addonQuantity, 'addonQuantity' => $addonQuantity
                            ];
                            $addonsArr[$find1->id] = $addonsArr1;
                        }
                    }
                }
            }
        }
        if ($addons) {
            for ($i = 0; $i < count($addons); $i++) {
                $find1 = AddonList::find($addons[$i]);
                if ($find1) {
                    if (Auth::guard('customer')->check()) {
                        $cartAddon = new CartAddons;
                        $cartAddon->cart_id = $cartId;
                        $cartAddon->addon_id = $addons[$i];
                        $cartAddon->quantity = $addonQuantity;
                        $cartAddon->price = $find1->price;
                        $cartAddon->save();
                    }
                    $total += $find1->price * $addonQuantity;
                    $addonsArr1 = [
                        'addonId' => $find1->id, 'addonName' => $find1->name, 'addonPrice' => $find1->price, 'addonTotal' => $find1->price * $addonQuantity, 'addonQuantity' => $addonQuantity
                    ];
                    $addonsArr[$find1->id] = $addonsArr1;
                }
            }
        }
        $cart = session()->get('cart');
        $total += $quantity * $find->price;
        if (!$cart) {
            $chefName = 'Menurie Chef';
            if ($find->chef_id != 0)
                $chefName = $find->chef->first_name . ' ' . $find->chef->last_name;
            if ($addons) {
                sort($addons);
                $ids = '';
                foreach ($addons as $itemIds)
                    $ids .= $itemIds;
                $cart[$itemId . '-' . $ids] = [
                    "id" => $find->id,
                    "name" => $find->item_name,
                    "chef" => $chefName,
                    "quantity" => $quantity,
                    "price" => $find->price,
                    "photo" => $find->item_image,
                    "subTotal" => $quantity * $find->price,
                    "total" => $total,
                    "addons" => $addonsArr
                ];
                session()->put('cart', $cart);
            } else {
                $cart = [
                    $itemId => [
                        "id" => $find->id,
                        "name" => $find->item_name,
                        "chef" => $chefName,
                        "quantity" => $quantity,
                        "price" => $find->price,
                        "photo" => $find->item_image,
                        "subTotal" => $quantity * $find->price,
                        "total" => $total,
                        "addons" => $addonsArr
                    ]
                ];
                session()->put('cart', $cart);
            }
        } else {
            $chefName = 'Menurie Chef';
            if ($find->chef_id != 0)
                $chefName = $find->chef->first_name . ' ' . $find->chef->last_name;
            $oldQuantity = 0;
            if ($addons) {
                sort($addons);
                $ids = '';
                foreach ($addons as $itemIds)
                    $ids .= $itemIds;
                if (isset($cart[$itemId . '-' . $ids])) {
                    $oldQuantity = $cart[$itemId . '-' . $ids]['quantity'];
                    $cart[$itemId . '-' . $ids]['quantity'] = $oldQuantity + 1;
                    $cart[$itemId . '-' . $ids]['total'] = $oldQuantity * $cart[$itemId . '-' . $ids]['subTotal'] * $total;
                    foreach ($cart[$itemId . '-' . $ids]['addons'] as $item)
                        $item['addonQuantity'] += 1;
                    session()->put('cart', $cart);
                } else {
                    sort($addons);
                    $ids = '';
                    foreach ($addons as $itemIds)
                        $ids .= $itemIds;

                    $cart[$itemId . '-' . $ids] = [
                        "id" => $find->id,
                        "name" => $find->item_name,
                        "chef" => $chefName,
                        "quantity" => $quantity + $oldQuantity,
                        "price" => $find->price,
                        "photo" => $find->item_image,
                        "subTotal" => $quantity * $find->price,
                        "total" => $total,
                        "addons" => $addonsArr
                    ];
                    session()->put('cart', $cart);
                }
            } else {
                if (isset($cart[$itemId]))
                    $oldQuantity = $cart[$itemId]['quantity'];
                $cart[$itemId] = [
                    "id" => $find->id,
                    "name" => $find->item_name,
                    "chef" => $chefName,
                    "quantity" => $quantity + $oldQuantity,
                    "price" => $find->price,
                    "photo" => $find->item_image,
                    "subTotal" => $quantity * $find->price,
                    "total" => $total,
                    "addons" => $addonsArr
                ];
                session()->put('cart', $cart);
            }
            if (!isset($cart[$itemId]) && !$addons) {
                $cart[$itemId] = [
                    "id" => $find->id,
                    "name" => $find->item_name,
                    "chef" => $chefName,
                    "quantity" => $quantity + $oldQuantity,
                    "price" => $find->price,
                    "photo" => $find->item_image,
                    "subTotal" => $quantity * $find->price,
                    "total" => $total,
                    "addons" => $addonsArr
                ];
                session()->put('cart', $cart);
            }
        }
        $getCartCount = session()->get('cart');
        return response()->json(['status' => true, 'count' => count($getCartCount)], 200);
    }

    public function removeCartItem($item)
    {
        $cart = session()->get('cart');
        if (isset($cart[$item])) {
            unset($cart[$item]);
            session()->put('cart', $cart);
            if (Auth::guard('customer')->check())
                $find = Cart::where('customer_id', Auth::guard('customer')->user()->id)->where('item_id', $item)->delete();
            return response()->json(['status' => true], 200);
        } else
            return response()->json(['status' => true], 200);
    }

    public function removeAddonItem($item, $addon)
    {
        $cart = session()->get('cart');
        if (isset($cart[$item]['addons'][$addon])) {
            unset($cart[$item]['addons'][$addon]);
            session()->put('cart', $cart);
            if (Auth::guard('customer')->check()) {
                $find = Cart::where('customer_id', Auth::guard('customer')->user()->id)->where('item_id', $item)->first();
                if ($find)
                    $delete = CartAddons::where('cart_id', $find->id)->where('addon_id', $addon)->delete();
            }
            return response()->json(['status' => true], 200);
        }
        return response()->json(['status' => true], 200);
    }

    public function updateItemQuantity($item, $quantity)
    {
        $cart = session('cart');
        $total = 0;
        $prev = $cart[$item]['quantity'];
        $cart[$item]['quantity'] = $quantity;
        $price = $cart[$item]['price'] * $quantity;
        $cart[$item]['subTotal'] = $price;
        session()->put('cart', $cart);
        foreach ($cart as $item1)
            $total += $item1['subTotal'];

        if (isset($cart[$item]['addons'])) {
            foreach ($cart[$item]['addons'] as $item1)
                $total += $item1['addonTotal'];
        }
        if (Auth::guard('customer')->check()) {
            $find = Cart::where('customer_id', Auth::guard('customer')->user()->id)->where('item_id', $item)->first();
            $find->quantity = $quantity;
            $find->save();
        }
        return response()->json(['status' => true, 'sub' => $price, 'subTotal' => number_format($total, 2), 'taxTotal' => number_format($total * 0.089, 2), 'total' => number_format(($total * 0.089) + $total, 2)], 200);
    }

    public function updateAddonQuantity($item, $addon, $quantity)
    {
        $cart = session('cart');
        $cart[$item]['addons'][$addon]['addonQuantity'] = $quantity;
        $price = $cart[$item]['addons'][$addon]['addonPrice'];
        $sub = $cart[$item]["subTotal"];
        $total = $quantity * $price;
        $sub = $cart[$item]['addons'][$addon]['addonTotal'] = $quantity * $price;
        session()->put('cart', $cart);
        foreach ($cart as $item) {
            $total += $item['subTotal'];
        }
        if (Auth::guard('customer')->check()) {
            $find = Cart::where('customer_id', Auth::guard('customer')->user()->id)->where('item_id', $item)->first();
            if ($find) {
                $update = CartAddons::where('cart_id', $find->id)->where('addon_id', $addon)->first();
                $update->quantity = $quantity;
                $update->save();
            }
        }
        return response()->json(['status' => true, 'sub' => $sub, 'subTotal' => number_format($total, 2), 'taxTotal' => number_format($total * 0.089, 2), 'total' => number_format(($total * 0.089) + $total, 2)], 200);
    }

    public function emptyCart()
    {
    }
}
