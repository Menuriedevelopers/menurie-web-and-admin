<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Events\OrderStatusChanged;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Stripe;
use App\HotelRoomService;
use App\ShippingAddress;
use App\PaymentHistory;
use App\OrderDetail;
use App\OrderAddons;
use App\AddonList;
use App\Customer;
use App\Order;
use App\Cart;
use App\Chef;
use App\ChefDevice;
use App\Item;
use App\CustomerDevice;
use Pusher\Pusher;

class CheckoutController extends Controller
{
    public $pusher;

    public function __construct()
    {
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => false
        );
        $this->pusher = new Pusher(
            "68f9e094a21250f4ccdc",
            "674c8a47c4552b15ca15",
            "1282636",
            $options
        );
    }

    public function checkoutForm()
    {
        // if(Auth::guard('customer')->check())
        return view('site.checkout');
        // return redirect("/login?redirect=checkout");
    }

    public function store(Request $request)
    {
        $request->validate([
            'address' => 'required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'state' => 'required',
            'zipcode' => 'required',
            'delivery_type' => 'required'
        ]);
        $cart = session()->get('cart');
        if ($cart) {
            $total = 0;
            foreach ($cart as $item) {
                $total += $item['subTotal'];
                if ($item['addons']) {
                    foreach ($item['addons'] as $addon) {
                        $total += $addon['addonTotal'];
                    }
                }
            }
            DB::beginTransaction();
            if (Auth::guard('customer')->user())
                $customer = Auth::guard('customer')->user();
            else {
                $find = Customer::where('email', $request->email)->where('phone', $request->phone)->where('guest', 1)->first();
                if ($find) {
                    $customer = $find;
                    Auth::guard('customer')->login($customer);
                }
                $customer = new Customer;
                $customer->first_name = $request->input('first_name');
                $customer->last_name = $request->input('last_name');
                $customer->email = $request->input('email');
                $customer->phone = $request->input('phone');
                $customer->guest = 1;
                $customer->save();
                Auth::guard('customer')->login($customer);
            }
            $location = session()->get('location');
            $chefs = Chef::where('lat_long', '!=', '')->get();
            $dist = '';
            $distanceArr = array();
            foreach ($chefs as $item) {
                $arr = explode(',', $item->lat_long);
                if ($location['lat'] == $arr[0] && $location['lng'] == $arr[1])
                    array_push($distanceArr, array("distance" => 0.00, "chef" => $item->id));
                else {
                    $theta = $location['lng'] - $arr[1];
                    $dist = sin(deg2rad($location['lat'])) * sin(deg2rad($arr[0])) +  cos(deg2rad($location['lat'])) * cos(deg2rad($arr[0])) * cos(deg2rad($theta));
                    $dist = acos($dist);
                    $dist = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    $kilometers = ($miles * 1.609344);
                    $arr = array("distance" => round($kilometers, 2), "chef" => $item->id);
                    array_push($distanceArr, $arr);
                }
            }
            $min = min(array_column($distanceArr, 'distance'));
            $min = min($distanceArr);
            $chef = Chef::find($min['chef']);
            $order = new Order;
            $order->date = date('d F, Y');
            $order->time = date('H:i A');
            $order->lat = $location['lat'];
            $order->lng = $location['lng'];
            $order->customer_id = $customer->id;
            $order->chef_id = $chef->id;
            $order->sub_total = $total;
            $order->tax = ($total * 0.089);
            $order->delivery_charges = "0.00";
            $order->service_charges = "0.00";
            $order->total_price = ($total * 0.089) + $total;
            $order->payment_method = ($request->stripeToken) ? 'stripe' : 'COD';
            $order->order_status = 'Placed';
            $order->save();
            $shipping = new ShippingAddress;
            $shipping->order_id = $order->id;
            $shipping->customer_id = $customer->id;
            $shipping->delivery_to = $request->delivery_type;
            $shipping->address = $request->address;
            $shipping->instruction = $request->address;
            $shipping->delivery_note = $request->note ?? '';
            $shipping->save();
            $hotel = new HotelRoomService;
            if ($request->delivery_type == 'hotel') {
                $hotel->order_id = $order->id;
                $hotel->hotel_name = $request->hotelName;
                $hotel->person_name = $request->personName;
                $hotel->floor = $request->floor;
                $hotel->room_no = $request->roomNo;
                $hotel->delivery_time = $request->deliveryTime;
                $hotel->save();
            }
            foreach ($cart as $item) {
                $orderDetail = new OrderDetail;
                $orderDetail->order_id = $order->id;
                $orderDetail->item_id = $item['id'];
                $orderDetail->quantity = $item['quantity'];
                $orderDetail->price = $item['price'];
                $orderDetail->save();
                if ($item['addons']) {
                    foreach ($item['addons'] as $addon) {
                        $orderAddon = new OrderAddons;
                        $orderAddon->order_detail_id = $orderDetail->id;
                        $orderAddon->addon_id = $addon['addonId'];
                        $orderAddon->quantity = $addon['addonQuantity'];
                        $orderAddon->price = $addon['addonPrice'];
                        $orderAddon->save();
                    }
                }
            }
            $order1 = Order::with('chef', 'customer', 'detail')->find($order->id);
            event(new OrderStatusChanged($order1));
            $get = Customer::find($order1->customer_id);
            $find = Cart::where('customer_id', $get->id)->delete();
            $getOrder = app('App\Http\Controllers\API\CheckoutController')->orderObj($order->id);
            $this->pusher->trigger('order-' . $getOrder['orderId'], 'change-event', $getOrder);
            if ($get->notify == 1) {
                $devices = CustomerDevice::where('customer_id', $get->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices as $item)
                    $notify = app('App\Http\Controllers\API\CheckoutController')->notification($item, 'Your Order is Confirmed & Preparing', 'Tap to see the detail of your order', ['status' => 'order', 'order_id' => $order->id]);
            }
            if ($chef->notify == 1) {
                $devices = ChefDevice::where('chef_id', $chef->id)->distinct('fcm_token')->pluck('fcm_token');
                foreach ($devices as $item)
                    $notify = app('App\Http\Controllers\API\CheckoutController')->notification($item, 'New order added', 'Tap to see the detail of the order', ['status' => 'order', 'order_id' => $order->id]);
            }
            DB::commit();
            session()->forget('cart');
            if ($request->has('stripeToken')) {
                $stripe = Stripe::make(env('STRIPE_SECRET_KEY'), '2020-03-02');
                $charge = $stripe->charges()->create([
                    'amount'   => $total,
                    'currency' => 'USD',
                    'source' => $request->stripeToken,
                    'receipt_email' => $customer->email,
                ]);
                if ($charge['status'] == 'succeeded') {
                    $order = Order::with('detail')->find($order->id);
                    $chargeId = $charge['id'];
                    return redirect()->route('customer.order', $order->id);
                } else
                    return redirect()->back()->with('message', 'An error occured while making transaction');
            }
            $order = Order::with('detail')->find($order->id);
            return redirect()->route('customer.order', $order->id);
        }
    }
}
