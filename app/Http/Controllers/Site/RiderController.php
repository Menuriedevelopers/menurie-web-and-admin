<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Rider;

class RiderController extends Controller
{
    public function __construct()
    {
        // $this->middleware('Rider');
        // if(!Auth::guard('rider'))
        //     return redirect()->route('rider.login');
    }

    public function index()
    {
        if(!Auth::guard('rider'))
            return redirect()->route('rider.login');
        return view('site.rider.riderWelcome');
    }

}
