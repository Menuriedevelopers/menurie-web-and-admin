<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function rider()
    {
        return $this->belongsTo('App\Rider');
    }

    public function chef()
    {
        return $this->belongsTo('App\Chef');
    }
}
