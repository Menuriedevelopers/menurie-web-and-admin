<?php

namespace App\Listeners;

use App\Events\OrderUpdateChef;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OrderUpdateChefListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderUpdateChef  $event
     * @return void
     */
    public function handle(OrderUpdateChef $event)
    {
        //
    }
}
