<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartAddons extends Model
{
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function addon()
    {
        return $this->belongsTo('App\AddonList');
    }
}
