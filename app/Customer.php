<?php

namespace App;

use Illuminate\Foundation\Auth\User as Model;

class Customer extends Model
{
    protected $guarded = ['id'];
    protected $hidden = [
        'stripe_id', 'guest', 'password', 'created_at', 'updated_at',
    ];
}
