<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function orderAddons()
    {
        return $this->hasMany('App\OrderAddons');
    }
}
