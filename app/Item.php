<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function chef()
    {
        return $this->belongsTo('App\Chef');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function addons()
    {
        return $this->hasMany('App\AddonList');
    }
}
