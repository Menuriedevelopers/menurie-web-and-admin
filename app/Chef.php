<?php

namespace App;

use Illuminate\Foundation\Auth\User as Model;

class Chef extends Model
{
    protected $guarded = ['id'];
    protected $hidden = [
        'password','created_at', 'updated_at',
    ];
}
