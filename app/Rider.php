<?php

namespace App;

use Illuminate\Foundation\Auth\User as Model;

class Rider extends Model
{
    protected $hidden = [
        'password','created_at', 'updated_at',
    ];
}
