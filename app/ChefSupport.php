<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChefSupport extends Model
{

    public function chef()
    {
        return $this->belongsTo('App\Chef');
    }
}
