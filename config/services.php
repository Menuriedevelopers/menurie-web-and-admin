<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id' => '385113356029345',
        'client_secret' => '2413aa5cad88ca112808718fb0977ddf',
        'redirect' => 'https://menurie.com/callback/facebook',
      ],

    'google' => [
        'client_id' => '151715500347-lsqu6tu08rnigqveae35rarmeqipc6h5.apps.googleusercontent.com',
        'client_secret' => 'HmKVmUV1eYnmrQPQuh_QGRfr',
        'redirect' => 'https://www.menurie.com/auth/google/callback',
      ],
];
