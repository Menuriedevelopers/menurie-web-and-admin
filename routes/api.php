<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

Route::post('auth/apple/callback', 'Auth\CustomerAuthController@handleAppleCallback');
Route::get('users', 'API\ChefController@users');

Route::post('chef/auth', 'API\ChefController@register');
Route::post('chef/auth/verify', 'API\ChefController@verifySignup');
Route::post('chef/verify', 'API\ChefController@verify');
Route::post('chef/logout', 'API\ChefController@logout');
Route::get('chef/delete/{id}', 'API\ChefController@delete');
Route::post('chef/email/verify', 'API\ChefController@newEmailVerify');
Route::post('chef/forgotPassword', 'API\ChefController@forgotPassword');
Route::post('chef/forgotPassword/verify', 'API\ChefController@verifyForgotPasswordRequest');
Route::post('chef/resetPassword', 'API\ChefController@resetPassword');
Route::get('chef/profile/{id}', 'API\ChefController@profile');
Route::get('chef/notify/{id}/{status}', 'API\ChefController@notifyMe');
Route::post('chef/edit', 'API\ChefController@editProfile');
Route::get('chef/times/{id}', 'API\ChefController@times');
// Chef add category
Route::get('chef/restaurantList/', 'Admin\RestaurantController@showList');
Route::post('chef/addCategory', 'API\ChefController@addCategory');
Route::get('chef/categoryList', 'API\ChefController@categories');
Route::post('chef/category', 'API\ChefController@categoryActiveStatus');
Route::post('chef/editCategory', 'API\ChefController@editCategory');
Route::post('chef/deleteCategory', 'API\ChefController@deleteCategory');
// Chef add items

Route::get('chef/all/items/{id}', 'API\ChefController@allItems');
Route::get('chef/all/items/{id}/{category}', 'API\ChefController@allItems');

Route::post('chef/addItem', 'API\ChefController@addItem');
Route::get('chef/itemList', 'API\ChefController@items');
Route::post('chef/item', 'API\ChefController@itemActiveStatus');
Route::post('chef/editItem', 'API\ChefController@editItem');
Route::post('chef/deleteItem', 'API\ChefController@deleteItem');
// Chef special items
Route::post('chef/addSpecialItem', 'API\ChefController@addSpecialItem');
Route::get('chef/SpecialItemList/{chefId}', 'API\ChefController@SpecialItems');
Route::post('chef/editSpecialItem', 'API\ChefController@editSpecialItem');
// Chef orders work
Route::get('chef/orders/status/{status}/{chef}', 'API\OrderController@orderByStatus');
Route::get('chef/orders/detail/{order}', 'API\OrderController@orderDetail');
Route::get('chef/orders/{type}/{chef}', 'API\OrderController@ordersList');
Route::get('chef/orders/{chef}', 'API\OrderController@orderCounts');
Route::get('chef/order/update/status/{id}/{chef}', 'API\OrderController@updateOrderStatus');
// Item Addon
Route::post('chef/item/addOn', 'API\ChefController@addOn');
Route::post('chef/item/addOn/{id}', 'API\ChefController@editAddOn');
Route::get('chef/item/addOn/delete/{id}', 'API\ChefController@addOnDelete');

Route::get('chef/online/{id}', 'API\RiderController@onlineRiders');
Route::get('chef/assign/order/{id}/{rider}', 'API\RiderController@acceptRequest');
Route::get('chef/assigned/order/{id}', 'API\OrderController@orderAssignedRider');
Route::post('chef/support', 'API\ChefController@support');
Route::post('chef/request', 'API\ChefController@request');

Route::post('chef/ticket', 'API\ChefController@addTicket');
Route::get('chef/tickets/{id}/{type}', 'API\ChefController@tickets');
Route::get('chef/ticket/detail/{id}', 'API\ChefController@ticket');
Route::get('chef/ticket/close/{id}', 'API\ChefController@closeTicket');

// Rider login register and reset password
Route::get('rider/status/{id}/{status}', 'API\RiderController@online');
Route::post('rider/auth', 'API\RiderController@register');
Route::post('rider/verify', 'API\RiderController@verify');
Route::post('rider/auth/verify', 'API\RiderController@verifySignup');
Route::post('rider/email/verify', 'API\RiderController@newEmailVerify');
Route::post('rider/logout', 'API\RiderController@logout');
Route::get('rider/delete/{id}', 'API\RiderController@delete');
Route::get('rider/profile/{id}', 'API\RiderController@profile');
Route::get('rider/notify/{id}/{status}', 'API\RiderController@notifyMe');
Route::post('rider/edit', 'API\RiderController@editProfile');
Route::post('rider/forgotPassword', 'API\RiderController@forgotPassword');
Route::post('rider/forgotPassword/verify', 'API\RiderController@verifyForgotPasswordRequest');
Route::post('rider/resetPassword', 'API\RiderController@resetPassword');
Route::post('rider/documents', 'API\RiderController@uploadDocuments');
Route::get('rider/orders/{status}/{rider}', 'API\RiderController@orders');
Route::get('rider/order/detail/{id}', 'API\RiderController@orderDetail');
Route::get('rider/order/complete/{id}', 'API\RiderController@completeOrder');
Route::get('rider/order/current/{id}', 'API\RiderController@currentOrder');
Route::post('rider/support', 'API\RiderController@support');

Route::post('rider/ticket', 'API\RiderController@addTicket');
Route::get('rider/tickets/{id}/{type}', 'API\RiderController@tickets');
Route::get('rider/ticket/detail/{id}', 'API\RiderController@ticket');
Route::get('rider/ticket/close/{id}', 'API\RiderController@closeTicket');

// Customer login register and rest password
Route::post('customer/support', 'API\CustomerController@support');
Route::post('customer/auth', 'API\CustomerController@register');
Route::post('customer/auth/verify', 'API\CustomerController@verifySignup');
Route::post('customer/auth/validate', 'API\CustomerController@verifySignup1');
Route::post('customer/verify', 'API\CustomerController@verify');
Route::post('customer/email/verify', 'API\CustomerController@newEmailVerify');
Route::post('customer/logout', 'API\CustomerController@logout');
Route::get('customer/profile/{id}', 'API\CustomerController@profile');
Route::get('customer/notify/{id}/{status}', 'API\CustomerController@notifyMe');
Route::get('customer/delete/{id}', 'API\CustomerController@delete');
Route::get('customer/address/{id}', 'API\CustomerController@addresses');
Route::get('customer/address/delete/{id}', 'API\CustomerController@deleteAddress');
Route::post('customer/edit', 'API\CustomerController@editProfile');
Route::post('customer/forgotPassword', 'API\CustomerController@forgotPassword');
Route::post('customer/forgotPassword/verify', 'API\CustomerController@verifyForgotPasswordRequest');
Route::post('customer/resetPassword', 'API\CustomerController@resetPassword');
Route::post('customer/save/address', 'API\CustomerController@addAddress');

Route::post('customer/ticket', 'API\CustomerController@addTicket');
Route::get('customer/tickets/{id}/{type}', 'API\CustomerController@tickets');
Route::get('customer/ticket/detail/{id}', 'API\CustomerController@ticket');
Route::get('customer/ticket/close/{id}', 'API\CustomerController@closeTicket');


Route::get('customer/home/{userId}', 'API\CustomerController@home');
Route::get('customer/home/{userId}/{type}', 'API\CustomerController@home');
Route::get('customer/time/itemList/{user}/{category}/{chefId}', 'API\CustomerController@itemsSeeAll');
Route::get('customer/all/itemList/{user}/{category}/{chefId}', 'API\CustomerController@allItems');
Route::get('customer/all/specialItemList/{user}/{id}', 'API\CustomerController@allSpecialItems');
Route::get('customer/chefList/{restaurantId}', 'API\CustomerController@chefs');

Route::get('customer/orders/status/{status}/{customer}', 'API\CustomerController@orderByStatus');

// Cart CRUDS
Route::get('customer/mycart/{customer}', 'API\CartController@index');
Route::post('customer/mycart/add/{customer}', 'API\CartController@store');
Route::post('customer/mycart/update/{cartId}', 'API\CartController@update');
Route::get('customer/mycart/delete/{cartId}', 'API\CartController@softDelete');
Route::get('customer/mycart/delete/all/{customerId}', 'API\CartController@deleteAll');

// Checkout CRUD
Route::get('customer/orders/{customer}', 'API\CheckoutController@ordersList');
Route::get('customer/orders/detail/{order}', 'API\CheckoutController@orderDetail');
Route::post('customer/orders/create/{customer}', 'API\CheckoutController@createOrder');
Route::get('customer/orders/delete/{orderId}', 'API\CheckoutController@softDelete');

Route::post('customer/payment/card', 'API\CheckoutController@stripeTokenGenerate');
Route::get('customer/payment/cards/{id}', 'API\CheckoutController@myCards');
Route::get('customer/payment/card/delete/{id}', 'API\CheckoutController@deleteCard');


Route::get('customer/favorite/{id}/{userId}', 'API\CustomerController@favorite');
Route::get('customer/favorited/{loginId}', 'API\CustomerController@favoritedUsers');

Route::post('customer/survey', 'API\CustomerController@saveSurvey');
Route::get('customer/dismiss/{id}', 'API\CustomerController@dismissSurvey');
Route::get('customer/counts/{id}', 'API\CustomerController@counts');
Route::get('customer/all/{userId}/{chefId}', 'API\CustomerController@all');
Route::get('customer/item/{id}/{userId}', 'API\CustomerController@detail');
Route::post('customer/items/filter/{userId}', 'API\CustomerController@filterItem');

Route::post('support/message', 'API\ChatController@support_message');
Route::post('messaging', 'API\ChatController@send_message');
Route::post('all/messages', 'API\ChatController@listMessages');
Route::get('message/read/{id}/{group}', 'API\ChatController@messageRead');
