<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('clear-cache', function () {
    $exitCode = Artisan::call('config:clear');
    $exitCode1 = Artisan::call('cache:clear');
    $exitCode2 = Artisan::call('route:clear');
    $exitCode3 = Artisan::call('view:clear');
    return redirect('/');
});

Route::prefix('admin')->group(function () {
    Auth::routes(['register' => false, 'reset' => false]);
});



Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::view('chat', 'admin.chat')->name('chat');
    Route::get('chat/get_messages/{user_id}/{limit?}/{offset?}', 'Admin\ChatController@get_messages');
    Route::post('chat/auth', 'Admin\ChatController@auth');
    Route::post('chat/send_message', 'Admin\ChatController@send_message');
    Route::get('/home', 'Admin\IndexController@index')->name('indexPage');
    Route::get('/overall/{type}', 'Admin\IndexController@overall')->name('overall');
    Route::get('/overall/{type}/{chef}', 'Admin\IndexController@overall');
    Route::view('settings', 'admin.settings')->name('settings');
    Route::post('save/data', 'Admin\IndexController@saveData')->name('saveData');
    Route::post('update/password', 'Admin\IndexController@updatePassword')->name('updatePassword');
    Route::view('addRestaurant', 'admin.restaurant.addRestaurant')->name('addRestaurantPage');
    Route::post('addRestaurant', 'Admin\RestaurantController@saveRestaurant')->name('saveRestaurant');
    Route::get('restaurants', 'Admin\RestaurantController@index')->name('restaurantsListPage');
    Route::get('delete/restaurant/{id}', 'Admin\RestaurantController@softDelete')->name('deleteRestaurant');
    // Chef side
    Route::get('chefs/tickets/{type}', 'Admin\ChefController@tickets')->name('chefTickets');
    Route::get('chefs/ticket/conversation/{id}', 'Admin\ChefController@ticket')->name('chefTicket');
    Route::get('chefs', 'Admin\ChefController@index')->name('chefsListPage');
    Route::get('chef/detail/{chef}', 'Admin\ChefController@show')->name('chefPage');
    Route::get('verify/chef/{id}', 'Admin\ChefController@verify')->name('verifyChef');
    Route::get('delete/chef/{id}', 'Admin\ChefController@softDelete')->name('deleteChef');
    Route::get('update/range/chef/{id}', 'Admin\ChefController@updateRange')->name('updateChefRange');
    // Customer side
    Route::get('customers/tickets/{type}', 'Admin\CustomerController@tickets')->name('customerTickets');
    Route::get('customers/ticket/conversation/{id}', 'Admin\CustomerController@ticket')->name('customerTicket');
    Route::get('customers', 'Admin\CustomerController@index')->name('customersListPage');
    Route::get('customers/{id}', 'Admin\CustomerController@show')->name('customersDetailPage');
    Route::get('delete/customer/{id}', 'Admin\CustomerController@softDelete')->name('deleteCustomer');
    // Order Side
    Route::get('daily/orders', 'Admin\OrderController@index')->name('ordersListPage');
    Route::get('orders/{id}', 'Admin\OrderController@show')->name('orderDetail');
    Route::get('orders/chefs/{restaurant}', 'Admin\OrderController@getChefs')->name('getChefs');
    Route::get('delete/order/{id}', 'Admin\OrderController@softDelete')->name('deleteOrder');
    Route::post('assign/order/{id}', 'Admin\OrderController@assignOrder')->name('admin.chef.assign');

    Route::get('sales', 'Admin\IndexController@sales')->name('sales');
    Route::get('top/sales/dishes', 'Admin\IndexController@topSales')->name('topSales');

    Route::get('special', 'Site\ChefController@special')->name('special');
    Route::get('special/pending', 'Site\ChefController@unapprovedSpecial')->name('unapprovedSpecial');
    Route::get('items', 'Site\ChefController@items')->name('items');
    Route::get('item/{id}', 'Site\ChefController@item')->name('item');
    Route::get('item', 'Site\ChefController@addItem')->name('addItem');
    Route::post('item', 'Site\ChefController@storeItem')->name('storeItem');
    Route::get('item/{id}/edit', 'Site\ChefController@edit')->name('editItem');
    Route::post('item/{id}/update', 'Site\ChefController@update')->name('updateItem');
    Route::get('delete/{id}', 'Site\ChefController@softDelete')->name('deleteItem');

    Route::get('categories', 'Site\ChefController@categories')->name('categories');
    Route::get('category/{id}', 'Site\ChefController@category')->name('category');
    Route::get('category', 'Site\ChefController@addCategory')->name('addCategory');
    Route::post('category', 'Site\ChefController@storeCategory')->name('storeCategory');
    Route::get('category/{id}/edit', 'Site\ChefController@editCategory')->name('editCategory');
    Route::post('category/{id}/update', 'Site\ChefController@updateCategory')->name('updateCategory');
    Route::get('delete/category/{id}', 'Site\ChefController@deleteCategory')->name('deleteCategory');


    Route::get('chef/support', 'Admin\IndexController@chefSupports')->name('chefSupports');
    Route::get('customer/support', 'Admin\IndexController@customerSupports')->name('customerSupports');
    Route::get('rider/support', 'Admin\IndexController@riderSupports')->name('riderSupports');
    Route::get('chef/reply/{person}/{id}', 'Admin\IndexController@reply')->name('chefReply');
    Route::get('customer/reply/{person}/{id}', 'Admin\IndexController@reply')->name('customerReply');
    Route::get('rider/reply/{person}/{id}', 'Admin\IndexController@reply')->name('riderReply');
    Route::post('send/reply', 'Admin\IndexController@sendReply')->name('sendReply');

    Route::get('chef/requests', 'Admin\IndexController@chefRequests')->name('chefRequests');
    Route::get('chef/request/approve/{id}', 'Admin\IndexController@chefRequestApprove')->name('chefRequestApprove');
    Route::get('special/approve/{id}', 'Admin\IndexController@approveSpecial');
    // Rider side
    Route::get('riders/tickets/{type}', 'Admin\RiderController@tickets')->name('riderTickets');
    Route::get('riders/ticket/conversation/{id}', 'Admin\RiderController@ticket')->name('riderTicket');
    Route::get('riders', 'Admin\RiderController@index')->name('ridersListPage');
    Route::get('rider/{rider}', 'Admin\RiderController@show')->name('riderPage');
    Route::get('verify/rider/{id}', 'Admin\RiderController@verify')->name('verifyRider');
    Route::get('delete/rider/{id}', 'Admin\RiderController@softDelete')->name('deleteRider');
});
// Chef works
Route::prefix('chef')->as('chef.')
    ->group(function () {
        Route::get('register', 'Auth\ChefAuthController@showRegisterForm')->name('register');
        Route::post('register', 'Auth\ChefAuthController@register')->name('register');
        Route::get('login', 'Auth\ChefAuthController@showLoginForm')->name('login');
        Route::post('login', 'Auth\ChefAuthController@login')->name('login');
        Route::middleware('auth:chef')->group(function () {
            Route::get('logout', 'Auth\ChefAuthController@logout')->name('logout');
            Route::get('settings/{id}', 'Site\ChefController@settingsForm')->name('settings');
            Route::post('settings/{id}', 'Site\ChefController@setting');
            Route::get('/', 'Site\ChefController@index')->name('index');
            Route::get('orders', 'Site\ChefController@orders')->name('myOrders');
            Route::get('orders/{id}', 'Site\ChefController@showOrder')->name('orderDetail');
            Route::get('order/accept/{id}', 'Site\ChefController@acceptOrder')->name('acceptOrder');
        });
        // Route::get('order/decline/{id}', 'Site\ChefController@showOrder')->name('orderDetail');
        /*Route::post('orders/assign', 'Site\ChefController@assignRider')->name('assignRider');
        Route::get('items', 'Site\ChefController@items')->name('items');
        Route::get('item/{id}', 'Site\ChefController@item')->name('item');
        Route::get('item', 'Site\ChefController@addItem')->name('addItem');
        Route::post('item', 'Site\ChefController@storeItem')->name('storeItem');
        Route::get('item/{id}/edit', 'Site\ChefController@edit')->name('editItem');
        Route::post('item/{id}/update', 'Site\ChefController@update')->name('updateItem');
        Route::get('delete/{id}', 'Site\ChefController@softDelete')->name('deleteItem');*/
    });
// Customer Logins
Route::get('register', 'Auth\CustomerAuthController@showRegisterForm')->name('customerRegister');
Route::post('register', 'Auth\CustomerAuthController@register')->name('customer.register');
Route::get('login', 'Auth\CustomerAuthController@showLoginForm')->name('customerLogin');
Route::post('login', 'Auth\CustomerAuthController@login')->name('customer.login');
Route::get('logout', 'Auth\CustomerAuthController@logout')->name('customer.logout');
Route::get('auth/facebook', 'Auth\CustomerAuthController@redirectToFacebook');
Route::get('callback/facebook', 'Auth\CustomerAuthController@handleFacebookCallback');
Route::get('auth/google', 'Auth\CustomerAuthController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\CustomerAuthController@handleGoogleCallback');

Route::get('/', 'Site\IndexController@index')->name('index');
Route::get('/autocomplete', 'Site\IndexController@autocomplete')->name('autocomplete');
Route::get('/search-result/{name}', 'Site\IndexController@search_result');
Route::post('/searching', 'Site\IndexController@searching_result')->name('searching');
Route::get('item/{id}', 'Site\IndexController@itemDetail');
Route::get('chef/{id}', 'Site\IndexController@chefDetail')->name('breakFast');
Route::get('checkout', 'Site\CheckoutController@checkoutForm')->name('checkout');
Route::post('stripe-payment', 'Site\CheckoutController@store')->name('stripe.store');
Route::get('foods', 'Site\IndexController@list')->name('foodDetail');
Route::get('foods/{type}', 'Site\IndexController@list');
Route::get('location/{place}/{lat}/{lng}', 'Site\IndexController@saveLocation');
Route::view('forgot', 'site.forgetPage')->name('customer.forget');
Route::post('forgot', 'Site\IndexController@forgot')->name('forget');
Route::post('forgot/verify', 'Site\IndexController@forgotVerify')->name('customer.forgotVerify');
Route::post('reset', 'Site\IndexController@reset')->name('reset');
Route::get('contact', function () {
    return view('site.contact-us');
})->name('contact');
Route::get('about', function () {
    return view('site.about');
})->name('about');
Route::get('privacy', function () {
    return view('site.privacy');
})->name('privacy');

Route::get('mobileapps/terms', function () {
    return view('site.mobileapps/terms');
})->name('terms');


Route::get('mobileapps/privacy', function () {
    return view('site.mobileapps/privacy');
})->name('mobileapps/privacy');

Route::get('term', function () {
    return view('site.term');
})->name('term');

Route::view('rider', 'site.riderId')->name('riderId');
Route::view('rider/otp', 'site.rider.riderOtp')->name('riderOtp');
Route::get('user/profile', 'Site\IndexController@user')->name('userProfile');

Route::get('user-orders', 'Site\IndexController@user_orders')->name('user-orders');

Route::post('change-password', 'ChangePasswordController@store')->name('change.password');

Route::post('user/edit-profile', 'Site\IndexController@edit_profile')->name('edit-profile');

Route::post('user/edit-image', 'Site\IndexController@edit_image')->name('edit-image');

Route::post('user/edit-name', 'Site\IndexController@edit_name')->name('edit-name');

Route::post('user/edit-email', 'Site\IndexController@edit_email')->name('edit-email');

Route::post('user/edit-phone', 'Site\IndexController@edit_phone')->name('edit-phone');


Route::get('order/{id}', 'Site\IndexController@order')->name('customer.order');

Route::view('faqs', 'site.faq')->name('faq');
Route::view('cart', 'site.cart')->name('cart');
Route::view('cartModal', 'site.modals.cartModal');
Route::post('addToCart', 'Site\CartController@addToCart')->name('customer.addToCart');
Route::get('change/item/quantity/{item}/{quantity}', 'Site\CartController@updateItemQuantity');
Route::get('change/addon/quantity/{item}/{addon}/{quantity}', 'Site\CartController@updateAddonQuantity');
Route::get('remove/item/{item}', 'Site\CartController@removeCartItem');
Route::get('remove/item/addon/{item}/{addon}', 'Site\CartController@removeAddonItem');


Route::prefix('rider')->as('rider.')
    ->group(function () {
        Route::get('register', 'Auth\RiderAuthController@showRegisterForm')->name('register');
        Route::post('register', 'Auth\RiderAuthController@register')->name('register');
        Route::get('login', 'Auth\RiderAuthController@showLoginForm')->name('login');
        Route::post('login', 'Auth\RiderAuthController@login')->name('login');
        Route::get('logout', 'Auth\RiderAuthController@logout')->name('logout');
        Route::get('welcome', 'Site\RiderController@index')->name('welcome');
    });
