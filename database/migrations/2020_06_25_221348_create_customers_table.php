<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('stripe_id')->default('');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('image')->default('');
            $table->string('email');
            $table->string('password')->default('');
            $table->string('phone', 100);
            $table->string('city')->default('');
            $table->string('area')->default('');
            $table->string('post_code')->default('');
            $table->string('platform', 50)->default('');
            $table->string('fcm_token')->default('');
            $table->boolean('guest')->default(0);
            $table->boolean('verified')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
