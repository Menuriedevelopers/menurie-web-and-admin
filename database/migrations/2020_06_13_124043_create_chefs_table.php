<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chefs', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('image')->default('');
            $table->string('email');
            $table->string('password');
            $table->string('phone',100);
            $table->string('address')->default('');
            $table->string('city')->default('');
            $table->string('lat_long')->default('');
            $table->string('area')->default('');
            $table->string('post_code')->default('');
            $table->string('platform',50)->default('');
            $table->boolean('verified')->default(0);
            $table->boolean('approved')->default(0);
            $table->boolean('online')->default(0);
            $table->string('cuisines')->default('');
            $table->string('fcm_token')->default('');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chefs');
    }
}
