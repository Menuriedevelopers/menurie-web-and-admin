<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riders', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('image')->default('');
            $table->string('email');
            $table->string('password');
            $table->string('phone',100);
            $table->string('city')->default('');
            $table->string('area')->default('');
            $table->string('post_code')->default('');
            $table->string('platform',50)->default('');
            $table->string('id_front')->default('');
            $table->string('id_back')->default('');
            $table->string('vehicle_no')->default('');
            $table->boolean('verified')->default(0);
            $table->boolean('approved')->default(0);
            $table->boolean('online')->default(0);
            $table->string('fcm_token')->default('');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riders');
    }
}
