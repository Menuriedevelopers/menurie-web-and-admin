<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddonListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addon_lists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('item_id');
            $table->string('addon_name');
            $table->integer('price')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        // Schema::table('addon_lists', function (Blueprint $table) {
        //     $table->foreign('item_id')->references('id')
        //         ->on('addons')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addon_lists');
    }
}
