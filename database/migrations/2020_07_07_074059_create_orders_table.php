<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('chef_id')->default(0);
            $table->decimal('sub_total', 9, 2);
            $table->decimal('tax', 9, 2);
            $table->decimal('delivery_charges', 9, 2);
            $table->decimal('service_charges', 9, 2);
            $table->decimal('total_price', 9, 2);
            $table->string('promo_code')->nullable();
            $table->string('payment_method');
            $table->string('order_status');
            $table->string('date');
            $table->string('time');
            $table->string('brand')->default('');
            $table->string('last4')->default('');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
