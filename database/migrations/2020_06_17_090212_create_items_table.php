<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('chef_id');
            $table->unsignedBigInteger('special')->default(0);
            $table->unsignedBigInteger('category_id');            
            $table->string('item_name');            
            $table->text('description');
            $table->integer('price')->default(0);
            $table->string('item_image');
            $table->boolean('is_active')->default(1);
            $table->integer('count');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('items', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')
                ->on('categories')->onDelete('cascade');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->foreign('chef_id')->references('id')
                ->on('chefs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
