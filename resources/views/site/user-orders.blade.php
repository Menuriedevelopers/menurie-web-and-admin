@extends('site.layout.base')
@section('title','User')
@section('styles')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('site/asset/styles/userProfile.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
@endsection
@section('content')
<div class="container" style="margin-top: 100px">
    <div class="row">
        <div class="col-12 ">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Pending</a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Delivered</a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Cancelled</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent" style="background-color: #F5F5F5;">
              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="container py-3 mb-2">
                  <h4>Pending Orders</h4>
                    @foreach($pendingOrders as $order)
                        @foreach($order->detail as $item)
                            @php $itemObj = App\Item::find($item->item_id); @endphp
                            <div style="background-color: white;border-radius: 10px" class="mt-3">
                                <div class="row px-3 py-3">
                                    <div class="col-sm-10">
                                        <p>#M-{{ $order->id }}</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <p>{{ $order->date }}</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>x{{$item->quantity}}</p>
                                    </div>
                                    <div class="col-sm-4">
                                    <p>{{$itemObj->item_name}}</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>{{'$'.number_format($itemObj->price, 2)}}</p>
                                    </div>
                                    <div class="col-sm-10">
                                        <p>Total Price: {{ '$'.number_format($order->total_price,2) }}</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn" style="background-color: #8D5440;color: white;">{{ $order->order_status }}</button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                  </div>
              </div>
              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  <div class="container py-3 mb-2">
                    <h4>Delivered Orders</h4>
                      @foreach($completedOrders as $order)
                        @foreach($order->detail as $item)
                            @php $itemObj = App\Item::find($item->item_id); @endphp
                            <div style="background-color: white;border-radius: 10px" class="mt-3">
                                <div class="row px-3 py-3">
                                    <div class="col-sm-10">
                                        <p>#M-{{ $order->id }}</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <p>{{ $order->date }}</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>x{{$item->quantity}}</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>{{$itemObj->item_name}}</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>{{'$'.number_format($itemObj->price, 2)}}</p>
                                    </div>
                                    <div class="col-sm-10">
                                        <p>Total Price: {{ '$'.number_format($order->total_price,2) }}</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn" style="background-color: #8D5440;color: white;">{{ $order->order_status }}</button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                      @endforeach
                    </div>
              </div>
              <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <div class="container py-3 mb-2">
                    <h4>Cancelled Orders</h4>
                      @foreach($cancelledOrders as $order)
                        @foreach($order->detail as $item)
                            @php $itemObj = App\Item::find($item->item_id); @endphp
                            <div style="background-color: white;border-radius: 10px" class="mt-3">
                                <div class="row px-3 py-3">
                                    <div class="col-sm-10">
                                        <p>#M-{{ $order->id }}</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <p>{{ $order->date }}</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>x{{$item->quantity}}</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>{{$itemObj->item_name}}</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>{{'$'.number_format($itemObj->price, 2)}}</p>
                                    </div>
                                    <div class="col-sm-10">
                                        <p>Total Price: {{ '$'.number_format($order->total_price,2) }}</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn" style="background-color: #8D5440;color: white;">{{ $order->order_status }}</button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                      @endforeach
                    </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function () {
        $("#current1").addClass("current_active");
        //         setTim4eout(function(){// wait for 5 secs(2)
        $(".nav-link").css({
            'color': 'lightslategray'
        });
        $(".active").css({
            'color': '#843232'
        }); // then reload the page.(3)
        //   }, 500);
        // $("#my-tab3").css({ 'color': 'lightslategray' });
        // $("#my-tab4").css({ 'color': 'lightslategray' });

    });

    function greet() {
        $(".nav-link").css({
            'color': 'lightslategray'
        });
        $(".active").css({
            'color': '#843232'
        });
    }
    setTimeout(greet, 100);
    $("#my-tab1").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current1").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current4").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab2").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current2").addClass("current_active");
        $("#current1").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current4").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab3").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current3").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current1").removeClass("current_active");
        $("#current4").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab4").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current4").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current1").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });

</script>
@endsection
