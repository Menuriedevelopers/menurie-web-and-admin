@extends('site.layout.base')
@section('styles')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
@endsection
@section('title','Food')
@section('content')
<!--<div class="second-navbar">
    <div class="container">
        <div class="row row-spacing">
            <div class="col-lg-4 col-md-6 col-sm-12">
            </div>
            <div>
                <div class="second-navbar-img">
                  <img class="app-stores" width="200" src="{{asset('site/asset/images/appstore.png')}}" alt="">&nbsp;&nbsp;
                  <img class="app-stores" width="200" src="{{asset('site/asset/images/playstore.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>-->
<main class="mt-lg-5">
    <div class="rating-wrapper pt-lg-4">
        <div class="pt-lg-5">
            <h1>Enjoy Every Meal</h1>
        </div>
        {{--<div class=" rating-filter-section ">
            <div class="dropdown dropdown-rating-section show">
                <span>Sort:</span>
                <a class="btn btn-secondary dropdown-toggle text-dark" href="#" role="button"
                    id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Ratings
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">5</a>
                    <a class="dropdown-item" href="#">More than 3</a>
                    <a class="dropdown-item" href="#">3</a>
                </div>
            </div>
            <div class="filter-section">
                <img src="/site/asset/images/filter.png" alt="">
                <a href="#">Filter</a>
            </div>
        </div>--}}
    </div>
</main>
<!--<div class="modal modal-cart fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div id="content" class="modal-dialog modal-dialog2" role="document">
    </div>
</div>
<div class="modal modal3 fade right cartModal" id="sideModalTR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>-->
<div class="food-cart">
  <div class="container-fluid">
    <nav>
      <div class="nav nav-tabs mb-4 justify-content-center" id="nav-tab" role="tablist">
        @foreach($categories as $item)
        <a class="nav-item nav-link {{($loop->iteration == 1 && $type=='NA' || $type==$item->name)?'active':''}}" id="{{$item->name}}-tab" data-toggle="tab" href="#{{str_replace(' ','',$item->name)}}" role="tab" aria-controls="{{$item->name}}" aria-selected="true">{{$item->name}}</a>
        @endforeach
      </div>
    </nav>
  </div>
    <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="tab-content" id="nav-tabContent">
                @foreach($categories as $item)
                  <div class="tab-pane fade show {{($loop->iteration == 1 && $type=='NA' || $type==$item->name)?'active':''}}" id="{{str_replace(' ','',$item->name)}}" role="tabpanel" aria-labelledby="{{$item->name}}-tab">
                    <div class="row">
                      @foreach($item->items as $item1)
                      <div class="col-md-3 col-sm-4 col-xs-12 mb-4 food-card">
                          <div class="card">
                            <a data-id="{{ $item1->id }}" class="cart-button">
                              <div class="card-header">
                                  <div class="row">
                                      <div class="col-md-8 col-sm-8 col-8">
                                          <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                          <i class="fa fa-heart-o" aria-hidden="true"
                                              onclick="changeImage()"></i>
                                      </div>
                                  </div>
                              </div>
                              <div class="card-body">
                                  <img src="{{ asset($item1->item_image) }}" alt="...">
                              </div>
                              <div class="card-footer text-center">
                                  <h5 class="">{{$item1->item_name }}</h5>
                                  @php
                                        if ($item1->chef_id == 0)
                                            $chefName = 'Menurie Chef';
                                        else
                                            $chefName = $item1->chef->first_name . ' ' . $item1->chef->last_name;
                                    @endphp
                                  <p class="">by {{ $chefName }} <br>${{ number_format((int)$item1->price, 2) }}</p>
                              </div>
                            </a>
                          </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                @endforeach
            </div>
          </div>
        </div>
    </div>
    <br>
</div>
<br><br>
@endsection
@section('scripts')
<script src="{{ asset('site/asset/scripts/foodDetail.js') }}"></script>
<script src="{{ asset('site/asset/scripts/script.js') }}"></script>
@endsection
