@extends('site.layout.base')
@section('title','Chef')
@section('styles')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('site/asset/styles/breakFast.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
@endsection
@section('content')
<!-------Second Modal-------------->
<div class="modal modal-cart fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog2 " role="document">
        <div class="modal-content modal-content2">
            <div class="modal-header modal-header2">
                <table class="table ">
                    <tbody>
                        <tr class="modal-header-tr">
                            <th scope="row">
                                <img src="/site/asset/images/food2.png" width="90">
                            </th>
                            <td>
                                <div class="modal-dish-name" style="color: white;"><b style="font-size: 18px;"> Jumbo
                                        Shrimp</b> <br> by Jonathan Doe.</div>
                            </td>
                            <td style="color: white; ">$9.99 <br>
                                <div class="modal-amount-controller">
                                    <span class="modal-btn1" style="color: white;">
                                        <i class="fa fa-minus" onclick="minusNum()"></i>
                                    </span>
                                    <span class="modal-btn2" style="color: white;" id="num-btn">01</span>
                                    <span class="modal-btn3 " style="color: white;">
                                        <i class="fa fa-plus " onclick="plusNum()"></i>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-body modal-body2">
                <table class="table ">
                    <div class="h4">ADD ON</div>
                    <tbody>
                        <tr>
                            <th scope="row" class="dish-name">
                                <input type="radio">
                                <span>Grilled Chicken</span>
                            </th>
                            <td class="modal-controller21">
                                <!-- <div class="modal-amount-controller2"> -->
                                <span class="modal-btn1">
                                    <i class="fa fa-minus" onclick="minusNum()"></i>
                                </span>
                                <span class="modal-btn2" id="num-btn">01</span>
                                <span class="modal-btn3 ">
                                    <i class="fa fa-plus " onclick="plusNum()"></i>
                                </span>
                                <!-- </div> -->
                            </td>
                            <td>$2.00</td>

                        </tr>
                        <tr>
                            <th scope="row" class="dish-name">
                                <input type="radio">
                                <span>Grilled Chicken</span>

                            </th>
                            <td class="modal-controller21">
                                <!-- <div class="modal-amount-controller2"> -->
                                <span class="modal-btn1">
                                    <i class="fa fa-minus" onclick="minusNum()"></i>
                                </span>
                                <span class="modal-btn2" id="num-btn">01</span>
                                <span class="modal-btn3 ">
                                    <i class="fa fa-plus " onclick="plusNum()"></i>
                                </span>
                                <!-- </div> -->
                            </td>
                            <td>$2.00</td>
                        </tr>
                        <tr>
                            <th scope="row" class="dish-name">
                                <input type="radio">
                                <span>Grilled Chicken</span>

                            </th>
                            <td class="modal-controller21">
                                <!-- <div class="modal-amount-controller2"> -->
                                <span class="modal-btn1">
                                    <i class="fa fa-minus" onclick="minusNum()"></i>
                                </span>
                                <span class="modal-btn2" id="num-btn">01</span>
                                <span class="modal-btn3 ">
                                    <i class="fa fa-plus " onclick="plusNum()"></i>
                                </span>
                                <!-- </div> -->
                            </td>
                            <td>$2.00</td>
                        </tr>
                        <tr>
                            <th scope="row" class="dish-name">
                                <input type="radio">
                                <span>Grilled Chicken</span>
                            </th>
                            <td class="modal-controller21">
                                <!-- <div class="modal-amount-controller2"> -->
                                <span class="modal-btn1">
                                    <i class="fa fa-minus" onclick="minusNum()"></i>
                                </span>
                                <span class="modal-btn2" id="num-btn">01</span>
                                <span class="modal-btn3 ">
                                    <i class="fa fa-plus " onclick="plusNum()"></i>
                                </span>
                                <!-- </div> -->
                            </td>
                            <td>$2.00</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer modal-footer2">
                <button type="button" class="btn btn-danger modal-link-btn"> <a
                        href="{{ route('cart') }}">Add to Cart - $30</a></button>
                <button type="button" style="color: white;" class="btn  btn-close" data-dismiss="modal">
                    Cancel</button>
            </div>
        </div>
    </div>
</div>
<!------------------Third Modal-------------->
<div class="modal modal3 fade right" id="sideModalTR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <!-- Add class .modal-side and then add class .modal-top-right (or other classes from list above) to set a position to the modal -->
    <div class="modal-dialog modal-dialog3 modal-side modal-top-right" role="document">
        <div class="modal-content modal-content3">
            <div class="modal-header modal-header3">
                <h4 class="modal-title w-100" id="myModalLabel">My Cart</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-body3">
                <div class="container-cart">
                    <!-- <div class="row"> -->
                    <div class=" cart-col-1">
                        <div><img src="/site/asset/images/food2.png" width="60">
                            <i class="fa fa-times" style="margin-left: 10px;" aria-hidden="true">2</i></div>
                    </div>
                    <div class=" cart-col-2">
                        <b>Jumbo Shrimp</b> <br> <span class="text-secondary">by Jonathan Doe.</span>
                    </div>
                    <div class=" cart-col-3 text-secondary">$9.99</div>
                </div><br>
                <div class="container-cart">
                    <!-- <div class="row"> -->
                    <div class=" cart-col-1">
                        <div><img src="/site/asset/images/food2.png" width="60">
                            <i class="fa fa-times" style="margin-left: 10px !important;" aria-hidden="true">2</i>
                        </div>
                    </div>
                    <div class=" cart-col-2">
                        <b>Jumbo Shrimp</b> <br> <span class="text-secondary">by Jonathan Doe.</span>
                    </div>
                    <div class=" cart-col-3 text-secondary">$9.99</div>
                </div><br>
                <div class="container-cart">
                    <!-- <div class="row"> -->
                    <div class=" cart-col-1">
                        <div><img src="/site/asset/images/food2.png" width="60">
                            <i class="fa fa-times" style="margin-left: 10px;" aria-hidden="true">2</i>
                        </div>
                    </div>
                    <div class=" cart-col-2">
                        <b>Jumbo Shrimp</b> <br> <span class="text-secondary">by Jonathan Doe.</span>
                    </div>
                    <div class=" cart-col-3 text-secondary">$9.99</div>
                </div><br>
                <div class="cart-bill-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 text-secondary bill-detail" style="font-size: 14px;"> Bill detail:</div>
                        </div>
                    </div>
                    <div class="container-total-item">
                        <div class=" item-heading"> Item Total:</div>
                        <div class=" item-amount"> 28.00$</div>
                    </div>
                    <div class="container-delivery">
                        <div class=" delivery-heading"> Delivery Fee:</div>
                        <div class=" delivery-fee"> $4.00</div>
                    </div>
                    <hr>
                    <div class="container-total">
                        <div class=" total-amount-heading"> Total:</div>
                        <div class=" total-amount"> $32.00</div>
                    </div><br>
                    <div class="container">
                        <div class="row">
                            <div class="col-12 cart-checkout">
                                <button class="btn btn-danger btn-checkout1"> <a
                                        href="{{ route('cart') }}">Checkout</a></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<main>
    <div class="main-bottom-header">
        <div class="bottom-header">
            <div class="container-fluid bottom-header-wrapper  p0">
                <div class="row">
                    <div class="col-12 col-lg-3 col-md-3 col-sm-6  b-header1 b-header11">
                        <img class="border-content-img" src="/site/asset/images/header-content.png">
                        <span class="pure-veg">Pure Veg.</span>
                        <div class="b-header111">
                            <h3>Jonathan Doe.</h3>
                        </div>
                        <div class="b-header111">
                            <p>jonathandoe@gmail.com</p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2  b-header1 b-header12 ">
                        <i class="fa fa-star"></i>
                        <spna>4.3</spna><br>
                        <span>Total Ratings</span>
                    </div>
                    <div class="col-lg-2 col-md-2   b-header1 b-header13 ">
                        <i class="fa fa-clock-o"></i>
                        <spna>45 minutes</spna><br>
                        <span>Delivery Time</span>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-2   b-header1 b-header14 ">
                        <i class="fa fa-clock-o"></i>
                        <spna>$$$</spna><br>
                        <span>Price Range</span>
                    </div>
                    <div class="  col-lg-3 col-md-3  col-sm-6 b-header1 b-header15 b-btn">
                        <button class="btn btn-danger">
                            <img src="/site/asset/images/heart.png" width="15">
                            Favorite</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
    <div class="container">
        <div class="row">
            <div class="col-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item new-item-style" id="current1">
                        <a class="nav-link active" id="my-tab1" data-toggle="tab" href="#Orders" role="tab"
                            aria-controls="Orders" aria-selected="true">Break fast</a>
                    </li>
                    <li class="nav-item new-item-style" id="current2">
                        <a class="nav-link" id="my-tab2" data-toggle="tab" href="#Favorites" role="tab"
                            aria-controls="Favorites" aria-selected="false">Fruits & Juices</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="Orders" role="tabpanel" aria-labelledby="Orders-tab">
                        <div class="main-box clearfix">
                            <br>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 h4" style="color: black !important;">
                                        Breakfast
                                    </div>
                                </div>
                            </div>
                            <div class="food-cart">
                                <div class="container">
                                    <div class="row" style="margin-bottom: 3%;">
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header" style="background:white; border:none;">
                                                    <div class="row">
                                                        <div class="col-md-8 col-sm-8 col-8">
                                                            <span class="header-text"> <i class="fa fa-star"
                                                                    aria-hidden="true"></i> 4.5</span>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-4 heart-class text-right"
                                                            id="colo2-heart">
                                                            <i class="fa fa-heart-o" style="cursor: pointer;"
                                                                aria-hidden="true" onclick="changeImage()"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="width: 95%;">
                                                    <img src="/site/asset/images/food2.png" class="card-img-top"
                                                        alt="...">
                                                </div>
                                                <div class="card-footer text-center"
                                                    style="background: white;border:none">
                                                    <h5 class="food-cart-content-01">Jumbo Shrimp</h5>
                                                    <p class="food-cart-content-01">by Jonathan doe <br>$9.99</p>
                                                    <button href="#" class="cart-button" data-toggle="modal"
                                                        data-target="#modalCart"><i class="fa fa-plus"
                                                            style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header" style="background:white; border:none;">
                                                    <div class="row">
                                                        <div class="col-md-8 col-sm-8 col-8">
                                                            <span class="header-text"> <i class="fa fa-star"
                                                                    aria-hidden="true"></i> 4.5</span>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                            <i class="fa fa-heart-o" aria-hidden="true"
                                                                onclick="changeImage()"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="width: 95%;">
                                                    <img src="/site/asset/images/food3.png" class="card-img-top"
                                                        alt="...">
                                                </div>
                                                <div class="card-footer text-center"
                                                    style="background: white;border:none">
                                                    <h5 class="food-cart-content-01">Jumbo Shrimp</h5>
                                                    <p class="food-cart-content-01">by Jonathan doe <br>$9.99</p>
                                                    <button href="#" class="cart-button" data-toggle="modal"
                                                        data-target="#modalCart"><i class="fa fa-plus"
                                                            style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header" style="background:white; border:none;">
                                                    <div class="row">
                                                        <div class="col-md-8 col-sm-8 col-8">
                                                            <span class="header-text"> <i class="fa fa-star"
                                                                    aria-hidden="true"></i> 4.5</span>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                            <i class="fa fa-heart-o" aria-hidden="true"
                                                                onclick="changeImage()"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="width: 95%;">
                                                    <img src="/site/asset/images/food4.png" class="card-img-top"
                                                        alt="...">
                                                </div>
                                                <div class="card-footer text-center"
                                                    style="background: white;border:none">
                                                    <h5 class="food-cart-content-01">Jumbo Shrimp</h5>
                                                    <p class="food-cart-content-01">by Jonathan doe <br>$9.99</p>
                                                    <button href="#" class="cart-button" data-toggle="modal"
                                                        data-target="#modalCart"><i class="fa fa-plus"
                                                            style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header" style="background:white; border:none;">
                                                    <div class="row">
                                                        <div class="col-md-8 col-sm-8 col-8">
                                                            <span class="header-text"> <i class="fa fa-star"
                                                                    aria-hidden="true"></i> 4.5</span>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                            <i class="fa fa-heart-o" aria-hidden="true"
                                                                onclick="changeImage()"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="width: 95%;">
                                                    <img src="/site/asset/images/food5.png" class="card-img-top"
                                                        alt="...">
                                                </div>
                                                <div class="card-footer text-center"
                                                    style="background: white;border:none">
                                                    <h5 class="food-cart-content-01">Jumbo Shrimp</h5>
                                                    <p class="food-cart-content-01">by Jonathan doe <br>$9.99</p>
                                                    <button href="#" class="cart-button" data-toggle="modal"
                                                        data-target="#modalCart"><i class="fa fa-plus"
                                                            style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!----------------------------------Second row---------------------------->
                                    <div class="row">
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header" style="background:white; border:none;">
                                                    <div class="row">
                                                        <div class="col-md-8 col-sm-8 col-8">
                                                            <span class="header-text"> <i class="fa fa-star"
                                                                    aria-hidden="true"></i> 4.5</span>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                            <i class="fa fa-heart-o" aria-hidden="true"
                                                                onclick="changeImage()"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="width: 95%;">
                                                    <img src="/site/asset/images/food6.png" class="card-img-top"
                                                        alt="...">
                                                </div>
                                                <div class="card-footer text-center"
                                                    style="background: white;border:none">
                                                    <h5 class="food-cart-content-01">Jumbo Shrimp</h5>
                                                    <p class="food-cart-content-01">by Jonathan doe <br>$9.99</p>
                                                    <button href="#" class="cart-button" data-toggle="modal"
                                                        data-target="#modalCart"><i class="fa fa-plus"
                                                            style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header" style="background:white; border:none;">
                                                    <div class="row">
                                                        <div class="col-md-8 col-sm-8 col-8">
                                                            <span class="header-text"> <i class="fa fa-star"
                                                                    aria-hidden="true"></i> 4.5</span>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                            <i class="fa fa-heart-o" aria-hidden="true"
                                                                onclick="changeImage()"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="width: 95%;">
                                                    <img src="/site/asset/images/food7.png" class="card-img-top"
                                                        alt="...">
                                                </div>
                                                <div class="card-footer text-center"
                                                    style="background: white;border:none">
                                                    <h5 class="food-cart-content-01">Jumbo Shrimp</h5>
                                                    <p class="food-cart-content-01">by Jonathan doe <br>$9.99</p>
                                                    <button href="#" class="cart-button" data-toggle="modal"
                                                        data-target="#modalCart"><i class="fa fa-plus"
                                                            style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header" style="background:white; border:none;">
                                                    <div class="row">
                                                        <div class="col-md-8 col-sm-8 col-8">
                                                            <span class="header-text"> <i class="fa fa-star"
                                                                    aria-hidden="true"></i> 4.5</span>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                            <i class="fa fa-heart-o" aria-hidden="true"
                                                                onclick="changeImage()"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="width: 95%;">
                                                    <img src="/site/asset/images/food8.png" class="card-img-top"
                                                        alt="...">
                                                </div>
                                                <div class="card-footer text-center"
                                                    style="background: white;border:none">
                                                    <h5 class="food-cart-content-01">Jumbo Shrimp</h5>
                                                    <p class="food-cart-content-01">by Jonathan doe <br>$9.99</p>
                                                    <button href="#" class="cart-button" data-toggle="modal"
                                                        data-target="#modalCart"><i class="fa fa-plus"
                                                            style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header" style="background:white; border:none;">
                                                    <div class="row">
                                                        <div class="col-md-8 col-sm-8 col-8">
                                                            <span class="header-text"> <i class="fa fa-star"
                                                                    aria-hidden="true"></i> 4.5</span>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                            <i class="fa fa-heart-o" aria-hidden="true"
                                                                onclick="changeImage()"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="width: 95%;">
                                                    <img src="/site/asset/images/food9.png" class="card-img-top"
                                                        alt="...">
                                                </div>
                                                <div class="card-footer text-center"
                                                    style="background: white;border:none">
                                                    <h5 class="food-cart-content-01">Jumbo Shrimp</h5>
                                                    <p class="food-cart-content-01">by Jonathan doe <br>$9.99</p>
                                                    <button href="#" class="cart-button" data-toggle="modal"
                                                        data-target="#modalCart"><i class="fa fa-plus"
                                                            style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <!----------------------------------Second row---------------------------->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Favorites" role="tabpanel" aria-labelledby="Favorites-tab">
                        <!---------------------Favorite section------------>
                        <br><br>
                        <div class="container">
                            <div class="row">
                                <div class="col h3">
                                    Favorites
                                </div>
                            </div>
                        </div><br>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="card">
                                        <div class="card-header" style="background:white; border:none;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 col-8">
                                                    <span class="header-text"> <i class="fa fa-star"
                                                            aria-hidden="true"></i> 4.5</span>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                    <i class="fa fa-heart-o" aria-hidden="true"
                                                        onclick="changeImage()"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body" style="width: 95%;">
                                            <img src="/site/asset/images/food10.png" class="card-img-top" alt="...">
                                        </div>
                                        <div class="card-footer text-center" style="background: white;border:none">
                                            <h5 class="">Jumbo Shrimp</h5>
                                            <p class="">by Jonathan doe <br>$9.99</p>
                                            <button href="#" class="cart-button" data-toggle="modal"
                                                data-target="#modalCart"><i class="fa fa-plus"
                                                    style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="card">
                                        <div class="card-header" style="background:white; border:none;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 col-8">
                                                    <span class="header-text"> <i class="fa fa-star"
                                                            aria-hidden="true"></i> 4.5</span>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                    <i class="fa fa-heart-o" aria-hidden="true"
                                                        onclick="changeImage()"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body" style="width: 95%;">
                                            <img src="/site/asset/images/food11.png" class="card-img-top" alt="...">
                                        </div>
                                        <div class="card-footer text-center" style="background: white;border:none">
                                            <h5 class="">Jumbo Shrimp</h5>
                                            <p class="">by Jonathan doe <br>$9.99</p>
                                            <button href="#" class="cart-button" data-toggle="modal"
                                                data-target="#modalCart"><i class="fa fa-plus"
                                                    style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="card">
                                        <div class="card-header" style="background:white; border:none;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 col-8">
                                                    <span class="header-text"> <i class="fa fa-star"
                                                            aria-hidden="true"></i> 4.5</span>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                    <i class="fa fa-heart-o" aria-hidden="true"
                                                        onclick="changeImage()"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body" style="width: 95%;">
                                            <img src="/site/asset/images/food12.png" class="card-img-top" alt="...">
                                        </div>
                                        <div class="card-footer text-center" style="background: white;border:none">
                                            <h5 class="">Jumbo Shrimp</h5>
                                            <p class="">by Jonathan doe <br>$9.99</p>
                                            <button href="#" class="cart-button" data-toggle="modal"
                                                data-target="#modalCart"><i class="fa fa-plus"
                                                    style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="card">
                                        <div class="card-header" style="background:white; border:none;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 col-8">
                                                    <span class="header-text"> <i class="fa fa-star"
                                                            aria-hidden="true"></i> 4.5</span>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                    <i class="fa fa-heart-o" aria-hidden="true"
                                                        onclick="changeImage()"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body" style="width: 95%;">
                                            <img src="/site/asset/images/food13.png" class="card-img-top" alt="...">
                                        </div>
                                        <div class="card-footer text-center" style="background: white;border:none">
                                            <h5 class="">Jumbo Shrimp</h5>
                                            <p class="">by Jonathan doe <br>$9.99</p>
                                            <button href="#" class="cart-button" data-toggle="modal"
                                                data-target="#modalCart"><i class="fa fa-plus"
                                                    style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="card">
                                        <div class="card-header" style="background:white; border:none;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 col-8">
                                                    <span class="header-text"> <i class="fa fa-star"
                                                            aria-hidden="true"></i> 4.5</span>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                    <i class="fa fa-heart-o" aria-hidden="true"
                                                        onclick="changeImage()"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body" style="width: 95%;">
                                            <img src="/site/asset/images/food10.png" class="card-img-top" alt="...">
                                        </div>
                                        <div class="card-footer text-center" style="background: white;border:none">
                                            <h5 class="">Jumbo Shrimp</h5>
                                            <p class="">by Jonathan doe <br>$9.99</p>
                                            <button href="#" class="cart-button" data-toggle="modal"
                                                data-target="#modalCart"><i class="fa fa-plus"
                                                    style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="card">
                                        <div class="card-header" style="background:white; border:none;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 col-8">
                                                    <span class="header-text"> <i class="fa fa-star"
                                                            aria-hidden="true"></i> 4.5</span>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-4 text-right" id="">
                                                    <i class="fa fa-heart-o" aria-hidden="true"
                                                        onclick="changeImage()"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body" style="width: 95%;">
                                            <img src="/site/asset/images/food11.png" class="card-img-top" alt="...">
                                        </div>
                                        <div class="card-footer text-center" style="background: white;border:none">
                                            <h5 class="">Jumbo Shrimp</h5>
                                            <p class="">by Jonathan doe <br>$9.99</p>
                                            <button href="#" class="cart-button" data-toggle="modal"
                                                data-target="#modalCart"><i class="fa fa-plus"
                                                    style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><br><br>
                        <!--------------------End of favorite section------------->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-------------------------------------------------------------------------->
</main>
@endsection
@section('scripts')
<script>
    $(document).ready(function () {
        $("#current1").addClass("current_active");
        //         setTimeout(function(){// wait for 5 secs(2)
        $(".nav-link").css({
            'color': 'lightslategray'
        });
        $(".active").css({
            'color': '#843232'
        }); // then reload the page.(3)
        //   }, 500);
        // $("#my-tab3").css({ 'color': 'lightslategray' });
        // $("#my-tab4").css({ 'color': 'lightslategray' });
    });

    function greet() {
        $(".nav-link").css({
            'color': 'lightslategray'
        });
        $(".active").css({
            'color': '#843232'
        });
    }
    setTimeout(greet, 100);
    $("#my-tab1").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current1").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current4").removeClass("current_active");

        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab2").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current2").addClass("current_active");
        $("#current1").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current4").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab3").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current3").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current1").removeClass("current_active");
        $("#current4").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab4").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current4").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current1").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });

</script>
<script src="/site/asset/scripts/breakFast.js"></script>
@endsection
