@extends('site.layout.base')
@section('title','User')
@section('styles')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('site/asset/styles/userProfile.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/style.css') }}">
<style type="text/css">
    .profile-back{
        background-color: #F5F5F5;
        margin-right:185px;
        margin-left:185px; 
        border-radius: 15px;
    }
    .data-card{
        background-color: white;
        margin-left: 170px;
        margin-right: 170px;
        border-radius: 15px;
    }
    @media(max-width: 768px){
        .profile-back{
            background-color: #F5F5F5;
            margin-right:0px;
            margin-left:0px; 
            border-radius: 15px;
        }   
    }
    @media(max-width: 425px){
        .data-card{
            background-color: white;
            margin-left: 50px;
            margin-right: 50px;
            border-radius: 15px;
        }
    }
</style>
@endsection
@section('content')
<div class="container" style="margin-top: 8rem;margin-bottom: 5rem;">
    <h3 style="margin-left: 184px;">My Profile</h3>
    <div class="text-center profile-back pt-4 pb-5 ">
        <div style="max-height: 150px;overflow: hidden;">
            <a href="#" class="pop"><img src="@if(auth('customer')->user()->image != '') {{asset(auth('customer')->user()->image)}} @else{{ asset('user_avatar.png') }}@endif" alt="{{ auth('customer')->user()->first_name.' '.auth('customer')->user()->last_name }}" class="mx-auto d-block img-fluid"
            style="width: 20%;height: 5%;max-height: 5%;min-height: 5%;border-radius: 10px"></a>
            <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">              
                  <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <img src="" class="imagepreview" style="width: 100%;" >
                  </div>
                </div>
              </div>
            </div>
        </div>
        <button type="button" class="btn btn-outline-dark mt-2" data-toggle="modal" data-target="#exampleModal2">
          Change Picture
        </button>
        <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Picture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="POST" action="{{ route('edit-image') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="user_image" required>
                    <input type="hidden" name="id" value="{{ auth('customer')->user()->id }}">
                    <input style="margin-left: 113px;margin-top: 20px;" type="submit" name="submit_btn" value="Update" class="btn btn-primary save-profile">
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="data-card mt-3">
            <div class="row py-3 text-left pl-2">
                <div class="col-sm-10">
                    <h6 class="pl-3">Name</h6>
                </div>
                <div class="col-sm-2">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#name_edit"><i class="fa fa-edit" style="font-size: 22px;cursor: pointer;color: #8D5440;"></i></a>
                </div>
                <div class="modal fade" id="name_edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Name</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="row justify-content-center">
                            <form method="POST" action="{{ route('edit-name') }}">
                                @csrf
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="first_name" value="{{ auth('customer')->user()->first_name }}" required>
                                </div>
                                <br>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="last_name" value="{{ auth('customer')->user()->last_name }}" required>
                                </div>
                                <input type="hidden" name="id" value="{{ auth('customer')->user()->id }}">
                                <input style="margin-left: 113px;margin-top: 20px;" type="submit" name="submit_btn" value="Update" class="btn btn-primary save-profile">
                            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <label class="name lead pl-4 ml-1" style="line-height: 11px; margin-top: 4%;">{{ auth('customer')->user()->first_name.' '.auth('customer')->user()->last_name }}</label>
            </div>
        </div>
        <div class="data-card mt-3">
            <div class="row py-3 text-left pl-2">
                <div class="col-sm-10">
                    <h6 class="pl-3">Email</h6>
                </div>
                <div class="col-sm-2">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#edit_email"><i class="fa fa-edit" style="font-size: 22px;cursor: pointer;color: #8D5440;"></i></a>
                </div>
                <div class="modal fade" id="edit_email" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Email</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="row justify-content-center">
                            <form method="POST" action="{{ route('edit-email') }}">
                                @csrf
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="email" value="{{ auth('customer')->user()->email }}" required>
                                </div>
                                <input type="hidden" name="id" value="{{ auth('customer')->user()->id }}">
                                <input style="margin-left: 113px;margin-top: 20px;" type="submit" name="submit_btn" value="Update" class="btn btn-primary save-profile">
                            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <label class="name lead pl-4 ml-1" style="line-height: 11px; margin-top: 4%;">{{ auth('customer')->user()->email }}</label>
            </div>
        </div>
        <div class="data-card mt-3">
            <div class="row py-3 text-left pl-2">
                <div class="col-sm-10">
                    <h6 class="pl-3">Phone</h6>
                </div>
                <div class="col-sm-2">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#edit_phone"><i class="fa fa-edit" style="font-size: 22px;cursor: pointer;color: #8D5440;"></i></a>
                </div>
                <div class="modal fade" id="edit_phone" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Phone</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="row justify-content-center">
                            <form method="POST" action="{{ route('edit-phone') }}">
                                @csrf
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="phone" value="{{ auth('customer')->user()->phone }}" required>
                                </div>
                                <input type="hidden" name="id" value="{{ auth('customer')->user()->id }}">
                                <input style="margin-left: 113px;margin-top: 20px;" type="submit" name="submit_btn" value="Update" class="btn btn-primary save-profile">
                            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <label class="name lead pl-4 ml-1" style="line-height: 11px; margin-top: 4%;">{{ auth('customer')->user()->phone }}</label>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function () {
        $("#current1").addClass("current_active");
        //         setTimeout(function(){// wait for 5 secs(2)
        $(".nav-link").css({
            'color': 'lightslategray'
        });
        $(".active").css({
            'color': '#843232'
        }); // then reload the page.(3)
        //   }, 500);
        // $("#my-tab3").css({ 'color': 'lightslategray' });
        // $("#my-tab4").css({ 'color': 'lightslategray' });

    });

    function greet() {
        $(".nav-link").css({
            'color': 'lightslategray'
        });
        $(".active").css({
            'color': '#843232'
        });
    }
    setTimeout(greet, 100);
    $("#my-tab1").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current1").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current4").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab2").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current2").addClass("current_active");
        $("#current1").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current4").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab3").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current3").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current1").removeClass("current_active");
        $("#current4").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $("#my-tab4").click(function () {
        function greet() {
            $(".nav-link").css({
                'color': 'lightslategray'
            });
            $(".active").css({
                'color': '#843232'
            });
        }
        setTimeout(greet, 100);
        $("#current4").addClass("current_active");
        $("#current2").removeClass("current_active");
        $("#current3").removeClass("current_active");
        $("#current1").removeClass("current_active");
        // $(".nav-link").css({ 'color': 'lightslategray'});
        //         $(".active").css({ 'color': '#843232' });
    });
    $(function() {
        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');   
        });     
    });
</script>
@endsection
