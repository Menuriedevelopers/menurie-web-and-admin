<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="appleid-signin-client-id" content="com.drudotstechweblogin.menurieuser">
    <meta name="appleid-signin-scope" content="name email">
    <meta name="appleid-signin-redirect-uri" content="https://dev.menurie.com/api/auth/apple/callback">
    <meta name="appleid-signin-state" content="{{csrf_token()}}">
    <meta name="appleid-signin-response-type" content="code">
    <meta name="appleid-signin-locale" content="en_US">
    <title> Menurie - login</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @include('site.chef.layouts.styles')
</head>
<body class="vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="auth-login" class="row flexbox-container">
                    <div class="col-xl-2 col-2">
                        <img src="{{ asset('site/asset/images/header12.png') }}" alt="">
                    </div>
                    <div class="col-xl-8 col-8">
                        <div class="card bg-authentication mb-0">
                            <div class="row m-0">
                                <div class="col-md-6 col-sm-12 px-0">
                                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                        <div class="card-header pb-1">
                                            <div class="card-title text-center">
                                                <a href="{{route('index')}}"><img width="100" src="{{asset('site/asset/images/img1.png')}}" alt="Menurie"></a><br><br>
                                                <h4 class="text-center mb-2">Welcome Back</h4>
                                            </div>
                                        </div>
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="divider">
                                                    <div class="divider-text text-uppercase text-muted"><small> login with
                                                            email</small>
                                                    </div>
                                                </div>
                                                <form method="POST" action="{{ route('customerLogin') }}">
                                                    @csrf
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-600">Email address</label>
                                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter email address">
                                                        @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-bold-600">Password</label>
                                                        <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter password" name="password" required autocomplete="current-password">
                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <button type="submit" class="btn btn-menurie glow w-100 position-relative">Login<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                                    <hr>
                                                    <div class="text-right"><a href="{{route('customer.forget')}}"><small class="mr-25">Forgot password?</small></a></div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-12 mb-1">
                                                            <a style="font-size:smaller;" class="btn btn-primary btn-block" href="{{ url('auth/facebook') }}"><i  style="top:0px;" class="fa fa-facebook"></i>&nbsp;Sign in with Facebook</a>
                                                        </div>
                                                        <div class="col-12 mb-1">
                                                            <a style="font-size:smaller;" class="btn btn-danger btn-block" href="{{ url('auth/google') }}"><i  style="top:0px;" class="fa fa-google"></i>&nbsp;Sign in with Google</a>
                                                        </div>
                                                        {{-- <div id="appleid-signin" class="col-12 mb-1" data-color="black" data-border="false" data-border-radius="15" data-width="400" data-height="32" data-type="sign-in"></div> --}}
                                                        <div class="col-12 mb-1">
                                                            <a style="font-size:smaller;background-color: #F5F5F5;" class="btn btn-block" href="https://appleid.apple.com/auth/authorize?client_id=com.drudotstechweblogin.menurieuser&redirect_uri=https://dev.menurie.com/api/auth/apple/callback&response_type=code%20id_token&state=6y5pAB7KJcEdm7boRKIyvFpSzKTWU9hKQE58ameo&scope=name%20email&response_mode=form_post&frame_id=bb13e6c6-8943-4e8e-bf29-f73af54dd4bb&m=22&v=1.5.4&locale=en_US"><i style="top:0px;" class="fa fa-apple"></i>&nbsp;Sign in With Apple</a>
                                                        </div>
                                                    </div>
                                                </form>
                                                <hr>
                                                <div class="text-center"><small class="mr-25">new to Menurie?</small><a href="{{route('customer.register')}}"><small>Sign up</small> </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                    <div class="card-content">
                                    <img class="img-fluid" src="{{ asset('site/asset/images/login-img1.png') }}" alt="branding logo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-2">
                        <img src="{{ asset('site/asset/images/header2.png') }}" alt="">
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js"></script>
</body>
</html>
