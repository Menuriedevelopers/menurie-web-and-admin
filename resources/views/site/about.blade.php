@extends('site.layout.base')
@section('title','About')
@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('site/asset/styles/cart.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/contact-us.css') }}">
@endsection
@section('Orders - Menurie')
@section('content')
<div class="second-navbar">
    <div class="container">
        <div class="row row-spacing">
            <div class="col-lg-4 col-md-6 col-sm-12">
            </div>
            <div>
              <img class="app-stores" width="200" src="{{asset('site/asset/images/appstore.png')}}" alt="">&nbsp;&nbsp;
              <img class="app-stores" width="200" src="{{asset('site/asset/images/playstore.png')}}" alt="">
            </div>
        </div>
    </div>
</div>
<div class="container">
  <div class="row">
    <div class="find-us">
      <div class="container">
        <div class="row mb-4">
        <div class="col-12">
          <div class="text-center h3">
            About <span class="menurie-text">Menurie</span>
          </div>
          <div class="">
            <p>Menurie is a next generation tech food company. We are a platform that provides to customers meals created by certified culinary chefs. We empower kitchen teams  with creativity to cook fusion foods, healthy cuisines, and other tasty dishes and meal created and prepared with the highest standards and always with our customers needs as priority. To disrupt the way we order
            food  and delivered is our mission and  we have the cravings and energy to do so one dish at a time.</p>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
