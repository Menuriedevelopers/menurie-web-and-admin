@extends('site.layout.base')
@section('styles')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
@endsection
@section('title','Product')
@section('content')
<main class="mt-lg-5">
    <div class="rating-wrapper pt-lg-4">
        <div class="pt-lg-5">
            <h2>Your search result</h2>
        </div>
    </div>
</main>
<div class="container">
  <div class="text-center">
    <h3>No Product Found</h3>
  </div>
</div>
<br><br>
@endsection
@section('scripts')
<script src="{{ asset('site/asset/scripts/foodDetail.js') }}"></script>
<script src="{{ asset('site/asset/scripts/script.js') }}"></script>
@endsection
