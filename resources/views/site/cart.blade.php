@extends('site.layout.base')
@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('site/asset/styles/cart.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
@endsection
@section('title','Cart')
@section('content')
    <div class="container mt-5">
        <div class="row pt-5">
            <div class="col-lg-12 col-md-12">
                <div id="content" class="main-box clearfix">
                    @php $cart = session('cart'); $total = 0; @endphp
                    @if($cart)
                    <div class="table-responsive">
                        <table class="table user-list">
                            <thead>
                                <tr>
                                    <th class="border-0">
                                        <div class="text-costom-heading">My Cart</div>
                                    </th>
                                </tr>
                                <tr>
                                    <th class="border-top-0 text-center"><span>Item detail</span></th>
                                    <th class="border-top-0 text-center"><span>Quantity</span></th>
                                    <th class="border-top-0 text-center"><span>Price</span></th>
                                    <th class="border-top-0 text-center"><span>Action</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cart as $index => $cartItem)
                                @php
                                    $total += $cartItem['subTotal'] * $cartItem['quantity'];
                                @endphp
                                <tr>
                                    <td class="w-50">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-12">
                                                <img src="{{ $cartItem['photo'] }}" alt="" class="w-100">  
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-12">
                                                <div class="card-title1 my-1">{{ $cartItem['name'] }}</div>
                                                <div class="user-subhead gray-col">by {{ $cartItem['chef']}}</div> 
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-lg-flex d-md-flex d-lg-block justify-content-center text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-outline-light btn-number minus rounded-left left-plus-btn"  data-type="minus" data-field="{{$cartItem['id']}}" >

                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                            <span> <input type="text" id="{{$cartItem['id']}}" name="{{$cartItem['id']}}" class="form-control input-number input-number1 p-0 center-plus mx-auto" value="{{ $cartItem['quantity']}}" onchange="changeQuantity({{$cartItem['id']}});if(parseInt(this.value,10)<10)this.value='0'+this.value;" min="01" max="9999"></span>
                                            <span class="input-group-btn">
                                                <button class="btn btn-outline-light btn-number plus rounded-right right-plus-btn" data-type="plus" data-field="{{$cartItem['id']}}" >
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <b id="{{$cartItem['id']}}-price">${{ number_format($cartItem['subTotal'], 2)}}</b>
                                    </td>
                                    <td class="text-center">
                                        <a herf="#" onclick="removeItem('{{ $index }}')" class="btn btn-outline-light menurie-text">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                @if($cartItem['addons'])
                                <tr>
                                <th class="border-top-0"><span>{{$cartItem['name']}} Addons</span></th>
                                </tr>
                                @foreach($cartItem['addons'] as $addon)
                                @php
                                    $total += $addon['addonTotal'] * $cartItem['quantity'];
                                @endphp
                                <tr>
                                <td>{{ $addon['addonName'] }}</td>
                                    <td>
                                        {{-- <div class="d-flex">
                                            <span class="input-group-btn">
                                                <button type="button"
                                                    class="btn btn-outline-light btn-number minus rounded-left"
                                                    data-type="minus" data-field="quant2[1]">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                            <span> <input type="text" name="quant2[1]" id="{{$cartItem['id']}}-{{$addon['addonId']}}"
                                                    class="form-control input-number input-number1 p-0" value="{{ $addon['addonQuantity']}}" onchange="changeAddonQuantity({{$cartItem['id']}},{{$addon['addonId']}});if(parseInt(this.value,10)<10)this.value='0'+this.value;" min="01" max="9999"></span>
                                            <span class="input-group-btn">
                                                <button class="btn btn-outline-light btn-number plus rounded-right" data-type="plus" data-field="quant2[1]">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div> --}}
                                    </td>
                                <td class="text-center" ><b id="{{$cartItem['id']}}-{{$addon['addonId']}}-addonPrice">${{ $addon['addonTotal'] }}</b></td>
                                <td class="text-center">
                                    {{-- <a herf="#" onclick="removeAddon({{ $cartItem['id']}},{{$addon['addonId'] }})" class="btn btn-outline-light menurie-text">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a> --}}
                                </td>
                                </tr>
                                @endforeach
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="p-3">
                    <div class="row text-right">
                    <div class="col d-inline">
                        <span> Subtotal</span>
                    </div>
                    <div class="col d-inline">
                        <span id="subTotal">${{ number_format($total,2) }}</span>
                    </div>
                    </div>
                    <div class="row text-right">
                    <div class="col d-inline">
                        <span> Tax</span>
                    </div>
                    <div class="col d-inline">
                        <span id="taxTotal">${{ number_format($total * 0.089,2) }}</span>
                    </div>
                    </div>
                    <div class="row text-right">
                    <div class="col d-inline">
                        <span> Delivery Fee</span>
                    </div>
                    <div class="col d-inline">
                        <span>$0.00</span>
                    </div>
                    </div>
                    <div class="row text-right">
                    <div class="col d-inline">
                        <span> Service charge</span>
                    </div>
                    <div class="col d-inline">
                        <span>$0.00</span>
                    </div>
                    </div>
                    <div class="row text-right">
                    <div class="col d-inline">
                        <span> Total</span>
                    </div>
                    <div class="col d-inline">
                        <span id="total1">${{ number_format(($total * 0.089)+$total,2) }}</span>
                    </div>
                    </div>
                </div>
                <div class="text-center">
                <button class="btn btn-custom btn-cart-checkout w-auto"><a href="{{route('foodDetail')}}">Add More Items</a></button>
                    <button type="button" class="btn btn-custom btn-cart-checkout">
                        <a href="{{ route('checkout') }}"> Checkout </a></button>
                </div>
            @else
                <p class="text-center text-muted mb-5">No Item added in cart</p>
            @endif
            </div>
            </div>
        </div>
    </div>
    <br><br>
@endsection
@section('scripts')
    <script>
        $('.btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min'))
                input.val(currentVal - 1).change();
            // if(parseInt(input.val()) == input.attr('min')) {
                // $(this).attr('disabled', true);
            // }
        } else if(type == 'plus') {
            if(currentVal < input.attr('max'))
                input.val(currentVal + 1).change();
            // if(parseInt(input.val()) == input.attr('max')) {
                // $(this).attr('disabled', true);
            // }
        }
    } else
        input.val(0);
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
    function changeQuantity(e)
    {
        valueCurrent = $("#"+e).val();

        $.ajax({
            method: "GET",
            url: 'change/item/quantity/'+e+'/'+valueCurrent,
            mimeType: "multipart/form-data",
            contentType: false,
            success: function (data) {
                var json = JSON.parse(data);
                if (json.status == true) {
                    $("#"+e+"-price").html('$'+json.sub);
                    $("#taxTotal").html('$'+json.taxTotal);
                    $("#subTotal").html('$'+json.subTotal);
                    $("#total1").html('$'+json.total);
                }
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    }
    function changeAddonQuantity(x,y)
    {
        valueCurrent = $("#"+x+"-"+y).val();
        $.ajax({
            method: "GET",
            url: 'change/addon/quantity/'+x+'/'+y+'/'+valueCurrent,
            mimeType: "multipart/form-data",
            contentType: false,
            success: function (data) {
                var json = JSON.parse(data);
                if (json.status == true) {
                    $("#"+x+"-"+y+"-addonPrice").html('$'+json.sub);
                    $("#taxTotal").html('$'+json.taxTotal);
                    $("#subTotal").html('$'+json.subTotal);
                    $("#total1").html('$'+json.total);
                }
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    }
    function removeItem(e)
    {
        $.ajax({
            method: "GET",
            url: 'remove/item/'+e,
            mimeType: "multipart/form-data",
            contentType: false,
            success: function (data) {
                var json = JSON.parse(data);
                if (json.status == true)
                window.location.reload();
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    }
    function removeAddon(e,f)
    {
        $.ajax({
            method: "GET",
            url: 'remove/item/addon/'+e+'/'+f,
            mimeType: "multipart/form-data",
            contentType: false,
            success: function (data) {
                var json = JSON.parse(data);
                if (json.status == true)
                    window.location.reload();
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    }
    </script>
@endsection
