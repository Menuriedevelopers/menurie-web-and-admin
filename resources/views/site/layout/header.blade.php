<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta property="og:title" content="Menurie" />
        <meta property="og:type" content="jpg" />
        <meta property="og:url" content="https://www.dev.menurie.com/" />
        <meta property="og:image" content="https://www.dev.menurie.com/favicon_io/favicon-32x32.png" />
        @yield('styles')
        <title>@yield('title') - Menurie </title>
        <link rel="shortcut icon" href="favicon_io/favicon-32x32.png" type="image/x-icon">
        <link rel="stylesheet" href="{{ asset('site/asset/styles/style.css') }}">
        <style type="text/css">
            .dropdown-toggle::after {
            display: inline-block;
            margin-left: 0.255em;
            vertical-align: 0.255em;
            content: "";
            border-top: 0em solid;
            border-right: 0.3em solid transparent;
            border-bottom: 0;
            border-left: 0.3em solid transparent;
        }
        </style>
    </head>
    <body>
        {{-- @if(Request::path()!='checkout')
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Location</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="">
                                <div>
                                    <label for="location">Add new Location</label>
                                    <div>
                                        <img class="location-icon-1" data-target="#modalPoll-1" src="{{ asset('site/asset/images/location.png') }}"
                                        width="18" alt="">
                                       <input type="text" id="pac-input" class="location-input py-0 w-100 px-3">
                                        <hr>
                                        <div>
                                            <label for="current">Current Location</label>
                                            <div id="mapper" style="height:150px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif --}}
        <div>
            <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light bg-white">
                <a class="nav-link ml-lg-4 pl-lg-3 pl-0" href="{{ route('index') }}">
                    <img src="{{ asset('site/asset/images/img1.png') }}" width="65" height="65">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ml-lg-3 ml-md-3 ml-0">
                        @if(Request::path()!='checkout')
                        <div class="header-location">
                            <h5 class="m-0 good-heading text-left pl-1"><span id="daytime"></span> @auth('customer'){{Auth::guard('customer')->user()->first_name.'!'}}@endauth</h5>
                            {{-- <div class="d-flex">
                                <p class="m-0 header-location-text location-input" type="button" data-toggle="modal" data-target="#exampleModal" style="max-width: 215px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">{{ $location['name'] ?? 'Add location' }} </p>
                                <p>↓</p>
                            </div> --}}
                        </div>
                        @endif
                        <li class="nav-item dropdown bottom-list my-auto">
                        </li>
                    </ul>
                    <span class="navbar-text">
                        <ul class="navbar-nav mr-auto">
                            @auth('customer')
                            <li class="nav-item bottom-list my-auto">
                                <a class="nav-link" href="{{ route('user-orders') }}">
                                    <span>Orders</span>
                                </a>
                            </li> 
                            <li class="nav-item bottom-list my-auto">
                                <a class="nav-link" href="{{ route('userProfile') }}">
                                    <span>My Profile</span>
                                </a>
                            </li>
                            <li class="nav-item bottom-list my-auto">
                                <div class="dropdown">
                                  <a href="javascript:void(0)" class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration: none;">
                                    Settings
                                  </a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="height: 288px">
                                    <h4 class="ml-2" style="color: #8D5440">Account</h4>
                                    <a class="dropdown-item" href="{{ route('userProfile') }}">Edit Profile</a>
                                    <!-- The Modal -->
                                  <a href="javascript:void(0)" class="dropdown-item" data-toggle="modal" data-target="#myModal">
                                    Change Password
                                  </a>
                                    <a class="dropdown-item" href="#">Delete Account</a>
                                    <h4 class="ml-2" style="color: #8D5440">Application</h4>
                                    <a class="dropdown-item" href="#">Terms and Condition</a>
                                    <a class="dropdown-item" href="{{route('privacy')}}">Privacy Policy</a>
                                  </div>
                                </div>
                            </li>
                            <li class="nav-item bottom-list my-auto">
                                <a class="nav-link" href="/contact">
                                    <span>Help and Contact Us</span>
                                </a>
                            </li>
                            @endauth
                            <li class="nav-item bottom-list-last my-auto">
                                @auth('customer')
                                <a class="nav-link mr-5" href="{{ route('customer.logout') }}">Logout
                                </a>
                                <i class="fa fa-heart-o mr-1" style="font-size: 24px;margin-top: 9px;color: #8D5440;font-weight: 600;"></i>
                                @else
                                <a class="nav-link" href="{{ route('customerLogin') }}">
                                    <button type="button" class="btn btn-danger pb-2 b-rounded pb-0" style="margin-top: -4px;">
                                    Sign in</button>
                                </a>
                                @endauth
                                <div class="dropdown">
                                    <span class="last-icon-img my-auto pl-3 dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src="/site/asset/icons/shopping-cart.svg" width="20">
                                            @php
                                            $cartCount = [];
                                            if(session()->get('cart'))
                                                $cartCount = session()->get('cart');
                                            @endphp
                                            @if(count($cartCount)!=0)
                                            <div class="batch"><p class="m-0 cart-count">{{count($cartCount)}}</p></div>
                                            @endif
                                    </span>
                                    <div class="dropdown-menu cartModal" id="sideModalTR" aria-labelledby="dropdownMenuLink" style="width: 340px;left: -250px;">
                                        <a href="" class="dropdown-item"></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </span>
                </div>
            </nav>
            @auth('customer')<order-alert user_id="{{ auth('customer')->user()->id }}"></order-alert>@endauth
        </div>