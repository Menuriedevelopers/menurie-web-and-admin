@php
$location = session()->get('location');
@endphp
@if(Request::path() != '/')
@include('site.layout.header')
@endif
@yield('content')
@include('site.layout.footer')