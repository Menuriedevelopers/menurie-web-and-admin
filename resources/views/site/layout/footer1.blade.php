<div class="modal modal3 fade right cartModal" id="sideModalTR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-4 col-md-3">
                <span><img src="{{ asset('site/asset/images/img1.png') }}" width="80" alt=""></span><br><br><br>
                <p class="text-justify">
                    Lorem ipsum dolor sit amet. <br>
                    Lorem ipsum dolor sit amet. <br>
                    Lorem ipsum dolor sit amet. <br>
                </p><br><br>
                <ul class="social-icons">
                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="linkedin" href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
                </p>
            </div>
            <div class="col-lg-2 col-sm-3 col-md-2">
                <ul class="footer-links">
                    <li><a href="#"><b>ABOUT</b></a></li><br>
                    <li><a href="#">Home</a></li><br>
                    <li><a href="#">About Us</a></li><br>
                    <li><a href="#">Company</a></li><br>
                    <li><a href="#">Our Team</a></li><br>
                    <li><a href="#">Careers</a></li><br>
                    <li><a href="#">Manage Blogs</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-sm-3 col-md-2">
                <ul class="footer-links">
                    <li><a href="#"><b>CONTACT</b></a></li><br>
                    <li><a href="#">Health & Support</a></li><br>
                    <li><a href="#">Contact with Us</a></li><br>
                    <li><a href="#">Ride with us</a></li><br>
                </ul>
            </div>
            <div class="col-lg-2 col-sm-3 col-md-2">
                <ul class="footer-links">
                    <li><a href="#"><b>LEGAL</b></a></li><br>
                    <li><a href="#">Terms & conditions</a></li><br>
                    <li><a href="#">Refund & cancellation</a></li><br>
                    <li><a href="#">Company</a></li><br>
                    <li><a href="#">Our Team</a></li><br>
                    <li><a href="#">Careers</a></li><br>
                    <li><a href="#">Manage Blogs</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-xs-6 col-md-3">
                <ul class="footer-links">
                    <li><a href="#"><b>NEWS LETTER</b></a></li><br>
                    <div class="form-group input-icons">
                        <i class="icon">
                            <img src="{{ asset('site/asset/images/msg.png') }}" width="15" />
                        </i>
                        <input type="email" class="form-control newsletter-input"
                            class="footer-input-field" placeholder="Enter email" />
                    </div>
                    <button class="btn ">SEND</button>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; All Rights Reserved by
                </p>
            </div>
        </div>
    </div>
</footer>
<script>
    function initMap() {
        if("{{ ($location['lat'])}}")
        {
            var uluru = {lat: {{(double)$location['lat'] }}, lng: {{(double)$location['lng']}} };
            var map = new google.maps.Map(document.getElementById('mapper'), {
            center: uluru,
            zoom: 17
            });
            var marker = new google.maps.Marker({position: uluru, map: map});
        }
        else
        var map = new google.maps.Map(document.getElementById('mapper'));
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });
        autocomplete.addListener('place_changed', function() {
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          var lat = place.geometry.location.lat();
          var lng = place.geometry.location.lng();
          $.ajax({
              url:"/location/"+place.name+"/"+lat+"/"+lng,
              success: function(result){
                  console.log(result);
                  $(".location-input").html(place.name);
                  map.setCenter(place.geometry.location);
                  map.setZoom(20);
              }
          });
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(20);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true); 

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          /*infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;*/
          infowindow.open(map, marker);
        });
      }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWE8j2EUrrXSzy23EFZ1Y-PyUSjvccCFM&libraries=places&callback=initMap" async defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>    
<script>
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
    $(".cartModal").load('/cartModal');
    function plusNum() {
        var quantity = $("#item-quantity").val();
        quantity++;
        $("#item-quantity").val(quantity);
        $('.numBtn').html(quantity);
        var price = $("#item-price").val(); 
        var total = quantity * price;
        $("#itemTotal").text(total);
    }
    function minusNum() {
        var quantity = $("#item-quantity").val();
        if(quantity > 1)
        {
            quantity--;
            $("#item-quantity").val(quantity);
            $('.numBtn').html(quantity);
            var price = $("#item-price").val(); 
            var total = quantity * price;
            $("#itemTotal").text(total);
        }
    }

    function addonPlus(e,i) {
        var quantity = $("#"+e+"-"+i).val();
        quantity++;
        $("#"+e+"-"+i).val(quantity);
        $("#quantity-"+i).text(quantity);
        var price = $("#addon-price-"+i).val();
        var total = quantity * price;
        $("#price-"+i).text("$"+total);
    }
    function addonMinus(e,i) {
        var quantity = $("#"+e+"-"+i).val();
        if(quantity > 0 )
        {
            quantity--;
            $("#"+e+"-"+i).val(quantity);
            $("#quantity-"+i).text(quantity);
            if(quantity != 0)
            {
                var price = $("#addon-price-"+i).val();
                var total = quantity * price;
                $("#price-"+i).text("$"+total);
            }
        }
    }

    $(".cart-button").on('click',function(){
        var id = $(this).attr('data-id');
        $('#content').load("/item/"+id, function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
        $("#modalCart").modal('show');
        if(statusTxt == "error")
        window.location.reload();
        });
    });

    $('body').on('click','.close span',function(){
        console.log('closed');
        $(".cartModal").css('display','none');
        $(".modal-backdrop").removeClass('show');
    });

    $(document).on('submit','.item-form',function(){
        event.preventDefault();
        var link = '{{ route('customer.addToCart') }}';
        $.ajax({
                method: "POST",
                url: link,
                data: new FormData(this),
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $(".add-to-cart-btn").attr("disabled", true);
                }, success: function (data) {
                    $("button[type=submit]").attr("disabled", false);
                    $(".add-to-cart-btn").html('Item Added!')
                    $(".add-to-cart-btn").css("disabled", false);
                    var json = JSON.parse(data);
                    console.log(json.status);
                    if (json.status == true) {
                        $(".cartModal").load('/cartModal');
                        $("#modalCart").modal('hide');
                        $(".cartModal").modal('show');
                    }
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }
            });    
    });
</script>
@yield('scripts')
</body>
</html>
