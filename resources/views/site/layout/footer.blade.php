<div class="modal modal-cart fade" id="modalCart" tabindex="-1" role="dialog">
    <div id="content" class="modal-dialog modal-dialog2 modal-dialog5" role="document">
    </div>
</div>
@auth('customer')
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form method="POST" action="{{ route('change.password') }}">
            @csrf 
             @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
             @endforeach 
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">Current Password</label>
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control border" name="current_password" autocomplete="current-password">
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>
                <div class="col-md-6">
                    <input id="new_password" type="password" class="form-control border" name="new_password" autocomplete="current-password">
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>
                <div class="col-md-6">
                    <input id="new_confirm_password" type="password" class="form-control border" name="new_confirm_password" autocomplete="current-password">
                </div>
            </div>
            <input type="hidden" name="id" value="{{ auth('customer')->user()->id }}">
            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Update Password
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endauth
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-4 col-md-6">
                <a href="{{route('index')}}"><span><img src="{{ asset('site/asset/images/img1.png') }}" width="80" alt=""></span></a><br><br><br>
                <p class="text-justify">
                    Enjoy every meal <br>
                    Order with Menurie <br>
                </p><br><br>
                <ul class="social-icons">
                    {{-- <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="linkedin" href="#"><i class="fa fa-instagram"></i></a></li> --}}
                </ul>
                </p>
            </div>
            <div class="col-lg-2 col-sm-3 col-md-6">
                <ul class="footer-links">
                    <li><a href="{{route('index')}}">Home</a></li><br>
                    <li><a href="{{route('about')}}">About Menurie</a></li><br>
                    <!-- <li><a href="#">Our Team</a></li><br>
                    <li><a href="#">Careers</a></li><br>
                    <li><a href="#">Manage Blogs</a></li> -->
                </ul>
            </div>
            <div class="col-lg-2 col-sm-3 col-md-6">
                <ul class="footer-links">
                    <li><a href="{{route('contact')}}">Contact us</a></li><br>
                    <li><a href="{{route('privacy')}}">Privacy Policy</a></li><br>
                    <li><a href="{{route('term')}}">Terms and Conditions</a></li><br>
                    {{-- <li><a href="{{ route('chef.register') }}">Cook with us</a></li><br>
                    <li><a href="{{ route('rider.register') }}">Ride with us</a></li><br> --}}
                </ul>
            </div>
            <div class="col-lg-4 col-xs-6 col-md-12">
                <ul class="footer-links">
                    <li><a href="#"><b style="color: #8D5440;">NEWS LETTER</b></a></li><br>
                    <div class="row m-0">
                      <div class="text-left pr-3">
                        <div class="form-group input-icons">
                            <i class="icon">
                                <img src="{{ asset('site/asset/images/msg.png') }}" width="15" />
                            </i>
                            <input type="email" class="form-control newsletter-input pl-5"
                                class="footer-input-field" placeholder="Enter email" />
                        </div>
                      </div>
                      <div class="text-right">
                        <button class="btn ">Submit</button>
                      </div>
                    </div>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; Menurie inc.</p>
            </div>
        </div>
    </div>
</footer>
@php
$lat = $location['lat']?? 0;
$lng = $location['lng']?? 0;
@endphp
<script> 
    function initMap() {
        var lat1 = parseFloat("{{$lat}}");
        var lng1 = parseFloat("{{$lng}}");
        if(lat1 != 0)
        {
            var uluru = {lat: lat1, lng: lng1 };
            var map = new google.maps.Map(document.getElementById('mapper'), {
            center: uluru,
            zoom: 17
            });
            var marker = new google.maps.Marker({position: uluru});
        }
        else
        var map = new google.maps.Map(document.getElementById('mapper'));
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });
        autocomplete.addListener('place_changed', function() {
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          var lat = place.geometry.location.lat();
          var lng = place.geometry.location.lng();
          $.ajax({
              url:"/location/"+place.name+"/"+lat+"/"+lng,
              success: function(result){
                  console.log(result);
                  $(".location-input").html(place.name);
                  map.setCenter(place.geometry.location);
                  map.setZoom(20);
              }
          });
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(18);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });
      }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLgNOQKcWZoxR_NqxLHYQPcbzjrw5EjRU&libraries=places&callback=initMap" async defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script>
    $(function(){
        getLocation();
    });
    function getLocation() {
        var lat1 = parseFloat("{{$lat}}");
        var lng1 = parseFloat("{{$lng}}");
        if(lat1 == 0 && lng1 == 0){
            if (navigator.geolocation)
                navigator.geolocation.getCurrentPosition(showPosition);
        }
    }

    function showPosition(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        const geocoder = new google.maps.Geocoder();
        geocodeLatLng(geocoder, lat, lng);
        
    }
    function geocodeLatLng(geocoder, lat, lng) {
        const latlng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng),
        };

        geocoder
            .geocode({ location: latlng })
            .then((response) => {
                var name = response.results[0].formatted_address;
                $.ajax({
                    url:"/location/"+name+"/"+lat+"/"+lng,
                    success: function(result){
                        $(".location-input").html(name);
                        // $(".location-section-text").html(name);
                        window.location.reload;
                    }
                });
            })
            .catch((e) => console.log(e));
    }
</script>
@auth('customer')
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
  auth: {
    headers: {
      'X-CSRF-Token': "{{ csrf_token() }}"
    }
  },
  cluster: 'ap2',
  forceTLS: true
});
// Pusher.logToConsole = true;
var channel = pusher.subscribe('order-alert');
channel.bind('App\\Events\\OrderStatusChanged', function(data) {
  if(data.customer_id == "{{auth()->guard('customer')->user()->id}}")
  {
      var notify = new Notification('Menurie', {
         body: 'Thanks for placing the order with Menurie!',
         icon: 'https://www.menurie.com/site/asset/images/img1.png',
     });
     notify.onclick = function(){
       event.preventDefault();
       window.open('/order/'+data.id, '_blank');
     };
  }
});
</script>
@endauth
<script>
    /*const tz = Intl.DateTimeFormat().resolvedOptions().timeZone;
    const str = new Date(new Date().toLocaleString('en-US', { timeZone: tz }));
    const hours = str.getHours();
        if( hours > 22 && hours <= 24 || hours >= 1 && hours < 6 )
        $("#daytime").text('Good night');
        if(hours > 16 && hours < 22)
        $("#daytime").text('Good evening');
        if(hours > 6 && hours < 16)
        $("#daytime").text('Good afternoon');*/

    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
    $(".cartModal").load('/cartModal');

    function plusNum()
    {
        var quantity = $("#item-quantity").val();
        quantity++;
        $("#item-quantity").val(quantity);
        $('.numBtn').html(quantity);
        var price = $("#item-price").val();
        var total = (quantity * price).toFixed(2);
        $("#itemTotal").text(total);
    }

    function minusNum()
    {
        var quantity = $("#item-quantity").val();
        if(quantity > 1)
        {
            quantity--;
            $("#item-quantity").val(quantity);
            $('.numBtn').html(quantity);
            var price = $("#item-price").val();
            var total = (quantity * price).toFixed(2);
            $("#itemTotal").text(total);
        }
    }

    function addonPlus(e,i)
    {
        var quantity = $("#"+e+"-"+i).val();
        quantity++;
        $("#"+e+"-"+i).val(quantity);
        $("#quantity-"+i).text(quantity);
        var price = $("#addon-price-"+i).val();
        var total = (quantity * price).toFixed(2);
        $("#price-"+i).text("$"+total);
    }
    
    function addonMinus(e,i)
    {
        var quantity = $("#"+e+"-"+i).val();
        if(quantity > 0 )
        {
            quantity--;
            $("#"+e+"-"+i).val(quantity);
            $("#quantity-"+i).text(quantity);
            if(quantity != 0)
            {
                var price = $("#addon-price-"+i).val();
                var total = (quantity * price).toFixed(2);
                $("#price-"+i).text("$"+total);
            }
        }
    }

    $(".cart-button").on('click',function(){
        var id = $(this).attr('data-id');
        $('#content').load("/item/"+id, function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
        $("#modalCart").modal('show');
        if(statusTxt == "error")
        window.location.reload();
        });
    });

    $('body').on('click','.close',function(){
        console.log('closed');
        $(".cartModal").css('display','none');
        $('.cartModal').removeClass('show');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    });

    $(document).on('submit','.item-form',function(){
        event.preventDefault();
        var link = '{{ route('customer.addToCart') }}';
        $.ajax({
                method: "POST",
                url: link,
                data: new FormData(this),
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $(".add-to-cart-btn").attr("disabled", true);
                }, success: function (data) {
                    console.log(data);
                    $("button[type=submit]").attr("disabled", false);
                    $(".add-to-cart-btn").html('Item Added!')
                    $(".add-to-cart-btn").css("disabled", false);
                    var json = JSON.parse(data);
                    if (json.status == true) {
                        $(".cart-count").text(json.count);
                        $(".cartModal").load('/cartModal');
                        $("#modalCart").modal('hide');
                        $("#sideModalTR").toggle();
                    }
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }
            });
    });

    function toggle()
    {
        event.preventDefault();
        $("#sideModalTR").toggle();
    }

    function removeItem(e)
    {
        $.ajax({
            method: "GET",
            url: 'remove/item/'+e,
            mimeType: "multipart/form-data",
            contentType: false,
            success: function (data) {
                var json = JSON.parse(data);
                if (json.status == true)
                $(".cartModal").load('/cartModal');
                    // window.location.reload();
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    }
</script>
@yield('scripts')
</body>
</html>
