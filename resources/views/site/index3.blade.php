@extends('site.layout.base')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('site/asset/styles/style.css') }}">
    <!-- <link rel="stylesheet" href="{{asset('site/asset/styles/flickity-docs.css?6')}}" media="screen"> -->
    <!-- <link rel="stylesheet" href="{{ asset('site/asset/styles/infinite-slider.css') }}"> -->
    <!-- Slick CSS -->

<style>
 .slide {
    margin-left: 20px;
 }
</style>
    <title>Menurie</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light" id="headnev">
    <a class="navbar-brand" href="{{ route('index') }}">
            <img src="{{ asset('site/asset/images/img1.png') }}" width="65" alt="svg">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <span class="navbar-text">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item bottom-list">
                        <a class="nav-link" href="{{ route('customerRegister') }}">
                            <span>Become a chef</span>
                        </a>
                    </li>
                    
                    <li class="nav-item bottom-list">
                        <a class="nav-link" href="{{ route('rider.register') }}">
                            <span>Become a Driver</span>
                        </a>
                    </li>
                    @auth('customer')
                    <li class="nav-item bottom-list">
                    <a class="nav-link" href="{{ route('userProfile') }}">
                            <span>My Profile</span>
                        </a>
                    </li>
                    @endauth
                    <li class="nav-item bottom-list-last">
                        @auth('customer')
                        <a class="nav-link" href="{{ route('customer.logout') }}">
                            <button type="button" class="btn btn-danger">Logout</button>
                        </a>
                        @else
                    <a class="nav-link" href="{{ route('customerLogin') }}">
                            <button type="button" class="btn btn-danger">
                                <img src="{{ asset('site/asset/images/signin.png') }}" width="12">
                            Sign In</button>
                        </a>
                        @endauth
                        <span class="last-icon-img">
                            <img src="/site/asset/images/lastnavicon.png " data-toggle="modal"
                                data-target="#sideModalTR" width="15">
                        </span>
                    </li>
                </ul>
            </span>
        </div>
    </nav>
    <!---------------------------End of Navbar------------------------>
    <!---------Modal--------------->
    <!------------First Modal--------->
    <div class="modal modal1 fade left" id="modalPoll-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true" data-backdrop="false">
        <div class="modal-dialog modal-dialog1   modal-left modal-notify modal-info" role="document"
            style="height: 680px;">
            <div class="modal-content modal-content1">
                <div>
                    </p>
                    <!-- <button type="button" class="close-modal1" data-dismiss="modal" aria-label="Close"> -->
                    <div class="cross-modal1"> <i class="fa fa-times" class="close-modal1" data-dismiss="modal"
                            aria-label="Close" aria-hidden="true"></i>
                    </div><!-- <span aria-hidden="true" >×</span> -->
                    <!-- </button> -->
                </div>
                <br><br>
                <div class="modal-body modal-body1">
                    <div class="container">
                        <div class="row">
                            <div class="col h2"
                                style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                                Search by Location
                            </div>
                        </div>
                        <br><br>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col ">
                                <div class="form-group input-icons">
                                    <!-- <span> -->
                                    <i class="icon fa fa-search " id="search-btn">
                                    </i>
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Search for an area..."
                                        onfocus="removeContent()" />
                                    <div class="map-img" id="map-img">
                                        <img src="/site/asset/images/map.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><br><br>
                    <div class="container-fluid">
                        <div class="row ">
                            <div class="col-6 col-md-8 col-sm-8 text-secondary">
                                RECENT LOCATION
                            </div>
                            <div class="col-6 col-md-4 col-sm-4 clear">
                                <a href="" style="color: rgb(141,64,84) !important;">clear</a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <i class="fa fa-map-marker"></i>
                                <b> 36 PARK AVE,NEW YORK </b><br>
                                <span class="text-secondary"> United States of America</span>
                            </div>

                        </div>
                    </div>
                    <br>

                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <i class="fa fa-map-marker"></i>
                                <b> 36 PARK AVE,NEW YORK </b><br>
                                <span class="text-secondary"> United States of America</span>

                                <!-- United States of America -->
                            </div>

                        </div>
                    </div>
                    <br>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <i class="fa fa-map-marker"></i>
                                <b> 36 PARK AVE,NEW YORK </b><br>
                                <span class="text-secondary"> United States of America</span>

                                <!-- United States of America -->
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Modal: modalPoll -->
    </div>
    <div class="modal modal-cart fade" id="modalCart" tabindex="-1" role="dialog">
        <div id="content" class="modal-dialog modal-dialog2 " role="document">
        </div>
    </div>
    <div class="modal modal3 fade right cartModal" id="sideModalTR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div> 
    </div>
    </div>
    <div class="bottom-section">
        <div class="header-first-img">
            <img src="{{ asset('site/asset/images/header12.png') }}" width="240">
        </div>
        <div class="header-search-wrapper">
            <h1>Find Favorite <span style="color: rgb(141,84,64);">Food</span></h1>
            <div class="header-location">
                <img class="location-icon" data-target="#modalPoll-1" src="{{ asset('site/asset/images/location.png') }}"
                        width="18" alt="">
                <span>
                    <input type="text" id="pac-input" class="location-input" placeholder="Lets start with you location">
                </span>
            </div>
            <br><br><br>
            <div class="header-input-wrapper">
                <input class="mx-sm-0 mb-0" type="password" class="form-control" id="inputPassword2"
                    placeholder="Search your favorite food">
                <button type="submit" class="btn  mb-2">SEARCH</button>
            </div>
        </div>
        <div class="header-last-img ">
            <img src="{{ asset('site/asset/images/header2.png') }}" width="240">
        </div>
    </div>
    <div class="second-header-section">
        <div class="container">
            <div class="row">
                <div class="col application-heading">
                    <h1>Download Our New Application</h1>
                </div>
                <div class="col app-section">
                    <div class="app-store">
                        <img src="{{ asset('site/asset/images/play-store.png') }}" width="150"><br><br>
                        <img src="{{ asset('site/asset/images/app-store.png') }}" width="150">
                    </div>
                    <div class="second-header-mobile">
                        <img src="{{ asset('site/asset/images/mobile.png') }}" height="290">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="container mt-3">
        <div class="row">
            <div class="col h3 ">
                Trending Dishes
            </div> 
            <div class="col-sm-6 col-xs-2 col-md-4 col-lg-3 col-xl-3 text-right">
                <a class="view-btn" href="{{ route('foodDetail') }}">See What's trending</a></div>
        </div>
    </div>
    <br><br>
    <div class="container">
        <div class="col">
            @foreach($sItems as $item) 
            <div class="col-md-3 col-sm-4 col-xs-12 mb-4">
                <div class="card" style="min-height: 450px;max-height:450px;">
                    <div class="card-header" style="background:white; border:none;">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-8">
                                <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                <i class="fa fa-heart-o" style="cursor: pointer;" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="margin-left:0px;width: 95%;">
                        <img width="200" height="200" src="{{asset($item->item_image)}}" class="rounded-circle card-img-top" alt="...">
                    </div>
                    <div class="card-footer text-center" style="background: white;border:none">
                        <h5 class="">{{$item->item_name}}</h5>
                        <p class="">by {{$item->chef->first_name.' '.$item->chef->last_name}} <br>${{ $item->price }}</p>
                        <button data-id="{{ $item->id }}" class="cart-button" ><i class="fa fa-plus" style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                        </button>
                    </div>
                </div> 
            </div>
            @endforeach
            <!-- hi -->
            <div style="outline: none;" class="carousel" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;prevNextButtons&quot;: false, &quot;pageDots&quot;: false }">
        <div class="col-md-4 col-lg-3 col-sm-6 col-xs-12 mb-4">
              <div id="round">
                 <div class="card">
                    <div class="card-header" style="background:#8d5440; border:none; border-radius:20px 20px 0 0;">
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-8 col-sm-8 col-8">
                                <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                <i class="fa fa-heart-o" style="color: #fff; cursor: pointer;" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center" style="margin-left:0px;width: 100%; background-color: #8d5440;min-height: auto;">
                        <img height="150" width="150" src="./food4.png" alt="...">
                    </div>
                    <div class="card-footer text-center" style="background: white; border-radius: 0 0 20px 20px">
                        <h5 class="">Handsome </h5>
                        <p class="text-muted"> By Novu   </p>
                        <p style="font-weight: 500; margin-top: -10px;"> $50  </p>
                        <button data-id="zw" class="cart-button" ><i class="fa fa-plus" style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                        </button>
                    </div>
                </div> 
            </div>
      </div>
    
        <div class="col-md-4 col-lg-3 col-sm-6 col-xs-12 mb-4">
              <div id="round">
                 <div class="card">
                    <div class="card-header" style="background:#8d5440; border:none; border-radius:20px 20px 0 0;">
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-8 col-sm-8 col-8">
                                <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                <i class="fa fa-heart-o" style="color: #fff; cursor: pointer;" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center" style="margin-left:0px;width: 100%; background-color: #8d5440;min-height: auto;">
                        <img height="150" width="150" src="./food4.png" alt="...">
                    </div>
                    <div class="card-footer text-center" style="background: white; border-radius: 0 0 20px 20px">
                        <h5 class="">Handsome </h5>
                        <p class="text-muted"> By Novu   </p>
                        <p style="font-weight: 500; margin-top: -10px;"> $50  </p>
                        <button data-id="zw" class="cart-button" ><i class="fa fa-plus" style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                        </button>
                    </div>
                </div> 
            </div>
      </div>
    
        <div class="col-md-4 col-lg-3 col-sm-6 col-xs-12 mb-4">
              <div id="round">
                 <div class="card">
                    <div class="card-header" style="background:#8d5440; border:none; border-radius:20px 20px 0 0;">
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-8 col-sm-8 col-8">
                                <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                <i class="fa fa-heart-o" style="color: #fff; cursor: pointer;" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center" style="margin-left:0px;width: 100%; background-color: #8d5440;min-height: auto;">
                        <img height="150" width="150" src="./food4.png" alt="...">
                    </div>
                    <div class="card-footer text-center" style="background: white; border-radius: 0 0 20px 20px">
                        <h5 class="">Handsome </h5>
                        <p class="text-muted"> By Novu   </p>
                        <p style="font-weight: 500; margin-top: -10px;"> $50  </p>
                        <button data-id="zw" class="cart-button" ><i class="fa fa-plus" style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                        </button>
                    </div>
                </div> 
            </div>
      </div>
    
        <div class="col-md-4 col-lg-3 col-sm-6 col-xs-12 mb-4">
              <div id="round">
                 <div class="card">
                    <div class="card-header" style="background:#8d5440; border:none; border-radius:20px 20px 0 0;">
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-8 col-sm-8 col-8">
                                <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                <i class="fa fa-heart-o" style="color: #fff; cursor: pointer;" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center" style="margin-left:0px;width: 100%; background-color: #8d5440;min-height: auto;">
                        <img height="150" width="150" src="./food4.png" alt="...">
                    </div>
                    <div class="card-footer text-center" style="background: white; border-radius: 0 0 20px 20px">
                        <h5 class="">Handsome </h5>
                        <p class="text-muted"> By Novu   </p>
                        <p style="font-weight: 500; margin-top: -10px;"> $50  </p>
                        <button data-id="zw" class="cart-button" ><i class="fa fa-plus" style="color: rgb(141, 84, 64);" aria-hidden="true"></i> Add
                        </button>
                    </div>
                </div> 
            </div>           
          </div>
        
    </div>   <!-- dsds -->
    </div>
</div>
    <div class="container mt-3">
        <div class="row">
            <div class="col h3 ">
                Best Dishes
            </div>
            <div class="col-sm-6 col-xs-2 col-md-4 col-lg-3 col-xl-3 text-right">
                <a class="view-btn" href="{{ route('foodDetail') }}">All our best dishes</a></div>
        </div>
    </div>
    <br><br> 
    <div class="container">
        <div class="row">
            @foreach($items as $item) 
            <div class="card item-card card-bottom-list mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4 col-sm-4 text-center">
                        <img class="p-3 rounded-circle" src="{{ asset($item->item_image) }}" width="150" height="150">
                    </div>
                    <div class="col-md-8 col-sm-6">
                        <div class="card-body ">
                            <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                            <p class="card-text">
                                <div class="overflow font-weight-bold ">{{$item->item_name }}</div>
                            <div class="overflow text-secondary"> by {{ $item->chef->first_name.' '.$item->chef->last_name }}</div>
                                <div class="text-right">
                                <div class="text-secondary font-weight-bold"> ${{ $item->price }}</div>
                                <button data-id="{{ $item->id }}" class="cart-button">
                                    <i class="fa fa-plus" style="color: rgb(141, 84, 64);" aria-hidden="true"></i>
                                    Add </button>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div> 
            @endforeach
        </div>
    </div>
    <!-------------------Trending Section--------------------------------------->
    <br><br>
    <div class="trending-section-wrapper">
        <div class="container ">
            <div class="row trending-heading">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-3 h3">
                    Trending Chefs
                </div>
            </div>
        </div>
        <br><br>
        <div class="container">
            <section style="outline: none;" class="carousel" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;prevNextButtons&quot;: false, &quot;pageDots&quot;: false }">
              <div class="slide">
                <div class="list1">
                    <div class="list1-img mt-1 p-2">
                        <img src="{{ asset('site/asset/images/trending1.png') }}" width="60">
                    </div>
                    <div class="list1-name mt-3 p-2">
                        <span class="header-text1"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span> <br>
                        <div class="h6"> <a href="{{ route('breakFast') }}">Rebecca Doe</a></div>
                    </div>
                </div>
              </div>
              <div class="slide">
                <div class="list1">
                    <div class="list1-img mt-1 p-2">
                        <img src="{{ asset('site/asset/images/trending1.png') }}" width="60">
                    </div>
                    <div class="list1-name mt-3 p-2 p-2">
                        <span class="header-text1"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span> <br>
                        <div class="h6"> <a href="{{ route('breakFast') }}">Rebecca Doe</a></div>
                    </div>
                </div>
              </div>
              <div class="slide">
                <div class="list1">
                    <div class="list1-img mt-1 p-2">
                        <img src="{{ asset('site/asset/images/trending1.png') }}" width="60">
                    </div>
                    <div class="list1-name mt-3 p-2">
                        <span class="header-text1"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span> <br>
                        <div class="h6"> <a href="{{ route('breakFast') }}">Rebecca Doe</a></div>
                    </div>
                </div>
              </div>
              <div class="slide">
                <div class="list1">
                    <div class="list1-img mt-1 p-2">
                        <img src="{{ asset('site/asset/images/trending1.png') }}" width="60">
                    </div>
                    <div class="list1-name mt-3 p-2">
                        <span class="header-text1"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span> <br>
                        <div class="h6"> <a href="{{ route('breakFast') }}">Rebecca Doe</a></div>
                    </div>
                </div>
              </div>
              <div class="slide">
                  <div class="list1">
                    <div class="list1-img mt-1 p-2">
                        <img src="{{ asset('site/asset/images/trending1.png') }}" width="60">
                    </div>
                    <div class="list1-name mt-3 p-2">
                        <span class="header-text1"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span> <br>
                        <div class="h6"> <a href="{{ route('breakFast') }}">Rebecca Doe</a></div>
                    </div>
                </div>
              </div>
              <div class="slide">
                  <div class="list1">
                    <div class="list1-img mt-1 p-2">
                        <img src="{{ asset('site/asset/images/trending1.png') }}" width="60">
                    </div>
                    <div class="list1-name mt-3 p-2">
                        <span class="header-text1"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span> <br>
                        <div class="h6"> <a href="{{ route('breakFast') }}">Rebecca Doe</a></div>
                    </div>
                </div>
              </div>
              <div class="slide">
                  <div class="list1">
                    <div class="list1-img mt-1 p-2">
                        <img src="{{ asset('site/asset/images/trending1.png') }}" width="60">
                    </div>
                    <div class="list1-name mt-3 p-2">
                        <span class="header-text1"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span> <br>
                        <div class="h6"> <a href="{{ route('breakFast') }}">Rebecca Doe</a></div>
                    </div>
                </div>
              </div>
              <div class="slide">
                  <div class="list1">
                    <div class="list1-img mt-1 p-2">
                        <img src="{{ asset('site/asset/images/trending1.png') }}" width="60">
                    </div>
                    <div class="list1-name mt-3 p-2">
                        <span class="header-text1"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span> <br>
                        <div class="h6"> <a href="{{ route('breakFast') }}">Rebecca Doe</a></div>
                    </div>
                </div>
              </div>
            </section>
          </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('site/asset/scripts/home.js') }}"></script>
    <script src="{{ asset('site/asset/scripts/script.js') }}"></script>
<script src="{{asset('site/asset/scripts/flickity-docs.min.js?2')}}"></script>
    <script>
        $(document).ready(function(){
        
        });
        function removeContent() {
            var a = document.getElementById('map-img').style.display = "none";
            var b = document.getElementById('search-btn').style.display = "none";
        }
    </script>
@endsection 