@extends('site.layout.base')
@section('title','Privacy Policy')
@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('site/asset/styles/cart.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/contact-us.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<style type="text/css">
    nav{
        display: none!important;
    }
</style>
@endsection
@section('Orders - Menurie')
@section('content')
<section id="privacy-mainpage">
    <div class="container">
        <div class="row">
            <div class="col-12 privacy-page">
                <h5>Menurie, Inc. Privacy Policy</h5>

                <p>We, menurie are committed to protecting the privacy of all users of our website menurie.com and evrymeal.com or mobile applications (together, the "Sites"). Please read the following privacy policy that explains how we use and protect your information. We are the "controller" of the information we process, unless otherwise stated.</p>

                <h5>Contact Details:</h5>
                <p>If you have any questions or requests concerning this privacy policy or how we handle your data more generally, you can get in touch with us using by contacting our general customer services team.</p>

                <h5>How We Collect Your Information:</h5>
                <p>We collect your personal information when you interact with us or use our services, such as when you use our Sites to place an order. We also look at how visitors use our Sites, to help us improve our services and optimise customer experience.</p>

                <h5>We collect information:</h5>
                <p>When you create an account with us or you change your account settings; when you place an order with us and during the order process (including for payment and order delivery); when you give us your consent to contact you via email, phone, post, message, push notification, or via our chat function, to send you marketing campaigns, tell you about Menurie initiatives, or to invite you to participate in surveys about our services, or our partners’ services; when you contact us directly via email, phone, post, message or via our chat function; and when you browse and use our Sites (before and after you create an account with us). We also collect information from third party sites, such as advertising and social media platforms and our fraud detection provider. If you link your Social Media or your third-party accounts to us, we will keep a record of your Social Media handle, and the other information that is made available to us according to your Social Media account settings. If your employer signs up for Menurie for Business, we may receive your contact details from your employer to enable us to provide our services to you.
                </p>

                <h5>Information We Collect From You</h5>
                <p>As part of our commitment to the privacy of our customers and visitors to our Sites more generally, we want to be clear about the sorts of information we will collect from you.</p>
                <p>When you visit the Sites or make a Menurie order through the Sites, including any partner’s website we work with for providing delivery services, you are asked to provide information about yourself including your name, contact details, delivery address, order details, loyalty scheme details where applicable, and payment information such as credit or debit card information. We will also collect information from you when you contact our riders using the chat function on our Sites. We may also collect your date of birth to verify your age when you purchase age restricted items.</p>
                <p>We also collect information about your usage of the Sites and information about you from any messages you post to the Sites or when you contact us or provide us with feedback, including via e-mail, letter, phone or chat function. If you contact us by phone, we record the call for training and service improvement purposes, and make notes in relation to your call. We collect technical information from your mobile device or computer, such as its operating system, the device and connection type and the IP address from which you are accessing our Sites.</p>
                <p>We also collect technical information about your use of our services through a mobile device, for example, carrier, location data and performance data such as mobile payment methods, interaction with other retail technology such as use of NFC Tags, QR Codes and/or use of mobile vouchers. Unless you have elected to remain anonymous through your device and/or platform settings, this information may be collected and used by us automatically if you use the service through your mobile device(s) via any Menurie mobile application, through your mobile's browser or otherwise.</p>
                <p>We may process health information about you only where you volunteer and consent to this, for example if you report any specific food allergies after placing an order.</p>
                <h5>Use Of Your Information</h5>
                <p>We will only process the data we collect about you if there is a reason for doing so, and if that reason is permitted under data protection law.</p>
                <p>We use your information where we need to in order to provide you with the service you have requested or to enter into a contract. You do not have to provide this information to us, but we may be unable to provide our services to you without the following information:</p>
                <p>to enable us to provide you with access to the relevant parts of the Sites, we collect necessary cookies from your device (see our Cookie Policy for further information, including on other types of cookies that we use);to supply the services you have requested, we collect your name, contact details, delivery address and order details ;to enable us to collect payment from you, we collect your credit or debit card information; and to contact you where necessary concerning our services, such as to resolve issues you may have with your order, we collect the information listed above and any additional information we may need to resolve your issue. We also process your data where we have a legitimate interest for doing so - for example personalisation of our service, including processing data to make it easier and faster for you to place orders. We have listed these reasons below:</p>
                <p>to improve the effectiveness and quality of service that our customers can expect from us in the future; to tailor content that we or our partners display to you, for example so that we can show you partners which are in your area or make sure you see the advertising which is most relevant to you, based on characteristics determined by us; to enable our customer support team to help you with any enquiries or complaints in the most efficient way possible and to provide a positive customer experience; to contact you for your views and feedback on our services or our partners’ services and/or products and to notify you if there are any important changes or developments to the Sites or our services, including letting you know that our services are operating in a new area, where you have asked us to do so; to send you information by post about our products, services, promotions and Menurie initiatives (if you do not want to receive these, you can let us know by getting in touch (see Contact Details)); to analyse your activity on the Sites so that we can administer, support, improve and develop our business and for statistical and analytical purposes and to help us to prevent fraud; and to detect, investigate, report and seek to prevent fraud or crime. We also process your data to enforce our contractual terms with you and any other agreement, and for the exercise or defence of legal claims and to protect the rights of Deliveroo, our partners, riders, or others (including to prevent fraud).</p>
                <p>If you submit comments and feedback regarding the Sites and the services, we may use such comments and feedback on the Sites and in any marketing or advertising materials. We will only identify you for this purpose by your first name and the city in which you live such comments and feedback may be shared with our partners to assess and improve their services. We will also analyse data about your use of our services from your location data to create profiles relating to you and for you. This means that we may make certain assumptions about what you may be interested in and use this, for example, to send you more tailored marketing communications, to present you with partners that we think you will prefer, or to let you know about special offers or products which we think you may be interested in (including Deliveroo for Business). This activity is referred to as profiling. You have certain rights in relation to this type of processing. Please see the Your Rights section below for more information. We may also use your information to comply with any legal obligation or regulatory requirement to which we are subject.
                </p>
                <h5>Menurie For Business</h5>
                <p>We also process your information to determine whether you may be interested in hearing about our Menurie  for Business service and, if your employer signs up for Menurie  for Business, to make this service available to you. Where we think you are using your Menurie  account for business purposes and your company may be interested in our Menurie for Business service, where appropriate, we may contact you (by email or telephone) to let you know about this service. We do this as it's in our legitimate business interests. You have the right to object to receiving these types of communications, which you can do by responding to our emails to unsubscribe or by contacting us using the contact details in this privacy policy. If your employer signs up for Menurie for Business, we will contact you to let you know that the Menurie for Business service is available to you. If you would like to take up your employer’s offer to use Menurie  for Business, we will tag your Menurie  account as having a Menurie  for Business allowance. For more information, please contact your employer. When you use Menurie  for Business, we will process your information for the purposes set out in the Use of Your Information section above. We will also share personal data relating to your order (such as the order date and time, the payment amount and the partner with which the order was placed) with your employer.</p>

                <h5>Cookies</h5>
                <p>You can set your browser to refuse all or some browser cookies, or to alert you when websites set or access cookies. If you disable or refuse cookies, please note that some parts of the Site may become inaccessible or not function properly.</p>

                <h5>Marketing</h5>
                <p>Where you have given your consent or where we have a legitimate interest for doing so (and are permitted to do so by law) we will use your information to let you know about our other products and services, or Menurie initiatives that may be of interest to you and we may contact you to do so by email, post, phone, or push notification.</p>
                <p>We use online advertising to keep you aware of what we’re up to and to help you see and find our products.</p>

                <p>You may see Menurie banners and ads when you are on other websites and apps, such as Social Media. We manage this through a variety of digital marketing networks. We also use a range of advertising technologies. The banners and ads you see are based on information we hold about you, or your previous use of Menurie (for example, your Menurie order history) or on Menurie banners or ads you have previously clicked on. We use your customer’s profile to send you communications that are the most relevant to you. You have certain rights in relation to this type of processing. Please see the Your Rights section below for more information.</p>
                <p>For more information on our use of advertising technologies and Cookies, please see our Cookie Policy.</p>
                <p>You can control your marketing preferences by: visiting our website www.menurie.com or mobile application; clicking on "Account" (for our website this is under the drop-down menu); and scrolling down to "Marketing Preferences". Where you have chosen at a device level to begin or continue receiving push notifications from us, we may send you push notifications relating to the services that you have requested from us and information about our services, offers and Menurie initiatives. You can choose to stop receiving push notifications from us at any time by changing your preferences on your mobile device. We may still contact you through email where you have opted out of direct marketing with service communications, including, but not limited to, correspondence providing information about service interruption and delivery safety.</p>

                <h5>Automated Decision Making</h5>
                <p>We conduct fraud checks on all customers. Where we believe there may be fraudulent activity we may block you from placing an order and using our Sites.</p>

                <p>We undertake fraud checks on all customers because this is necessary for us to perform our contracted services to customers, by ensuring that the services we (and all our partners) provide are duly paid for, and also so that individuals themselves are protected from fraudulent transactions on their cards.</p>

                <p>Given the volumes of customers and orders we deal with, we use automated systems including a third party fraud detection provider, which analyses your order data in order to make automated decisions as to whether or not we will accept an order. We find this is a fairer, more accurate and more efficient way of conducting fraud checks since human checks would simply not be possible in the timeframes and given the volumes of customers that we deal with.</p>

                <p>The checks and decisions that are made look at various components including known industry indicators of fraud which our expert fraud detection provider makes available to us, as well as fraud patterns we have detected on our Sites. When combined, these generate an automated score indicating the likelihood of a fraudulent transaction. If our systems indicate a high score for you, then we may decline an order or even block you from our services. The specific fraud indicators are dynamic so will change depending on what types of fraud are being detected in the wider world, country and our Sites at any particular time.</p>

                <p>You have certain rights in respect of this activity - please see the Your Rights section below for more information. Our fraud detection is in place to protect all of our customers as well as menurie. You have the right to contest any fraud decision made about you and to be given more information about why any such decision was made by contacting us using the Contact Details above.</p>

                <h5>Retention Of Your Information</h5>
                <p>We will not retain your information for any longer than we think is necessary.</p>
                <p>Information that we collect will be retained for as long as needed to fulfil the purposes outlined in the Use of Your Information section above, in line with our legitimate interest or for a period specifically required by applicable regulations or laws, such as retaining the information for regulatory reporting purposes. When determining the relevant retention periods, we will take into account factors including: our contractual obligations and rights in relation to the information involved; legal obligation(s) under applicable law to retain data for a certain period of time;</p>
                <p>Statute of limitations under applicable law(s); our legitimate interests where we have carried out balancing tests (see the Use of Your Information section above); (potential) disputes; and guidelines issued by relevant data protection authorities.</p>
                <p>Otherwise, we securely erase your information where we no longer require your information for the purposes collected.</p>

                <h5>Disclosure Of Your Information</h5>
                <p>The information we collect about you will be transferred to and stored on our servers located within the USA . We are very careful and transparent about who else your information is shared with.</p>

                <p>We share your information with other Menurie group companies only where necessary for the purposes set out in the Use of Your Information section above. We share your information with third party service providers which provide services on our behalf. The types of third party service providers whom we share your information with include for example:</p>

                <p>Payment providers (including online payment providers and fraud detection providers);</p>
                <p>IT service providers (including cloud providers); Restaurant and grocery partners; Riders;</p>
                <p>Customer support partners (including, but not limited to, companies that assist us to provide customer or technical support); and Marketing and advertising partners (to learn more about our website data sharing policy please see our Cookie Policy mentioned in the Cookies section above).</p>
                <p>Menurie also shares your information when we promote a programme or offer a service or product in conjunction with a third-party business partner. We will share your information with that partner to assist in marketing or to provide the associated product or service. In most of those cases, the programme or offer will include the name of the third-party business partner, either alone or with ours. An example of such a business partner relationship would be a partner that we partner with for providing delivery services.</p>

                <p>If you submit comments and feedback regarding the Sites, services, and our partners we may share such comments and feedback with our partners. In addition we may share health information about you with our partners where you volunteer and consent to this, for example if you report any specific food allergies after placing an order.</p>

                <p>Menurie will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy and applicable laws when it is transferred to third parties.</p>

                <p>If our business enters into a joint venture with, purchases or is sold to or merged with another business entity, your information may be disclosed or transferred to the target company, our new business partners or owners or their advisors.</p>

                <h5>We may also share your information:</h5>

                <p>If we are under a duty to disclose or share your information in order to comply with (and/or where we believe we are under a duty to comply with) any legal obligation or regulatory requirement. This includes exchanging information with other companies and other organisations for the purposes of fraud protection and prevention, and with law enforcement for crime prevention;</p>

                <p>in order to enforce our contractual terms with you and any other agreement; to protect the rights of Menurie, our partners, riders, or others, including to prevent fraud; and with such third parties as we reasonably consider necessary in order to prevent crime, e.g. the police or for health and safety purposes. International transfers of data:</p>
                <p>In some cases the personal data we collect from you might be processed outside of the United States, such as in the European Economic Area (“EEA”), the United Kingdom, the Philippines and the countries in which Menurie operates. These countries may not have the same protections for your personal data as United States, the UK or EEA has. However, we are obliged to ensure that the personal data that is processed by us and our suppliers outside of the USA, UK or EEA is protected in the same ways as it would be if it was processed within the USA, UK or the EEA. There are therefore certain safeguards in place when your data is processed outside of the USA, UK or the EEA.</p>
                <p>We ensure a similar degree of protection is afforded to it by ensuring at least one of the following safeguards is implemented: your personal data is transferred to countries that have been deemed to provide an adequate level of protection for personal data by the Secretary of State; and we use any Secretary of State approved data transfer mechanisms, Please contact us using the Contact Details above if you want further information on the countries to which personal data may be transferred and the specific mechanism used by us when transferring your personal data out of the United States, UK or the EEA.</p>

                <h5>Security</h5>
                <p>We adopt robust technologies and policies to ensure the personal information we hold about you is suitably protected. We take steps to protect your information from unauthorised access and against unlawful processing, accidental loss, destruction and damage. Where you have chosen a password that allows you to access certain parts of the Sites, you are responsible for keeping this password confidential. We advise you not to share your password with anyone. Unfortunately, the transmission of information via the internet is not completely secure. Although we will take steps to protect your information, we cannot guarantee the security of your data transmitted to the Sites; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p>

                <h5>Your Rights</h5>
                <p>Subject to applicable law, you may have a number of rights concerning the data we hold about you. If you wish to exercise any of these rights, please contact our Data Protection Officer using the Contact Details set out above. For additional information please contact us.</p>

                <p>The right to be informed. You have the right to be provided with clear, transparent and easily understandable information about how we use your information and your rights. This is why we’re providing you with the information in this policy.</p>

                <p>The right of access. You have the right to obtain access to your information (if we’re processing it). This will enable you, for example, to check that we’re using your information in accordance with data protection law. If you wish to access the information we hold about you in this way, please get in touch (see Contact Details).</p>

                <p>The right to rectification. You are entitled to have your information corrected if it is inaccurate or incomplete. You can request that we rectify any errors in information that we hold by contacting us (see Contact Details).
                </p>

                <p>The right to erasure. This is also known as 'the right to be forgotten' and, in simple terms, enables you to request the deletion or removal of certain of the information that we hold about you by contacting us (see Contact Details). This right is not absolute and only applies in certain circumstances.</p>

                <p>The right to restrict processing. You have rights to 'block' or 'suppress' further use of your information in certain circumstances. When processing is restricted, we can still store your information, but will not use it further.
                </p>

                <p>The right to data portability. You have the right to obtain your personal information in an accessible and transferrable format so that you can re-use it for your own purposes across different service providers. This is not an absolute right however and there are exceptions. To learn more please get in touch (see Contact Details).</p>

                <p>The right to lodge a complaint. You have the right to lodge a complaint about the way we handle or process your information with a competent data protection authority (see Complaints).</p>

                <p>The right to withdraw consent. If you have given your consent to anything we do with your information (i.e. if we rely on consent as a legal basis for processing certain information), you have the right to withdraw that consent at any time. You can do this by contacting us (see Contact Details). Withdrawing consent will not, however, make unlawful our use of your information before you withdraw consent.</p>

                <p>The right to object to processing. You have the right to object to certain types of processing, including processing for direct marketing and profiling. You can object by changing your marketing preferences, disabling cookies as set out in the Cookies and Marketing sections above or by getting in touch (see Contact Details). You also have the right not to be subject to a decision based solely on automated processing, such as in connection with our fraud checks. You can exercise this right by contesting the fraud decision made against you (see Automated Decision Making), or by getting in touch (see Contact Details).</p>

                <h5>Changes To Our Privacy Policy</h5>
                <p>We update this Privacy Policy from time to time, so remember to check back in, in case anything has changed and, where appropriate, we may notify you of the changes for example by email or push notification.</p>

                <p>This privacy policy was last updated: 05/17/2021</p>

                <h5>Complaints</h5>
                <p>If you’re not satisfied with our response to any complaint or believe our processing of your information does not comply with data protection law, you can contact us and make a complain.Menurie, Inc</p>
            </div>
        </div>
    </div>
</section>
@endsection