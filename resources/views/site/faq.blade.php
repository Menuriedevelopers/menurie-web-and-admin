@extends('site.layout.base')
@section('title','FAQs')
@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('site/asset/styles/cart.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/contact-us.css') }}">
<style>
    .bg-menurie{
        background: #8d5540;
        color: #fff;
    }
</style>
@endsection
@section('content')
<div class="second-navbar">
    <div class="container">
        <div class="row row-spacing">
            <div class="col-lg-4 col-md-6 col-sm-12">
            </div>
            <div>
              <img class="app-stores" width="200" src="{{asset('site/asset/images/appstore.png')}}" alt="">&nbsp;&nbsp;
              <img class="app-stores" width="200" src="{{asset('site/asset/images/playstore.png')}}" alt="">
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="find-us">
        <div class="row mb-4">
        <div class="col-12">
          <div class="text-center h3">
            FAQ's <span class="menurie-text">Menurie</span>
          </div>
            <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header bg-menurie" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn text-white btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Who we are
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aspernatur maiores, eveniet minus aliquid itaque officia tempore vitae mollitia? Ad maiores laudantium ratione nesciunt voluptatem hic magnam fuga voluptates esse quisquam.
                    </div>
                  </div>
                </div>
                <br><br>
                <div class="card">
                  <div class="card-header bg-menurie" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn text-white btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        What we do
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequuntur excepturi exercitationem impedit ullam? Dolorum unde fuga eaque aliquid commodi fugit eum, distinctio alias cum doloremque illo fugiat consequuntur, illum culpa?
                    </div>
                  </div>
                </div>
                <br><br>
                <div class="card">
                  <div class="card-header bg-menurie" id="headingThree">
                    <h2 class="mb-0">
                      <button class="btn text-white btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Things to know before using Menurie
                      </button>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et veniam temporibus, hic natus architecto ratione blanditiis illo quas excepturi recusandae unde dignissimos nostrum ullam? Maxime autem ducimus nesciunt eaque est.
                    </div>
                  </div>
                </div>
                <br><br>
              </div>
        </div>
        </div>
    </div>
</div>
@endsection
