@php $cart = session('cart'); @endphp
<div>
  {{-- <div class="modal-header modal-header3">
      <h4 class="modal-title w-100" id="myModalLabel">My Cart</h4>
  </div> --}}
  <div class="modal-body modal-body3">
    @if($cart)
    @php
        $total = 0.00;
    @endphp
    <div class="items-list" style="overflow-x: hidden;overflow-y:scroll;height:190px">
      @foreach($cart as $index1 => $cartItem)
        <div class="container-cart px-0">
          <div class="row m-0" id="cart-item-{{$cartItem['id']}}">
            <div class=" cart-col-1">
              <div>
                <i class="fa fa-times" aria-hidden="true">{{ $cartItem['quantity'] }}</i>
                <img class="rounded-circle" src="{{ $cartItem['photo'] }}" width="60">
              </div>
            </div>
            <div class="cart-detail pl-2">
            <b>{{ $cartItem['name'] }}</b> <br>
            @php
                $total += $cartItem['price'] * $cartItem['quantity'] ;
            @endphp
            </div>
            <div class="cart-col-3 text-secondary">${{ number_format($cartItem['quantity'] * $cartItem['price'],2) }}</div>
            @foreach($cartItem['addons'] as $index=> $addon)
              <div class="row">
                <div class="col-3">
                  <p class="m-0" style="color:#8D5440;">Addons</p>
                </div>
                <div class="cart-detail pl-4 col-6">
                  <span class="text-secondary">{{ $addon['addonName']}}</span>
                </div>
                <div class="col-3 text-secondary addon-price">
                  <p>&dollar;{{number_format($addon['addonPrice']*$addon['addonQuantity'],2)}}</p>
                </div>
              </div>
            @php
                $total = $total + ($addon['addonTotal'] * $cartItem['quantity']);
            @endphp
            @endforeach
            <div class="col-8 d-flex text-right">
              <div class="col-6"><a href="/cart" style="text-decoration:none;color: rgb(141,84,64);"><img width="15" src="{{asset('site/asset/icons/edit_cat_ic.png')}}" alt=""></a></div>
              <div class="col-3"><a href="javascript:none" onclick="removeItem('{{ $index1 }}')" class="pl-1" style="text-decoration:none;color: rgb(141,84,64);"><img width="15" src="{{asset('site/asset/icons/del_ic.png')}}" alt=""></a></div>
            </div>
          </div>
        </div>
      <hr>
    @endforeach
    </div>
    <div class="cart-bill-section mb-5">
      <div class="container">
          <div class="row m-0">
              <div class="col-12 mx-0 px-0 text-secondary bill-detail" style="font-size: 14px;"> Bill detail:</div>
          </div>
      </div>
      <div class="row px-0 m-0">
        <div class="col-6">
            <div class="item-heading"> Items Total:</div>
        </div>
        <div class="col-6">
            <div class="item-amount text-right">${{ number_format($total,2) }}</div>
        </div>
      </div>
      <div class="row px-0 m-0">
        <div class="col-6">
            <div class=" delivery-heading">Tax:</div>
        </div>
        <div class="col-6">
            <div class="delivery-fee text-right"> ${{ number_format($total * 0.089,2) }}</div>
        </div>
      </div>
      <div class="row px-0 m-0">
        <div class="col-6">
            <div class=" delivery-heading"> Delivery Fee:</div>
        </div>
        <div class="col-6">
            <div class="delivery-fee text-right"> $0.00</div>
        </div>
      </div>
      <div class="row px-0 m-0">
        <div class="col-8">
            <div class=" delivery-heading"> Service charges:</div>
        </div>
        <div class="col-4">
            <div class="delivery-fee text-right"> $0.00</div>
        </div>
      </div>
      <hr>
      <div class="row px-0 m-0">
        <div class="col-6">
            <div class="total-amount-heading"> Total:</div>
        </div>
        <div class="col-6">
          <div class="item-amount text-right">${{ number_format(($total * 0.089)+$total,2) }}</div>
        </div>
      </div>
      <br>
    @else
    <div class="cart-bill-section">
      <div class="container-total m-0 pt-3">
        <label>No items added in cart</label>
      </div>
    </div>
    @endif
    </div>
    <div class="container">
      <div class="row">
          <div class="col-12 cart-checkout">
              <a href="#" onclick="toggle();"><button class="btn btn-danger btn-checkout1 w-100 m-0">Add more items</button></a>
          </div>
          <div class="col-12 d-flex cart-checkout my-2">
              <button class="btn btn-danger btn-checkout1 mr-2"> <a href="{{ route('checkout')}}">Checkout</a></button>
              <button class="btn btn-danger btn-checkout1"> <a href="{{ route('cart') }}">My Cart</a></button>
          </div>
      </div>
    </div>
  </div>
</div>
