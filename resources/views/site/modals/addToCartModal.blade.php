
	<div class="modal-content modal-content2" style="background-color: #fff">
    <div class="modal-header d-block py-0 border-0">
        <div class="row m-0">
            <div class="col-12 mx-auto my-2">
                <div class="cart-img mx-auto">
                    <img src="{{ asset($item->item_image) }}" class="" width="90">
                </div>
            </div>
        </div>
        <div class="row m-0 my-2">
            <div class="col-12">
                <div class="modal-dish-name mb-0" style="color: #00000;">
                    {{ $item->item_name }}<span class="d-block">by
                        @if($item->chef_id != 0)
                        {{$item->chef->first_name.' '.$item->chef->last_name}}
                        @else
                        Menurie Chef
                        @endif
                        <span>
                        </div>
                    </div>
                    <div class="col-6">
                    </div>
                </div>
                <div class="row m-0">
                	<div class="col-12">
                		<p class="show-less-div-3 mb-2">{{ $item->description }}</p>
                	</div>
                </div>
                <div class="row m-0 mb-2">
                    <div class="col-6 model-price">
                        Price<br>
                        <span>$</span><span id="itemTotal">{{ $item->price }}</span>
                    </div>
                    <div class="col-6 my-auto">
                        <div class="d-flex justify-content-end">
                            <div class="model-btn-1">
                                <i class="fa fa-minus" onclick="minusNum()"></i>
                            </div>
                            <div class="numBtn" id="num-btn">1</div>
                            <div class="model-btn-2">
                                <i class="fa fa-plus " onclick="plusNum()"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <form class="item-form" >
                @csrf
            <div class="modal-body modal-body2">
                <table class="table ">
                    <div class="h5">Add on:</div>
                    <tbody>
                        @foreach ($item->addons as $addon)
                        <tr>
                            <td scope="row" class="dish-name text-left p-1 d-flex">
                                <label class="check-container">
                                  <input type="checkbox" name="addon_id[]" value="{{$addon->id}}">
                                  <span class="checkmark"></span>
                                </label>
                                <span style="word-break: break-all;">{{ $addon->name }}</span>
                            </td>
                            {{-- <td class="modal-controller21 p-1">
                                <span class="modal-btn1">
                                    <i class="fa fa-minus" onclick="addonMinus('addonQuantity',{{$addon->id}})"></i>
                                </span>
                                <span class="modal-btn2" id="quantity-{{$addon->id}}">0</span>
                                <span class="modal-btn3 ">
                                    <i class="fa fa-plus " onclick="addonPlus('addonQuantity',{{$addon->id}})"></i>
                                </span>
                            </td> --}}
                            <td class="p-1 text-right" id="price-{{ $addon->id }}">${{ number_format($addon->price, 2) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
                <input type="hidden" value="1" id="item-quantity" name="item_quantity">
                <input type="hidden" value="{{ $item->id }}" name="item_id">
                <input type="hidden" id="item-price" value="{{ $item->price }}" name="item_price">
                {{-- @foreach($item->addons as $addon)
                <input type="hidden" name="addon_id[]" value="{{ $addon->id }}">
                <input type="hidden" id="addonQuantity-{{$addon->id}}" value="0" name="addon_quantity[]">
                <input type="hidden" id="addon-price-{{$addon->id}}" value="{{ $addon->price }}">
                @endforeach --}}
                <div class="modal-footer cart-footer-btn">
                    <div class="row m-0 w-100">
                        <div class="col-4 my-auto pl-0">
                            <button type="button" class="btn" data-dismiss="modal">
                            Cancel</button>
                        </div>
                        <div class="col-8 pr-0">
                            <button type="submit" class="btn btn-danger add-to-cart-btn w-100 d-flex justify-content-center"> Add to Cart <div class="add-cart-btn"><i class="fa fa-plus" aria-hidden="true"></i></div></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <script src="{{ asset('site/asset/scripts/showmoreless.min.js') }}" type="text/javascript"></script>
		<script type="text/javascript">
		    $(document).ready(function(e){
		        $('.show-less-div-3').myOwnLineShowMoreLess({
		            showLessLine:2
		        });
		    });
		</script>