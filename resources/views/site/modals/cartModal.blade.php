@php $cart = session('cart'); @endphp
<div>
  <style type="text/css">
    
    .quantity {
      display: flex;
      padding: 0;
    }
    .quantity__minus,
    .quantity__plus {
      display: block;
      width: 22px;
      height: 23px;
      margin: 0;
      background: white;
      border: 1px solid #8D5440;
      color:white;
      text-decoration: none;
      text-align: center;
      line-height: 23px;
    }
    .quantity__minus:hover,
    .quantity__plus:hover {
      background: #8D5440;
      text-decoration: none;
      color: #FFFFFF;
    } 
    .quantity__minus {
      border-radius: 3px 0 0 3px;
    }
    .quantity__plus {
      border-radius: 0 3px 3px 0;
    }
    .quantity__input {
      width: 32px;
      height: 23px;
      margin: 0;
      padding: 0;
      text-align: center;
      border-top: 2px solid #dee0ee;
      border-bottom: 2px solid #dee0ee;
      border-left: 1px solid #dee0ee;
      border-right: 2px solid #dee0ee;
      background: #fff;
      color: #8184a1;
    }
    .quantity__minus:link,
    .quantity__plus:link {
      color: #8184a1;
    } 
    .quantity__minus:visited,
    .quantity__plus:visited {
      color: #fff;
    }
    .image-padding{
      margin-left: -50px;
    }
    @media(max-width: 425px){
      .image-padding{
        margin-left: 0px;
      } 
    }
  </style>
  {{-- <div class="modal-header modal-header3">
      <h4 class="modal-title w-100" id="myModalLabel">My Cart</h4>
  </div> --}}
  <div class="modal-body modal-body3">
    @if($cart)
    @php
        $total = 0.00;
    @endphp
    <div class="col-12 cart-checkout">
        <a href="#" onclick="toggle();"><button class="btn btn-danger btn-checkout1 w-100 m-0">Add more items</button></a>
    </div>
    <div class="items-list" style="overflow-x: hidden;overflow-y:scroll;height:190px;margin-top: 15px">
      @foreach($cart as $index1 => $cartItem)
        <div class="container-cart px-0">
          <div class="row m-0" id="cart-item-{{$cartItem['id']}}">
            <div class="row">
                <div class="col-sm-4"><a href="javascript:none" onclick="removeItem('{{ $index1 }}')" class="pl-1" style="text-decoration:none;color: rgb(141,84,64);"><img width="15" src="{{asset('site/asset/icons/del_ic.png')}}" alt=""></a></div>
                <div class="col-sm-4 image-padding">
                  <img class="rounded-circle pl-2" src="{{ $cartItem['photo'] }}" width="60">
                </div>
                <div class="col-sm-4">
                  <div class="cart-detail pl-2">
                  <b>{{ $cartItem['name'] }}</b> <br>
                  @php
                      $total += $cartItem['price'] * $cartItem['quantity'] ;
                  @endphp
                  </div>
                  <div class="cart-col-3">${{ number_format($cartItem['quantity'] * $cartItem['price'],2) }}</div>
                  @foreach($cartItem['addons'] as $index=> $addon)
                    <div class="row">
                      <div class="col-3">
                        <p class="m-0" style="color:#8D5440;">Addons</p>
                      </div>
                      <div class="cart-detail pl-4 col-6">
                        <span class="text-secondary">{{ $addon['addonName']}}</span>
                      </div>
                      <div class="col-3 text-secondary addon-price">
                        <p>&dollar;{{number_format($addon['addonPrice']*$addon['addonQuantity'],2)}}</p>
                      </div>
                    </div>
                  @php
                      $total = $total + ($addon['addonTotal'] * $cartItem['quantity']);
                  @endphp
                  @endforeach
                </div>
            </div>
            <div class="col-8 d-flex text-right">
              <div class="quantity mt-2">
                  <a href="#" class="quantity__minus">-</a>
                  <input name="quantity" type="text" class="quantity__input" value="1">
                  <a href="#" class="quantity__plus">+</a>
              </div>
            </div>
          </div>
        </div>
      <hr>
    @endforeach
    </div>
    <div class="cart-bill-section mb-5">
      <div class="container">
          <div class="row m-0">
              <div class="col-12 mx-0 px-0 text-secondary bill-detail" style="font-size: 14px;"> Bill detail:</div>
          </div>
      </div>
      <div class="row px-0 m-0">
        <div class="col-6">
            <div class="item-heading"> Items Total:</div>
        </div>
        <div class="col-6">
            <div class="item-amount text-right">${{ number_format($total,2) }}</div>
        </div>
      </div>
      <div class="row px-0 m-0">
        <div class="col-6">
            <div class=" delivery-heading">Tax:</div>
        </div>
        <div class="col-6">
            <div class="delivery-fee text-right"> ${{ number_format($total * 0.089,2) }}</div>
        </div>
      </div>
      <div class="row px-0 m-0">
        <div class="col-6">
            <div class=" delivery-heading"> Delivery Fee:</div>
        </div>
        <div class="col-6">
            <div class="delivery-fee text-right"> $0.00</div>
        </div>
      </div>
      <div class="row px-0 m-0">
        <div class="col-8">
            <div class=" delivery-heading"> Service charges:</div>
        </div>
        <div class="col-4">
            <div class="delivery-fee text-right"> $0.00</div>
        </div>
      </div>
      <hr>
      <div class="row px-0 m-0">
        <div class="col-6">
            <div class="total-amount-heading"> Total:</div>
        </div>
        <div class="col-6">
          <div class="item-amount text-right">${{ number_format(($total * 0.089)+$total,2) }}</div>
        </div>
      </div>
      <br>
    @else
    <div class="cart-bill-section">
      <div class="container-total m-0 pt-3">
        <label>No items added in cart</label>
      </div>
    </div>
    @endif
    </div>
    <div class="container justify-content-center">
      <div class="row">
          <div class="col-12 d-flex cart-checkout my-2">
              <button class="btn btn-danger btn-checkout1 mr-2"> <a href="{{ route('checkout')}}">Checkout</a></button>
              <button class="btn btn-danger btn-checkout1"> <a href="{{ route('cart') }}">My Cart</a></button>
          </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function() {
      const minus = $('.quantity__minus');
      const plus = $('.quantity__plus');
      const input = $('.quantity__input');
      minus.click(function(e) {
        e.preventDefault();
        var value = input.val();
        if (value > 1) {
          value--;
        }
        input.val(value);
      });
      
      plus.click(function(e) {
        e.preventDefault();
        var value = input.val();
        value++;
        input.val(value);
      })
    });
  </script>
</div>
