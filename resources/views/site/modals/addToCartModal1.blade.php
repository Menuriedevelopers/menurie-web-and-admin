<div class="modal-content modal-content2">
    <div class="modal-header modal-header2">
        <table class="table ">
            <tbody>
                <tr class="modal-header-tr">
                    <th scope="row">
                        <img src="{{ asset($item->item_image) }}" class="rounded-circle" width="90">
                    </th>
                    <td>
                        <div class="modal-dish-name" style="color: white;"><b style="max-width:135px;font-size: 18px;">
                        {{ $item->item_name }}</b> <br> by {{$item->chef->first_name.' '.$item->chef->last_name}}</div>
                    </td>
                    <td style="color: white; ">
                        <div class="modal-amount-controller">
                            <span class="modal-btn1" style="color: white;">
                                <i class="fa fa-minus" onclick="minusNum()"></i>
                            </span>
                            <span class="modal-btn2 numBtn" style="color: white;" id="num-btn">1</span>
                            <span class="modal-btn3 " style="color: white;">
                                <i class="fa fa-plus " onclick="plusNum()"></i>
                            </span><br>
                            $<span id="itemTotal">{{ $item->price }}</span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-body modal-body2">
        <table class="table ">
            <div class="h4">ADD ON</div>
            <tbody>
                @foreach ($item->addons as $addon)
                <tr>
                    <td scope="row" class="dish-name text-left p-1">
                    <span style="word-break: break-all;">{{ $addon->name }}</span>
                    </td>
                    <td class="modal-controller21 p-1">
                        <span class="modal-btn1">
                            <i class="fa fa-minus" onclick="addonMinus('addonQuantity',{{$addon->id}})"></i>
                        </span>
                        <span class="modal-btn2" id="quantity-{{$addon->id}}">0</span>
                        <span class="modal-btn3 ">
                        <i class="fa fa-plus " onclick="addonPlus('addonQuantity',{{$addon->id}})"></i>
                        </span>
                        <!-- </div> -->
                    </td>
                    <td id="price-{{ $addon->id }}">${{ $addon->price }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <form class="item-form" >
        @csrf
        <input type="hidden" value="1" id="item-quantity" name="item_quantity">
        <input type="hidden" value="{{ $item->id }}" name="item_id">
        <input type="hidden" id="item-price" value="{{ $item->price }}" name="item_price">
            @foreach($item->addons as $addon)
        <input type="hidden" name="addon_id[]" value="{{ $addon->id }}">
        <input type="hidden" id="addonQuantity-{{$addon->id}}" value="0" name="addon_quantity[]">
            <input type="hidden" id="addon-price-{{$addon->id}}" value="{{ $addon->price }}">
            @endforeach
        <div class="modal-footer modal-footer2">
            <button type="submit" class="btn btn-danger add-to-cart-btn"> Add to Cart </button>
            <button type="button" style="color: white;" class="btn  btn-close" data-dismiss="modal">
                Cancel</button>
        </div>
    </form>
</div> 