@php $cart = session('cart'); @endphp
<div class="modal-dialog modal-dialog3 modal-side modal-top-right" role="document">
    <div class="modal-content modal-content3">
        <div class="modal-header modal-header3">
            <h4 class="modal-title w-100" id="myModalLabel">My Cart</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body modal-body3">
            @if($cart)
                @php
                    $total = 0;
                @endphp
                @foreach($cart as $cartItem)
            <div class="container-cart">
                <!-- <div class="row"> -->
                <div class=" cart-col-1">
                <div><img class="rounded-circle" src="{{ $cartItem['photo'] }}" width="60">
                <i class="fa fa-times" style="margin-left: 10px;" aria-hidden="true">{{ $cartItem['quantity'] }}</i></div>
                </div>
                <div class=" cart-col-2">
            <b>{{ $cartItem['name'] }}</b> <br>
            @foreach($cartItem['addons'] as $addon)
            <i class="fa fa-times" aria-hidden="true">{{ $addon['addonQuantity']}}</i> <span class="text-secondary">by {{ $addon['addonName']}}</span> <span class="text-secondary">${{ $addon['addonPrice']}}</span><br>
            @endforeach
             <span class="text-secondary">by {{ $cartItem['chef']}}</span>
                </div>
            <div class=" cart-col-3 text-secondary">${{ $cartItem['total'] }}</div>
            </div> 
            <br>
            @php
                $total += $cartItem['total'];
            @endphp
            @endforeach
            <div class="cart-bill-section">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-secondary bill-detail" style="font-size: 14px;"> Bill detail:</div>
                    </div>
                </div>
                <div class="container-total-item">
                    <div class=" item-heading"> Items Total:</div>
                <div class=" item-amount">$ {{ $total }}</div>
                </div>
                <div class="container-delivery">
                    <div class=" delivery-heading"> Delivery Fee:</div>
                    <div class=" delivery-fee"> $4.00</div>
                </div>
                <hr>
                <div class="container-total">
                    <div class=" total-amount-heading"> Total:</div>
                <div class=" total-amount"> ${{ ($total + 4) }}</div>
                </div><br>
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-flex cart-checkout">
                            <button class="col-6 m-1 btn btn-danger btn-checkout1"> <a href="{{ route('checkout')}}?redirect=checkout">Checkout</a></button>
                            <button class="col-6 m-1 btn btn-danger btn-checkout1"> <a href="{{ route('cart') }}">My Cart</a></button>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>