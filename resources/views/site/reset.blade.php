<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/site/asset/styles/loginStyle.css" />
    <link rel="stylesheet" href="{{ asset('site/asset/styles/intlTelInput.css') }}">
    <title>Reset Password</title>
</head>

<body>
    <section>
        <div class="container-fluid login-page-wrapper p0">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 login-img-section">
                </div>
                <div class="col-xl-5 col-lg-5 col-md-10 login-form-section">
                  @if(session('error'))
                    <div class="alert alert-danger">
                        {!! session('error') !!}
                    </div>
                    @endif
                    <form method="post" action="{{route('reset')}}">
                      @csrf
                        <div class="headings">
                            <h1>Reset you Password.</h1>
                            <p>Enter your new password and <br> login
                            </p>
                        </div><br><br>

                        <div class="form-group mb-50">
                            <label class="text-bold-600">Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Enter password"/>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <input type="hidden" name="phone" value="{{$phone}}">
                        <div class="form-group mb-50">
                            <label class="text-bold-600">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password"/>
                            @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-danger">Submit</button>
                        <br><br>
                        <a href="{{route('customer.login')}}">Go to login</a>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
</body>
</html>
