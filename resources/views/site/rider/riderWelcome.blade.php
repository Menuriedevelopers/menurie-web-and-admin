<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('site/asset/styles/style.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <title>Menurie</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light" id="headnev">
        <a class="navbar-brand" href="#">
            <img src="{{ asset('site/asset/images/img1.png') }}" width="65" alt="svg">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">

            </ul>
            <span class="navbar-text">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item bottom-list">
                        <a class="nav-link" href="{{ route('index') }}">
                            <span>Home</span>
                        </a>
                    </li>
                    </li>
                    </li>
                    <li class="nav-item bottom-list">
                        <a class="nav-link" href="#">
                            <span>Help</span>
                        </a>
                    </li>
                    <li class="nav-item bottom-list-last">
                        @auth('rider')
                    <a class="nav-link" href="{{route('rider.logout')}}">
                            <button type="button" class="btn btn-danger">
                                Logout</button>
                        </a>
                        @else
                        <a class="nav-link" href="{{ route('rider.login') }}">
                            <button type="button" class="btn btn-danger">
                                <img src="{{ asset('site/asset/images/signin.png') }}" width="12">
                                Sign In</button>
                        </a>
                        @endauth
                        <span class="last-icon-img">
                            <img src="{{ asset('site/asset/images/lastnavicon.png') }}" data-toggle="modal"
                                data-target="#sideModalTR" width="15">
                        </span>
            </span>
            </li>
            </ul>
            </span>
        </div>
    </nav>
    </div>
    <!---------------------------End of Navbar------------------------>

    <div class="bottom-section">
        <div class="header-first-img">
            <img src="{{ asset('site/asset/images/header12.png') }}" width="240">
        </div>
        <div class="header-search-wrapper">
            <h1>Thankyou for registering with <span style="color: rgb(141,84,64);">Menurie</span></h1>
            <br><br><br>
            <h2>For more information download our Rider Application</h2>
        </div>
        <div class="header-last-img ">
            <img src="{{ asset('site/asset/images/header2.png') }}" width="240">
        </div>

    </div>

    <div class="second-header-section">
        <div class="container">
            <div class="row">
                <div class="col application-heading">
                    <h1>Download Our New Application</h1>
                </div>
                <div class="col app-section">
                    <div class="app-store">
                        <img src="{{ asset('site/asset/images/play-store.png') }}" width="150"><br><br>
                        <img src="{{ asset('site/asset/images/app-store.png') }}" width="150">
                    </div>
                    <div class="second-header-mobile">
                        <img src="{{ asset('site/asset/images/mobile.png') }}" height="290">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br><br><br>
    <!---------------------------- Site footer ----------------->
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3  col-sm-6 col-md-3">
                    <span><img src="{{ asset('site/asset/images/img1.png') }}" width="80" alt=""></span><br><br><br>
                    <p class="text-justify">
                        Lorem ipsum dolor sit amet. <br>
                        Lorem ipsum dolor sit amet. <br>
                        Lorem ipsum dolor sit amet. <br>

                    </p><br><br>
                    <ul class="social-icons">
                        <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a class="linkedin" href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                    </p>
                </div>
                <div class="col-lg-2  col-xs-6 col-md-2">
                    <ul class="footer-links">
                        <li><a href="#"><b>ABOUT</b></a></li><br>
                        <li><a href="#">Home</a></li><br>
                        <li><a href="#">About Us</a></li><br>
                        <li><a href="#">Company</a></li><br>
                        <li><a href="#">Our Team</a></li><br>
                        <li><a href="#">Careers</a></li><br>
                        <li><a href="#">Manage Blogs</a></li>
                    </ul>
                </div>
                <div class="col-lg-2  col-xs-6 col-md-2">
                    <ul class="footer-links">
                        <li><a href="#"><b>CONTACT</b></a></li><br>
                        <li><a href="#">Health & Support</a></li><br>
                        <li><a href="#">Contact with Us</a></li><br>
                        <li><a href="#">Ride with us</a></li><br>
                    </ul>
                </div>
                <div class="col-lg-2  col-xs-6 col-md-2">
                    <ul class="footer-links">
                        <li><a href="#"><b>LEGAL</b></a></li><br>
                        <li><a href="#">Terms & conditions</a></li><br>
                        <li><a href="#">Refund & cancellation</a></li><br>
                        <li><a href="#">Company</a></li><br>
                        <li><a href="#">Our Team</a></li><br>
                        <li><a href="#">Careers</a></li><br>
                        <li><a href="#">Manage Blogs</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-xs-6 col-md-3">
                    <ul class="footer-links">
                        <li><a href="#"><b>NEWS LETTER</b></a></li><br>
                        <div class="form-group input-icons">
                            <i class="icon">
                                <img src="{{ asset('site/asset/images/msg.png') }}" width="15" />
                            </i>
                            <input style="background-color: white;" type="email" class="form-control"
                                class="footer-input-field" placeholder="Enter email" />
                        </div>
                        <button class="btn ">SEND</button>
                    </ul>
                </div>
            </div>
            <hr>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <p class="copyright-text">Copyright &copy; All Rights Reserved by
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ asset('site/asset/scripts/home.js') }}"></script>
    <script>
        function removeContent() {
            var a = document.getElementById('map-img').style.display = "none";
            var b = document.getElementById('search-btn').style.display = "none";
        }

        var i = 0;

        function plusNum() {
            // document.getElementById('num-btn').innerHTML = ++i;
            document.getElementsByClassName('numBtn')[0].innerHTML = ++i;
        }

        function minusNum() {
            document.getElementById('numBtn')[0].innerHTML = --i;
        }

    </script>
    <script src="{{ asset('site/asset/scripts/script.js') }}"></script>

</body>

</html>
