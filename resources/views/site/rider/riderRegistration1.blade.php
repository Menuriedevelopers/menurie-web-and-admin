<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{ asset('site/asset/styles/riderRegistration.css') }}" />
    <link rel="stylesheet" href="{{ asset('site/asset/styles/intlTelInput.css') }}">
    <title>Rider - Menurie</title>
</head>

<body>
    <section>
        <div class="container-cloud">
            <div class="cloud1">
                <img src="{{ asset('site/asset/images/Path 153.png') }}" width="200">
            </div>
            <div class="cloud2">
                <img src="{{ asset('site/asset/images/Path 154.png') }}" width="180">
            </div>
            <div class="cloud3">
            </div>
            <div class="cloud4">
            </div>
        </div>
        </div> 
        <div class="container-fluid login-page-wrapper p0">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 login-img-section " class="img-responsive">
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 login-form-section">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{ route('rider.register') }}">
                        @csrf
                        <div class="headings">
                            <h1>
                                Create
                            </h1>
                            <p>
                                Please register to be a part of Menurie
                            </p>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6 col-sm-6 input-icons">
                                <i class="icon">
                                    <img src="{{ asset('site/asset/images/registration-man.png') }}" width="15" />
                                </i>
                                <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" />                            
                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 input-icons">
                                <i class="icon">
                                    <img src="{{ asset('site/asset/images/registration-man.png') }}"
                                        width="15" />
                                </i>
                                <input type="text" name="last_name" class="form-control" placeholder="Enter Last Name" />
                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6 col-sm-6 input-icons">
                                <i class="icon">
                                    <img src="{{ asset('site/asset/images/login-msg.png') }}"
                                        width="15" />
                                </i>
                                <input type="email" name="email" class="form-control" placeholder="Enter Email" />
                                @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 input-icons">
                                <i class="icon">
                                    <img src="{{ asset('site/asset/images/tel.png') }}"
                                        width="15" />
                                </i>
                                <input type="tel" class="form-control" name="phone" id="phone" placeholder="Enter Phone" />
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-6 col-sm-6 input-icons">
                                <i class="icon">
                                    <img src="{{ asset('site/asset/images/login-password.png') }}"
                                        width="15" />
                                </i>
                                <input type="password" name="password" class="form-control" placeholder="Enter Password" />
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div><div class="form-group col-xs-6 col-sm-6 input-icons">
                                <i class="icon">
                                    <img src="{{ asset('site/asset/images/login-password.png') }}"
                                        width="15" />
                                </i>
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password" />
                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            <label class="form-check-label" for="exampleCheck1">You have agreed our terms & conditions
                            </label>
                        </div>
                        <br />
                        <div class="register-btn">
                            <button type="submit" class="btn btn-danger register-btn">
                                 REGISTER NOW 
                            </button>
                            <p>Already have an account?</p><a class="btn btn-danger" href="{{route('rider.login')}}">Login</a>
                        </div>
                        <div></div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <br><br><br>
    <script src="{{ asset('site/asset/scripts/intlTelInput.js') }}"></script>
    <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
          // allowDropdown: false,
          autoHideDialCode: false,
          // autoPlaceholder: "off",
          // dropdownContainer: document.body,
          // excludeCountries: ["us"],
          // formatOnDisplay: false,
          // geoIpLookup: function(callback) {
          //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
          //     var countryCode = (resp && resp.country) ? resp.country : "";
          //     callback(countryCode);
          //   });
          // },
          hiddenInput: "full_number",
          initialCountry: "auto",
          // localizedCountries: { 'de': 'Deutschland' },
          // nationalMode: false,
          // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
          // placeholderNumberType: "MOBILE",
          // preferredCountries: ['cn', 'jp'],
          separateDialCode: true,
          utilsScript: "{{ asset('site/asset/scripts/utils.js') }}",
        });
      </script>
</body>

</html>
