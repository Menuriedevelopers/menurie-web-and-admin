<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{ asset('site/asset/styles/otp.css') }}" />
    <title>Menurie - Verify</title>
</head>

<body>
    <section>
        <div class="container-fluid login-page-wrapper p0">
            <div class="row">
                <div class="col-xl-6 col-lg-6  col-md-12 login-img-section">
                </div>
                <div class="col-xl-5 col-lg-5  col-md-10 login-form-section">
                    <form method="post" action="{{ route('customer.forgotVerify') }}">
                      @csrf
                      <input type="hidden" name="phone" value="{{$phone}}">
                        <div class="headings">
                            <h1>Enter OTP sent to the given mobile number to reset your password</h1>
                            <p>Enter OTP code below to <br /></p>
                        </div><br><br>
                        <div class="form-group input-icons input-icons-wrapper">
                            <input type="text" class="form-control number-inputs" id="number-input1" maxlength="1" placeholder="*" name="p-1" />
                            <input type="text" class="form-control number-inputs" id="number-input2" maxlength="1" placeholder="*" name="p-2" />
                            <input type="text" class="form-control number-inputs" id="number-input3" maxlength="1" placeholder="*" name="p-3" />
                            <input type="text" class="form-control number-inputs" id="number-input4" maxlength="1" placeholder="*" name="p-4" />
                            <input type="text" class="form-control number-inputs" id="number-input5" maxlength="1" placeholder="*" name="p-5" />
                            <input type="text" class="form-control number-inputs" id="number-input6" maxlength="1" placeholder="*" name="p-6"/>
                        </div><br>
                        <button type="submit" class="btn btn-danger otp-btn"> Verify</button>
                        <br><br><br>
                        <a href="/forgot">RESEND</a>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{asset('site/asset/scripts/imask.js')}}" charset="utf-8"></script>
    <script>
    var numberMask1 = IMask(
document.getElementById('number-input1'),
{
  mask: Number,
  min: 0,
  max: 9
});var numberMask2 = IMask(
document.getElementById('number-input2'),
{
mask: Number,
min: 0,
max: 9
});
var numberMask3 = IMask(
document.getElementById('number-input3'),
{
mask: Number,
min: 0,
max: 9
});
var numberMask4 = IMask(
document.getElementById('number-input4'),
{
mask: Number,
min: 0,
max: 9
});
var numberMask5 = IMask(
document.getElementById('number-input5'),
{
mask: Number,
min: 0,
max: 9
});
var numberMask6 = IMask(
document.getElementById('number-input6'),
{
mask: Number,
min: 0,
max: 9
});
$(".number-inputs").keyup(function () {
    if (this.value.length == this.maxLength) {
      $(this).next('.number-inputs').focus();
    }
});
    </script>
</body>
</html>
