<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{ asset('site/asset/styles/registration.css') }}" />
    <title>Menurie - Rider login </title>
</head>

<body>
    <section>
        <div class="container-cloud">
            <div class="cloud1">
                <img src="{{ asset('site/asset/images/Path 153.png') }}" width="200">
            </div>
            <div class="cloud2">
                <img src="{{ asset('site/asset/images/Path 154.png') }}" width="180">
            </div>
            <div class="cloud3">
            </div>
            <div class="cloud4">
            </div>
        </div>
        </div>
        <div class="container-fluid login-page-wrapper p0">
            <div class="row">
                <div class="col-xl-5 col-sm-5 col-lg-5 col-md-4 login-img-section">
                </div>
                <div class="col-xl-5 col-sm-5 col-lg-5 col-md-6 login-form-section">
                    <form method="POST" action="{{ route('rider.login') }}">
                        @csrf
                        <div class="headings">
                            <h1>
                                WELCOME to Menurie
                            </h1>
                            <p>
                                Please log in to your account to <br />
                                continue with Menurie
                            </p>
                        </div>
                        <div class="form-group input-icons">
                            <i class="icon">
                                <img src="{{ asset('site/asset/images/login-msg.png') }}" width="15" />
                            </i>
                            <input type="email" name="email" class="form-control" placeholder="Enter email" />
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group input-icons">
                            <i class="icon">
                                <img src="{{ asset('site/asset/images/login-password.png') }}"
                                    width="16" />
                            </i>
                            <input type="password" name="password" class="form-control" placeholder="Enter Password" />
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <br />
                        <div class="container">
                            <div class="row">
                                <div class="col rider-forget-password">
                                    <a href=""> Forget Password? </a>
                                </div>
                            </div>
                        </div><br>
                        <div>
                            <button type="submit" class="btn btn-danger register-btn">
                                LOG IN NOW
                            </button>
                        </div>
                        <br><br><br>
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    Become a Rider
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col rider-new-account">
                                    <a href="{{ route('rider.register') }}">Create New </a>
                                </div>
                            </div>
                        </div>
                        <div></div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <br><br><br>
</body>

</html>
