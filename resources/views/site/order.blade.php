@extends('site.layout.base')
@section('styles')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.align-bottomcss">
<link rel="stylesheet" href="{{ asset('site/asset/styles/orderStyle.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<style>
    .ant-steps-item-finish .ant-steps-item-icon > .ant-steps-icon .ant-steps-icon-dot{
        background: #8d5440 !important;
    }
    .ant-steps-dot .ant-steps-item-content, .ant-steps-dot.ant-steps-small .ant-steps-item-content{
        width: 220px !important;
    }
    .ant-steps-item-process .ant-steps-item-icon > .ant-steps-icon .ant-steps-icon-dot{
        background: #8d5440 !important;
    }
    .ant-steps-item-finish > .ant-steps-item-container > .ant-steps-item-tail::after{
        background-color: #8d5440 !important;
    }
</style>
@endsection
@section('title','Order Track')
@section('content')
<div class="modal modal3 fade right cartModal" id="sideModalTR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>
</div>
</div>
<div class="container">
    <div class="row">
        <div class="col order-tracker-heading">
            <div class="left-icon "><i class="fa fa-angle-left" aria-hidden="true"></i></div>
            <div class="track-heading h3">Order</div>
        </div>
        <div class="col"></div>
        <div class="col"></div>
    </div>
</div>
<br><br><br>
<div class="container">
    <div class="row">
        <div class="col-md-7 offset-md-1 order-time-detail">
            <div class="row order-components">
                <div class="col">
                    <b> #OD-{{ $order->id }} </b><br>
                    Order number
                    <!-- <span>|</span> -->
                </div><span class="vertical-line"></span>
                <div class="col">
                    @php
                        $date = $order->created_at;
                        $orderDate = date_format($date,'j M,Y');
                    @endphp
                    <b> {{$orderDate}} </b> <br>
                    Order date
                </div><span class="vertical-line"></span>
                <div class="col">
                    <b> 45 Min </b><br>
                    Estimate Time
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="user-order-amount text-secondary"> <b>${{ $order->total_price }}</b></div>
            <div class="reorder-btn">
                <button class="btn btn-danger"> <a
                        href="{{ route('foodDetail') }}">Re-order</a></button>
            </div>
        </div>
    </div>
</div>
<!--End of third container-------->
<br><br><br>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 ">
            <div class="row order-components">
                <div class="col">
                    <div class="h3">Order status</div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
@php
    $status = 0;
    if($order->order_status == 'Placed')
    $status = 1;
    if($order->order_status == 'Preparing')
    $status = 2;
    if($order->order_status == 'Picked')
    $status = 3;
    if($order->order_status == 'Delivered')
    $status = 4;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 order-tracker-place">
            <div id="app"><order-step order_id="{{$order->id}}" current="{{ $status }}"></order-step></div>
        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 ">
            <div class="row order-components">
                <div class="col">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
@foreach($order->detail as $item)
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 order-tracker-place">
            <div class="row">
                <div class="col-8">
                    <span style="margin-left: 20px;" class="order-img"><img
                            src="{{ asset('site/asset/images/Rectangle 108.png') }}"
                            width="30"></span>
                    <span><b>{{$item->item->item_name}}</b>x{{$item->quantity}}</span>
                </div>
                <div class="col text-right">
                    <div class="text-secondary">${{number_format($item->price, 2)}}</div>
                </div>
                <div class="col text-right">
                    <div class="text-secondary">${{number_format($item->price*$item->quantity, 2)}}</div>
                </div>
            </div>
        </div>
        <div class="col-md-8 offset-md-1">
            <div class="row">
                <div class="col adds-on">
                    <div class="text-secondary"><b>Add on</b></div>
                </div>
            </div>
        </div>
        @foreach($item->orderAddons as $addon)
        <div class="col-md-8 offset-md-1">
            <div class="row">
                <div class="col  adds-on">
                <div class="text-secondary">{{ $addon->addon->name }} x{{$addon->quantity}}</div>
                </div>
                <div class="col text-right">
                <div class="text-secondary">${{ number_format($addon->price, 2) }}</div>
                </div>
                <div class="col text-right">
                <div class="text-secondary">${{ number_format($addon->price*$addon->quantity, 2) }}</div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<br><br>
@endforeach
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 ">
            <div class="row order-components">
                <div class="col">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 order-tracker-delivery">
            <div class="row">
                <div class="col">
                    <div class="text-secondary">Sub total</div>
                </div>
                <div class="col text-right">
                <div class="text-secondary">${{ number_format($order->sub_total, 2) }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 order-tracker-delivery">
            <div class="row">
                <div class="col">
                    <div class="text-secondary">Tax</div>
                </div>
                <div class="col text-right">
                <div class="text-secondary">${{ $order->tax }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 order-tracker-delivery">
            <div class="row">
                <div class="col">
                    <div class="text-secondary">Delivery charges (0%)</div>
                </div>
                <div class="col text-right">
                <div class="text-secondary">${{ $order->delivery_charges }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 order-tracker-delivery">
            <div class="row">
                <div class="col">
                    <div class="text-secondary">Service charges</div>
                </div>
                <div class="col text-right">
                <div class="text-secondary">${{ $order->service_charges }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-1 order-tracker-delivery">
            <div class="row">
                <div class="col">
                    <div class="text-secondary"><b>Total</b></div>
                </div>
                <div class="col text-right">
                <div class="text-secondary"><b> ${{ number_format($order->total_price, 2) }}</b></div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
@endsection
@section('scripts')
<script src="/js/app.js"></script>
@endsection
