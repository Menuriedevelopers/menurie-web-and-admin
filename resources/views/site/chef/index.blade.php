@extends('site.chef.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section id="dashboard-analytics">
                <div class="row">
                    <!-- Sales Chart Starts-->
                    <div class="col-xl-12 col-md-6 col-12 sales-card">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <div class="card-title-content">
                                    <h4 class="card-title">Sales</h4>
                                    <small class="text-muted">Calculated in last 7 days</small>
                                </div>
                                <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div id="sales-chart" class="mb-2"></div>
                                    <div class="d-flex justify-content-between my-1">
                                        <div class="sales-info d-flex align-items-center">
                                            <i class='bx bx-up-arrow-circle text-primary font-medium-5 mr-50'></i>
                                            <div class="sales-info-content">
                                                <h6 class="mb-0">Best Sellings</h6>
                                                <small class="text-muted">{{$best['date']}}</small>
                                            </div>
                                        </div>
                                        <h6 class="mb-0">{{$best['sale']}}</h6>
                                    </div>
                                    <div class="d-flex justify-content-between mt-2">
                                        <div class="sales-info d-flex align-items-center">
                                            <i class='bx bx-down-arrow-circle icon-light font-medium-5 mr-50'></i>
                                            <div class="sales-info-content">
                                                <h6 class="mb-0">Lowest Sellings</h6>
                                                <small class="text-muted">{{$low['date']}}</small>
                                            </div>
                                        </div>
                                        <h6 class="mb-0">{{$low['sale']}}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Growth Chart Starts-->
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-12 activity-card">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Activity</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body pt-1">
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-primary m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-bar-chart-alt-2 text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Total Sales</small>
                                            <small class="float-right">{{$sales}}</small>
                                            <div class="progress progress-bar-primary progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="100" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-success m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-dollar text-success"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Income Amount</small>
                                            <small class="float-right">${{$income}}</small>
                                            <div class="progress progress-bar-success progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="100" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex activity-content">
                                        <div class="avatar bg-rgba-warning m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-stats text-warning"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Total completed orders</small>
                                            <small class="float-right">{{$completed}}</small>
                                            <div class="progress progress-bar-warning progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="100" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex mb-75">
                                        <div class="avatar bg-rgba-danger m-0 mr-75">
                                            <div class="avatar-content">
                                                <i class="bx bx-check text-danger"></i>
                                            </div>
                                        </div>
                                        <div class="activity-progress flex-grow-1">
                                            <small class="text-muted d-inline-block mb-50">Preparing Orders</small>
                                            <small class="float-right">{{$preparing}}</small>
                                            <div class="progress progress-bar-danger progress-sm">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="100" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="card ">
                            <div class="card-header">
                                <h4 class="card-title">
                                    Today Orders Timeline
                                </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <ul class="widget-timeline mb-0">
                                        @php
                                            $orders = App\Order::where('chef_id', Auth::guard('chef')->user()->id)->get()->take(5);
                                        @endphp
                                        @foreach($orders as $item)
                                        <li class="timeline-items timeline-icon-primary active">
                                            <div class="timeline-time">{{date_format(date_create($item->created_at), 'F m, d')}}</div>
                                            <h6 class="timeline-title">Order # {{$item->id}} worth: {{$item->total_price}}</h6>
                                            <p class="timeline-text">{{$item->created_at->diffForHumans()}}</p>
                                            <div class="timeline-content">
                                                @php
                                                    $details = App\OrderDetail::with('item', 'orderAddons')->where('order_id', $item->id)->get();
                                                @endphp
                                                @foreach ($details as $item1 )
                                                    <p>{{$item1->item->item_name ?? '#'.$item1->item_id.' N/A'}}</p>
                                                    @if($item1->orderAddons)
                                                    <p>
                                                        (@foreach($item1->orderAddons as $item2) <small> {{$item2->addon->addon_name}}</small>@endforeach)
                                                    </p>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Dashboard Analytics end -->

        </div>
    </div>
</div>
</div>
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
@endsection
@section('scripts')
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>

var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
  auth: {
    headers: {
      'X-CSRF-Token': "{{ csrf_token() }}"
    }
  },
  cluster: 'ap2',
  forceTLS: true
});
// Pusher.logToConsole = true;
var channel = pusher.subscribe('order-alert');
channel.bind('App\\Events\\OrderStatusChanged', function(data) {
  console.log(data);
  var notify = new Notification('Menurie', {
     body: 'New order added!',
     icon: 'https://www.menurie.com/site/asset/images/img1.png',
 });
 notify.onclick = function(){
   event.preventDefault();
   window.open('/chef/orders/'+data.id, '_blank');
 };
});
var salesChartOptions = {
    chart: {
      height: 100,
      type: 'bar',
      stacked: true,
      toolbar: {
        show: false
      }
    },
    grid: {
      show: false,
      padding: {
        left: 0,
        right: 0,
        top: -20,
        bottom: -15
      }
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '20%',
        endingShape: 'rounded'
      },
    },
    legend: {
      show: false
    },
    dataLabels: {
      enabled: false
    },
    colors: ['#5A8DEE', '#E2ECFF'],
    series: [{
      name: 'Sales',
      data: @json($salesArr)
    }],
    xaxis: {
      categories: @json($dates),
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      },
      labels: {
        style: {
          colors: '#828D99'
        },
        offsetY: -5
      }
    },
    yaxis: {
      show: false,
      floating: true,
    },
    tooltip: {
      x: {
        show: false,
      },
    }
  }

  var salesChart = new ApexCharts(
    document.querySelector("#sales-chart"),
    salesChartOptions
  );

  salesChart.render();
</script>
@endsection
