@extends('site.chef.main')
@section('styles')
<link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/forms/select/select2.min.css">
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="users-view">
                    <div class="row">
                        <div class="col-12 col-sm-7">
                            <div class="media mb-2">
                                <a class="mr-1" href="#">
                                    <img src="{{asset('site/asset/icons/user_ic.png')}}" alt="user" class="users-avatar-shadow rounded-circle" height="30" width="30">
                                </a>
                                <div class="media-body pt-25">
                                <h4 class="media-heading"><span class="users-view-name">{{ $order->customer->first_name }}</span><span class="text-muted font-medium-1"> @</span><span class="users-view-username text-muted font-medium-1 ">{{ $order->customer->email }}</span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 px-0 d-flex justify-content-end align-items-center px-1 mb-2">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Received at:</td>
                                                <td>{{ $order->created_at }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Restaurant:</td>
                                                <td class="users-view-latest-activity">{{ $order->restaurant_id }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Total:</td>
                                                <td class="users-view-verified">{{ $order->total_price }}</td>
                                                </tr>
                                                @if ($order->promocode != null)
                                                <tr>
                                                    <td>Promo:</td>
                                                <td class="users-view-verified">{{ $order->promo_code }}</td>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <td>Payment Method:</td>
                                                <td class="users-view-role">{{ $order->payment_method }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status:</td>
                                                <td><span class="badge badge-light-info users-view-status">{{ $order->order_status }}</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                @if($order->order_status == 'Picked')
                                                <thead>
                                                    <tr>
                                                        <th>Riders</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                            <form id="assign" method="POST" action="{{route('chef.assignRider')}}">
                                                @csrf
                                                <input type="hidden" name="order" value="{{$order->id}}">
                                                    <tr>
                                                        <td>
                                                            @php
                                                                $riderOrder = App\RiderOrder::where('order_id', $order->id)->first();
                                                                $rider = App\Rider::find($riderOrder->rider_id);
                                                            @endphp
                                                        </td>
                                                        <td>
                                                          <img src="{{$rider->image}}" alt="{{$rider->first_name}}">
                                                        </td>
                                                        <td>{{$rider->first_name}}</td>
                                                        <td>{{$rider->last_name}}</td>
                                                        <td><a href="tel:{{$rider->phone}}">{{$rider->phone}}</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                         
                                                        </td>
                                                    </tr>
                                            </form>
                                                </tbody>
                                                @endif
                                                @if($order->order_status == 'Preparing')
                                                <thead>
                                                    <tr>
                                                        <th>Find Riders</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                            <form id="assign" method="POST" action="{{route('chef.assignRider')}}">
                                                @csrf
                                                <input type="hidden" name="order" value="{{$order->id}}">
                                                    <tr>
                                                        <td>
                                                          Inhouse riders:
                                                        </td>
                                                        <td>
                                                          <section class="basic-select2">
                                                              <div class="form-group">
                                                              <select name="rider" class="select2 form-control" id="basicSelect">
                                                                <option value="0">Select rider</option>
                                                                  @foreach ($riders as $item)
                                                              <option value="{{ $item->id }}">{{ $item->first_name .' '.$item->last_name }}</option>
                                                                  @endforeach
                                                              </select>
                                                              </div>
                                                          </section>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                          <button type="submit" class="btn btn-success ml-1">
                                                              <i class="bx bx-check d-block d-sm-none"></i>
                                                              <span class="d-none d-sm-block">Assign</span>
                                                          </button>
                                                        </td>
                                                        {{--<td><label class="h3">Or</label></td>
                                                        <td></td>
                                                         <td><button type="button" class="btn btn-info">Send request</button></td> --}}
                                                    </tr>
                                            </form>
                                                </tbody>
                                                @endif

                                                @if($order->order_status == 'Placed')
                                                    <thead>
                                                        <tr>
                                                            Order Request
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><a class="btn btn-menurie" href="/chef/order/accept/{{$order->id}}">Accept</a></td>
                                                            {{-- <td><a class="btn btn-danger" href="/chef/decline/order/{{$order->id}}">Decline</a></td> --}}
                                                        </tr>
                                                    </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row bg-primary bg-lighten-5 rounded mb-2 mx-25 text-center text-lg-left">
                                    <div class="col-12 col-sm-4 p-2">
                                        <h4 class="text-primary mb-0">Order Address:</h4>
                                    </div>
                                    <div class="col-12 col-sm-4 p-2">
                                    </div>
                                    <div class="col-12 col-sm-4 p-2">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Deliver To:</td>
                                            <td class="users-view-username">{{ $order->address->delivery_to }}</td>
                                            </tr>
                                            @if($order->address->delivery_to == 'hotel')
                                            <tr>
                                                <td>Hotel:</td>
                                            <td></td>
                                            </tr>
                                            <tr>
                                                <td>Room No:</td>
                                                <td>XYZ Corp. Ltd.</td>
                                            </tr>
                                            <tr>
                                                <td>Delivery Time:</td>
                                                <td></td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td>Address:</td>
                                            <td class="users-view-name">{{ $order->address->address }}</td>
                                            </tr>
                                            <tr>
                                                <td>Note:</td>
                                            <td class="users-view-email">{{ $order->address->delivery_note }}</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <h5 class="mb-1"><i class="bx bx-info-circle"></i> Order Detail</h5>
                                    <table class="table table-borderless">
                                        <tbody>
                                            @foreach($order->detail as $detail)
                                            <tr>
                                                <td>Item:</td>
                                                <td>
                                                    {{ $detail->item->item_name }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Quantity</td>
                                            <td>{{ $detail->quantity }}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Addons:
                                                    <div class="row mx-1">
                                                        @php
                                                            $get = App\OrderAddons::where('order_detail_id',$detail->id)->get();
                                                        @endphp
                                                        @foreach($get as $addon)
                                                        <div class="col">
                                                        <p>{{$addon->addon->name}}</p><span>{{$addon->quantity}}</span>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <h5 class="mb-1"><i class="bx bx-info-circle"></i>Charges</h5>
                                    <table class="table table-borderless mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Service Charges:</td>
                                            <td>{{ $order->serice_charges }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tax:</td>
                                                <td>{{ $order->tax }}</td>
                                            </tr>
                                            <tr>
                                                <td>Sub Total:</td>
                                            <td>{{ $order->sub_total }}</td>
                                            </tr>
                                            <tr>
                                                <td>Total:</td>
                                            <td>{{ $order->total_price }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="../../../app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="../../../app-assets/js/scripts/forms/select/form-select2.js"></script>
@endsection
