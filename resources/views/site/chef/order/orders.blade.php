@extends('site.chef.main')
@section('styles')
<link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/forms/select/select2.min.css">
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Orders</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('chef.index') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            @if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
@if(session()->has('error'))
<div class="alert alert-danger">
    {{ session()->get('error') }}
</div>
@endif
            <!-- div start -->
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of orders</h4>
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>Customer Name</th>
                                            <th>Payment Method</th>
                                            <th>Total</th>
                                            <th>Order Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($orders as $item)
                                            <tr>
                                            <td class="text-bold-500">{{ $item->customer->first_name . $item->customer->last_name }}</td>
                                            <td class="text-bold-500">{{ $item->payment_method }}</td>
                                            <td class="text-bold-500">{{ $item->total_price }}</td>
                                            <td class="text-bold-500">{{ $item->order_status }}</td>
                                                <td>
                                                    <a href="{{ route('chef.orderDetail',$item->id) }}" class="btn btn-outline-primary mr-1 mb-1" ><i class="bx bx-show" ></i><span class="align-middle ml-25">Detail</span></a>                 
                                                </td>
                                            </tr>
                                        @empty
                                            <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No orders yet</h3> </td> </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                <div class="mx-1">{{ $orders->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- div end -->
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@section('scripts')
<script src="../../../app-assets/js/scripts/modal/components-modal.js"></script>
<script src="../../../app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="../../../app-assets/js/scripts/forms/select/form-select2.js"></script>
@endsection