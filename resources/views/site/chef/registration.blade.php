<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title> Menurie - Chef register</title>
    @include('site.chef.layouts.styles')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('site/asset/styles/intlTelInput.css') }}">
    <style>
.bigdrop{
    width: 430px !important;
}
    </style>
</head>
<body class="vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="auth-login" class="row flexbox-container">
                    <div class="col-xl-2 col-2">
                    <img src="{{ asset('site/asset/images/header12.png') }}" alt="">
                    </div>
                    <div class="col-xl-8 col-8">
                        <div class="card bg-authentication mb-0">
                            <div class="row m-0">
                                <div class="col-md-6 col-sm-12 px-0">
                                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                        <div class="card-header">
                                            <div class="card-title text-center">
                                                <a href="{{route('index')}}"><img width="100" src="{{asset('site/asset/images/chef.png')}}" alt="menurie"></a><br><br>
                                                <h4 class="mb-2">Register with us!</h4>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <p> <small> Please enter your details to sign up and be part of Menurie</small>
                                            </p>
                                        </div>
                                        <div class="card-content">
                                            <div class="card-body">
                                                @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                                <form method="POST" action="{{route('chef.register')}}">
                                                    @csrf
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 mb-50">
                                                            <label>first name</label>
                                                            <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" value="{{old('first_name')}}"/>
                                                            @error('first_name')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group col-md-6 mb-50">
                                                            <label>last name</label>
                                                            <input type="text" name="last_name" class="form-control" placeholder="Enter Last Name" value="{{old('last_name')}}"/>
                                                            @error('last_name')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-600">Email</label>
                                                        <input type="email" name="email" class="form-control" placeholder="Enter Email" value="{{old('email')}}" />
                                                        @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group mb-2">
                                                        <label class="text-bold-600">Phone</label>
                                                        <input type="tel" class="form-control" name="phone" id="phone" placeholder="Enter Phone" value="{{old('phone')}}" />
                                                        @error('phone')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-600">Password</label>
                                                        <input type="password" name="password" class="form-control" placeholder="Enter password"/>
                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-600">Confirm Password</label>
                                                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password"/>
                                                        @error('password_confirmation')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-600">Enter kitchen address</label>
                                                        <input type="text" id="pac-input" class="form-control" name="address" placeholder="Enter address" value="{{old('address')}}">
                                                        <input type="hidden" id="latlong" name="latlong">
                                                    </div>
                                                    <button type="submit" class="btn btn-menurie glow position-relative w-100">SIGN UP<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                                </form>
                                                <hr>
                                            <div class="text-center"><small class="mr-25">Already have an account?</small><a href="{{route('chef.login')}}"><small>Sign in</small> </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                    <div class="card-content">
                                    <img class="img-fluid" src="{{ asset('site/asset/images/login-img1.png') }}" alt="branding logo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-2">
                        <img src="{{ asset('site/asset/images/header2.png') }}" alt="">
                        </div>
                </section>
            </div>
        </div>
    </div>
    @include('site.chef.layouts.scripts')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
    <script src="{{ asset('site/asset/scripts/intlTelInput.js') }}"></script>
    <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
          // allowDropdown: false,
          autoHideDialCode: false,
          // autoPlaceholder: "off",
          // dropdownContainer: document.body,
          // excludeCountries: ["us"],
          // formatOnDisplay: false,
          geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
          hiddenInput: "full_number",
          initialCountry: "auto",
          // localizedCountries: { 'de': 'Deutschland' },
          // nationalMode: false,
          // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
          // placeholderNumberType: "MOBILE",
          // preferredCountries: ['cn', 'jp'],
          separateDialCode: true,
          utilsScript: "{{ asset('site/asset/scripts/utils.js') }}",
        });
        // $(document).ready(function() {
        //     $("#restaurant_id").select2({dropdownCssClass : 'bigdrop'});
        // });
        function initMap() {
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components','adr_address', 'geometry', 'icon', 'name']);
        autocomplete.addListener('place_changed', function() {
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          $("#latlong").val(place.geometry.location.lat()+','+place.geometry.location.lng());
          var address = '';
          if (place.address_components) {
            console.log(place);
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }
        });
      }
      </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLgNOQKcWZoxR_NqxLHYQPcbzjrw5EjRU&libraries=places&callback=initMap"
      async defer></script>
</body>
</html>
