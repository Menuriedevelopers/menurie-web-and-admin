@extends('site.chef.main')
@section('styles')
<link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/forms/select/select2.min.css">
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Items</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('chef.index') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
            @endif
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of items</h4>
                            <div class="text-right">
                                <a href="{{ route('chef.addItem') }}" class="btn btn-primary">Add Item</a>
                            </div>
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Price</th>
                                            <th>{{($type != 'special')? 'Category': 'Chef'}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($items as $item)
                                            <tr>
                                            <td class="text-bold-500">{{ $item->item_name }}</td>
                                            <td class="text-bold-500">{{ $item->description }}</td>
                                            <td class="text-bold-500">{{ $item->price }}</td>
                                            <td class="text-bold-500">{{ ($type != 'special') $item->category->name : $item->chef->first_name.' '.$item->chef->last_name }}</td>
                                            <td>
                                            <a href="{{ route('chef.editItem',$item->id) }}" class="btn btn-outline-warning mr-1 mb-1" ><i class="bx bx-edit" ></i><span class="align-middle ml-25">Edit</span></a>                 
                                            </td>
                                            </tr>
                                        @empty
                                            <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No items added</h3> </td> </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                <div class="mx-1">{{ $items->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@section('scripts')
<script src="../../../app-assets/js/scripts/modal/components-modal.js"></script>
<script src="../../../app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="../../../app-assets/js/scripts/forms/select/form-select2.js"></script>
@endsection