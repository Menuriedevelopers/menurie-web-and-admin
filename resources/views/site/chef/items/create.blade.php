@extends('site.chef.main')
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Items</h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('chef.items') }}">items</a>
                                    </li>
                                    <li class="breadcrumb-item active"><a href="#">Add new Item</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Vertical form layout section start -->
                <section id="basic-vertical-layouts">
                    <div class="row match-height">
                        <div class="col-md-6 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Item info</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        @if(session()->has('message'))
                                                    <div class="alert alert-success">
                                                        {{ session()->get('message') }}
                                                    </div>
                                                    @endif
                                                    @if(session()->has('error'))
                                                    <div class="alert alert-danger">
                                                        {{ session()->get('error') }}
                                                    </div>
                                                    @endif
                                                    @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                    <form method="POST" class="contact-repeater" action="{{ route('chef.storeItem') }}" enctype="multipart/form-data" class="form form-vertical">
                                        @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label>Special Item</label>
                                                            <input type="checkbox" id="specialItem" class="form-control" name="specialItem">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="item_name">Item Name</label>
                                                            <input type="text" id="item_name" class="form-control" name="item_name" placeholder="Item Name" value="{{ old('item_name') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="category">Category</label>
                                                            <select value="{{ old('category') }}" class="form-control" name="category" id="category">
                                                                <option value="">Select Category</option>
                                                                @foreach ($categories as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="item_price">Item Price</label>
                                                            <input class="form-control" type="text" id="item_price" name="item_price" placeholder="Item Price" value="{{ old('item_price') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-1">
                                                        <label for="item_image">Item Image</label>
                                                        <fieldset>
                                                            <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="item_image">Upload</span>
                                                            </div>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" id="item_image" name="item_image" aria-describedby="item_image">
                                                                <label class="custom-file-label" for="item_image">Choose file</label>
                                                            </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="item_count">Total items</label>
                                                        <input type="integer" class="form-control" name="item_count" id="item_count" value="{{ old('item_count') }}">
                                                    </div>
                                                    <div data-repeater-list="addons" class="col-6 custom-control custom-switch mb-1">
                                                        <div class="col-12 d-flex mb-1">
                                                            <div class="col-6">
                                                                <label for="addon_name">Addons</label>
                                                            </div>
                                                            <div class="col text-right">
                                                                <button class="btn btn-icon rounded-circle btn-primary" type="button" data-repeater-create>
                                                                    <i class="bx bx-plus"></i>
                                                                </button>
                                                                <span class="ml-1 font-weight-bold text-primary">ADD Addons</span>
                                                            </div>
                                                        </div>
                                                        <div class="row justify-content-between" data-repeater-item>
                                                            <div class="col-md-4 col-12 form-group d-flex align-items-center">
                                                                <i class="bx bx-menu mr-1"></i>
                                                                <input type="text" name="addon_name" class="form-control" placeholder="Name" value="{{ old('addons.*.addon_name') }}">
                                                            </div>
                                                            <div class="col-md-4 col-12 form-group">
                                                            <input type="text" name="addon_price" class="form-control" placeholder="Price" value="{{ old('addons.*.addon_price') }}">
                                                            </div>
                                                            <div class="col-md-4 col-12 form-group">
                                                                <button class="btn btn-icon btn-danger rounded-circle" type="button" data-repeater-delete>
                                                                    <i class="bx bx-x"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 mb-1">
                                                        <label for="item_desc">Description</label>
                                                    <textarea name="item_description" id="item_desc" class="form-control" cols="30" rows="5">{{ old('item_description') }}</textarea>
                                                    </div>
                                                    </div>
                                                    <div class="col-12 d-flex justify-content-end">
                                                        <button type="submit" class="btn btn-menurie mr-1 mb-1">Submit</button>
                                                        <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic Vertical form layout section end -->
            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection
@push('scripts')
<script src="{{ asset('admin/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/forms/form-repeater.js') }}"></script>
@endpush