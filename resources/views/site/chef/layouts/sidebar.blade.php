<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="">
                    <div class="brand-logo">
                    </div>
                    <h2 class="brand-text mb-0">Menurie</h2>
                </a></li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i style="color:#8D5440;" class="bx bx-x d-block d-xl-none font-medium-4"></i><i style="color:#8D5440;" class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block" data-ticon="bx-disc"></i></a>
            </li>
        </ul>
    </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
            <li class=" nav-item"><a href="{{ route('chef.index') }}"><i class="menu-livicon" data-icon="desktop"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
                </li> 
                
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Items">Items</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('chef.items') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span></a>
                        </li>
                    <li><a href="{{ route('chef.addItem') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="{{ route('chef.myOrders') }}"><i class="menu-livicon" data-icon="notebook"></i>Orders</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->