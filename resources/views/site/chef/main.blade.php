<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <title>Menurie - Chef Admin Panel</title>
    @include('site.chef.layouts.meta')
    @include('site.chef.layouts.styles')
    @yield('styles')
</head>
<!-- END: Head-->
@include('site.chef.layouts.header')
@include('site.chef.layouts.sidebar')
@yield('content')
@include('site.chef.layouts.footer')