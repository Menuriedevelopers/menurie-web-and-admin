@extends('site.layout.base')
@section('title','Contact us')
@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('site/asset/styles/cart.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/contact-us.css') }}">
@endsection
@section('Orders - Menurie')
@section('content')
<div class="second-navbar">
    <div class="container">
        <div class="row  row-spacing">
            <div class="col-lg-4 col-md-6 col-sm-12">
            </div>
            <div>
              <img class="app-stores" width="200" src="{{asset('site/asset/images/appstore.png')}}" alt="">&nbsp;&nbsp;
              <img class="app-stores" width="200" src="{{asset('site/asset/images/playstore.png')}}" alt="">
            </div>
        </div>
    </div>
</div>
<div class="container">
  <div class="row">
    <div class="col find-us">
      <div class="container">
        <div class="row mb-4">
          <div class="">
            <div class="col-md-12">
              <div class="section-heading">
                <h3>Send us a Message</h3>
              </div>
            </div>
            <div class="col-md-12">
              <div class="contact-form">
                <form id="contact" action="" method="post">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                      <fieldset>
                        <input name="name" type="text" class="form-control" id="name" placeholder="Full Name" required="">
                      </fieldset>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                      <fieldset>
                        <input name="email" type="text" class="form-control" id="email" placeholder="E-Mail Address" required="">
                      </fieldset>
                    </div>
                    <div class="col-lg-12">
                      <fieldset>
                        <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your Message" required=""></textarea>
                      </fieldset>
                    </div>
                    <div class="col-lg-12">
                      <fieldset>
                        <button type="submit" id="form-submit" class="filled-button">Send Message</button>
                      </fieldset>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
