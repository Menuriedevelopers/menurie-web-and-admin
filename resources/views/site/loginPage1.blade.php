<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{ asset('site/asset/styles/loginStyle.css') }}" />
    <title>Login - Menurie</title>
</head>

<body>
    <section>
        <div class="container-fluid login-page-wrapper p0">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 login-img-section">
                </div>
                <div class="col-xl-5 col-lg-5 col-md-10 login-form-section">
                @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{ route('customer.login')}}?redirect={{Request::input('redirect')}}">
                        @csrf
                        <div class="headings">
                            <h1>
                                Welcome to<span style="color: rgb(141, 84, 64);"> Menurie</span>
                            </h1>
                            <p>
                                Please log in to your account to <br />
                                continue with Menurie
                            </p>
                        </div>
                        <div class="form-group input-icons">
                            <i class="icon">
                                <img src="{{ asset('site/asset/images/login-msg.png') }}" width="15" />
                            </i>
                            <input type="email" name="email" class="form-control" placeholder="Enter email" />
                        </div>
                        <div class="form-group input-icons">
                            <i class="icon">
                                <img src="{{ asset('site/asset/images/login-password.png') }}" width="15" />
                            </i>
                            <input type="password" name="password" class="form-control" placeholder="Enter Password" />
                            <br />
                            <div class="forget-password"> <a href="{{ route('forget') }}">Forgot Password? </a></div>
                            <button type="submit" class="btn btn-danger login-btn"> LOG IN NOW</button>
                            <br /><br /><br /><br />
                            <div class="create-new-account">Do not have an account?</p>
                                <a href="{{ route('customerRegister') }}">Create New</a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
