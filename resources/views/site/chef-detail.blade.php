@extends('site.layout.base')
@section('title','Chef')
@section('styles')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<!--<link rel="stylesheet" href="{{ asset('site/asset/styles/breakFast.css') }}">-->
@endsection
@section('content')

<div class="modal modal-cart fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div id="content" class="modal-dialog modal-dialog2" role="document">
    </div>
</div>
<main>
    <div class="main-bottom-header">
        <div style="color:white;" class="bottom-header1">
            <div class="container-fluid bottom-header-wrapper  p0">
                <div class="row">
                    <div class="col-12 col-lg-2 col-md-2 col-sm-6  b-header1 b-header11">
                        <img width="80" class="border-content-img" src="{{ ($chef->image)?asset($chef->image):'/site/asset/images/chef-1.png' }}">
                    </div>
                    <div class="col-12 col-lg-3 col-md-3 col-sm-6  b-header1 b-header11">
                        <img class="border-content-img" src="/site/asset/images/header-content.png">
                        <span class="pure-veg">Pure Veg.</span>
                        <div class="b-header111">
                            <h3>{{$chef->first_name.' '.$chef->last_name}}</h3>
                        </div>
                        <div class="b-header111">
                            <p>{{$chef->email}}</p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2  b-header1 b-header12 ">
                        <i class="fa fa-star"></i>
                        <spna>4.3</spna><br>
                        <span>Total Ratings</span>
                    </div>
                    <div class="col-lg-2 col-md-2   b-header1 b-header13 ">
                        <i class="fa fa-clock-o"></i>
                        <spna>45 minutes</spna><br>
                        <span>Delivery Time</span>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-2   b-header1 b-header14 ">
                        <i class="fa fa-clock-o"></i>
                        <spna>$$$</spna><br>
                        <span>Price Range</span>
                    </div>
                    {{--<div class="  col-lg-3 col-md-3  col-sm-6 b-header1 b-header15 b-btn">
                        <button class="btn btn-danger">
                            <img src="/site/asset/images/heart.png" width="15">
                            Favorite</button>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    <br><br>
<div class="container">
        <div class="row">
          <div class="col-12">
            <div class="row">
              @foreach($items as $item1)
              <div class="col-md-3 col-sm-4 col-xs-12 mb-4">
                  <div class="card" style="min-height: 450px;max-height:450px;border-radius:1.25rem !important;">
                    <a data-id="{{ $item1->id }}" class="cart-button">
                      <div class="card-header" style="background:white; border:none;border-radius:20px 20px 0 0;">
                          <div class="row">
                              <div class="col-md-8 col-sm-8 col-8">
                                  <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                              </div>
                              <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                  <i class="fa fa-heart-o" style="cursor: pointer;" aria-hidden="true"
                                      onclick="changeImage()"></i>
                              </div>
                          </div>
                      </div>
                      <div class="card-body" style="width: 95%;">
                          <img src="{{ asset($item1->item_image) }}" alt="...">
                      </div>
                      <div class="card-footer text-center" style="background: white;border:none;border-radius:0 0 20px 20px;">
                          <h5 class="">{{$item1->item_name }}</h5>
                          <p class="">${{ $item1->price }}</p>
                      </div>
                    </a>
                  </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
    </div>
</main>
@endsection
@section('scripts')
<script src="{{ asset('site/asset/scripts/foodDetail.js') }}"></script>
<script src="{{ asset('site/asset/scripts/script.js') }}"></script>
@endsection