@extends('site.layout.base')
@section('styles')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/style.css') }}">
<style type="text/css">
  .form-style{
    margin-left: 0px;margin-right: 400px;margin-top: 15px;
  }
  ul.typeahead{
      margin-left: -65px;
      width: 701px;
      height: 300px;
      margin-top: 6px;
      border: none;
  }
  @media(max-width: 1024px) and (min-width: 769px){
    ul.typeahead{
        margin-left: -45px;
        width: 530px;
        height: 300px;
        margin-top: 6px;
        border: none;
    } 
  }
  .flickity-viewport{
    overflow-x: hidden;
  }
  button.flickity-button.flickity-prev-next-button.previous{
    background-image: url(/site/asset/icons/left-arrow.svg);
    width: 32px;
    height: 32px;
    background-repeat: no-repeat;
    border: none;
    border-radius: 50%;
  }
  button.flickity-button.flickity-prev-next-button.next{
    background-image: url(/site/asset/icons/next.svg);
    width: 32px;
    height: 32px;
    background-repeat: no-repeat;
    border: none;
    border-radius: 50%;
    margin-left: 10px;
  }
  @media(max-width: 768px) and (min-width: 426px){
    .form-style{
      margin-left: 0px;margin-right: 400px;margin-top: 50px;
    } 
  }
  @media(max-width: 425px){
    .form-style{
      margin-left: 0px;margin-right: 0px;margin-top: 56px;
    }  
  }
</style>
@endsection
@section('title','Food')
@section('content')
<div class="container mt-2">
  <div class="header-search-wrapper">
      <h2>Your search <span class="menurie-text">Results</span></h2>
      <div class="header-location">
          <form method="POST" action="{{ route('searching') }}" class="form-style" style="background-color: #F5F5F5">
              @csrf
              <div class="row m-0 py-2 px-2">
                <div class="col-1 pr-0 my-auto pl-2 text-left">
                  <i class="fa fa-search pt-0" aria-hidden="true"></i> 
                </div>
                <div class="col-lg-10 col-md-10 col-9 my-auto pl-0 pr-0">
                      <input type="text" name="record" class="location-input w-100 typeahead" placeholder="Search food here..." value="{{ $product }}" autocomplete="off" required>
                </div>
                <div class="col-lg-1 col-md-1 col-2 px-0 my-auto ms-0">
                  <div class="rounded-btn ml-auto">
                      <button type="submit" class="btn text-white">
                        Search
                        {{-- <img src="/site/asset/images/next-icon.svg"> --}}
                      </button>
                    <!-- <a href="" type="submit"><img src="/site/asset/images/next-icon.svg"></a> -->
                  </div> 
                </div>
              </div>    
          </form>
      </div>
  </div>
</div>
<div class="container mt-3">
  @if($count<2)
    <h5>{{ $count }} dish found</h5>
  @else
    <h5>{{ $count }} dishes found</h5>
  @endif
</div>
<div class="container mt-4">
    <div class="row">
      @foreach($items as $item)
      <div class="col-md-3 col-sm-4 col-xs-12 mb-4">
        <div id="round">
          <div class="card">
              <div class="card-header">
                  <div class="row">
                      <div class="col-md-8 col-sm-8 col-8">
                          <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                      </div>
                      <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                          <i class="fa fa-heart-o" aria-hidden="true"></i>
                      </div>
                  </div>
              </div>
              <div class="card-body text-center pb-0">
                  <div class="card-body-image ml-0">    
                      <img width="150" height="150" src="{{asset($item->item_image)}}" alt="...">
                  </div>
              </div>
              <div class="overflow card-footer text-left border-0 pt-0">
                  <h5 class="mb-0">{{$item->item_name}}</h5>
                  @php
                      if ($item->chef_id == 0)
                          $chefName = 'Menurie Chef';
                      else
                          $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                  @endphp
                  <div class="row m-0">
                      <div class="col-12 p-0">
                          <p class="text-muted m-0 col-gray mb-3">Breakfast <!--{{$chefName}}--></p>
                      </div>
                      <div class="col-8 p-0 my-auto">
                          <p class="m-0 font-weight-bold colr-gray">${{ $item->price }}</p>
                      </div>
                      <div class="col-4 p-0 text-right mt-auto">
                          <a data-id="{{ $item->id }}" class="btn cart-button"><i class="fa fa-plus" aria-hidden="true"></i></a>
                      </div>
                  </div>
              </div>
          </div>          
        </div>
      </div>
      @endforeach
    </div>
</div>
<br><br>
@endsection
@section('scripts')
<script src="{{ asset('site/asset/scripts/home.js') }}"></script>
<script src="{{ asset('site/asset/scripts/foodDetail.js') }}"></script>
<script src="{{ asset('site/asset/scripts/script.js') }}"></script>
<script src="{{asset('site/asset/scripts/flickity-docs.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js" integrity="sha512-HWlJyU4ut5HkEj0QsK/IxBCY55n5ZpskyjVlAoV9Z7XQwwkqXoYdCIC93/htL3Gu5H3R4an/S0h2NXfbZk3g7w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
    var path="{{route('autocomplete')}}";
    $('.typeahead').typeahead({
        source: function(terms,process){
            return $.get(path,{terms:terms},function(data){
                return process(data);
            });
        },
        updater: function(item) {
            var name = item.replace(/\s+/g, '-');
            window.location.href = window.location.origin + '/search-result/' + name;
        }
    });
</script>
@endsection
