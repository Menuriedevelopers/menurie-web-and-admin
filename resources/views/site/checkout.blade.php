@extends('site.layout.base')
@section('title','Checkout')
@section('styles')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('site/asset/styles/checkout.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/checkout2.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<script src="https://js.stripe.com/v3/"></script>
@endsection
@section('content')
    <div class="container">
        <div class="row mt-lg-5 mt-md-5 mx-0">
        <form id='payment-form' action="{{ route('stripe.store') }}" method="POST">
            @csrf
            <div class="col-lg-6 col-md-8 col-sm-10 pay-form px-0">
                <div class="pay-form-checkout">Checkout</div>
                <div class="row mx-0">
                    <div class="col">
                        <div class="pay-form-delivery">Delivery Information</div>
                        <div class="row">
                            <div class="col-12">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @php
                                $fname = '';
                                $lname = '';
                                $email ='';
                                $phone ='';
                                @endphp
                                @auth('customer')
                                  @php
                                    $fname = @auth('customer')->user()->first_name;
                                    $lname = @auth('customer')->user()->last_name;
                                    $email = @auth('customer')->user()->email;
                                    $phone = @auth('customer')->user()->phone;
                                  @endphp
                                @endauth
                                <div class="form-group">
                                    <label>First name</label>
                                    <input type="text" class="form-control checkout-form-name text-left" name="first_name" value="{{old('first_name',$fname)}}" placeholder="Enter first name">
                                </div>
                                <div class="form-group">
                                    <label>Last name</label>
                                    <input type="text" class="form-control checkout-form-name text-left" name="last_name" value="{{old('last_name',$lname)}}" placeholder="Enter last name">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control checkout-form-name text-left" name="email" aria-describedby="emailHelp" value="{{old('email',$email)}}" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label>Phone number</label>
                                    <input type="text" class="form-control checkout-form-name text-left" name="phone" value="{{old('phone',$phone)}}" placeholder="Enter phone number">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12 d-flex">
                                            @php $location = session()->get('location'); 
                                            if($location){
                                                $locationName = $location['name'];
                                                $locationLat = $location['lat'];
                                                $locationLng = $location['lng'];
                                            }else{
                                                $locationName = '';
                                                $locationLat = '';
                                                $locationLng = '';
                                            }
                                            @endphp
                                            <input type="text" name="address" class="form-control checkout-form-city text-left" id="pac-input" value="{{$locationName}}" placeholder="Address #1">
                                            <input type="hidden" name="latitude" value="{{$locationLat}}">
                                            <input type="hidden" name="longitude" value="{{$locationLng}}">
                                            <div class=" header-location">
                                                <img class="location-icon" src="/site/asset/images/location.png" width="18" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" name="address2" class="form-control checkout-form-city text-left"
                                                placeholder="Address #2">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <select name="state" class="form-control checkout-form-option">
                                                {{ get_state_list( old('state') ) }}
                                            </select>
                                        </div>
                                        <div class="col"><input type="text" name="zipcode" class="form-control checkout-form-zip" placeholder="Zip Code"></div>
                                    </div>
                                </div>
                                <div>
                                    <div class="row m-0">
                                        <div class="delivery-to">
                                            Delivery To</div>
                                        <div class="col-12 cc-selector px-0">
                                            <input id="home" type="radio" name="delivery_type" value="home" />
                                            <label class="drinkcard-cc home" for="home" title="Home">
                                                <div class="w-70"><img src="{{ asset('site/asset/icons/icon_Home.png') }}">Home</div>
                                            </label>
                                            <input id="office" type="radio" name="delivery_type" value="office" />
                                            <label class="drinkcard-cc office" for="office" title="Office">
                                                <div class="w-70"><img src="{{ asset('site/asset/icons/Icon_office.png') }}">Office</div>
                                            </label>
                                            <input id="cinema" type="radio" name="delivery_type" value="cinema" />
                                            <label class="drinkcard-cc cinema" for="cinema" title="Cinema">
                                                <div class="w-70"><img src="{{ asset('site/asset/icons/Cinema_Icon.png') }}">Cinema</div>
                                            </label>
                                            <input id="outdoor" type="radio" name="delivery_type" value="outdoor" />
                                            <label class="drinkcard-cc outdoor" for="outdoor"
                                                title="Outdoor">
                                                <div class="w-70"><img src="{{ asset('site/asset/icons/Outdoor_icon.png') }}">Outdoor</div>
                                            </label>
                                            <input id="stadium" type="radio" name="delivery_type" value="stadium" />
                                            <label class="drinkcard-cc stadium" for="stadium"
                                                title="Stadium">
                                                <div class="w-70"><img src="{{ asset('site/asset/icons/Stadium_Icon.png') }}">Stadium</div>
                                            </label>
                                            <input id="hotel" type="radio" name="delivery_type" value="hotel" />
                                            <label class="drinkcard-cc hotel" for="hotel" title="Hotel">
                                                <div class="w-70"><img src="{{ asset('site/asset/icons/hotel_Icon.png') }}">Hotel</div>
                                            </label>
									        <input id="other" type="radio" name="delivery_type" value="other" />
									        <label class="drinkcard-cc other" for="other" title="Other">
									            <div class="w-70"><i class="fa fa-plus" aria-hidden="true"></i>Others</div>
									        </label>
                                        </div>
                                    </div>
                                    <div id="hotel-inputs" style="display:none;" class="form-group">
                                        <div class="form-group">
                                            <input type="text" name="hotelName" class="form-control checkout-form-name text-left"
                                                placeholder="Hotel Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="roomNo" class="form-control checkout-form-name text-left"
                                                placeholder="Room No">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="floor" class="form-control checkout-form-name text-left"
                                                placeholder="Floor">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="personName" class="form-control checkout-form-name text-left"
                                                placeholder="Person getting the order?">
                                        </div>
                                        <div class="form-group">
                                            <input type="time" name="deliveryTime" class="form-control checkout-form-name text-left"
                                                placeholder="Delivery Time">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="note" id="note" cols="30" rows="10" placeholder="Delivery Note"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="payment-head row mx-0">
                    <div class="col-12 mb-3">
                        <span class="d-block">Payment</span>
                    </div>
                    <div class="col-md-2 col-sm-3 col-3 card-img1">
                        <img src="{{ asset('site/asset/images/card1.png') }}" class="mx-auto d-block img-fluid my-bank-icon w-100">
                    </div>
                    <div class="col-md-2 col-sm-3 col-3 card-img2">
                        <img src="{{ asset('site/asset/images/card4.png') }}" class="mx-auto d-block img-fluid my-bank-icon w-100">
                    </div>
                    <div class="col-md-2 col-sm-3 col-3 card-img3">
                        <img src="{{ asset('site/asset/images/card3.png') }}" class="mx-auto d-block img-fluid my-bank-icon w-100">
                    </div>
                    <div class="col-md-2 col-sm-3 col-3 card-img4">
                        <img src="{{ asset('site/asset/images/card2.png') }}" class="mx-auto d-block img-fluid my-bank-icon w-100">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="row mx-0">
                            <div class="col-12">
                                <div id="card-element">
                                </div>
                                <div id="card-errors" role="alert"></div>
                                <div class="d-inline-flex row flex-nowrap w-100 m-0 mt-4">
                                    <button type="submit" class="col-5 btn btn-primary card-holder-submit pay-btn my-auto m-0 w-100"> Pay Now</button>
                                    <div class="payment-head col-2 my-auto text-center px-0">OR</div>
                                    <button type="button" class="col-5 btn btn-primary card-holder-submit cod-btn my-auto m-0 w-100">
                                    Cash on delivery</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
            <div class="col-lg-5 col-md-12 col-sm-10 second-checkout-section mx-auto mt-5">
                <div class="modal1">
                    <div class="modal-dialog1 model-cart">
                        <div class="modal-content1 mx-auto">
                            <div id="mapper" class="mapperr"></div>
                            <br>
                            <div class="modal-header modal-header3">
                                <h4 class="modal-title w-100">My Cart</h4>
                            </div><br><br>
                            <div class="modal-body modal-body3">
                                @php $cart = session('cart'); @endphp
                                @if($cart)
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach($cart as $cartItem)
                                <div class="container-cart">
                                  <div class="cart-col-1 col-4">
                                    <div>
                                      <i class="fa fa-times" aria-hidden="true">{{ $cartItem['quantity'] }}</i>
                                      <img class="rounded-circle" src="{{ $cartItem['photo'] }}" width="60">
                                    </div>
                                  </div>
                                  <div class="cart-col-2 col-6 my-auto pt-0 px-0">
                                    <b>{{ $cartItem['name'] }}</b> <br>
                                    @php
                                        $total += $cartItem['subTotal'];
                                    @endphp
                                    @foreach($cartItem['addons'] as $addon)
                                    <div class="">
                                      <span class="text-secondary">by {{ $addon['addonName']}}</span>&nbsp;&nbsp;({{number_format($addon['addonPrice'], 2)}}&nbsp;<i class="fa fa-times" aria-hidden="true">{{ $addon['addonQuantity']}}</i>)
                                    </div>
                                    <br>
                                    @php
                                        $total = $total + $addon['addonTotal'];
                                    @endphp
                                    @endforeach
                                    </div>
                                <div class=" cart-col-3 text-secondary col-2 my-auto">${{ number_format($cartItem['quantity'] * $cartItem['price'],2) }}</div>
                                </div>
                                <hr>
                                @endforeach
                                <div class="cart-bill-section">
                                  <div class="container">
                                      <div class="row">
                                          <div class="col-12 text-secondary item-heading"> Bill detail:</div>
                                      </div>
                                  </div>
                                  <div class="row bill-boxs">
                                    <div class="col-6">
                                        <div class="item-heading"> Items Total:</div>
                                    </div>
                                    <div class="col-6">
                                        <div class="item-amount text-right">${{ number_format($total,2) }}</div>
                                    </div>
                                  </div>
                                  <div class="row bill-boxs">
                                    <div class="col-6">
                                        <div class=" delivery-heading">Tax:</div>
                                    </div>
                                    <div class="col-6">
                                        <div class="delivery-fee  text-right"> ${{ number_format($total * 0.089,2) }}</div>
                                    </div>
                                  </div>
                                  <div class="row bill-boxs">
                                    <div class="col-6">
                                        <div class=" delivery-heading"> Delivery Fee:</div>
                                    </div>
                                    <div class="col-6">
                                        <div class="delivery-fee  text-right"> $0.00</div>
                                    </div>
                                  </div>
                                  <div class="row bill-boxs">
                                    <div class="col-6">
                                        <div class=" delivery-heading">Service charges:</div>
                                    </div>
                                    <div class="col-6">
                                        <div class="delivery-fee  text-right"> $0.00</div>
                                    </div>
                                  </div>
                                    <hr>
                                    <div class="row bill-boxs">
                                      <div class="col-6">
                                          <div class="total-amount-heading"> Total:</div>
                                      </div>
                                      <div class="col-6">
                                        <div class="item-amount text-right">${{ number_format(($total * 0.089)+$total,2) }}</div>
                                      </div>
                                    </div>
                                    <br>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    @endsection
@section('scripts')
<script>
    $("input[name='delivery_type']").change(function (e) {
        var value = $("input[type='radio']:checked").val();
        if (value == 'hotel')
        {
            $("#hotel-inputs").css('display', 'block');
            $("#hotel-inputs").attr('required', true);
        }
        else
        {
            $("#hotel-inputs").css('display', 'none');
            $("#hotel-inputs").attr('required', false);
        }
    });

    $(".cod-btn").on('click',function(){
        $("#payment-form").submit();
    });

var publishable_key = '{{ env('STRIPE_PUBLISHABLE_KEY') }}';
// Create a Stripe client.
var stripe = Stripe(publishable_key);

// Create an instance of Elements.
var elements = stripe.elements();

// Create an instance of the card Element.
var card = elements.create('card');

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.on('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
        displayError.textContent = event.error.message;
    } else {
        displayError.textContent = '';
    }
});

// Handle form submission.
var form = document.getElementById('payment-form');
var form1 = document.getElementsByClassName('pay-btn');
form.addEventListener('submit', function(event) {
    event.preventDefault();
    stripe.createToken(card).then(function(result) {
        if (result.error) {
            // Inform the user if there was an error.
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
        } else {
            // Send the token to your server.
            stripeTokenHandler(result.token);
        }
    });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
}
</script>
@endsection
