@extends('site.layout.base')
@section('content')
@php $location = session()->get('location'); @endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta property="og:title" content="Menurie" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="https://www.dev.menurie.com/" />
    <meta property="og:image" content="https://www.dev.menurie.com/favicon_io/favicon-32x32.png" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
    <link rel="stylesheet" href="{{ asset('site/asset/styles/style.css') }}">
    <link rel="stylesheet" href="{{ asset('site/asset/styles/cart-style.css') }}">
    <link rel="shortcut icon" href="favicon_io/favicon-32x32.png" type="image/x-icon">
    <title>Menurie</title>
    <style>
        ul.typeahead{
            margin-left: -43px;
            width: 462px;
            height: 300px;
            margin-top: 10px;
            border: none;
        }
        @media (max-width: 425px) and (min-width: 376px){
            ul.typeahead{
                margin-left: -33px;
                width: 338px;
                height: 300px;
                margin-top: 10px;
                border: none;
                overflow-x: hidden;
            }   
        }
        @media (max-width: 375px){
            ul.typeahead{
                margin-left: -28px;
                width: 294px;
                height: 300px;
                margin-top: 10px;
                border: none;
                overflow-x: hidden;
            }
        }
        .dropdown-toggle::after {
            display: inline-block;
            margin-left: 0.255em;
            vertical-align: 0.255em;
            content: "";
            border-top: 0em solid;
            border-right: 0.3em solid transparent;
            border-bottom: 0;
            border-left: 0.3em solid transparent;
        }
        .pac-container {
            z-index: 10000 !important;
        }
        .slide {
          margin-left: 20px;
          /* height: 200px; */
        }
        .carousel-cell{
          width: 75%;
          margin-right: 10px;
        }
        .flickity-viewport{
          overflow-x: hidden;
        }
        /* .chef-container .carousel .flickity-viewport{
          height: 200px !important;
        } */
        .flickity-button-icon{
            display: none;
        }
        button.flickity-button.flickity-prev-next-button.previous{
          background-image: url('/site/asset/icons/left-arrow.svg');
          width: 32px;
          height: 32px;
          background-repeat: no-repeat;
          border: none;
          border-radius: 50%;
        }
        button.flickity-button.flickity-prev-next-button.next{
          background-image: url('/site/asset/icons/next.svg');
          width: 32px;
          height: 32px;
          background-repeat: no-repeat;
          border: none;
          border-radius: 50%;
          margin-left: 10px;
        }
    </style>
</head>
<body>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Location</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="">
                            <div>
                                <label for="location">Add new Location</label>
                                <div class="location-box-input">
                                    <img class="location-icon-1" data-target="#modalPoll-1" src="{{ asset('site/asset/images/location.png') }}"
                                    width="18" alt="">
                                    <input type="text" id="pac-input" class="location-input py-0 w-100 px-3">
                                    <hr>
                                    <div>
                                        <label for="current">Current Location</label>
                                        <div id="mapper" style="height:150px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light" id="headnev">
    <a class="navbar-brand" href="{{ route('index') }}">
            <img src="{{ asset('site/asset/images/img1.png') }}" width="65" alt="svg">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto px-lg-3 px-md-3 my-auto">
                <div class="header-location border-0 px-0">
                    <h5 class="m-0 good-heading text-left pl-1"><span id="daytime"></span> @auth('customer'){{Auth::guard('customer')->user()->first_name.'!'}}@endauth</h5>
                    {{-- <div class="d-flex">
                        <p class="m-0 header-location-text location-input" type="button" data-toggle="modal" data-target="#exampleModal" style="max-width: 215px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;float: left;">{{ $location['name'] ?? 'Add location' }}</p>
                        <p> ↓</p>
                    </div> --}}
                </div>
                <li class="nav-item dropdown bottom-list my-auto">
                </li>
            </ul>
            <span class="navbar-text">
                <ul class="navbar-nav mr-auto">
                    {{-- <li class="nav-item bottom-list">
                        <a class="nav-link" href="{{ route('chef.register') }}">
                            <span>Become a Chef</span>
                        </a>
                    </li>
                    <li class="nav-item bottom-list">
                        <a class="nav-link" href="{{ route('rider.register') }}">
                            <span>Become a Driver</span>
                        </a>
                    </li> --}}
                    @auth('customer')
                    <li class="nav-item bottom-list my-auto">
                        <a class="nav-link" href="{{ route('user-orders') }}">
                            <span>Orders</span>
                        </a>
                    </li> 
                    <li class="nav-item bottom-list">
                    <a class="nav-link" href="{{ route('userProfile') }}">
                            <span>My Profile</span>
                        </a>
                    </li>
                    <li class="nav-item bottom-list my-auto">
                        <div class="dropdown">
                          <a href="javascript:void(0)" class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration: none;">
                            Settings
                          </a>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="height: 288px">
                            <h4 class="ml-2" style="color: #8D5440">Account</h4>
                            <a class="dropdown-item" href="{{ route('userProfile') }}">Edit Profile</a>
                            <a href="javascript:void(0)" class="dropdown-item" data-toggle="modal" data-target="#myModal">
                                    Change Password
                            </a>
                            <a class="dropdown-item" href="#">Delete Account</a>
                            <h4 class="ml-2" style="color: #8D5440">Application</h4>
                            <a class="dropdown-item" href="{{route('term')}}">Terms and Condition</a>
                            <a class="dropdown-item" href="{{route('privacy')}}">Privacy Policy</a>
                          </div>
                        </div>
                    </li>
                    <li class="nav-item bottom-list my-auto">
                        <a class="nav-link" href="{{route('contact')}}">
                            <span>Help and Contact Us</span>
                        </a>
                    </li>
                    @endauth
                    <li class="nav-item bottom-list-last">
                        @auth('customer')
                        <a class="nav-link mr-5" href="{{ route('customer.logout') }}">
                            Logout
                        </a>
                        <i class="fa fa-heart-o mr-1" style="font-size: 24px;margin-top: 9px;color: #8D5440;font-weight: 600;"></i>
                        @else
                        <a class="nav-link" href="{{ route('customerLogin') }}">
                            <button type="button" class="btn btn-danger pb-2 b-rounded pb-0" style="margin-top: -4px;">
                                <!--<img src="{{ asset('site/asset/images/signin.png') }}" width="12">-->
                            Sign in</button>
                        </a>
                        @endauth
                        <div class="dropdown">
                            <span class="last-icon-img my-auto pl-3 dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/site/asset/icons/shopping-cart.svg" width="20">
                                    @php
                                    $cartCount = [];
                                    if(session()->get('cart'))
                                        $cartCount = session()->get('cart');
                                    @endphp
                                    @if(count($cartCount)!=0)
                                    <div class="batch"><p class="m-0 cart-count">{{count($cartCount)}}</p></div>
                                    @endif
                            </span>
                            <div class="dropdown-menu cartModal" id="sideModalTR" aria-labelledby="dropdownMenuLink" style="width: 340px;left: -250px;">
                                <a href="" class="dropdown-item"></a>
                            </div>
                        </div>
                    </li>
                </ul>
            </span>
        </div>
    </nav>
    </div>
    </div>
    <div class="bottom-section">
        <div class="header-first-img">
            <img src="{{ asset('site/asset/images/left-img.png') }}" width="240">
        </div>
        <div class="header-search-wrapper text-center">
        {{-- <img width="100" src="{{asset('site/asset/images/img1.png')}}" alt="Menurie"> --}}
            <h1>Find Favorite <span class="menurie-text">Food</span></h1>
            <div class="location-section">
                {{-- <img data-target="#modalPoll-1" src="https://dev.menurie.com/site/asset/images/location.png" width="18" alt="">  --}}
                <p class="m-0" data-toggle="modal" data-target="#exampleModal"><span class="location-marker"><i class="fa fa-map-marker" aria-hidden="true"></i></span><span class="location-input">{{ $location['name'] ?? 'Select location' }}</span><span class="location-down-arrow"></span></p>
            </div>
            <div class="header-location my-3">
                <form method="POST" action="{{ route('searching') }}">
                    @csrf
                    <div class="row m-0">
                      <div class="col-1 pr-0 my-auto pl-2 text-left">
                        <i class="fa fa-search pt-0" aria-hidden="true"></i> 
                      </div>
                      <div class="col-lg-10 col-md-10 col-9 my-auto pl-0 pr-0">
                            <input type="text" name="record" class="location-input w-100 typeahead" placeholder="Search food here..." autocomplete="off" required>
                      </div>
                      <div class="col-lg-1 col-md-1 col-2 px-0 my-auto ms-0">
                        <div class="rounded-btn ml-auto">
                            <button type="submit" class="btn text-white">
                                Search
                                {{-- <img src="/site/asset/images/next-icon.svg"> --}}
                            </button>
                          <!-- <a href="" type="submit"><img src="/site/asset/images/next-icon.svg"></a> -->
                        </div> 
                      </div>
                    </div>    
                </form>
            </div>
        </div>
        <div class="header-last-img ">
            <img src="{{ asset('site/asset/images/right-img.png') }}" width="240">
        </div>
    </div>
    <div class="mobile-img-section"></div>
    <div class="second-header-section">
        <div class="container">
            <div class="row">
                <div class="col application-heading">
                    <h1>Download Our New Application</h1>
                </div>
                <div class="col app-section">
                    <div class="app-store">
                        <img src="{{ asset('site/asset/images/play-store.png') }}" width="150"><br><br>
                        <img src="{{ asset('site/asset/images/app-store.png') }}" width="150">
                    </div>
                    <div class="second-header-mobile">
                        <img src="{{ asset('site/asset/images/mobile.png') }}" height="290">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    @isset($sItems)
        @if(count($sItems) > 0)
            <div class="container mt-3">
                <div class="row mb-2">
                    <div class="col h3 ml-3 char-spacing px-0">Special Dishes</div>
                    <div class="text-right">
                        <a class="view-btn" href="{{ route('foodDetail') }}">All our best dishes</a></div>
                </div>
            </div>
            <div class="container">
                <div style="outline: none;" class="carousel" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;pageDots&quot;: false }">
                    @foreach($sItems as $item)
                    <div class="best-card card item-card card-bottom-list mb-3">
                        <a data-id="{{ $item->id }}" class="cart-button">
                            <div class="row no-gutters">
                                <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                                <div class="col-5 text-center best-pro-img">
                                    <img class="p-3 rounded-circle" src="{{ asset($item->item_image) }}">
                                </div>
                                <div class="col-7">
                                    <div class="card-body ml-0 px-0 pr-3">
                                        <p class="card-text">
                                            @php
                                                if ($item->chef_id == 0)
                                                    $chefName = 'Menurie Chef';
                                                else
                                                    $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                                            @endphp
                                            <div class="overflow font-weight-bold ">{{$item->item_name }}</div>
                                            <div class="overflow text-secondary"> <!--by {{ $chefName }}--></div>
                                            <!--<div>
                                                <div class="text-secondary font-weight-bold"> ${{ $item->price }}</div>
                                            </div>-->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        @endif
    @endisset
    <div class="container mt-3">
        <div class="row mb-2">
            <div class="col h3 ml-4 char-spacing px-0">Best Dishes</div>
            <div class="text-right">
                <a class="view-btn" href="{{ route('foodDetail') }}">See all dishes</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div style="outline: none;" class="carousel" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;pageDots&quot;: false }">
            @foreach($items as $item)
            <div class="carousel-cell col-md-6 col-lg-3 col-sm-6 col-xs-12 mb-4 px-0">
                <div id="round">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-8">
                                    <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-body text-center pb-0">
                            <div class="card-body-image ml-0">    
                                <img width="150" height="150" src="{{asset($item->item_image)}}" alt="...">
                            </div>
                        </div>
                        <div class="overflow card-footer text-left border-0 pt-0">
                            <h5 class="mb-0">{{$item->item_name}}</h5>
                            @php
                                if ($item->chef_id == 0)
                                    $chefName = 'Menurie Chef';
                                else
                                    $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                            @endphp
                            <div class="row m-0">
                                <div class="col-12 p-0">
                                    <p class="text-muted m-0 col-gray mb-3">Breakfast <!--{{$chefName}}--></p>
                                </div>
                                <div class="col-8 p-0 my-auto">
                                    <p class="m-0 font-weight-bold colr-gray">${{ $item->price }}</p>
                                </div>
                                <div class="col-4 p-0 text-right mt-auto">
                                    <a data-id="{{ $item->id }}" class="btn cart-button"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>
        	</div>
            @endforeach
        </div>
    </div>
    <br>
    <div class="container mt-3">
        <div class="row mb-2">
            <div class="col h3 ml-4 char-spacing px-0">{{$breakfast->name}}</div>
            <div class="text-right">
                <a class="view-btn" href="{{ route('foodDetail',$breakfast->name) }}">See all items</a></div>
        </div>
    </div>
    <div class="container">
        <div style="outline: none;" class="carousel" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;pageDots&quot;: false }">
            @foreach($breakfast->items as $item)
            <div class="carousel-cell col-md-6 col-lg-3 col-sm-6 col-xs-12 mb-4 px-0">
                <div id="round">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-8">
                                    <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-body text-left pb-0">
                            <div class="card-body-image ml-0">    
                                <img width="150" height="150" src="{{asset($item->item_image)}}" alt="...">
                            </div>
                        </div>
                        <div class="overflow card-footer text-left border-0 pt-0">
                            <h5 class="mb-0">{{$item->item_name}}</h5>
                            @php
                                if ($item->chef_id == 0)
                                    $chefName = 'Menurie Chef';
                                else
                                    $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                            @endphp
                            <div class="row m-0">
                                <div class="col-12 p-0">
                                    <p class="text-muted m-0 col-gray mb-3">Breakfast <!--{{$chefName}}--></p>
                                </div>
                                <div class="col-8 p-0 my-auto">
                                    <p class="m-0 font-weight-bold colr-gray">${{ $item->price }}</p>
                                </div>
                                <div class="col-4 p-0 text-right mt-auto">
                                    <a data-id="{{ $item->id }}" class="btn cart-button"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>
        	</div>
            @endforeach
        </div>
    </div>
    <br>
    <div class="container mt-3">
        <div class="row mb-2">
            <div class="col h3 ml-4 char-spacing px-0">{{$pizza->name}}</div>
            <div class="text-right">
                <a class="view-btn" href="{{ route('foodDetail',$pizza->name) }}">See all items</a></div>
        </div>
    </div>
    <div class="container">
        <div style="outline: none;" class="carousel" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;pageDots&quot;: false }">
            @foreach($pizza->items as $item)
            <div class="carousel-cell col-md-6 col-lg-3 col-sm-6 col-xs-12 mb-4 px-0">
                <div id="round">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-8">
                                    <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-body text-left pb-0">
                            <div class="card-body-image ml-0">    
                                <img width="150" height="150" src="{{asset($item->item_image)}}" alt="...">
                            </div>
                        </div>
                        <div class="overflow card-footer text-left border-0 pt-0">
                            <h5 class="mb-0">{{$item->item_name}}</h5>
                            @php
                                if ($item->chef_id == 0)
                                    $chefName = 'Menurie Chef';
                                else
                                    $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                            @endphp
                            <div class="row m-0">
                                <div class="col-12 p-0">
                                    <p class="text-muted m-0 col-gray mb-3">Breakfast <!--{{$chefName}}--></p>
                                </div>
                                <div class="col-8 p-0 my-auto">
                                    <p class="m-0 font-weight-bold colr-gray">${{ $item->price }}</p>
                                </div>
                                <div class="col-4 p-0 text-right mt-auto">
                                    <a data-id="{{ $item->id }}" class="btn cart-button"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>
        	</div>
            @endforeach
        </div>
    </div>
    <br>
    <div class="container mt-3">
        <div class="row mb-2">
            <div class="col h3 ml-4 char-spacing px-0">{{$salads->name}}</div>
            <div class="text-right">
                <a class="view-btn" href="{{ route('foodDetail') }}">See all items</a></div>
        </div>
    </div>
    <div class="container">
        <div style="outline: none;" class="carousel" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;pageDots&quot;: false }">
            @foreach($salads->items as $item)
            <div class="carousel-cell col-md-6 col-lg-3 col-sm-6 col-xs-12 mb-4 px-0">
                <div id="round">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-8">
                                    <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-body text-left pb-0">
                            <div class="card-body-image ml-0">    
                                <img width="150" height="150" src="{{asset($item->item_image)}}" alt="...">
                            </div>
                        </div>
                        <div class="overflow card-footer text-left border-0 pt-0">
                            <h5 class="mb-0">{{$item->item_name}}</h5>
                            @php
                                if ($item->chef_id == 0)
                                    $chefName = 'Menurie Chef';
                                else
                                    $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                            @endphp
                            <div class="row m-0">
                                <div class="col-12 p-0">
                                    <p class="text-muted m-0 col-gray mb-3">Breakfast <!--{{$chefName}}--></p>
                                </div>
                                <div class="col-8 p-0 my-auto">
                                    <p class="m-0 font-weight-bold colr-gray">${{ $item->price }}</p>
                                </div>
                                <div class="col-4 p-0 text-right mt-auto">
                                    <a data-id="{{ $item->id }}" class="btn cart-button"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>
        	</div>
            @endforeach
        </div>
    </div>
    <br>
    <div class="container mt-3">
        <div class="row mb-2">
            <div class="col h3 ml-4 char-spacing px-0">{{$lunch->name}}</div>
            <div class="text-right">
                <a class="view-btn" href="{{ route('foodDetail') }}">See all items</a></div>
        </div>
    </div>
    <div class="container">
        <div style="outline: none;" class="carousel" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;pageDots&quot;: false }">
            @foreach($lunch->items as $item)
            <div class="carousel-cell col-md-6 col-lg-3 col-sm-6 col-xs-12 mb-4 px-0">
                <div id="round">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-8">
                                    <span class="header-text"> <i class="fa fa-star" aria-hidden="true"></i> 4.5</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-4 heart-class text-right" id="colo2-heart">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-body text-left pb-0">
                            <div class="card-body-image ml-0">    
                                <img width="150" height="150" src="{{asset($item->item_image)}}" alt="...">
                            </div>
                        </div>
                        <div class="overflow card-footer text-left border-0 pt-0">
                            <h5 class="mb-0">{{$item->item_name}}</h5>
                            @php
                                if ($item->chef_id == 0)
                                    $chefName = 'Menurie Chef';
                                else
                                    $chefName = $item->chef->first_name . ' ' . $item->chef->last_name;
                            @endphp
                            <div class="row m-0">
                                <div class="col-12 p-0">
                                    <p class="text-muted m-0 col-gray mb-3">Breakfast <!--{{$chefName}}--></p>
                                </div>
                                <div class="col-8 p-0 my-auto">
                                    <p class="m-0 font-weight-bold colr-gray">${{ $item->price }}</p>
                                </div>
                                <div class="col-4 p-0 text-right mt-auto">
                                    <a data-id="{{ $item->id }}" class="btn cart-button"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>
        	</div>
            @endforeach
        </div>
    </div>
    <br>
    {{-- <div class="trending-section-wrapper">
        <div class="container ">
            <div class="row trending-heading mb-2">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-3 h3 ml-3 char-spacing px-0">
                    Trending Chefs
                </div>
            </div>
        </div>
        <div class="container chef-container">
            <section class="carousel trending-chef-box" data-flickity="{ &quot;freeScroll&quot;: true, &quot;contain&quot;: true, &quot;pageDots&quot;: false }">
                @foreach($chefs as $item)
                <div class="mb-4 m10">
                <div class="slide">
                    <div class="list1 row">
                        <div class="list1-img mt-1 col-5 my-auto">
                            <img height="60" width="60" src="{{ ($item->image)?asset($item->image):'/site/asset/images/chef-1.png' }}" width="60">
                        </div>
                        <div class="list1-name mt-3 pl-0 col-7 my-auto">
                            <div class="h6 mb-0"> <a href="{{ route('breakFast',$item->id) }}">{{$item->first_name .' '.$item->last_name}}</a></div>
                        </div>
                    </div>
                </div>
              </div>
              @endforeach
            </section>
          </div>
    </div> --}}
    @auth('customer')
    <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Password</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <form method="POST" action="{{ route('change.password') }}">
              @csrf 
               @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
               @endforeach 
              <div class="form-group row">
                  <label for="password" class="col-md-4 col-form-label text-md-right">Current Password</label>
                  <div class="col-md-6">
                      <input id="password" type="password" class="form-control border" name="current_password" autocomplete="current-password">
                  </div>
              </div>
              <div class="form-group row">
                  <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>
                  <div class="col-md-6">
                      <input id="new_password" type="password" class="form-control border" name="new_password" autocomplete="current-password">
                  </div>
              </div>
              <div class="form-group row">
                  <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>
                  <div class="col-md-6">
                      <input id="new_confirm_password" type="password" class="form-control border" name="new_confirm_password" autocomplete="current-password">
                  </div>
              </div>
              <input type="hidden" name="id" value="{{ auth('customer')->user()->id }}">
              <div class="form-group row mb-0">
                  <div class="col-md-8 offset-md-4">
                      <button type="submit" class="btn btn-primary">
                          Update Password
                      </button>
                  </div>
              </div>
          </form>
        </div>
      </div>
    </div>
    </div>
    @endauth
@endsection
@section('scripts')
<script src="{{ asset('site/asset/scripts/home.js') }}"></script>
<script src="{{ asset('site/asset/scripts/script.js') }}"></script>
<script src="{{asset('site/asset/scripts/flickity-docs.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js" integrity="sha512-HWlJyU4ut5HkEj0QsK/IxBCY55n5ZpskyjVlAoV9Z7XQwwkqXoYdCIC93/htL3Gu5H3R4an/S0h2NXfbZk3g7w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
    var path="{{route('autocomplete')}}";
    $('.typeahead').typeahead({
        source: function(terms,process){
            return $.get(path,{terms:terms},function(data){
                return process(data);
            });
        },
        updater: function(item) {
            var name = item;          
            window.location.href = window.location.origin + '/search-result/' + name;
        }
    });
</script>
@endsection
