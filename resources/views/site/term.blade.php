@extends('site.layout.base')
@section('title','Terms & Conditions')
@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('site/asset/styles/cart.css') }}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="{{ asset('site/asset/styles/foodDetail.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/contact-us.css') }}">
<link rel="stylesheet" href="{{ asset('site/asset/styles/navbar.css') }}">
@endsection
@section('Orders - Menurie')
@section('content')
<div class="second-navbar">
    <div class="container">
        <div class="row row-spacing">
            <div class="col-lg-4 col-md-6 col-sm-12">
            </div>
            <div>
              <img class="app-stores" width="200" src="{{asset('site/asset/images/appstore.png')}}" alt="">&nbsp;&nbsp;
              <img class="app-stores" width="200" src="{{asset('site/asset/images/playstore.png')}}" alt="">
            </div>
        </div>
    </div>
</div>
<section id="privacy-mainpage">
    <div class="container">
        <div class="row">
            <div class="col-12 privacy-page">
                <h5>Menurie, inc.  Terms Of Service</h5>

                <p>Welcome to Menurie. This page (together with the documents referred to in it) tells you the terms (the “Terms”) which apply when you order any menu items (the "Items") from our menurie and/ or evrymeal website or mobile applications and related services (each referred to as an “Application”).</p>
                <p>Please read these Terms carefully before creating your menurie account or using our Application. If you have any questions relating to these Terms please contact using the webpage. If you are a consumer, you have certain legal rights when you order Items using our Application.  Your legal rights are not affected by these Terms, which apply in addition to them and do not replace them.  By setting up your menurie account, you confirm that you accept these Terms.</p>

                <h5>Purpose</h5>
                <p>Our objective is to provide  you to the businesses we partner with (“Partners") and allow you to order Items for delivery (our “Service”). Where you order from a Partner, Menurie acts as an agent on behalf of that Partner to conclude your order from our Application and to manage your experience throughout the order process. Once you have placed an order, delivery of your Items will be arranged  our Partner (“Partner Delivery) (each a “Delivery”) depending on the Partner. In some cases, the Partner may be owned by or affiliated with us.</p>

                <h5>Your account</h5>
                <p>Before you can place orders for Items using our Application, you need to open a Menurie account. When you open an account you may create a password, or other secure login method, and may also have to provide credit card details. You must keep any password you create, or other secure login method, secret, and prevent others from accessing your email account or mobile phone. If another person uses these methods to access your account, you will be responsible to pay for any Items they order, and we are not responsible for any other losses you suffer, unless the person using your password obtained it because we did not keep it secure.</p>
                <p>You may close your account at any time by requesting to do so in your account section of our website or contacting us using the contact details above. We may suspend your access to your account, or close it permanently, if we believe that your account has been used by someone else. We may also close your account if in our opinion you are abusing our Service (for example, by applying for refunds or credit to which we do not consider you are entitled, making repeated unreasonable complaints, mistreating our staff or riders, or any other good reason). If we close your account permanently we will refund any remaining account credit you have validly obtained from our customer service team or Application following any issue with an order, by applying a credit to your registered credit card, or if that is not possible for any reason, by way of a bank transfer using bank details (provided you have supplied them to us).</p>

                <h5>Service Availability</h5>
                <p>Each Partner has a prescribed delivery area. This delivery area may change at any time due to factors such as weather, or demand on our service. This is to ensure that Items reach your door at their best. Our Partners each decide their own operating hours. That means that the availability of our Service, and the range of Partners from which you can order, depends on the Partners in your area. If you try to order a delivery to a location outside the delivery area or operating hours of a Partner, or the Application is otherwise unavailable for any reason, we will notify you that ordering will not be possible.
                </p>

                <h5>Orders</h5>
                <p>When you place an order through our Application, it needs to be accepted by us or the Partner before it is confirmed. We will send you a notification if your order has been accepted (the "Confirmation Notice"). The contract for the supply of any Item you have ordered comes into existence when we send the Confirmation Notice. You are responsible for paying for all Items ordered using your account, and for related delivery charges, and for complying with these Terms, even if you have ordered the Item for someone else. Some Partners operate a minimum order value policy. This will be displayed on our Application. All Items are subject to availability.  Partners may use nuts or other allergens in the preparation of certain Items. Increasing numbers of Partners will be displaying dish by dish allergens information. Where that information is not available or if you have further questions, please contact the Partner prior to ordering if you have an allergy. We cannot guarantee that any of the Items sold by our Partners or kitchens are free of allergens.</p>


                <h5>Delivery</h5>
                <p>When you place an order you will have the choice to place it as an ASAP Delivery or a Scheduled Delivery. For an ASAP Delivery, we will tell you an estimated delivery time for your Item before you place the order, but we will attempt delivery as soon as possible; you must therefore be available to accept delivery from the time you place the order. For a Scheduled Delivery, we will tell you the time when the Item is expected to be delivered; you must be available to accept delivery for ten minutes before and ten minutes after that time.</p>
                <p>Unfortunately, despite our, and our Partner's best efforts, things do not always go to plan and factors such as traffic and weather conditions may prevent us from delivering your Item on time. If your order is more than 15 minutes late, and we haven’t notified you giving you the option to cancel your order, we will work with you to make things right unless you have caused the delay (e.g. because you gave us the wrong address or did not come to the door).</p>
                <p>We will attempt delivery at the address you provide to us when you place your order. If you need to change the delivery location after you have placed your order, we may be able to change to the address to an alternative one that is registered with your account if you let us know before the rider has been dispatched, and the new address is within the same zone as the address you originally ordered your Item to. If we cannot change the delivery address, you have the option to cancel the order, but if food preparation has started you will be charged the full price for the Item, and if the rider has been despatched you will also be charged for delivery.</p>
                <p>You will still be charged for the Item and for delivery in the event of a failed delivery if you have caused such failure for any reason. Reasons you might cause a delivery to fail include (but are not limited to):</p>

                <ul>
                    <li>You do not come to the door, did not pick up the phone when the rider contacted you using the contact information you have provided us and/or you picked up the phone but then failed to provide access within a reasonable amount of time, and the rider is unable to find a safe location to leave the food.</li>
                    <li>The rider refuses to deliver the Item to you in accordance with section 8 (Age Restricted Products).</li>
                </ul>

                <h5>Your Rights if Something is Wrong With Your Items</h5>
                <p>You have a legal right to receive goods which comply with their description, which are of satisfactory quality and which comply with any specific requirements you tell us about (and we agree to) before you place your order. If you believe that the Items you have been delivered do not comply with these legal rights, please let us know. We may request a photograph showing the problem if it is something that can be seen by inspecting the Items. We will provide a refund or account credit in respect of the affected part of the Item, and also in respect of delivery if the whole order was affected, unless we have reasonable cause to believe that the problem was caused after delivery.</p>

                <p>Prior to processing your refund or account credit, we may take into account relevant factors including the details of the order, including your account history, what happened on delivery and information from the Partner.</p>

                <h5>Age Restricted and Regulated Products</h5>
                <p>Age restricted products (including, without limitation, alcohol, tobacco and cigarettes) can only be sold and delivered to persons aged 21 or over. in the united states. By  placing an order for an age restricted product, you confirm that you are at least 21 years old. Menurie and Evrymeal operate an age verification policy whereby customers ordering age restricted products will be asked by the rider to provide proof that they are aged 21 or over before the delivery is completed. The rider may refuse to deliver any age restricted product to any person unless they can provide valid photo ID proving that they are aged 21 or over. The Partner and the rider may refuse to deliver alcohol to any person who is, or appears to be under the influence of either alcohol or drugs. Orders for items containing alcohol may only be delivered to a location that is a residential or business address. If delivery of any age restricted product is refused, you will still be charged for the relevant Item and for delivery.. Cancellation</p>

                <p>You may cancel an order without charge at any time before the Partner has started preparing the food (a "Started Order"). If you wish to cancel an order before it becomes a Started Order, please contact us immediately, via our Application. If the Partner confirms the order was not a Started Order, we will refund your payment (excluding any discount, or Voucher that was applied to the order - see Voucher and Account Credit Terms for more detail). If you cancel any order after it becomes a Started Order, you will be charged the full price for the Items, and if the rider has been despatched you will also be charged for delivery.</p>
                <p>Menurie and the Partner may notify you that an order has been cancelled at any time. You will not be charged for any orders cancelled by us or the Partner, and we will reimburse you for any payment already made using the same method you used to pay for your order. We may also apply credit to your account to reflect the inconvenience caused.</p>

                <h5>Prices, Payment and Offers</h5>
                <p>Prices include tax. You confirm that you are using our Service for personal, non-commercial use. Menurie may operate dynamic pricing some of the time, which means that prices of Items and delivery may change while you are browsing. Prices can also change at any time at the discretion of the Partner. We reserve the right to charge a Service Fee, which may be subject to change, for the provision of our Services. You will be notified of any applicable Service Fee and taxes prior to purchase on the checkout page on our Application. No changes will affect existing confirmed orders, unless there is an obvious pricing mistake. Nor will changes to prices affect any orders in process and appearing within your basket, provided you complete the order within 2 hours of creating the basket. If you do not conclude the order before the 2 hour cut-off the items will be removed from your basket automatically and the price change will apply. If there is an obvious pricing mistake we will notify you as soon as we can and you will have the choice of confirming the order at the original price or cancelling the order without charge and with a full refund of any money already paid.  Where Menurie or any Partner makes a delivery, we or the Partner may also charge you a delivery fee. This will be notified to you during the order process before you complete your order</p>

                <p>The total price of your order will be set out on the checkout page on our Application, including the prices of Items and Delivery and applicable Service Fees and taxes.</p>

                <p>Payment for all Items and deliveries can be made on our Application by credit or debit card, or other payment method made available by Menurie. Once your order has been confirmed your credit or debit card will be authorised and the total amount marked for payment. Payment is made directly to Menurie acting as agent on behalf of the Partner only. Payment may also be made by using vouchers or account credit. Use of these is subject to Menurie's Voucher and Account Credit Terms.</p>

                <p>We are authorised by our Partners to accept payment on their behalf and payment of the price of any Items or delivery charges to us will fulfil your obligation to pay the price to the Partner. In some cases, you can alternatively make your payment in cash directly to the Partner by paying the rider at the time of delivery.  Where cash payment is possible, this will be made clear on our Application before you place your order.</p>

                <p>Partners sometimes make special offers available through our Application. These are visible when you look at a Partner menu. These offers are at the discretion of the Partner. Unless the offer terms state a fixed or minimum period for which an offer will be available, it can be withdrawn at any time, unless you have already placed an order based on the offer and we have sent the Confirmation Notice.</p>

                <h5>Tips</h5>
                <p>When Placing Your Order:</p>
                <p>When you place an order, you will have the option to pay a tip to your rider or the Partner,  in addition to the purchase price of the Items in your order. Any payment will be collected by menuri using the payment method used for the original order and your rider or the Partner will receive 100% of any payment you choose to make.</p>
                <p>After You’ve Received Your Order:</p>
                <p>Once you’ve received your order, you may receive a notification giving you the chance to pay a tip to your rider. Menurie will collect payment on behalf of the rider, as their limited payment collection agent, and payment of the tips shall be considered a direct payment from you to the rider. We’ll share your first name with your rider when we notify them of the tip. Your rider will receive 100% of any payment you choose to make. As this payment is made after you receive your order, this payment is non-refundable and does not form part of your order. Depending on the payment method used for your original order, your tip may show up on your bank/credit card statement as a separate payment.</p>

                <h5>Our Responsibility for Loss or Damage That You Suffer</h5>
                <p>We are responsible to you for any loss or damage that you suffer that is a foreseeable result of our breaking these Terms or of failing to use reasonable care and skill in relation to your use of our Service. We are not responsible for any loss or damage that is not foreseeable. Loss or damage is “foreseeable” if it is either obvious that it will happen, or if you told us that it might happen, for example if you tell us about particular circumstances that might increase the loss or damage arising from our breach of these Terms before you place an order.</p>

                <p>We do not exclude or limit our responsibility to you for loss or damage where it would be unlawful to do so. This includes any responsibility for death or personal injury caused by our failure, or our employees’, agents’ or subcontractors’ failure, to use reasonable care and skill; for fraud or fraudulent misrepresentation; for breach of your legal rights in relation to the Items, as summarised at part 7 above. Subject to the previous paragraph, we are not responsible for any loss or damage that you suffer as a result of your own breach of these Terms, or as a result of any IT hardware or software failure other than a failure in our Applications.</p>

                <h5>Data Protection</h5>
                <p>We process your personal data in accordance with our Privacy Policy.</p>

                <h5>Other Terms</h5>
                <p>If either we or you have any right to enforce these Terms against the other, that right will not be lost even if the person who has the right delays enforcing it, or waives their right to enforce it in any instance.  If a court or other authority decides that any part of these Terms is illegal or ineffective, the rest of the terms will be unaffected and will remain in force.</p>

                <p>We may change these Terms from time to time. If we make any changes which affect your rights in relation to our Service, we will notify you. Changes to the Terms will not affect any orders you have placed where we have sent the Confirmation Notice. These Terms are governed by United States  law and you can bring legal proceedings in relation to our Service in the American courts.</p>
            </div>
        </div>
    </div>
</section>
@endsection