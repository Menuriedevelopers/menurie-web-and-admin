<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/site/asset/styles/loginStyle.css" />
    <link rel="stylesheet" href="{{ asset('site/asset/styles/intlTelInput.css') }}">
    <title>Forgot Password</title>
</head>

<body>
    <section>
        <div class="container-fluid login-page-wrapper p0">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 login-img-section">
                </div>
                <div class="col-xl-5 col-lg-5 col-md-10 login-form-section">
                  @if(session('error'))
                    <div class="alert alert-danger">
                        {!! session('error') !!}
                    </div>
                    @endif
                    <form method="post" action="{{request()->url()}}">
                      @csrf
                        <div class="headings">
                            <h1>Forgot Password?</h1>
                            <p>Enter your registered phone number below to <br>reset your password
                            </p>
                        </div><br><br>
                        <div class="form-group input-icons">
                            <i class="icon">
                                <img src="/site/asset/images/login-msg.png" width="15" />
                            </i>
                            <input type="tel" class="form-control" name="phone" id="phone" placeholder="Enter Phone" value="{{old('phone')}}" />
                        </div><br>
                        <button type="submit" class="btn btn-danger">Submit</button>
                        <br><br>
                        <a class="back-to-login" href="{{route('customer.login')}}">&#8592; Go to login</a>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{ asset('site/asset/scripts/intlTelInput.js') }}"></script>
    <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
          // allowDropdown: false,
          autoHideDialCode: false,
          // autoPlaceholder: "off",
          // dropdownContainer: document.body,
          // excludeCountries: ["us"],
          // formatOnDisplay: false,
          geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
          hiddenInput: "full_number",
          initialCountry: "auto",
          // localizedCountries: { 'de': 'Deutschland' },
          // nationalMode: false,
          // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
          // placeholderNumberType: "MOBILE",
          // preferredCountries: ['cn', 'jp'],
          separateDialCode: true,
          utilsScript: "{{ asset('site/asset/scripts/utils.js') }}",
        });
      </script>
</body>

</html>
