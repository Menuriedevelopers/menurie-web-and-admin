@extends('site.layout.base')
@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('site/asset/styles/cart.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('site/asset/styles/location-styles.css') }}">
@endsection
@section('Orders - Menurie')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div id="content" class="main-box clearfix">
                    <div class="table-responsive">
                        <table class="table user-list">
                            <thead>
                                <tr>
                                    <th style="border: none">
                                        <div class="text-costom-heading">My Cart</div>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="border-top: none"><span>Order</span></th>
                                    <th class="text-center" style="border-top: none"><span>PRICE</span></th>
                                    <th class="text-center" style="border-top: none"><span></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $item)
                                <tr>
                                    <td>
                                        <div class="card-title1">OD-{{ $item->id }}</div>
                                    </td>
                                    <td class="text-center">
                                        <b>${{number_format($item->total_price, 2)}}</b>
                                    </td>
                                    <td class="text-center">
                                        <div class="reorder-btn"><a herf="#" class="btn btn-outline-light" style="color:#843232;"><button class="btn btn-danger">Re-order</button>
                                        </a></div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
@endsection