@component('mail::message')
# Thankyou, {{$name}} for registering with us.

Our Admin has approved you, now you can get orders as per your kitchen timings.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
