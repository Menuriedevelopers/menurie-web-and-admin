@component('mail::message')
# Email update request

Before updating email please verify with the code sent on this email in the app to update email.

@component('mail::button', ['url' => '#'])
{{$code}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
