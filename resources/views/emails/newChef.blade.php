@component('mail::message')
# Menurie Admin

A new chef is registered with menurie login to admin panel to check chef details.

@component('mail::button', ['url' => 'https://menurie.com/admin'])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
