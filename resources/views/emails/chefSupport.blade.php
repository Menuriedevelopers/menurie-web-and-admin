@component('mail::message')
# Thankyou, {{$name}} for using our chef support system, below is the reply message of your support message

{{$message}}


Thanks,<br>
{{ config('app.name') }}
@endcomponent
