@component('mail::message')
# Thankyou, {{$name}} for choosing Menurie for your next meal.

We are glad to have you with and would be loved to see you order from here again.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
