@php
    $categoriesCount = App\Category::count();
    $dishesCount = App\Item::where('special', 0)->count();
    $specialDishesCount = App\Item::where('special', 1)->where('approved', 1)->count();
    $unapprovedCount =App\Item::where('special', 1)->where('approved', 0)->count();
    $chefsCount = App\Chef::count();
    $customersCount = App\Customer::count();
    $newCustomersCount = App\Customer::where('created_at', date('Y-m-d'))->count();

    $activeIds = App\Order::distinct('customer_id')->where('created_at', '>=', date('Y-m-d', strtotime("-7 days")))->pluck('customer_id');
    $activeCustomersCount = App\Customer::whereIn('id', $activeIds)->count();

    $inactiveIds = App\Order::distinct('customer_id')->where('created_at', '<', date('Y-m-d', strtotime("-7 days")))->pluck('customer_id');
    $inactiveCustomersCount = App\Customer::whereIn('id', $inactiveIds)->count();

    $realtimeIds = App\Order::distinct('customer_id')->where('created_at', date('Y-m-d'))->where('order_status', '!=', 'Delivered')->orWhere('created_at', date('Y-m-d'))->where('order_status', '!=', 'Cancelled')->distinct('customer_id')->pluck('customer_id');
    $realtimeCustomersCount = App\Customer::whereIn('id', $realtimeIds)->count();
    $ridersCount = App\Rider::count();
    $ordersCount = App\Order::count();
    $requestsCount= App\ItemRequest::whereHas('chef')->count();
@endphp
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Menurie Admin Panel</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/pages/app-chat.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern content-left-sidebar chat-application navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="content-left-sidebar">

    <!-- BEGIN: Header-->
    <div class="header-navbar-shadow"></div>
    <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon bx bx-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                        </ul>
                        <ul class="nav navbar-nav">
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-language nav-item">
                        </li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon bx bx-fullscreen"></i></a></li>
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name">{{ Auth::user()->name }}</span><span class="user-status text-muted">Available</span></div><span><img class="round" src="/user.png" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pb-0">
                                <a class="dropdown-item" href="{{route('settings')}}"><i class="bx bx-user mr-50"></i> Edit Admin Profile</a>
                                <div class="dropdown-divider mb-0"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="bx bx-power-off mr-50"></i> Logout</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->
    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="">
                        <div class="brand-logo">
                        </div>
                        <h2 class="brand-text mb-0">Menurie</h2>
                    </a></li>
                <li class="nav-item nav-toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i style="color:#8D5440;" class="bx bx-x d-block d-xl-none font-medium-4"></i><i style="color:#8D5440;" class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block" data-ticon="bx-disc"></i></a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
                <li class=" nav-item"><a href="{{ route('indexPage') }}"><i class="menu-livicon" data-icon="desktop"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Categories">Categories</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('categories') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$categoriesCount}}</span></a>
                        </li>
                    <li><a href="{{ route('addCategory') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Dishes">Dishes</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('items') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$dishesCount}}</span></a>
                        </li>
                    <li><a href="{{ route('addItem') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Special Dishes">Special Dishes</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('special') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$specialDishesCount}}</span></a>
                        </li>
                        <li><a href="{{ route('unapprovedSpecial') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Unapproved">Unapproved</span><span class="badge badge-success ml-2">{{$unapprovedCount}}</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href=""><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="Invoice">Chefs</span></a>
                    <ul class="menu-content">
                    <li><a href="{{ route('chefsListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">List</span><span class="badge badge-success ml-2">{{$chefsCount}}</span></a>
                    </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="User">Customers</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('customersListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$customersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('customersListPage') }}?type=new"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="New">New</span><span class="badge badge-success ml-2">{{$newCustomersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('customersListPage') }}?type=active"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Active">Active</span><span class="badge badge-success ml-2">{{$activeCustomersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('customersListPage') }}?type=inactive"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Inactive">Inactive</span><span class="badge badge-success ml-2">{{$inactiveCustomersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('customersListPage') }}?type=realtime"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Realtime">Realtime</span><span class="badge badge-success ml-2">{{$realtimeCustomersCount}}</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="User">Riders</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('ridersListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$ridersCount}}</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="{{ route('ordersListPage') }}"><i class="menu-livicon" data-icon="notebook"></i>Orders<span class="badge badge-success ml-2">{{$ordersCount}}</span></a>
                </li>
                <li class=" nav-item"><a href="{{ route('sales') }}"><i class="menu-livicon" data-icon="comments"></i>Sales</a>
                </li>
                <li class=" nav-item"><a href="{{ route('topSales') }}"><i class="menu-livicon" data-icon="comments"></i>Top Sale dishes</a>
                </li>
                <li class=" nav-item"><a href="{{ route('chefRequests') }}"><i class="menu-livicon" data-icon="notebook"></i>Requests<span class="badge badge-success ml-2">{{$requestsCount}}</span></a>
                </li>
                <li class="nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Tickets">Tickets</span></a>
                    <ul class="menu-content">
                        <li class="has-sub is-shown"><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Chef">Chef</span></a>
                            <ul class="menu-content" style="">
                                <li class="is-shown"><a href="{{ route('chefTickets',0) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Open">Open</span></a>
                                </li>
                                <li class="is-shown"><a href="{{ route('chefTickets',1) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Closed">Closed</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub is-shown"><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Customer">Customer</span></a>
                            <ul class="menu-content" style="">
                                <li class="is-shown"><a href="{{ route('customerTickets',0) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Open">Open</span></a>
                                </li>
                                <li class="is-shown"><a href="{{ route('customerTickets',1) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Closed">Closed</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub is-shown"><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Rider">Rider</span></a>
                            <ul class="menu-content" style="">
                                <li class="is-shown"><a href="{{ route('riderTickets',0) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Open">Open</span></a>
                                </li>
                                <li class="is-shown"><a href="{{ route('riderTickets',1) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Closed">Closed</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Support messages">Support messages</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('chefSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Chef">Chef</span></a>
                        </li>
                        <li><a href="{{ route('customerSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Customer">Customer</span></a>
                        </li>
                        <li><a href="{{ route('riderSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Rider">Rider</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-area-wrapper">
            {{-- <div class="sidebar-left">
                <div class="sidebar">
                    <!-- app chat sidebar start -->
                    <div class="chat-sidebar card">
                        <div class="chat-sidebar-list-wrapper mt-2">
                        </div>
                    </div>
                    <!-- app chat sidebar ends -->
                </div>
            </div> --}}
            <div class="content-right">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="content-header row">
                    </div>
                    <div class="content-body">
                        <!-- app chat overlay -->
                        <div class="chat-overlay"></div>
                        <!-- app chat window start -->
                        <section class="chat-window-wrapper">
                            <div class="chat-area">
                                <div class="chat-header">
                                    <header class="d-flex justify-content-between align-items-center border-bottom px-1 py-75">
                                        <div class="d-flex align-items-center">
                                            <div class="chat-sidebar-toggle d-block d-lg-none mr-1"><i class="bx bx-menu font-large-1 cursor-pointer"></i>
                                            </div>
                                            <div class="avatar chat-profile-toggle m-0 mr-1">
                                                <img class="chat-with-img" src="{{$ticket->customer->image}}" alt="avatar" height="36" width="36" />
                                            </div>
                                            <h6 class="mb-0 chat-with">{{$ticket->customer->first_name}}</h6>
                                        </div>
                                    </header>
                                </div>
                                <!-- chat card start -->
                                <div class="card chat-wrapper shadow-none">
                                    <div class="card-content">
                                        <div class="card-body chat-container">
                                            <div id="chat-content" class="chat-content">
                                                @foreach($messages as $item)
                                                    @if($item->from == 0)
                                                        <div class="chat">
                                                            <div class="chat-avatar">
                                                                <a class="avatar m-0">
                                                                    <img src="/user.png"alt="avatar" height="36" width="36" />
                                                                </a>
                                                            </div>
                                                            <div class="chat-body">
                                                                <div class="chat-message">
                                                                    <p>{{$item->message}}</p>
                                                                    <span class="chat-time">{{$item->created_at}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="chat chat-left">
                                                            <div class="chat-avatar">
                                                                <a class="avatar m-0">
                                                                    <img src="{{$ticket->customer->image}}"alt="avatar" height="36" width="36" />
                                                                </a>
                                                            </div>
                                                            <div class="chat-body">
                                                                <div class="chat-message">
                                                                    <p>{{$item->message}}</p>
                                                                    <span class="chat-time">{{$item->created_at}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer chat-footer border-top px-2 pt-1 pb-0 mb-1">
                                        <form class="d-flex align-items-center" id="chat-form">
                                            @csrf
                                            <input type="hidden" name="to" id="receiver" value="{{$ticket->customer_id}}">
                                            <input type="hidden" name="ticket" id="ticket" value="{{$ticket->id}}">
                                            <input type="text" class="form-control chat-message-send mx-1" placeholder="Type your message here..." id="message" name="message">
                                            <button type="submit" class="btn btn-primary glow send d-lg-flex"><i class="bx bx-paper-plane"></i>
                                                <span class="d-none d-lg-block ml-1">Send</span></button>
                                        </form>
                                    </div>
                                </div>
                                <!-- chat card ends -->
                            </div>
                        </section>
                        <!-- app chat window ends -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0"><button class="btn btn-menurie btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button></p>
        </footer>
    <!-- END: Footer-->
    <!-- BEGIN: Vendor JS-->
    <script src="/admin/app-assets/vendors/js/vendors.min.js"></script>
    <script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/admin/app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="/admin/app-assets/js/core/app-menu.js"></script>
    <script src="/admin/app-assets/js/core/app.js"></script>
    <script src="/admin/app-assets/js/scripts/components.js"></script>
    <script src="/admin/app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/admin/app-assets/js/scripts/pages/app-chat.js"></script>
    <!-- END: Page JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://js.pusher.com/7.0.3/pusher.min.js"></script>
<script>
    $(document).ready(function(){
        $(".card-body").animate({
                    scrollTop: $(
                      '.card-body').get(0).scrollHeight
                }, 1000);
    });

/*---------- Fetch latest messages -----------*/
    function fetch_messages(user_id, limit, offset,scroll){
        if(scroll == false){
            var firstMsg = $('.chat-content li:first');
            var curOffset = $(firstMsg).offset().top - $('.chat-content').scrollTop();
        }
        if(user_id == null){
            $("#chat-preloader").fadeOut(100);
            return;
        }
        $.ajax({
            url: '{{ url('admin/chat/get_messages') }}/' + user_id + '/' + limit + '/' + offset,
            beforeSend: function(){
                $("#chat-preloader").fadeIn(100);
            },
            success: function(data){
                $("#chat-preloader").fadeOut(100);
                var sender_image = $("#user-"+ user_id +'-img').attr('src');
        console.log(sender_image);
                var json = JSON.parse(data);
                if ( json.length == 0 )
                    load_more = false;
                $.each(json, function(key, msg) {
                    var d = new Date(msg.created_at),
                            month = '' + (d.getMonth()),
                            day = '' + d.getDate(),
                            year = d.getFullYear(),
                            hours = d.getHours(),
                            minutes = d.getMinutes();
                    var date = year+ ', '+month+' '+day+' - '+hours+':'+minutes;
                    if(msg.from == 0){
                            $('<div class="chat"><div class="chat-avatar"><a class="avatar m-0"><img src="/user.png"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + msg.message + '</p><span class="chat-time">'+date+'</span></div></div></div>').prependTo($('.chat-content'));
                    }else{
                            $('<div class="chat chat-left"><div class="chat-avatar"><a class="avatar m-0"><img src="'+ sender_image +'"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + msg.message + '</p><span class="chat-time">'+date+'</span></div></div></div>').prependTo($('.chat-content'));
                    }
                });

                if(scroll == true)
                    $(".chat-content").stop().animate({ scrollTop: $(".chat-content")[0].scrollHeight}, 200);
                else
                    $(".chat-content").stop().animate({ scrollTop: firstMsg.offset().top - curOffset}, 500);
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    $(document).on('submit','#chat-form',function(){
        event.preventDefault();
        newMessage();
    });

    /*------------* Send New Message *-------------*/
    function newMessage() {
        var currentdate = new Date();
        var date = currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
        message = $("#message").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url: '{{ url('admin/chat/send_message') }}',
            data:  new FormData($("#chat-form")[0]),
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('#message').val(null);
            },
            success: function(data){
                $("#chat-preloader").fadeOut(100);
                var json = JSON.parse(data);
                console.log(json);
                if(json['result'] != true){
                    $(".un-send").parent().remove();
                    $('#message').val(message);
                    $.each(json['message'], function(key, msg) {
                        toastr.error( msg );
                    });
                }
            },
            error: function (request, status, error) {
                $('#message').val(message);
                console.log(request.responseText);
            }
        });
    }

    var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
        authEndpoint: '{{ url('admin/chat/auth') }}',
        auth: {
        headers: {
            'X-CSRF-Token': "{{ csrf_token() }}"
        }
        },
        cluster: 'ap2',
        forceTLS: true
    });
    var presenceChannel = pusher.subscribe('chat-' + @json($ticket->id));
    presenceChannel.bind('pusher:subscription_error', function(status) {
        if (status == 408 || status == 503)
        presenceChannel = pusher.subscribe('chat-' + @json($ticket->id));
    });
    /*--------------* Get Message *----------*/
    presenceChannel.bind('message-event', function(data, metadata) {
        var d = new Date(data.created_at),
                month = '' + (d.getMonth()),
                day = '' + d.getDate(),
                year = d.getFullYear(),
                hours = d.getHours(),
                minutes = d.getMinutes();
                    var date = year+ ', '+month+' '+day+' - '+hours+':'+minutes;
        if(data.from == 0){
            $('.chat-content').append('<div class="chat"><div class="chat-avatar"><a class="avatar m-0"><img src="/user.png"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + data.message + '</p><span class="chat-time">'+date+'</span></div></div></div>');
        }
        else{
            $('.chat-content').append('<div class="chat chat-left"><div class="chat-avatar"><a class="avatar m-0"><img src="'+ @json($ticket->customer->image) +'"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + data.message + '</p><span class="chat-time">'+date+'</span></div></div></div>');
        }

        $(".card-body").animate({
                    scrollTop: $(
                      '.card-body').get(0).scrollHeight
                }, 1000);
    });
</script>
</body>
<!-- END: Body-->
</html>