@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Restaurants</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('indexPage') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            @if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
@if(session()->has('error'))
<div class="alert alert-danger">
    {{ session()->get('error') }}
</div>
@endif
            <!-- div start -->
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of our restaurants</h4>
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>Branch no</th>
                                            <th>Location</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($restaurants as $item)
                                            <tr>
                                            <td class="text-bold-500">{{ $item->branch_no }}</td>
                                            <td class="text-bold-500">{{ $item->location }}</td>
                                                <td>
                                                    <div class="modal-warning mr-1 mb-1 d-inline-block">
                                                <button class="btn btn-outline-warning mr-1 mb-1" data-toggle="modal" data-target="#warning" onclick="deleteRestaurant({{ $item->id }})" ><i class="bx bx-trash-alt" ></i><span class="align-middle ml-25">Delete</span></button>
                                                <div class="modal fade text-left" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-warning">
                                                                <h5 class="modal-title white" id="myModalLabel140">Deleting restaurant location</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <i class="bx bx-x"></i>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure you want to delete this restaurant location?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form id="deleteForm">
                                                                    @csrf
                                                                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                                    <span class="d-none d-sm-block">Close</span>
                                                                </button>
                                                
                                                                <button type="button" class="btn btn-warning ml-1" data-dismiss="modal" onclick="formSubmit()">
                                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                                    <span class="d-none d-sm-block">Delete</span>
                                                                </button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No restaurants added yet</h3> </td> </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- div end -->
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@section('scripts')
<script src="../../../app-assets/js/scripts/modal/components-modal.js"></script>
<script type="text/javascript">
    function deleteRestaurant(id)
    {
        var id = id;
        var url = '{{ route("deleteRestaurant", ":id") }}';
        url = url.replace(':id', id);
        $("#deleteForm").attr('action', url+'/');
    }

    function formSubmit()
    {
        $("#deleteForm").submit();
    }
</script>
@endsection