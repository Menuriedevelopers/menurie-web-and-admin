@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Riders</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('indexPage') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- div start -->
                <div class="row border rounded py-2 mb-2">
                    <div class="col-12 col-sm-6 col-lg-3 mt-2">
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mt-2">
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mt-2">
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mt-2">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle mr-1" type="button" id="dropdownMenuButtonIcon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-export mr-50"></i> Export
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonIcon" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                                <a class="dropdown-item" id="pdf" href="#">PDF</a>
                                <a class="dropdown-item" id="csv" href="#">CSV</a>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of all riders working with us</h4>
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>Rider name</th>
                                            {{-- <th>Image</th> --}}
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($riders as $item)
                                            <tr>
                                                <td class="text-bold-500">
                                                <a href="{{ route('riderPage',$item->id) }}">    {{ $item->first_name.' '. $item->last_name }}
                                                </td>
                                                {{-- <td>
                                                    @if ($item->image == '')
                                                        <img width="100" src="/app-assets/images/placeholderImage.png" alt="user">
                                                    @else
                                                    <img width="100" src="{{ $item->image }}" alt="{{ $item->first_name }}">    
                                                    @endif
                                                </td> --}}
                                                <td>
                                                    @if ($item->approved == 1)
                                                    <span class="badge badge-light-success">Approved</span>
                                                    @else
                                                    <span class="badge badge-light-danger">Not approved</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a class="delay" data-toggle="tooltip" title="Detail" data-delay="500" href="{{ route('riderPage',$item->id) }}"><i class="bx bx-info-circle" ></i></a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No riders registered yet</h3> </td> </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                {{-- <div class="mx-1">{{$riders->links()}}</div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- div end -->
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@section('scripts')
<script src="../../../app-assets/js/scripts/modal/components-modal.js"></script>
<script src="/admin/app-assets/js/scripts/tableHTMLExport.js"></script>
<script type="text/javascript">
    function verifyRider(id)
    {
        var id = id;
        var url = '{{ route("verifyRider", ":id") }}';
        url = url.replace(':id', id);
        $("#verifyForm").attr('action', url+'/');
    }

    function formSubmit()
    {
        $("#verifyForm").submit();
    }
    
    function deleteRider(id)
    {
        var id = id;
        var url = '{{ route("deleteRider", ":id") }}';
        url = url.replace(':id', id);
        $("#deleteForm").attr('action', url+'/');
    }

    function formSubmit1()
    {
        $("#deleteForm").submit();
    }   
    $('#csv').on('click',function(){
        $("#content").tableHTMLExport({type:'csv',filename:'customers.csv',});
    });
    $('#pdf').on('click',function(){
        $("#content").tableHTMLExport({type:'pdf',filename:'customers.pdf',});
    }); 
</script>
@endsection