@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users view start -->
            <section class="users-view">
                <!-- users view media object start -->
                <div class="row">
                    <div class="col-12 col-sm-7">
                        <div class="media mb-2">
                            <a class="mr-1" href="#">
                                @if ($customer->image == '')
                                    <img height="64" width="64" src="/app-assets/images/placeholderImage.png" alt="user">
                                @else
                                <img height="64" width="64" src="{{ $customer->image }}" alt="{{ $customer->first_name }}">    
                                @endif
                            </a>
                            <div class="media-body pt-25">
                            <h4 class="media-heading"><span class="users-view-name">{{ $customer->first_name.' '.$customer->last_name }}</span></h4>
                                <span>ID:</span>
                                <span class="users-view-id">{{ $customer->id }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-5 px-0 d-flex justify-content-end align-items-center px-1 mb-2">
                        <a href="#" class="btn btn-sm mr-25 border"><i class="bx bx-envelope font-small-3"></i></a>
                        <a href="#" data-toggle="modal" data-target="#warning" onclick="deleteCustomer({{ $customer->id }})" class="btn btn-sm btn-primary">Delete</a>
                    </div>
                </div>
                <!-- users view media object ends -->
                <!-- users view card data start -->
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            @if($breakfast != 0 || $lunch != 0 || $dinner != 0 || $midnight != 0)
                                <div class="row bg-warning bg-lighten-5 rounded mb-2 mx-25 text-center text-lg-left">
                                    <div class="col-12 col-sm-3 p-2">
                                        <h6 class="text-warning mb-0">Breakfast: <span class="font-large-1 align-middle">{{$breakfast}}</span></h6>
                                    </div>
                                    <div class="col-12 col-sm-3 p-2">
                                        <h6 class="text-warning mb-0">Lunch: <span class="font-large-1 align-middle">{{$lunch}}</span></h6>
                                    </div>
                                    <div class="col-12 col-sm-3 p-2">
                                        <h6 class="text-warning mb-0">Dinner: <span class="font-large-1 align-middle">{{$dinner}}</span></h6>
                                    </div>
                                    <div class="col-12 col-sm-3 p-2">
                                        <h6 class="text-warning mb-0">Midnight: <span class="font-large-1 align-middle">{{$midnight}}</span></h6>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Registered:</td>
                                            <td>{{ $customer->created_at }}</td>
                                            </tr>
                                            <tr>
                                                <td>Verified:</td>
                                                <td class="users-view-verified">
                                                    @if ($customer->verified == 1)
                                                        Yes
                                                    @else
                                                        No                                                        
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- users view card data ends -->
                <!-- users view card details start -->
                @php
                    $orders = App\Order::where('customer_id', $customer->id)->count();
                    $completed = App\Order::where('customer_id', $customer->id)->where('order_status', 'Delivered')->count();
                    $cancelled = App\Order::where('customer_id', $customer->id)->where('order_status', 'Cancelled')->count();
                @endphp
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row bg-primary bg-lighten-5 rounded mb-2 mx-25 text-center text-lg-left">
                                <div class="col-12 col-sm-4 p-2">
                                    <h6 class="text-primary mb-0">Orders: <span class="font-large-1 align-middle">{{$orders}}</span></h6>
                                </div>
                                <div class="col-12 col-sm-4 p-2">
                                    <h6 class="text-primary mb-0">Completed: <span class="font-large-1 align-middle">{{$completed}}</span></h6>
                                </div>
                                <div class="col-12 col-sm-4 p-2">
                                    <h6 class="text-primary mb-0">Cancelled: <span class="font-large-1 align-middle">{{$cancelled}}</span></h6>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>Name:</td>
                                        <td class="users-view-name">{{ $customer->first_name.' '. $customer->last_name }}</td>
                                        </tr>
                                        <tr>
                                            <td>E-mail:</td>
                                        <td class="users-view-email">{{ $customer->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>Contact:</td>
                                        <td>{{ $customer->phone }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h5 class="mb-1"><i class="bx bx-info-circle"></i> Recent Orders</h5>
                                <table class="table table-borderless mb-0">
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- users view card details ends -->

            </section>
            <!-- users view ends -->
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="modal fade text-left" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title white" id="myModalLabel140">Deleting customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this customer?
            </div>
            <div class="modal-footer">
                <form id="deleteForm">
                    @csrf
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>

                <button type="button" class="btn btn-warning ml-1" data-dismiss="modal" onclick="formSubmit1()">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Delete</span>
                </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="../../../app-assets/js/scripts/modal/components-modal.js"></script>
<script type="text/javascript">
    function deleteChef(id)
    {
        var id = id;
        var url = '{{ route("deleteCustomer", ":id") }}';
        url = url.replace(':id', id);
        $("#deleteForm").attr('action', url+'/');
    }

    function formSubmit1()
    {
        $("#deleteForm").submit();
    }
</script>
@endsection