@php
    $openTickets = App\Ticket::where('customer_id', '!=', 0)->where('status', 0)->count();
    $chefOpenTickets = App\Ticket::where('chef_id', '!=', 0)->where('status', 0)->count();
    $riderOpenTickets = App\Ticket::where('rider_id', '!=', 0)->where('status', 0)->count();
    $todayRequest = App\ItemRequest::whereHas('chef')->where('status', 0)->count();
    $unapprovedDishes =App\Item::where('special', 1)->where('approved', 0)->whereDate('created_at', date('Y-m-d'))->count();
    $todaySale = App\Order::where('order_status', 'Delivered')->sum('total_price');
@endphp
@extends('admin.main')
@push('styles')
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/vendors/css/extensions/dragula.min.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/pages/dashboard-analytics.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/pages/widgets.css">
@endpush
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section id="dashboard-analytics">
                <div class="row">
                    <div class="col-xl-6 col-md-6 progress-card">
                        <div class="card">
                            @php
                                $customersCount = App\Customer::count();
                                $normal = App\Customer::where('platform', '')->count();
                                $normalPerc = round(($normal * 100) / $customersCount,2);
                                $facebook = App\Customer::where('platform', 'facebook')->count();
                                $facebookPerc = round(($facebook * 100) / $customersCount,2);
                                $google = App\Customer::where('platform', 'google')->count();
                                $googlePerc = round(($google * 100) / $customersCount, 2);
                                $apple = App\Customer::where('platform', 'apple')->count();
                                $applePerc = round(($apple * 100) / $customersCount,2);
                            @endphp
                            <div class="card-header border-bottom d-flex justify-content-between align-items-center pr-2">
                                <h5 class="card-title">Customers by platform</h5>
                                <ul class="list-inline mb-0">
                                    <li class="mr-50"></li>
                                    <li>{{$customersCount}}</li>
                                </ul>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td class="w-25">Simple ({{$normal}})</td>
                                            <td>
                                                <div class="progress progress-bar-info progress-sm mb-0">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="24" aria-valuemin="80" aria-valuemax="100" style="width:{{$normalPerc}}%;"></div>
                                                </div>
                                            </td>
                                            <td class="w-25 text-right">{{$normalPerc}}%</td>
                                        </tr>
                                        <tr>
                                            <td class="w-25">Facebook ({{$facebook}})</td>
                                            <td>
                                                <div class="progress progress-bar-primary progress-sm mb-0">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="80" aria-valuemax="100" style="width:{{$facebookPerc}}%;"></div>
                                                </div>
                                            </td>
                                            <td class="w-25 text-right">{{$facebookPerc}}%</td>
                                        </tr>
                                        <tr>
                                            <td class="w-25">Apple ({{$apple}})</td>
                                            <td>
                                                <div class="progress progress-bar-secondary progress-sm mb-0">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80" aria-valuemax="100" style="width:{{$applePerc}}%;"></div>
                                                </div>
                                            </td>
                                            <td class="w-25 text-right">{{$applePerc}}%</td>
                                        </tr>
                                        <tr>
                                            <td class="w-25">Google ({{$google}})</td>
                                            <td>
                                                <div class="progress progress-bar-danger progress-sm mb-0">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="80" aria-valuemax="100" style="width:{{$googlePerc}}%;"></div>
                                                </div>
                                            </td>
                                            <td class="w-25 text-right">{{$googlePerc}}%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <a href="{{ route('unapprovedSpecial') }}">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto my-1">
                                            <i class="bx bx-error-circle font-medium-5"></i>
                                        </div>
                                        <p class="text-muted mb-0 line-ellipsis">Unapproved Special dishes</p>
                                        <h2 class="mb-0">{{$unapprovedDishes}}</h2>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <a href="{{ route('chefRequests') }}">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-primary mx-auto my-1">
                                            <i class="bx bx-error-circle font-medium-5"></i>
                                        </div>
                                        <p class="text-muted mb-0 line-ellipsis">Pending item requests today</p>
                                        <h2 class="mb-0">{{$todayRequest}}</h2>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <a href="{{ route('sales') }}">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto my-1">
                                            <i class="bx bx-money font-medium-5"></i>
                                        </div>
                                        <p class="text-muted mb-0 line-ellipsis">Today Sale</p>
                                        <h2 class="mb-0">{{'$'.number_format($todaySale,2)}}</h2>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="card widget-followers">
                            <a href="{{ route('riderTickets',0) }}">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                    <div>
                                        <h4 class="card-title">Rider Open tickets</h4>
                                        {{-- <small class="text-muted">System project</small> --}}
                                    </div>
                                    <div class="d-flex align-items-center widget-followers-heading-right">
                                        <h5 class="mr-2 font-weight-normal mb-0">{{$riderOpenTickets}}</h5>
                                    </div>
                                </div>
                            </a>
                            <a href="{{ route('chefTickets',0) }}">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                    <div>
                                        <h4 class="card-title">Chef Open tickets</h4>
                                        {{-- <small class="text-muted">System project</small> --}}
                                    </div>
                                    <div class="d-flex align-items-center widget-followers-heading-right">
                                        <h5 class="mr-2 font-weight-normal mb-0">{{$chefOpenTickets}}</h5>
                                    </div>
                                </div>
                            </a>
                            <a href="{{ route('customerTickets',0) }}">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                    <div>
                                        <h4 class="card-title">Customer Open tickets</h4>
                                        {{-- <small class="text-muted">System project</small> --}}
                                    </div>
                                    <div class="d-flex align-items-center widget-followers-heading-right">
                                        <h5 class="mr-2 font-weight-normal mb-0">{{$openTickets}}</h5>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-4 col-sm-6">
                        <div class="card widget-notification">
                            <div class="card-header border-bottom">
                                <h4 class="card-title d-flex align-items-center">
                                    <i class='bx bx-bell font-medium-4 mr-1'></i>Unapproved Riders
                                </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body p-0">
                                    <ul class="list-group list-group-flush">
                                        @php
                                            $riders = App\Rider::where('approved', 0)->get();
                                        @endphp
                                        @foreach($riders as $item)
                                            <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                                <a href="{{ route('riderPage',$item->id) }}">
                                                    <div class="list-left d-flex">
                                                        <div class="list-icon mr-1">
                                                            <div class="avatar bg-rgba-primary m-0">
                                                                <img class="img-fluid" src="{{($item->image != '')? $item->image: '/user_avatar.png'}}" alt="" height="38" width="38">
                                                            </div>
                                                        </div>
                                                        <div class="list-content">
                                                            <span class="list-title">{{$item->first_name. ' '. $item->last_name}}</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="readable-mark-icon" data-toggle="tooltip" data-placement="left" title="Mark as read">
                                                    <i class='bx bxs-circle text-light-danger font-medium-1'></i>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 col-sm-6">
                        <div class="card widget-notification">
                            <div class="card-header border-bottom">
                                <h4 class="card-title d-flex align-items-center">
                                    <i class='bx bx-bell font-medium-4 mr-1'></i>Unapproved Chefs
                                </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body p-0">
                                    <ul class="list-group list-group-flush">
                                        @php
                                            $chefs = App\Chef::where('approved', 0)->get();
                                        @endphp
                                        @foreach($chefs as $item)
                                            <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                                <a href="{{ route('chefPage',$item->id) }}">
                                                    <div class="list-left d-flex">
                                                        <div class="list-icon mr-1">
                                                            <div class="avatar bg-rgba-primary m-0">
                                                                <img class="img-fluid" src="{{($item->image != '')? $item->image: '/site/asset/images/chef-1.png'}}" alt="" height="38" width="38">
                                                            </div>
                                                        </div>
                                                        <div class="list-content">
                                                            <span class="list-title">{{$item->first_name. ' '. $item->last_name}}</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="readable-mark-icon" data-toggle="tooltip" data-placement="left" title="Mark as read">
                                                    <i class='bx bxs-circle text-light-danger font-medium-1'></i>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
</div>
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
@endsection
@push('scripts')
<script src="/admin/app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="/admin/app-assets/vendors/js/extensions/dragula.min.js"></script>
<script src="/admin/app-assets/js/scripts/cards/widgets.js"></script>
@endpush