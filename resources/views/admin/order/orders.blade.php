@extends('admin.main')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Orders</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('indexPage') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif
            <!-- div start -->
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="row border rounded py-2 mb-2">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <label for="filter">Filter</label>
                            <fieldset class="form-group">
                                <select class="form-control" id="filter">
                                    <option value="0">Filter orders</option>
                                    <option value="1">Daily</option>
                                    <option value="2">Weekly</option>
                                    <option value="3">Monthly</option>
                                    <option value="4">Yearly</option>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">  
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <label for="">Get orders by date range</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" name="daterange" placeholder="Select Date">
                                <div class="form-control-position">
                                    <i class="bx bx-calendar-check"></i>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row border rounded py-2 mb-2">
                        <div class="col-12 col-sm-6 col-lg-3 mt-2">
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mt-2">
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mt-2">
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mt-2">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle mr-1" type="button" id="dropdownMenuButtonIcon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="bx bx-export mr-50"></i> Export
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonIcon" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                                    <a class="dropdown-item" id="pdf" href="#">PDF</a>
                                    <a class="dropdown-item" id="csv" href="#">CSV</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="content" class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of orders</h4>
                            
                            <div class="row border rounded py-2 mb-2">
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <label>Breakfast</label>
                                    <fieldset class="form-group">
                                        <label>{{$breakfast}}</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <label>Lunch</label>
                                    <fieldset class="form-group">
                                        <label>{{$lunch}}</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <label>Dinner</label>
                                    <fieldset class="form-group">
                                        <label>{{$dinner}}</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <label>Midnight</label>
                                    <fieldset class="form-group">
                                        <label>{{$midnight}}</label>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table id="table" class="table table-hover mb-0">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>#</th>
                                            <th>Order #</th>
                                            <th>Restaurant</th>
                                            <th>Customer Name</th>
                                            <th>Customer Phone</th>
                                            <th>Payment Method</th>
                                            <th>Total</th>
                                            <th>Order Status</th>
                                            <th>Assigned rider</th>
                                            <th class="ignore">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total =0;
                                        @endphp
                                        @forelse ($orders as $item)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td class="text-bold-500">{{ $item->id }}</td>
                                                <td class="text-bold-500">
                                                    @php
                                                        $total += $item->total_price;
                                                        $chef = App\Chef::find($item->chef_id);
                                                        if($chef)
                                                            echo $chef->first_name.' '.$chef->last_name;
                                                        else
                                                            echo 'Menurie Chef';
                                                    @endphp
                                                </td>
                                                <td class="text-bold-500">{{ $item->customer->first_name.' ' . $item->customer->last_name }}</td>
                                                <td class="text-bold-500">{{ $item->customer->phone }}</td>
                                                <td class="text-bold-500">{{ $item->payment_method }}</td>
                                                <td class="text-bold-500">{{ '$'.number_format($item->total_price,2) }}</td>
                                                <td class="text-bold-500">{{ $item->order_status }}</td>
                                                <td class="text-bold-500">
                                                    @php
                                                        $assignee = App\RiderOrder::where('order_id', $item->id)->first()->rider_id ?? 0;
                                                        if($assignee != 0)
                                                        {
                                                            $rider = App\Rider::find($assignee);
                                                            if($rider)
                                                                echo $rider->first_name.' '.$rider->last_name;
                                                            else
                                                                echo 'Menurie Rider';
                                                        }else
                                                            echo 'Order not assigned';
                                                    @endphp
                                                </td>
                                                <td>
                                                    <a href="{{ route('orderDetail',$item->id) }}" class="btn btn-outline-primary mr-1 mb-1" ><i class="bx bx-show" ></i><span class="align-middle ml-25">Detail</span></a>
                                                    <div class="modal-warning mr-1 mb-1 d-inline-block">
                                                        <button class="btn btn-outline-warning mr-1 mb-1" data-toggle="modal" data-target="#warning" onclick="deleteOrder({{ $item->id }})" ><i class="bx bx-x" ></i><span class="align-middle ml-25">Cancel</span></button>
                                                        <div class="modal fade text-left" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header bg-warning">
                                                                        <h5 class="modal-title white" id="myModalLabel140">Cancel Order</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <i class="bx bx-x"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Are you sure you want to cancel this order?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <form id="deleteForm">
                                                                            @csrf
                                                                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                                                            <i class="bx bx-x d-block d-sm-none"></i>
                                                                            <span class="d-none d-sm-block">Close</span>
                                                                        </button>
                                                        
                                                                        <button type="button" class="btn btn-warning ml-1" data-dismiss="modal" onclick="formSubmit()">
                                                                            <i class="bx bx-check d-block d-sm-none"></i>
                                                                            <span class="d-none d-sm-block">Cancel Order</span>
                                                                        </button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @if($loop->last)
                                            <tr>
                                                <td colspan="4">Total Orders</td>
                                                <td colspan="5">{{count($orders)}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">Total Amount</td>
                                                <td colspan="5">{{number_format($total,2)}}</td>
                                            </tr>
                                            @endif
                                        @empty
                                            <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No orders yet</h3> </td> </tr>
                                        @endforelse

                                    </tbody>
                                </table>
                                <div class="mx-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- div end -->
        </div>
    </div>
</div>
<!-- END Content-->
@endsection
@section('scripts')
<script src="/admin/app-assets/js/scripts/modal/components-modal.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
<script src="/admin/app-assets/js/scripts/tableHTMLExport.js"></script>
<script type="text/javascript">
    function deleteOrder(id)
    {
        var id = id;
        var url = '{{ route("deleteOrder", ":id") }}';
        url = url.replace(':id', id);
        $("#deleteForm").attr('action', url+'/');
    }

    function formSubmit()
    {
        $("#deleteForm").submit();
    }
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    }, function(start, end, label) {
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });

    $("#filter").on('change', function(){
      var value = $(this).val();
        $.ajax({
            url: "{{request()->url()}}{!!$query!!}value="+value,
            method: 'get',
            success: function(result){
                console.log(result);
                $('#content').html(result);

            },error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    }, function(start, end, label) {
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        $.ajax({
            url: "{{request()->url()}}{!!$query!!}start="+start.format('YYYY-MM-DD')+"&end="+end.format('YYYY-MM-DD'),
            method: 'get',
            success: function(result){
                console.log(result);
                $('#content').html(result);

            },error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                }
        });
    });
  $('#csv').on('click',function(){
    $("#content").tableHTMLExport({type:'csv',filename:'sales.csv',});
  });
  $('#pdf').on('click',function(){
    $("#content").tableHTMLExport({type:'pdf',filename:'sales.pdf',});
  });
</script>
@endsection