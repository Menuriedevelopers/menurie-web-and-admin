<div class="card-header">
    <h4 class="card-title">List of orders</h4>
    <div class="row border rounded py-2 mb-2">
        <div class="col-12 col-sm-6 col-lg-3">
            <label>Breakfast</label>
            <fieldset class="form-group">
                <label>{{$breakfast}}</label>
            </fieldset>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <label>Lunch</label>
            <fieldset class="form-group">
                <label>{{$lunch}}</label>
            </fieldset>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <label>Dinner</label>
            <fieldset class="form-group">
                <label>{{$dinner}}</label>
            </fieldset>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <label>Midnight</label>
            <fieldset class="form-group">
                <label>{{$midnight}}</label>
            </fieldset>
        </div>
    </div>
</div>
<div class="card-content">
    <!-- table -->
    <div class="table-responsive">
        <table id="table" class="table table-hover mb-0">
            <thead class="thead-menurie">
                <tr>
                    <th>#</th>
                    <th>Order #</th>
                    <th>Restaurant</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Payment Method</th>
                    <th>Total</th>
                    <th>Order Status</th>
                    <th>Assigned rider</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $total =0;
                @endphp
                @forelse ($orders as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                    <td class="text-bold-500">{{ $item->id }}</td>
                    <td class="text-bold-500">
                        @php
                        $total += $item->total_price;
                            $chef = App\Chef::find($item->chef_id);
                            if($chef)
                            echo $chef->first_name.' '.$chef->last_name;
                            else
                                echo 'Menurie Chef';
                        @endphp
                    </td>
                    <td class="text-bold-500">{{ $item->customer->first_name.' ' . $item->customer->last_name }}</td>
                    <td class="text-bold-500">{{ $item->customer->phone }}</td>
                    <td class="text-bold-500">{{ $item->payment_method }}</td>
                    <td class="text-bold-500">{{ '$'.number_format($item->total_price,2) }}</td>
                    <td class="text-bold-500">{{ $item->order_status }}</td>
                    <td class="text-bold-500">
                        @php
                            $assignee = App\RiderOrder::where('order_id', $item->id)->first()->rider_id ?? 0;
                            if($assignee != 0)
                            {
                                $rider = App\Rider::find($assignee);
                                if($rider)
                                echo $rider->first_name.' '.$rider->last_name;
                                else
                                echo 'Menurie Rider';
                            }else
                                echo 'Order not assigned';
                        @endphp
                    </td>
                    <td>
                        <a href="{{ route('orderDetail',$item->id) }}" class="btn btn-outline-primary mr-1 mb-1" ><i class="bx bx-show" ></i><span class="align-middle ml-25">Detail</span></a>
                        <div class="modal-warning mr-1 mb-1 d-inline-block">
                            <button class="btn btn-outline-warning mr-1 mb-1" data-toggle="modal" data-target="#warning" onclick="deleteOrder({{ $item->id }})" ><i class="bx bx-x" ></i><span class="align-middle ml-25">Cancel</span></button>
                            <div class="modal fade text-left" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-warning">
                                            <h5 class="modal-title white" id="myModalLabel140">Cancel Order</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i class="bx bx-x"></i>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to cancel this order?
                                        </div>
                                        <div class="modal-footer">
                                            <form id="deleteForm">
                                                @csrf
                                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Close</span>
                                            </button>
                            
                                            <button type="button" class="btn btn-warning ml-1" data-dismiss="modal" onclick="formSubmit()">
                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Cancel Order</span>
                                            </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    </tr>
                    @if($loop->last)
                    <tr>
                        <td colspan="4">Total Orders</td>
                        <td colspan="5">{{count($orders)}}</td>
                    </tr>
                    <tr>
                        <td colspan="4">Total Amount</td>
                        <td colspan="5">{{number_format($total,2)}}</td>
                    </tr>
                    @endif
                @empty
                    <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No orders yet</h3> </td> </tr>
                @endforelse
            </tbody>
        </table>
        {{-- <div class="mx-1">{{$orders->links()}}</div> --}}
    </div>
</div>