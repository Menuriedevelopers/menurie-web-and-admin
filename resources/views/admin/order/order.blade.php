@extends('admin.main')
@section('styles')
<link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/forms/select/select2.min.css">
@endsection
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- users view start -->
                <section class="users-view">
                    <!-- users view media object start -->
                    <div class="row">
                        <div class="col-12 col-sm-7">
                            <div class="media mb-2">
                                <a class="mr-1" href="#">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-26.jpg" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
                                </a>
                                <div class="media-body pt-25">
                                <h4 class="media-heading"><span class="users-view-name">{{ $order->customer->name }}</span><span class="text-muted font-medium-1"> @</span><span class="users-view-username text-muted font-medium-1 ">{{ $order->customer->email }}</span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 px-0 d-flex justify-content-end align-items-center px-1 mb-2">
                        </div>
                    </div>
                    <!-- users view media object ends -->
                    <!-- users view card data start -->
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td>Received at:</td>
                                                <td>{{ $order->created_at }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Restaurant:</td>
                                                <td class="users-view-latest-activity">{{ $order->restaurant_id }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Total:</td>
                                                <td class="users-view-verified">{{ '$'.number_format($order->total_price, 2) }}</td>
                                                </tr>
                                                @if ($order->promocode != null)
                                                <tr>
                                                    <td>Promo:</td>
                                                <td class="users-view-verified">{{ $order->promo_code }}</td>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <td>Payment Method:</td>
                                                <td class="users-view-role">{{ $order->payment_method }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status:</td>
                                                <td><span class="badge 
                                                    {{($order->order_status =='Pending') ? 'badge-light-info':''}}
                                                    {{($order->order_status =='Preparing') ? 'badge-light-warning':''}}
                                                     users-view-status">{{ $order->order_status }}</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>{{ ($order->chef_id != 0 && $order->order_status == 'Placed')? 'Order requested to rider': 'Order Accepted by chef'}}</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($order->chef_id == 0)
                                            <form id="assign">
                                                @csrf
                                                    <tr>
                                                        <td>
                                                            <section class="basic-select2">  
                                                                <div class="form-group">
                                                                <select name="chef" class="select2 form-control" id="basicSelect">
                                                                    @foreach ($chefs as $item)
                                                                <option value="{{ $item->id }}">{{ $item->first_name .' '.$item->last_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                </div>
                                                            </section>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <button type="submit" class="btn btn-success ml-1">
                                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                                <span class="d-none d-sm-block">Assign</span>
                                                            </button>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                            </form>
                                            @else
                                            <tr>
                                                <td>
                                                    {{ $order->chef->first_name.' '. $order->chef->last_name }}
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- users view card data ends -->
                    <!-- users view card details start -->
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row bg-primary bg-lighten-5 rounded mb-2 mx-25 text-center text-lg-left">
                                    <div class="col-12 col-sm-4 p-2">
                                        <h4 class="text-primary mb-0">Order Address:</h4>
                                    </div>
                                    <div class="col-12 col-sm-4 p-2">
                                    </div>
                                    <div class="col-12 col-sm-4 p-2">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Deliver To:</td>
                                            <td class="users-view-username">{{ $order->address->delivery_to }}</td>
                                            </tr>
                                            @if($order->address->delivery_to == 'hotel')
                                            <tr>
                                                <td>Hotel:</td>
                                            <td></td>
                                            </tr>
                                            <tr>
                                                <td>Room No:</td>
                                                <td>XYZ Corp. Ltd.</td>
                                            </tr>
                                            <tr>
                                                <td>Delivery Time:</td>
                                                <td></td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td>Address:</td>
                                            <td class="users-view-name">{{ $order->address->address }}</td>
                                            </tr>
                                            <tr>
                                                <td>Note:</td>
                                            <td class="users-view-email">{{ $order->address->delivery_note }}</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <h5 class="mb-1"><i class="bx bx-info-circle"></i> Order Detail</h5>
                                    <table class="table table-borderless">
                                        <tbody>
                                            @foreach($order->detail as $detail)
                                            <tr>
                                                <td>Item:</td>
                                                <td>
                                                    {{ $detail->item->item_name }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Quantity</td>
                                            <td>{{ $detail->quantity }}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Addons:
                                                    <div class="row mx-1">
                                                        @php
                                                            $get = App\OrderAddons::where('order_detail_id',$detail->id)->get();
                                                        @endphp
                                                        @foreach($get as $addon)
                                                        <div class="col">
                                                        <p>{{$addon->addon->name}}</p><span>{{$addon->quantity}}</span>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <h5 class="mb-1"><i class="bx bx-info-circle"></i>Charges</h5>
                                    <table class="table table-borderless mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Service Charges:</td>
                                            <td>{{ $order->serice_charges }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tax:</td>
                                                <td>{{ $order->tax }}</td>
                                            </tr>
                                            <tr>
                                                <td>Sub Total:</td>
                                            <td>{{ $order->sub_total }}</td>
                                            </tr>
                                            <tr>
                                                <td>Total:</td>
                                            <td>{{ $order->total_price }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- users view card details ends -->
                </section>
                <!-- users view ends -->
            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection
@section('scripts')
<script src="../../../app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="../../../app-assets/js/scripts/forms/select/form-select2.js"></script>
<script>
    $('#assign').on('submit',function(){
        event.preventDefault();
        $.ajax({
                method: "POST",
                url: "{{ route('admin.chef.assign',$order->id) }}",
                data: new FormData(this),
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if(data ==true)
                    location.reload();
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }
            }); 
    })
</script>
@endsection
 