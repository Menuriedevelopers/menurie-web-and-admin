<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Menurie Admin Panel</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/admin/app-assets/css/pages/app-chat.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/admin/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern content-left-sidebar chat-application navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="content-left-sidebar">

    <!-- BEGIN: Header-->
    <div class="header-navbar-shadow"></div>
    <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon bx bx-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                            {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="" data-toggle="tooltip" data-placement="top" title="Email"><i class="ficon bx bx-envelope"></i></a></li>
                            <li class="nav-item d-none d-lg-block"><a class="nav-link" href="" data-toggle="tooltip" data-placement="top" title="Chat"><i class="ficon bx bx-chat"></i></a></li>
                            <li class="nav-item d-none d-lg-block"><a class="nav-link" href="" data-toggle="tooltip" data-placement="top" title="Todo"><i class="ficon bx bx-check-circle"></i></a></li>
                            <li class="nav-item d-none d-lg-block"><a class="nav-link" href="" data-toggle="tooltip" data-placement="top" title="Calendar"><i class="ficon bx bx-calendar-alt"></i></a></li> --}}
                        </ul>
                        <ul class="nav navbar-nav">
                            {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon bx bx-star warning"></i></a>
                            </li> --}}
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-language nav-item">
                        </li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon bx bx-fullscreen"></i></a></li>
                        {{-- <li class="nav-item nav-search">
                        </li> --}}
                        {{-- <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon bx bx-bell bx-tada bx-flip-horizontal"></i><span class="badge badge-pill badge-danger badge-up">5</span></a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header px-1 py-75 d-flex justify-content-between"><span class="notification-title">7 new Notification</span><span class="text-bold-400 cursor-pointer">Mark all as read</span></div>
                                </li>
                                <li class="scrollable-container media-list"><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-center">
                                            <div class="media-left pr-0">
                                                <div class="avatar mr-1 m-0"><img src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="39" width="39"></div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><span class="text-bold-500">Congratulate Socrates Itumay</span> for work anniversaries</h6><small class="notification-text">Mar 15 12:32pm</small>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="d-flex justify-content-between read-notification cursor-pointer">
                                        <div class="media d-flex align-items-center">
                                            <div class="media-left pr-0">
                                                <div class="avatar mr-1 m-0"><img src="../../../app-assets/images/portrait/small/avatar-s-16.jpg" alt="avatar" height="39" width="39"></div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><span class="text-bold-500">New Message</span> received</h6><small class="notification-text">You have 18 unread messages</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between cursor-pointer">
                                        <div class="media d-flex align-items-center py-0">
                                            <div class="media-left pr-0"><img class="mr-1" src="../../../app-assets/images/icon/sketch-mac-icon.png" alt="avatar" height="39" width="39"></div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><span class="text-bold-500">Updates Available</span></h6><small class="notification-text">Sketch 50.2 is currently newly added</small>
                                            </div>
                                            <div class="media-right pl-0">
                                                <div class="row border-left text-center">
                                                    <div class="col-12 px-50 py-75 border-bottom">
                                                        <h6 class="media-heading text-bold-500 mb-0">Update</h6>
                                                    </div>
                                                    <div class="col-12 px-50 py-75">
                                                        <h6 class="media-heading mb-0">Close</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between cursor-pointer">
                                        <div class="media d-flex align-items-center">
                                            <div class="media-left pr-0">
                                                <div class="avatar bg-primary bg-lighten-5 mr-1 m-0 p-25"><span class="avatar-content text-primary font-medium-2">LD</span></div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><span class="text-bold-500">New customer</span> is registered</h6><small class="notification-text">1 hrs ago</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cursor-pointer">
                                        <div class="media d-flex align-items-center justify-content-between">
                                            <div class="media-left pr-0">
                                                <div class="media-body">
                                                    <h6 class="media-heading">New Offers</h6>
                                                </div>
                                            </div>
                                            <div class="media-right">
                                                <div class="custom-control custom-switch">
                                                    <input class="custom-control-input" type="checkbox" checked id="notificationSwtich">
                                                    <label class="custom-control-label" for="notificationSwtich"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between cursor-pointer">
                                        <div class="media d-flex align-items-center">
                                            <div class="media-left pr-0">
                                                <div class="avatar bg-danger bg-lighten-5 mr-1 m-0 p-25"><span class="avatar-content"><i class="bx bxs-heart text-danger"></i></span></div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><span class="text-bold-500">Application</span> has been approved</h6><small class="notification-text">6 hrs ago</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between read-notification cursor-pointer">
                                        <div class="media d-flex align-items-center">
                                            <div class="media-left pr-0">
                                                <div class="avatar mr-1 m-0"><img src="../../../app-assets/images/portrait/small/avatar-s-4.jpg" alt="avatar" height="39" width="39"></div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><span class="text-bold-500">New file</span> has been uploaded</h6><small class="notification-text">4 hrs ago</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between cursor-pointer">
                                        <div class="media d-flex align-items-center">
                                            <div class="media-left pr-0">
                                                <div class="avatar bg-rgba-danger m-0 mr-1 p-25">
                                                    <div class="avatar-content"><i class="bx bx-detail text-danger"></i></div>
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><span class="text-bold-500">Finance report</span> has been generated</h6><small class="notification-text">25 hrs ago</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between cursor-pointer">
                                        <div class="media d-flex align-items-center border-0">
                                            <div class="media-left pr-0">
                                                <div class="avatar mr-1 m-0"><img src="../../../app-assets/images/portrait/small/avatar-s-16.jpg" alt="avatar" height="39" width="39"></div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><span class="text-bold-500">New customer</span> comment recieved</h6><small class="notification-text">2 days ago</small>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="dropdown-menu-footer"><a class="dropdown-item p-50 text-primary justify-content-center" href="javascript:void(0)">Read all notifications</a></li>
                            </ul>
                        </li> --}}
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name">{{ Auth::user()->name }}</span><span class="user-status text-muted">Available</span></div><span><img class="round" src="/user.png" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pb-0">
                                <a class="dropdown-item" href="{{route('settings')}}"><i class="bx bx-user mr-50"></i> Edit Admin Profile</a>
                                <div class="dropdown-divider mb-0"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="bx bx-power-off mr-50"></i> Logout</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->
    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="">
                        <div class="brand-logo">
                        </div>
                        <h2 class="brand-text mb-0">Menurie</h2>
                    </a></li>
                <li class="nav-item nav-toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i style="color:#8D5440;" class="bx bx-x d-block d-xl-none font-medium-4"></i><i style="color:#8D5440;" class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block" data-ticon="bx-disc"></i></a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
                <li class=" nav-item"><a href="{{ route('indexPage') }}"><i class="menu-livicon" data-icon="desktop"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Categories">Categories</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('categories') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span></a>
                        </li>
                    <li><a href="{{ route('addCategory') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Dishes">Dishes</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('items') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span></a>
                        </li>
                    <li><a href="{{ route('addItem') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Special Dishes">Special Dishes</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('special') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span></a>
                        </li>
                        <li><a href="{{ route('unapprovedSpecial') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Unapproved">Unapproved</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href=""><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="Invoice">Chefs</span></a>
                    <ul class="menu-content">
                    <li><a href="{{ route('chefsListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">List</span></a>
                    </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="User">Customers</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('customersListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="User">Riders</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('ridersListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span></a>
                        </li>
                        {{-- <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="View">Orders</span></a>
                        </li>
                        <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Edit">Support</span></a>
                        </li> --}}
                    </ul>
                </li>
                <li class=" nav-item"><a href="{{ route('ordersListPage') }}"><i class="menu-livicon" data-icon="notebook"></i>Orders</a>
                </li>
                <li class=" nav-item"><a href="{{ route('chefRequests') }}"><i class="menu-livicon" data-icon="notebook"></i>Requests</a>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Support messages">Support messages</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('chefSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Chef">Chef</span></a>
                        </li>
                        <li><a href="{{ route('customerSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Customer">Customer</span></a>
                        </li>
                        <li><a href="{{ route('riderSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Rider">Rider</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-area-wrapper">
            <div class="sidebar-left">
                <div class="sidebar">
                    <!-- app chat sidebar start -->
                    <div class="chat-sidebar card">
                        <div class="chat-sidebar-list-wrapper mt-2">
                            <h6 class="px-2 pb-25 mb-0">CHATS</h6>
                            @php
                                $ids = App\Chat::where('to', 0)->pluck('from');
                                $chats = App\Customer::select('id', 'first_name', 'last_name')->whereIn('id', $ids)->get();
                            @endphp
                            <ul class="chat-sidebar-list">
                                {{-- @foreach ($chats as $item)
                                <li class="contact">
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img id="user-{{$item->id}}-img" src="{{($item->image != '')? $item->image: '/user_avatar.png'}}" height="36" width="36" alt="">
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0" id="user-{{$item->id}}-name">{{$item->first_name.' ' . $item->last_name}}</h6>
                                    @php
                                        $unreadCount = App\Chat::where('to', 0)->where('from', $item->id)->where('is_read', 0)->count();
                                    @endphp
                                            <span class="text-muted badge {{($unreadCount == 0)? 'hidden': 'show'}}" id="user-{{$item->id}}-notifications">{{$unreadCount}}</span>
                                        </div>
                                    </div>
                                </li>    
                                @endforeach --}}
                            </ul>
                            <h6 class="px-2 pt-2 pb-25 mb-0">CONTACTS</h6>
                            @php
                                $customers = App\Customer::select('id', 'first_name', 'last_name')->get();
                            @endphp
                            <ul class="chat-sidebar-list" id="people-list">
                                @foreach ($customers as $item)
                                <li class="contact" id="{{$item->id}}">
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img id="user-{{$item->id}}-img" src="{{($item->image != '')? $item->image: '/user_avatar.png'}}" height="36" width="36" alt="">
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0" id="user-{{$item->id}}-name">{{$item->first_name.' ' . $item->last_name}}</h6>
                                    @php
                                        $unreadCount = App\Chat::where('to', 0)->where('from', $item->id)->where('is_read', 0)->count();
                                    @endphp
                                            <span class="text-muted badge {{($unreadCount == 0)? 'hidden': 'show'}}" id="user-{{$item->id}}-notifications">{{$unreadCount}}</span>
                                        </div>
                                    </div>
                                </li>    
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- app chat sidebar ends -->
                </div>
            </div>
            <div class="content-right">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="content-header row">
                    </div>
                    <div class="content-body">
                        <!-- app chat overlay -->
                        <div class="chat-overlay"></div>
                        <!-- app chat window start -->
                        <section class="chat-window-wrapper">
                            <div class="chat-start">
                                <span class="bx bx-message chat-sidebar-toggle chat-start-icon font-large-3 p-3 mb-1"></span>
                                <h4 class="d-none d-lg-block py-50 text-bold-500">Select a contact to start a chat!</h4>
                                <button class="btn btn-light-primary chat-start-text chat-sidebar-toggle d-block d-lg-none py-50 px-1">Start
                                    Conversation!</button>
                            </div>
                            <div class="chat-area d-none">
                                <div class="chat-header">
                                    <header class="d-flex justify-content-between align-items-center border-bottom px-1 py-75">
                                        <div class="d-flex align-items-center">
                                            <div class="chat-sidebar-toggle d-block d-lg-none mr-1"><i class="bx bx-menu font-large-1 cursor-pointer"></i>
                                            </div>
                                            <div class="avatar chat-profile-toggle m-0 mr-1">
                                                <img class="chat-with-img" src="" alt="avatar" height="36" width="36" />
                                            </div>
                                            <h6 class="mb-0 chat-with"></h6>
                                        </div>
                                    </header>
                                </div>
                                <!-- chat card start -->
                                <div class="card chat-wrapper shadow-none">
                                    <div class="card-content">
                                        <div class="card-body chat-container">
                                            <div class="chat-content">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer chat-footer border-top px-2 pt-1 pb-0 mb-1">
                                        <form class="d-flex align-items-center" id="chat-form">
                                            @csrf
                                            <input type="hidden" name="to" id="receiver">
                                            <input type="text" class="form-control chat-message-send mx-1" placeholder="Type your message here..." id="message" name="message">
                                            <button type="submit" class="btn btn-primary glow send d-lg-flex"><i class="bx bx-paper-plane"></i>
                                                <span class="d-none d-lg-block ml-1">Send</span></button>
                                        </form>
                                    </div>
                                </div>
                                <!-- chat card ends -->
                            </div>
                        </section>
                        <!-- app chat window ends -->
                        <!-- app chat profile right sidebar start -->
                        <section class="chat-profile">
                            <header class="chat-profile-header text-center border-bottom">
                                <span class="chat-profile-close">
                                    <i class="bx bx-x"></i>
                                </span>
                                <div class="my-2">
                                    <div class="avatar">
                                        <img src="/admin/app-assets/images/portrait/small/avatar-s-26.jpg" alt="chat avatar" height="100" width="100">
                                    </div>
                                    <h5 class="app-chat-user-name mb-0">Elizabeth Elliott</h5>
                                    <span>Devloper</span>
                                </div>
                            </header>
                            <div class="chat-profile-content p-2">
                                <h6 class="mt-1">ABOUT</h6>
                                <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                <h6 class="mt-2">PERSONAL INFORMATION</h6>
                                <ul class="list-unstyled">
                                    <li class="mb-25">email@gmail.com</li>
                                    <li>+1(789) 950-7654</li>
                                </ul>
                            </div>
                        </section>
                        <!-- app chat profile right sidebar ends -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0"><button class="btn btn-menurie btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button></p>
        </footer>
    <!-- END: Footer-->
    <!-- BEGIN: Vendor JS-->
    <script src="/admin/app-assets/vendors/js/vendors.min.js"></script>
    <script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/admin/app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="/admin/app-assets/js/core/app-menu.js"></script>
    <script src="/admin/app-assets/js/core/app.js"></script>
    <script src="/admin/app-assets/js/scripts/components.js"></script>
    <script src="/admin/app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/admin/app-assets/js/scripts/pages/app-chat.js"></script>
    <!-- END: Page JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    $(document).on('click','#people-list .contact',function(){
        load_more = true;
        offset = 0;
        limit = 20;
        var id = $(this).attr('id');
        $("#receiver").val(id);
        //Hidden Notification
        $("#user-"+ id +' .wrap > .notifications').html("0").addClass('hidden');
        var sender = $("#user-"+ id +'-name').html();
        var sender_image = $("#user-"+ id +'-img').attr('src');
        $(".chat-with").html(sender);
        $(".chat-with-img").attr('src',sender_image);
        /* Fetch Latest Messages */
        $('.chat-content').html("");
        fetch_messages(id, limit, offset, true);
    });

    $('.chat-notification').html(0).addClass('show').removeClass('hidden');

/*---------- Fetch latest messages -----------*/
    function fetch_messages(user_id, limit, offset,scroll){
        if(scroll == false){
            var firstMsg = $('.chat-content li:first');
            var curOffset = $(firstMsg).offset().top - $('.chat-content').scrollTop();
        }
        if(user_id == null){
            $("#chat-preloader").fadeOut(100);
            return;
        }
        $.ajax({
            url: '{{ url('admin/chat/get_messages') }}/' + user_id + '/' + limit + '/' + offset,
            beforeSend: function(){
                $("#chat-preloader").fadeIn(100);
            },
            success: function(data){
                $("#chat-preloader").fadeOut(100);
                var sender_image = $("#user-"+ user_id +'-img').attr('src');
        console.log(sender_image);
                var json = JSON.parse(data);
                if ( json.length == 0 )
                    load_more = false;
                $.each(json, function(key, msg) {
                    var d = new Date(msg.created_at),
                            month = '' + (d.getMonth()),
                            day = '' + d.getDate(),
                            year = d.getFullYear(),
                            hours = d.getHours(),
                            minutes = d.getMinutes();
                    var date = year+ ', '+month+' '+day+' - '+hours+':'+minutes;
                    if(msg.from == 0){
                            $('<div class="chat"><div class="chat-avatar"><a class="avatar m-0"><img src="/user.png"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + msg.message + '</p><span class="chat-time">'+date+'</span></div></div></div>').prependTo($('.chat-content'));
                    }else{
                            $('<div class="chat chat-left"><div class="chat-avatar"><a class="avatar m-0"><img src="'+ sender_image +'"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + msg.message + '</p><span class="chat-time">'+date+'</span></div></div></div>').prependTo($('.chat-content'));
                    }
                });

                if(scroll == true)
                    $(".chat-content").stop().animate({ scrollTop: $(".chat-content")[0].scrollHeight}, 200);
                else
                    $(".chat-content").stop().animate({ scrollTop: firstMsg.offset().top - curOffset}, 500);
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    $(document).on('submit','#chat-form',function(){
        event.preventDefault();
        newMessage();
    });

    /*------------* Send New Message *-------------*/
    function newMessage() {
        var currentdate = new Date();
        var date = currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
        message = $("#message").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url: '{{ url('admin/chat/send_message') }}',
            data:  new FormData($("#chat-form")[0]),
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('#message').val(null);
            },
            success: function(data){
                $("#chat-preloader").fadeOut(100);
                var json = JSON.parse(data);
                console.log(json);
                if(json['result'] == true){
                    var msg = json.data;
                    console.log(msg);
                    $(".un-send").prev().html(json['data']['message']);
                    $(".un-send").prev().attr('title',json['data']['created_at']);
                    $(".un-send").remove();
                    $('[data-toggle="tooltip"]').tooltip();
                    var d = new Date(msg.created_at),
                            month = '' + (d.getMonth()),
                            day = '' + d.getDate(),
                            year = d.getFullYear(),
                            hours = d.getHours(),
                            minutes = d.getMinutes();
                    var date = year+ ', '+month+' '+day+' - '+hours+':'+minutes;
                    if(msg.from == 0){
                            var sender_image = $("#user-"+ msg.to +'-img').attr('src');
                            // $('<div class="chat"><div class="chat-avatar"><a class="avatar m-0"><img src="'+ sender_image +'"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + msg.message + '</p><span class="chat-time">'+msg.created_at+'</span></div></div></div>').prependTo($('.chat-content'));
                            $('.chat-content').append('<div class="chat"><div class="chat-avatar"><a class="avatar m-0"><img src="/user.png"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + msg.message + '</p><span class="chat-time">'+date+'</span></div></div></div>');
                    }else{
                            var sender_image = $("#user-"+ msg.from +'-img').attr('src');
                            // $('<div class="chat chat-left"><div class="chat-avatar"><a class="avatar m-0"><img src="'+ sender_image +'"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + msg.message + '</p><span class="chat-time">'+msg.created_at+'</span></div></div></div>').prependTo($('.chat-content'));
                            $('.chat-content').append('<div class="chat chat-left"><div class="chat-avatar"><a class="avatar m-0"><img src="'+ sender_image +'"alt="avatar" height="36" width="36" /></a></div><div class="chat-body"><div class="chat-message"><p>' + msg.message + '</p><span class="chat-time">'+date+'</span></div></div></div>');
                    }
                    $(".chat-content").animate({ scrollTop: $(".chat-content").scrollHeight}, 1000);
                }else{
                    $(".un-send").parent().remove();
                    $('#message').val(message);
                    $.each(json['message'], function(key, msg) {
                        toastr.error( msg );
                    });
                }
            },
            error: function (request, status, error) {
                $('#message').val(message);
                console.log(request.responseText);
            }
        });
    }
</script>
</body>
<!-- END: Body-->
</html>