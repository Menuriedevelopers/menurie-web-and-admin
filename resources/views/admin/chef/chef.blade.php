@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users view start -->
            <section class="users-view">
                <!-- users view media object start -->
                @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
                @endif
                <div class="row">
                    <div class="col-12 col-sm-7">
                        <div class="media mb-2">
                            <a class="mr-1" href="#">
                                @if ($chef->image == '')
                                    <img height="64" width="64" src="/app-assets/images/placeholderImage.png" alt="user">
                                @else
                                <img height="64" width="64" src="{{ $chef->image }}" alt="{{ $chef->first_name }}">    
                                @endif
                            </a>
                            <div class="media-body pt-25">
                            <h4 class="media-heading"><span class="users-view-name">{{ $chef->first_name.' '.$chef->last_name }}</span></h4>
                                <span>ID:</span>
                                <span class="users-view-id">{{ $chef->id }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-5 px-0 d-flex justify-content-end align-items-center px-1 mb-2">
                        @if ($chef->approved == 0)
                        <a href="#" data-toggle="modal" data-target="#info" onclick="verifyChef({{ $chef->id }})" class="btn btn-sm mr-25 border">Approve</a>    
                        @endif
                        <a href="#" data-toggle="modal" data-target="#warning" onclick="deleteChef({{ $chef->id }})" class="btn btn-sm btn-primary">{{($chef->suspended == 1) ? 'Unsuspend' : 'Suspend'}}</a>
                    </div>
                </div>
                <!-- users view media object ends -->
                <!-- users view card data start -->
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            @if($breakfast != 0 || $lunch != 0 || $dinner != 0 || $midnight != 0)
                                <div class="row bg-warning bg-lighten-5 rounded mb-2 mx-25 text-center text-lg-left">
                                    <div class="col-12 col-sm-3 p-2">
                                        <h6 class="text-warning mb-0">Breakfast: <span class="font-large-1 align-middle">{{$breakfast}}</span></h6>
                                    </div>
                                    <div class="col-12 col-sm-3 p-2">
                                        <h6 class="text-warning mb-0">Lunch: <span class="font-large-1 align-middle">{{$lunch}}</span></h6>
                                    </div>
                                    <div class="col-12 col-sm-3 p-2">
                                        <h6 class="text-warning mb-0">Dinner: <span class="font-large-1 align-middle">{{$dinner}}</span></h6>
                                    </div>
                                    <div class="col-12 col-sm-3 p-2">
                                        <h6 class="text-warning mb-0">Midnight: <span class="font-large-1 align-middle">{{$midnight}}</span></h6>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Registered:</td>
                                            <td>{{ $chef->created_at }}</td>
                                            </tr>
                                            {{--<tr>
                                                <td>Verified:</td>
                                                <td class="users-view-verified">
                                                    @if ($chef->verified == 1)
                                                        Yes
                                                    @else
                                                        No                                                        
                                                    @endif
                                                </td>
                                            </tr>--}}
                                            <tr>
                                                <td>Approved:</td>
                                                <td class="users-view-role">
                                                    @if ($chef->approved == 1)
                                                        Yes
                                                    @else
                                                        No                                                        
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Order Range:</td>
                                                <td>
                                                    <a href="#" data-toggle="modal" data-target="#info1" onclick="updateRange({{ $chef->id }}, {{$chef->order_range}})" class="btn btn-sm btn-primary">{{$chef->order_range}}</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <tbody>
                                                <tr>
                                                    <td>Specialities:</td>
                                                    <td>{{ $chef->cuisines}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Unavailable dishes</td>

                                                    <td>
                                                        <a href="{{route('chefRequests').'?chef='.$chef->id}}">
                                                            @php
                                                                echo App\ItemRequest::distinct('item_id')->where('chef_id', $chef->id)->where('status', 1)->where('type', 0)->count();
                                                            @endphp
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- users view card data ends -->
                <!-- users view card details start -->
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row bg-primary bg-lighten-5 rounded mb-2 mx-25 text-center text-lg-left">
                                <div class="col-12 col-sm-3 p-2">
                                    <h6 class="text-primary mb-0">Preparing:
                                        <span class="font-large-1 align-middle">
                                            @php
                                                $orders = App\Order::where('order_status', 'Preparing')->where('chef_id', $chef->id)->count();
                                                echo $orders;
                                            @endphp
                                        </span>
                                    </h6>
                                </div>
                                <div class="col-12 col-sm-3 p-2">
                                    <h6 class="text-primary mb-0">Picked: 
                                        <span class="font-large-1 align-middle">
                                            @php
                                                $picked = App\Order::where('order_status', 'Picked')->where('chef_id', $chef->id)->count();
                                                echo $picked;
                                            @endphp
                                        </span>
                                    </h6>
                                </div>
                                <div class="col-12 col-sm-3 p-2">
                                    <h6 class="text-primary mb-0">Completed: 
                                        <span class="font-large-1 align-middle">
                                            @php
                                                $completed = App\Order::where('order_status', 'Delivered')->where('chef_id', $chef->id)->count();
                                                echo $completed;
                                            @endphp
                                        </span>
                                    </h6>
                                </div>
                                <div class="col-12 col-sm-3 p-2">
                                    <h6 class="text-primary mb-0">Cancelled:
                                        <span class="font-large-1 align-middle">
                                            0
                                        </span>
                                    </h6>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-borderless">
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                                <h5 class="mb-1"><i class="bx bx-info-circle"></i> Personal Info</h5>
                                <table class="table table-borderless mb-0">
                                    <tbody>
                                        <tr>
                                            <td>Name:</td>
                                        <td class="users-view-name">{{ $chef->first_name.' '. $chef->last_name }}</td>
                                        </tr>
                                        <tr>
                                            <td>E-mail:</td>
                                        <td class="users-view-email">{{ $chef->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>Contact:</td>
                                        <td>{{ $chef->phone }}</td>
                                        </tr>
                                        {{-- <tr>
                                            <td>City:</td>
                                        <td>{{ $chef->city }}</td>
                                        </tr> --}}
                                        <tr>
                                            <td>Address:</td>
                                            <td>{{ $chef->address }}</td>
                                        </tr>
                                        {{-- <tr>
                                            <td>Area:</td>
                                            <td>{{ $chef->area }}</td>
                                        </tr>
                                        <tr>
                                            <td>Post code:</td>
                                            <td>{{ $chef->post_code }}</td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- users view card details ends -->

            </section>
            <!-- users view ends -->
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="modal fade text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title white" id="myModalLabel130">Approve Chef</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <form id="verifyForm" >
                    @csrf
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>
                <button type="button" class="btn btn-info ml-1" data-dismiss="modal" onclick="formSubmit()">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Approve</span>
                </button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title white" id="myModalLabel140">{{($chef->suspended == 1) ? 'Unsuspending' : 'suspending'}} chef</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to {{($chef->suspended == 1) ? 'unsuspend' : 'suspend'}} this chef?
            </div>
            <div class="modal-footer">
                <form id="deleteForm">
                    @csrf
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>

                <button type="button" class="btn btn-warning ml-1" data-dismiss="modal" onclick="formSubmit1()">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">{{($chef->suspended == 1) ? 'Unsuspend' : 'Suspend'}}</span>
                </button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="info1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title white" id="myModalLabel140">Update order range of chef</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <form id="updateForm">
                @csrf
            <div class="modal-body">
                <input type="text" class="form-control" id="rangeInput" name="range">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>

                <button type="submit" class="btn btn-success ml-1">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Update</span>
                </button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="../../../app-assets/js/scripts/modal/components-modal.js"></script>
<script type="text/javascript">
    function verifyChef(id)
    {
        var id = id;
        var url = '{{ route("verifyChef", ":id") }}';
        url = url.replace(':id', id);
        $("#verifyForm").attr('action', url+'/');
    }

    function formSubmit()
    {
        $("#verifyForm").submit();
    }
    
    function deleteChef(id)
    {
        var id = id;
        var url = '{{ route("deleteChef", ":id") }}';
        url = url.replace(':id', id);
        $("#deleteForm").attr('action', url+'/');
    }

    function formSubmit1()
    {
        $("#deleteForm").submit();
    }
    
    function updateRange(id, range)
    {
        var id = id;
        var url = '{{ route("updateChefRange", ":id") }}';
        url = url.replace(':id', id);
        console.log(range);
        $("#rangeInput").val(range);
        $("#updateForm").attr('action', url+'/');
    }
</script>
@endsection