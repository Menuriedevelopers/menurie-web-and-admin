@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Chefs</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('indexPage') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- div start -->
                <div class="row border rounded py-2 mb-2">
                    <div class="col-12 col-sm-6 col-lg-3 mt-2">
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mt-2">
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mt-2">
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mt-2">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle mr-1" type="button" id="dropdownMenuButtonIcon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-export mr-50"></i> Export
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonIcon" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                                <a class="dropdown-item" id="pdf" href="#">PDF</a>
                                <a class="dropdown-item" id="csv" href="#">CSV</a>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of all chefs</h4>
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table width="100" class="table table-hover mb-0">
                                    <col width="15%">
                                    {{-- <col width="15%"> --}}
                                    <col width="15%">
                                    <col width="15%">
                                    <col width="10%">
                                    <col width="44%">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>Chef name</th>
                                            {{-- <th>Image</th> --}}
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($chefs as $item)
                                            <tr>
                                                <td class="text-bold-500">
                                                  <a href="{{ route('chefPage',$item->id) }}">  {{ $item->first_name.' '.$item->last_name }}</a>
                                                </td>
                                                {{-- <td>
                                                    @if ($item->image == '')
                                                        <img width="100" src="/app-assets/images/placeholderImage.png" alt="user">
                                                    @else
                                                    <img width="100" src="{{ $item->image }}" alt="{{ $item->first_name }}">    
                                                    @endif
                                                </td> --}}
                                                <td>{{ $item->email }}</td>
                                                <td>{{ $item->phone }}</td>
                                                <td>
                                                    @if ($item->approved == 1)
                                                    <span class="badge badge-light-success">Approved</span>
                                                    @else
                                                    <span class="badge badge-light-danger">Not approved</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <button class="delay btn btn-danger" data-toggle="tooltip" title="Unavailable dishes" data-delay="500">
                                                        @php
                                                            echo App\ItemRequest::distinct('item_id')->where('chef_id', $item->id)->where('status', 1)->where('type', 0)->count();
                                                        @endphp
                                                    </button>
                                                    <button class="delay btn btn-warning" data-toggle="tooltip" title="Pending orders" data-delay="500">
                                                        @php
                                                            echo App\Order::where('chef_id', $item->id)->where('order_status', 'Preparing')->orWhere('chef_id', $item->id)->where('order_status', 'Picked')->count();
                                                        @endphp
                                                    </button>
                                                    <button class="delay btn btn-success" data-toggle="tooltip" title="Completed orders" data-delay="500">
                                                        @php
                                                            echo App\Order::where('chef_id', $item->id)->where('order_status', 'Delivered')->count();
                                                        @endphp
                                                    </button>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No chefs registered yet</h3> </td> </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                {{-- <div class="mx-1">{{$chefs->links()}}</div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- div end -->
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@section('scripts')
<script src="/admin/app-assets/js/scripts/tableHTMLExport.js"></script>
<script>
  $('#csv').on('click',function(){
    $("#content").tableHTMLExport({type:'csv',filename:'customers.csv',});
  });
  $('#pdf').on('click',function(){
    $("#content").tableHTMLExport({type:'pdf',filename:'customers.pdf',});
  });
</script>
@endsection