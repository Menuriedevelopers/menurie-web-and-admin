<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><button class="btn btn-menurie btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button></p>
</footer>
<!-- END: Footer-->
@include('admin.layouts.scripts')
@yield('scripts')
</body>
<!-- END: Body-->
</html>