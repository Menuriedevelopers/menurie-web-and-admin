@php
    $categoriesCount = App\Category::count();
    $dishesCount = App\Item::where('special', 0)->count();
    $specialDishesCount = App\Item::where('special', 1)->where('approved', 1)->count();
    $unapprovedCount =App\Item::where('special', 1)->where('approved', 0)->count();
    
    $customersCount = App\Customer::count();
    $newCustomersCount = App\Customer::whereDate('created_at', date('Y-m-d'))->count();
    $activeactive = App\Order::distinct('customer_id')->where('created_at', '>=', date('Y-m-d', strtotime("-7 days")))->pluck('customer_id');
    $activeCustomersCount = App\Customer::whereIn('id', $activeactive)->count();
    $inactiveactive = App\Order::distinct('customer_id')->where('created_at', '<', date('Y-m-d', strtotime("-7 days")))->pluck('customer_id');
    $inactiveCustomersCount = App\Customer::whereIn('id', $inactiveactive)->count();
    $realtimeactive = App\Order::distinct('customer_id')->where('created_at', date('Y-m-d'))->where('order_status', '!=', 'Delivered')->orWhere('created_at', date('Y-m-d'))->where('order_status', '!=', 'Cancelled')->distinct('customer_id')->pluck('customer_id');
    $realtimeCustomersCount = App\Customer::whereIn('id', $realtimeactive)->count();

    $ridersCount = App\Rider::count();
    $suspendedRidersCount = App\Rider::where('suspended', 1)->latest()->count();
    $activeRidersCount = App\Rider::where('online', 1)->latest()->count();
    $inactiveRidersCount = App\Rider::where('online', 0)->latest()->count();

    $chefsCount = App\Chef::count();
    $suspendedChefsCount = App\Chef::where('suspended', 1)->count();
    $get = App\Chef::get();
    $active = [];
    $inactive = [];
    foreach ($get as $item) {
        date_default_timezone_set($item->timezone);
        $yesterday = date('l', strtotime("-1 days"));
        $time = date('H:i');
        $day = date('l');
        $getTime = App\ChefTiming::where('chef_id', $item->id)->where('day', $day)->first();
        $yesterdayTime = App\ChefTiming::where('chef_id', $item->id)->where('day', $yesterday)->first();
        if ($yesterdayTime) {
            if ($yesterdayTime->time != 'Closed') {
                $breakTime = explode('-', $yesterdayTime->time);
                $startTime = date_format(date_create($breakTime[0]), 'H:i');
                $startTimeHour = date_format(date_create($breakTime[0]), 'H');
                $endTime = date_format(date_create($breakTime[1]), 'H:i');
                $endTimeHour = date_format(date_create($breakTime[1]), 'H');
                if ($endTime < $startTime)
                    $active[] = $item->id;
            }
        }
        if (!$getTime)
            $inactive[] = $item->id;
        else {
            if ($getTime->time == 'Closed')
                $inactive[] = $item->id;
            else {
                $breakTime = explode('-', $getTime->time);
                $startTime = date_format(date_create($breakTime[0]), 'H:i');
                $endTime = date_format(date_create($breakTime[1]), 'H:i');
                $dateA = date('Y-m-d H:m:s');
                $d1 = new DateTime($startTime);
                $d2 = new DateTime();
                $interval = $d1->diff($d2);
                if ($interval->invert == 1 && $interval->i <= 30)
                    $inactive[] = $item->id;
                if ($startTime > $time)
                    $inactive[] = $item->id;
                elseif ($endTime < $startTime)
                    $active[] = $item->id;
                elseif ($startTime <= $time && $endTime > $time) {
                    $active[] = $item->id;
                } elseif ($endTime < $time)
                    $inactive[] = $item->id;
            }
        }
    }
    $ordersCount = App\Order::count();

    $requestsCount= App\ItemRequest::whereHas('chef')->count();
@endphp
<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="">
                    <div class="brand-logo">
                    </div>
                    <h2 class="brand-text mb-0">Menurie</h2>
                </a></li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i style="color:#8D5440;" class="bx bx-x d-block d-xl-none font-medium-4"></i><i style="color:#8D5440;" class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block" data-ticon="bx-disc"></i></a>
            </li>
        </ul>
    </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
                <li class=" nav-item"><a href="{{ route('indexPage') }}"><i class="menu-livicon" data-icon="desktop"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Categories">Categories</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('categories') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$categoriesCount}}</span></a>
                        </li>
                    <li><a href="{{ route('addCategory') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Dishes">Dishes</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('items') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$dishesCount}}</span></a>
                        </li>
                    <li><a href="{{ route('addItem') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Special Dishes">Special Dishes</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('special') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$specialDishesCount}}</span></a>
                        </li>
                        <li><a href="{{ route('unapprovedSpecial') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Unapproved">Unapproved</span><span class="badge badge-success ml-2">{{$unapprovedCount}}</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href=""><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="Invoice">Chefs</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('chefsListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$chefsCount}}</span></a>
                        </li>
                        <li><a href="{{ route('chefsListPage') }}?type=active"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Active">Active</span><span class="badge badge-success ml-2">{{count($active)}}</span></a>
                        </li>
                        <li><a href="{{ route('chefsListPage') }}?type=inactive"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Inactive">Inactive</span><span class="badge badge-success ml-2">{{count($inactive)}}</span></a>
                        </li>
                        <li><a href="{{ route('chefsListPage') }}?type=suspended"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Suspended">Suspended</span><span class="badge badge-success ml-2">{{$suspendedChefsCount}}</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="User">Customers</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('customersListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$customersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('customersListPage') }}?type=new"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="New">New</span><span class="badge badge-success ml-2">{{$newCustomersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('customersListPage') }}?type=active"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Active">Active</span><span class="badge badge-success ml-2">{{$activeCustomersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('customersListPage') }}?type=inactive"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Inactive">Inactive</span><span class="badge badge-success ml-2">{{$inactiveCustomersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('customersListPage') }}?type=realtime"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Realtime">Realtime</span><span class="badge badge-success ml-2">{{$realtimeCustomersCount}}</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="User">Riders</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('ridersListPage') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span><span class="badge badge-success ml-2">{{$ridersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('ridersListPage') }}?type=active"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Active">Active</span><span class="badge badge-success ml-2">{{$activeRidersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('ridersListPage') }}?type=inactive"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Inactive">Inactive</span><span class="badge badge-success ml-2">{{$inactiveRidersCount}}</span></a>
                        </li>
                        <li><a href="{{ route('ridersListPage') }}?type=suspended"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="suspended">Suspended</span><span class="badge badge-success ml-2">{{$suspendedRidersCount}}</span></a>
                        </li>
                        {{-- <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="View">Orders</span></a>
                        </li>
                        <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Edit">Support</span></a>
                        </li> --}}
                    </ul>
                </li>
                <li class=" nav-item"><a href="{{ route('ordersListPage') }}"><i class="menu-livicon" data-icon="notebook"></i>Orders<span class="badge badge-success ml-2">{{$ordersCount}}</span></a>
                </li>
                <li class=" nav-item"><a href="{{ route('sales') }}"><i class="menu-livicon" data-icon="comments"></i>Sales</a>
                </li>
                <li class=" nav-item"><a href="{{ route('topSales') }}"><i class="menu-livicon" data-icon="comments"></i>Top Sale dishes</a>
                </li>
                <li class=" nav-item"><a href="{{ route('chefRequests') }}"><i class="menu-livicon" data-icon="notebook"></i>Requests<span class="badge badge-success ml-2">{{$requestsCount}}</span></a>
                </li>
                <li class="nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Tickets">Tickets</span></a>
                    <ul class="menu-content">
                        <li class="has-sub is-shown"><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Chef">Chef</span></a>
                            <ul class="menu-content" style="">
                                <li class="is-shown"><a href="{{ route('chefTickets',0) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Open">Open</span></a>
                                </li>
                                <li class="is-shown"><a href="{{ route('chefTickets',1) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Closed">Closed</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub is-shown"><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Customer">Customer</span></a>
                            <ul class="menu-content" style="">
                                <li class="is-shown"><a href="{{ route('customerTickets',0) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Open">Open</span></a>
                                </li>
                                <li class="is-shown"><a href="{{ route('customerTickets',1) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Closed">Closed</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub is-shown"><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Rider">Rider</span></a>
                            <ul class="menu-content" style="">
                                <li class="is-shown"><a href="{{ route('riderTickets',0) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Open">Open</span></a>
                                </li>
                                <li class="is-shown"><a href="{{ route('riderTickets',1) }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Closed">Closed</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item"><a href="#"><i class="menu-livicon" data-icon="thumbnails-small"></i><span class="menu-title" data-i18n="Support messages">Support messages</span></a>
                    <ul class="menu-content">
                        <li><a href="{{ route('chefSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Chef">Chef</span></a>
                        </li>
                        <li><a href="{{ route('customerSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Customer">Customer</span></a>
                        </li>
                        <li><a href="{{ route('riderSupports') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Rider">Rider</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->