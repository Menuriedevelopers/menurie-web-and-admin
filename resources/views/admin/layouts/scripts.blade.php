<!-- BEGIN: Vendor JS-->
<script src="/admin/app-assets/vendors/js/vendors.min.js"></script>
<script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
<script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="/admin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<!-- BEGIN: Theme JS-->
<script src="/admin/app-assets/js/scripts/configs/vertical-menu-light.js"></script>
<script src="/admin/app-assets/js/core/app-menu.js"></script>
<script src="/admin/app-assets/js/core/app.js"></script>
<script src="/admin/app-assets/js/scripts/components.js"></script>
<script src="/admin/app-assets/js/scripts/footer.js"></script>
<!-- END: Theme JS-->
<script>
    $(document).ready(function(){
        $(".alert").slideDown(300).delay(5000).slideUp(300);
    });
</script>
@stack('scripts')