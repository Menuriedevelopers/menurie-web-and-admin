@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users view start -->
            <section class="users-view">
                <!-- users view media object start -->
                <div class="row">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                        @endif
                        @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="col-12 col-sm-7">
                        <div class="media mb-2">
                            <div class="media-body pt-25">
                            <h4 class="media-heading"><span class="users-view-name">{{ $category->name }}</span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-5 px-0 d-flex justify-content-end align-items-center px-1 mb-2">
                        <a href="#" class="btn btn-sm mr-25 border"><i class="bx bx-envelope font-small-3"></i></a>
                        <a href="#" data-toggle="modal" data-target="#warning" onclick="deleteItem({{ $category->id }})" class="btn btn-sm btn-primary">@if($category->is_active == 1) Inactive @else Active @endif</a>
                    </div>
                </div>
                <div class="row" id="table-hover-row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">List of items by {{$category->name}}</h4>
                                <div class="text-right">
                                    <a href="{{ route('addItem') }}" class="btn btn-primary">Add Item</a>
                                </div>
                            </div>
                            <div class="card-content">
                                <!-- table -->
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead class="thead-menurie">
                                            <tr>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Price</th>
                                                <th>Total Items</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($items as $item)
                                                <tr>
                                                <td class="text-bold-500"><a href="{{route('item',$item->id)}}">{{ $item->item_name }}</a></td>
                                                <td class="text-bold-500">{{ $item->description }}</td>
                                                <td class="text-bold-500">{{ $item->price }}</td>
                                                <td class="text-bold-500">{{ $item->count }}</td>
                                                <td>
                                                <a href="{{ route('editItem',$item->id) }}" class="btn btn-outline-warning mr-1 mb-1" ><i class="bx bx-edit" ></i><span class="align-middle ml-25">Edit</span></a>                 
                                                </td>
                                                </tr>
                                            @empty
                                                <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No items added by {{$category->name}}</h3> </td> </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="modal fade text-left" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title white" id="myModalLabel140">Deleting Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to @if($category->is_active == 1) inactive @else active @endif this category?
            </div>
            <div class="modal-footer">
                <form id="deleteForm">
                    @csrf
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>

                <button type="button" class="btn btn-warning ml-1" data-dismiss="modal" onclick="formSubmit1()">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">@if($category->is_active == 1) Inactive @else Active @endif</span>
                </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('app-assets/js/scripts/modal/components-modal.js') }}"></script>
<script type="text/javascript">
    function deleteItem(id)
    {
        var id = id;
        var url = '{{ route("deleteCategory", ":id") }}';
        url = url.replace(':id', id);
        $("#deleteForm").attr('action', url+'/');
    }

    function formSubmit1()
    {
        $("#deleteForm").submit();
    }
</script>
@endsection