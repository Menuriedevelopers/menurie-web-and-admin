@extends('admin.main')
@section('styles')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Top sale dishes</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('indexPage') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
            @endif
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="row border rounded py-2 mb-2">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <label for="filter">Filter</label>
                            <fieldset class="form-group">
                                <select class="form-control" id="filter">
                                    <option value="0">Filter orders</option>
                                    <option value="1">Daily</option>
                                    <option value="2">Weekly</option>
                                    <option value="3">Monthly</option>
                                    <option value="4">Yearly</option>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">  
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <label for="">Get orders by date range</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" name="daterange" placeholder="Select Date">
                                <div class="form-control-position">
                                    <i class="bx bx-calendar-check"></i>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row border rounded py-2 mb-2">
                        <div class="col-12 col-sm-6 col-lg-3 mt-2">
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mt-2">
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mt-2">
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mt-2">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle mr-1" type="button" id="dropdownMenuButtonIcon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="bx bx-export mr-50"></i> Export
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonIcon" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                                    <a class="dropdown-item" id="pdf" href="#">PDF</a>
                                    <a class="dropdown-item" id="csv" href="#">CSV</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="content" class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of top sale dishes</h4>
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table id="table" class="table table-hover mb-0">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>#</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $totalQ =0;
                                            $totalP=0;
                                        @endphp
                                        @forelse ($items as $item)
                                        @php
                                            $totalQ +=$item->quantity;
                                            $totalP += $item->price;
                                        @endphp
                                            <tr>
                                                <td class="text-bold-500">
                                                    {{$loop->iteration}}
                                                </td>
                                                <td class="text-bold-500">{{ $item->item_name }}</td>
                                                <td>{{$item->quantity}}</td>
                                                <td>{{'$'.number_format($item->price,2)}}</td>
                                                <td>
                                                </td>
                                            </tr>
                                            @if ($loop->last)
                                                <tr>
                                                    <td colspan="2">Total Quantity</td>
                                                    <td colspan="3">{{$totalQ}}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Total price</td>
                                                    <td colspan="3">{{'$'.number_format($totalP, 2)}}</td>
                                                </tr>
                                            @endif
                                        @empty
                                            <tr>
                                                <td class="text-center text-bold-500" colspan="8"> <h3>No orders yet</h3> </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
<script src="/admin/app-assets/js/scripts/tableHTMLExport.js"></script>
<script>
    
    $("#filter").on('change', function(){
      var value = $(this).val();
        $.ajax({
            url: "{{request()->url()}}?value="+value,
            method: 'get',
            success: function(result){
                console.log(result);
                $('#content').html(result);

            },error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                }
        });
    });
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    }, function(start, end, label) {
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        $.ajax({
            url: "{{request()->url()}}?start="+start.format('YYYY-MM-DD')+"&end="+end.format('YYYY-MM-DD'),
            method: 'get',
            success: function(result){
                console.log(result);
                $('#content').html(result);

            },error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                }
        });
    });
  $('#csv').on('click',function(){
    $("#content").tableHTMLExport({type:'csv',filename:'sales.csv'});
  })
  $('#pdf').on('click',function(){
    $("#content").tableHTMLExport({type:'pdf',filename:'sales.pdf'});
  })
</script>
@endpush