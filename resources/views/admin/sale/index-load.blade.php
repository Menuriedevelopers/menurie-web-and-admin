<div class="card-header">
    <h4 class="card-title">List of item's sales</h4>
</div>
<div class="card-content">
    <!-- table -->
    <div class="table-responsive">
        <table id="table" class="table table-hover mb-0">
            <thead class="thead-menurie">
                <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $totalQ =0;
                    $totalP=0;
                @endphp
                @forelse ($items as $item)
                @php
                    $totalQ +=$item->quantity;
                    $totalP += $item->price;
                @endphp
                    <tr>
                        <td class="text-bold-500">
                            {{$loop->iteration}}
                        </td>
                        <td class="text-bold-500">{{ $item->item_name }}</td>
                        <td>{{$item->quantity}}</td>
                        <td>{{'$'.number_format($item->price,2)}}</td>
                        <td>
                        </td>
                    </tr>
                    @if ($loop->last)
                        <tr>
                            <td colspan="2">Total Quantity</td>
                            <td colspan="3">{{$totalQ}}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Total price</td>
                            <td colspan="3">{{'$'.number_format($totalP, 2)}}</td>
                        </tr>
                    @endif
                @empty
                    <tr>
                        <td class="text-center text-bold-500" colspan="8"> <h3>No sales</h3> </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>