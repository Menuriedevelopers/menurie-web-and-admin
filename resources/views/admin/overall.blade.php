@extends('admin.main')
@push('styles')
    
<link rel="stylesheet" type="text/css" href="/admin/app-assets/css/pages/faq.css">
@endpush
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Overview</h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{route('indexPage')}}"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    @php
                                        if($chef != 0){
                                           $person = App\Chef::find($chef); 
                                        }
                                    @endphp
                                    <li class="breadcrumb-item active">{{($chef == 0)? 'Overview': 'OVer of '. $person->first_name}}
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <section class="faq-bottom">
                                                <div class="row d-flex justify-content-center mb-5">
                                                    <div class="col-sm-12 col-md-3 text-center border rounded p-2">
                                                        <h5>Total Earnings</h5>
                                                        <p class="text-muted font-medium-1">{{$earnings}}</p>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3 text-center border color-gray-faq rounded p-2">
                                                        <h5>Average earnings</h5>
                                                        <p class="text-muted font-medium-1">{{$average}}</p>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3 text-center border color-gray-faq rounded p-2">
                                                        <h5>Orders completed</h5>
                                                        <p class="text-muted font-medium-1">{{$completed}}</p>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3 text-center border color-gray-faq rounded p-2">
                                                        <h5>Earnings this month</h5>
                                                        <p class="text-muted font-medium-1">{{$month}}</p>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section id="chartjs-charts">
                    <div class="row">
                        <!-- Bar Chart -->
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row border rounded py-2 mb-2">
                                        <div class="col-12 col-sm-6 col-lg-3 p-2">
                                            <h4>Overview</h4>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="filter">Filter</label>
                                            <fieldset class="form-group">
                                                <select class="form-control" id="filter">
                                                    <option {{($type == 0)? 'selected': ''}} value="0">Last 30 days</option>
                                                    <option {{($type == 1)? 'selected': ''}} value="1">Monthly</option>
                                                    <option {{($type == 2)? 'selected': ''}} value="2">Yearly</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="bar-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->
    </div>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
@endsection
@push('scripts')
    <script src="{{asset('admin/app-assets/vendors/js/charts/chart.min.js')}}"></script>
    <script>var $primary = '#5A8DEE',
        $success = '#39DA8A',
        $danger = '#FF5B5C',
        $warning = '#FDAC41',
        $info = '#00CFDD',
        $label_color = '#475F7B',
        grid_line_color = '#dae1e7';
    
      var themeColors = [$primary, $warning, $danger, $success, $info, $label_color];
        var barChartctx = $("#bar-chart");
  // Chart Options
  var headText = @json($text);
  var barchartOptions = {
    // Elements options apply to all of the options unless overridden in a dataset
    // In this case, we are setting the border of each bar to be 2px wide
    elements: {
      rectangle: {
        borderWidth: 2,
        borderSkipped: 'left'
      }
    },
    responsive: true,
    maintainAspectRatio: false,
    responsiveAnimationDuration: 500,
    legend: { display: false },
    scales: {
      xAxes: [{
        display: true,
        gridLines: {
          color: grid_line_color,
        },
        scaleLabel: {
          display: true,
        }
      }],
      yAxes: [{
        display: true,
        gridLines: {
          color: grid_line_color,
        },
        scaleLabel: {
          display: true,
        },
        ticks: {
          stepSize: 1000
        },
      }],
    },
    title: {
      display: true,
      text: headText
    },
  };
  // Chart Data
  var index = @json($index);
  var counts = @json($counts);
  var barchartData = {
    labels: index,
    datasets: [{
      label: "Orders",
      data: counts,
      backgroundColor: themeColors,
      borderColor: "transparent"
    }]
  };
  var barChartconfig = {
    type: 'bar',

    // Chart Options
    options: barchartOptions,
    data: barchartData
  };
  // Create the chart
  var barChart = new Chart(barChartctx, barChartconfig);
  $("#filter").on('change', function(){
      var value = $(this).val();
      var chef = @json($chef);
      if(chef == 0)
      window.location.href = '/admin/overall/'+value
      else
      window.location.href = '/admin/overall/'+value+'/'+chef;
  })
    </script>
@endpush