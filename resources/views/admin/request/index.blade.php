@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Chef requests</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('indexPage') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
            @endif
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of chef requests</h4>
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>Name</th>
                                            <th>Item</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($requests as $item)
                                            <tr>
                                                <td class="text-bold-500">
                                                    @isset($item->chef->first_name)
                                                        {{$item->chef->first_name.' '.$item->chef->last_name}}
                                                    @else
                                                    Menurie Admin
                                                    @endisset
                                                </td>
                                                <td class="text-bold-500"><a href="{{route('item',$item->item_id)}}">{{ $item->item->item_name }}</a></td>
                                                <td>{{($item->type == 0)? 'Inactive request': 'Active request'}}</td>
                                                <td>{{($item->status == 0)? 'Pending': 'Accepted'}}</td>
                                                <td>
                                                    @if($item->status == 0)
                                                        <button class="btn btn-outline-success mr-1 mb-1" data-id="{{$item->id}}"><span class="align-middle ml-25">Approve</span></button>
                                                        @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td class="text-center text-bold-500" colspan="8"> <h3>No requests added</h3> </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                <div class="mx-1">{{ $requests->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.5/dist/sweetalert2.all.min.js" integrity="sha256-sq9BgeqJAlCLfjfAO+2diFGt5O8aIYNrds+yhlpFnvg=" crossorigin="anonymous"></script>
<script>
    $(".btn-outline-success").on('click', function(){
        var id = $(this).attr('data-id');
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, approve it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            swalWithBootstrapButtons.fire(
            'Approved!',
            'Item request approved.',
            'success'
            )
            window.location.href = '/admin/chef/request/approve/'+id;
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelled',
            'Item request not approved :)',
            'error'
            )
        }
    })
    });
</script>
@endpush