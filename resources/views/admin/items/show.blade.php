@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users view start -->
            <section class="users-view">
                <!-- users view media object start -->
                <div class="row">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                        @endif
                        @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="col-12 col-sm-7">
                        <div class="media mb-2">
                            <a class="mr-1" href="#">
                                <img height="64" width="64" src="{{ $item->item_image }}" alt="{{ $item->item_name }}">    
                            </a>
                            <div class="media-body pt-25">
                            <h4 class="media-heading"><span class="users-view-name">{{ $item->item_name }}</span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-5 px-0 d-flex justify-content-end align-items-center px-1 mb-2">
                        <a href="{{ route('editItem',$item->id) }}" class="btn btn-sm btn-warning" ><span class="align-middle">Edit</span></a>&nbsp;&nbsp;
                        <a href="#" data-toggle="modal" data-target="#warning" onclick="deleteItem({{ $item->id }})" class="btn btn-sm btn-primary">@if($item->is_active == 1) Inactive @else Active @endif</a>
                    </div>
                </div>
                <!-- users view media object ends -->
                <!-- users view card data start -->
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <table class="table table-borderless">
                                        <tbody>
                                            @if($item->category_id != 0)
                                                <tr>
                                                    <td>Item Category:</td>
                                                <td>{{ $item->category->name }}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td>Item Price:</td>
                                                <td class="users-view-verified">
                                                    {{ $item->price}}
                                                </td>
                                            </tr>
                                            {{-- <tr>
                                                <td>Total Items:</td>
                                                <td class="users-view-role">
                                                    {{ $item->count }}
                                                </td>
                                            </tr> --}}
                                            <tr>
                                                <td>Status:</td>
                                                <td>
                                                    @if ($item->is_active)
                                                        Active
                                                    @else
                                                        Inactive
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <tbody>
                                                <tr>
                                                    <td>Addons:</td>
                                                </tr>
                                                @foreach($item->addons as $addon)
                                                <tr>
                                                <td>{{ $addon->name }}</td>
                                                <td>{{ '$'.number_format($addon->price,2) }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- users view card data ends -->
                <!-- users view card details start -->
                @php
                    $orders = App\OrderDetail::where('item_id', $item->id)->distinct('order_id')->count();
                    $ids = App\OrderDetail::where('item_id', $item->id)->distinct('order_id')->pluck('order_id');
                    $completed = App\Order::whereIn('id', $ids)->where('order_status', 'Delivered')->count();
                    $cancelled = App\Order::whereIn('id', $ids)->where('order_status', 'Cancelled')->count();
                @endphp
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row bg-primary bg-lighten-5 rounded mb-2 mx-25 text-center text-lg-left">
                                <div class="col-12 col-sm-4 p-2">
                                    <h6 class="text-primary mb-0">Orders: <span class="font-large-1 align-middle">{{$orders}}</span></h6>
                                </div>
                                <div class="col-12 col-sm-4 p-2">
                                    <h6 class="text-primary mb-0">Completed: <span class="font-large-1 align-middle">{{$completed}}</span></h6>
                                </div>
                                <div class="col-12 col-sm-4 p-2">
                                    <h6 class="text-primary mb-0">Cancelled: <span class="font-large-1 align-middle">{{$cancelled}}</span></h6>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>Description:</td>
                                        <td>{{ $item->description }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="modal fade text-left" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title white" id="myModalLabel140">@if($item->is_active == 1) Inactive @else Active @endif Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to @if($item->is_active == 1) inactive @else active @endif this item?
            </div>
            <div class="modal-footer">
                <form id="deleteForm">
                    @csrf
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>

                <button type="button" class="btn btn-warning ml-1" data-dismiss="modal" onclick="formSubmit1()">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">@if($item->is_active == 1) Inactive @else Active @endif</span>
                </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/app-assets/js/scripts/modal/components-modal.js') }}"></script>
<script type="text/javascript">
    function deleteItem(id)
    {
        var id = id;
        var url = '{{ route("deleteItem", ":id") }}';
        url = url.replace(':id', id);
        $("#deleteForm").attr('action', url+'/');
    }

    function formSubmit1()
    {
        $("#deleteForm").submit();
    }
</script>
@endsection