@extends('admin.main')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Items</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('indexPage') }}"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
            @endif
            <div class="row" id="table-hover-row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">List of items</h4>
                            @if($type != 'special')
                                <div class="text-right">
                                    <a href="{{ route('addItem') }}" class="btn btn-primary">Add Item</a>
                                </div>
                            @endif
                        </div>
                        <div class="card-content">
                            <!-- table -->
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead class="thead-menurie">
                                        <tr>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>{{($type != 'special')? 'Category': 'Chef'}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($items as $item)
                                            <tr>
                                            <td class="text-bold-500"><a href="{{route('item',$item->id)}}">{{ $item->item_name }}</a></td>
                                            <td class="text-bold-500">{{ '$'.number_format($item->price, 2) }}</td>
                                            <td class="text-bold-500"> 
                                                @if($type != 'special') 
                                                    {{$item->category->name}}
                                                @else
                                                    @isset($item->chef->first_name)
                                                        {{$item->chef->first_name.' '.$item->chef->last_name}}
                                                    @else
                                                    Menurie Admin
                                                    @endisset
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->special == 1)
                                                    @if ($item->approved == 0)
                                                    <button class="btn btn-outline-success mr-1 mb-1" data-id="{{$item->id}}"><span class="align-middle ml-25">Approve</span></button>
                                                    @endif
                                                @else
                                                <a class="delay btn btn-success mr-1 mb-1" href="{{ route('ordersListPage').'?item='.$item->id }}" data-toggle="tooltip" title="Total orders" data-delay="500">
                                                    @php
                                                        echo App\OrderDetail::distinct('order_id')->where('item_id', $item->id)->count();
                                                    @endphp
                                                </a>
                                                    <a href="{{ route('editItem',$item->id) }}" class="btn btn-outline-warning mr-1 mb-1" ><i class="bx bx-edit" ></i><span class="align-middle ml-25">Edit</span></a>
                                                @endif
                                            </td>
                                            </tr>
                                        @empty
                                            <tr> <td class="text-center text-bold-500" colspan="8"> <h3>No items added</h3> </td> </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                <div class="mx-1">{{ $items->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.5/dist/sweetalert2.all.min.js" integrity="sha256-sq9BgeqJAlCLfjfAO+2diFGt5O8aIYNrds+yhlpFnvg=" crossorigin="anonymous"></script>
<script>
    $(".btn-outline-success").on('click', function(){
        var id = $(this).attr('data-id');
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, approve it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            swalWithBootstrapButtons.fire(
            'Approve!',
            'Are you sure you want to approve this special item?',
            'success'
            )
            window.location.href = '/admin/special/approve/'+id;
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelled',
            'Approval of special item cancelled',
            'error'
            )
        }
    })
    });
</script>
@endpush