@extends('admin.main')
@section('content')
@push('styles')
<link rel="stylesheet" type="text/css" href="/admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
<link rel="stylesheet" type="text/css" href="/admin/app-assets/css/plugins/forms/validation/form-validation.css">
@endpush
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">Account Settings</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active"> Account Settings
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- account setting page start -->
            <section id="page-account-settings">
                    <div class="row">
                        <b><span class="text-success" id="success-message"> </span><b>
                    </div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <!-- left menu section -->
                            <div class="col-md-3 mb-2 mb-md-0 pills-stacked">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center active" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="true">
                                            <i class="bx bx-cog"></i>
                                            <span>General</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" id="account-pill-password" data-toggle="pill" href="#account-vertical-password" aria-expanded="false">
                                            <i class="bx bx-lock"></i>
                                            <span>Change Password</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- right content section -->
                            <div class="col-md-9">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">
                                                    <form id="save-data" enctype="multipart/form-data">
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="controls">
                                                                        <label>Name</label>
                                                                        <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="{{Auth::user()->name}}" required data-validation-required-message="This name field is required">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="controls">
                                                                        <label>Email</label>
                                                                        <input type="text" id="email" name="email" class="form-control" placeholder="Enter email" value="{{Auth::user()->email}}"required data-validation-required-message="This email field is required">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                                <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
                                                                    changes</button>
                                                                <button type="reset" class="btn btn-light mb-1">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="tab-pane fade" id="account-vertical-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">
                                                    <form novalidate id="save-password">
                                                        <b><span class="text-success" id="success-message1"> </span><b>
                                                        <span id="error" class="text-danger"></span>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="controls">
                                                                        <label>Old Password</label>
                                                                        <input type="password" class="form-control" id="old" name="old_password" required placeholder="Old Password" data-validation-required-message="This old password field is required">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="controls">
                                                                        <label>New Password</label>
                                                                        <input type="password" name="password" id="password" class="form-control" placeholder="New Password" required data-validation-required-message="The password field is required" minlength="6">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="controls">
                                                                        <label>Retype new Password</label>
                                                                        <input type="password" name="password_confirmation" 
                                                                        id="password_confirmation" class="form-control" required data-validation-match-match="password" placeholder="New Password" data-validation-required-message="The Confirm password field is required" minlength="6">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                                <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
                                                                    changes</button>
                                                                <button type="reset" class="btn btn-light mb-1">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
</div>
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
@endsection
@push('scripts')
<script src="/admin/app-assets/js/scripts/pages/page-account-settings.js"></script>
   <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#save-data').on('submit', function(event){
        event.preventDefault();
        $('#success-message').text('');
        $('#image-error').text('');

        name = $('#name').val();
        email = $('#email').val();

        $.ajax({
          url: "{{route('saveData')}}",
          type: "POST",
          data:{
              name:name,
              email:email
          },
          success:function(response){
            console.log(response);
            if (response.status) {
              $('#success-message').text(response.message);
              $("#save-data")[0].reset();
            }
          },
          error: function(response) {
              console.log(response);
          }
         });
        });

        $('#save-password').on('submit', function(event){
        $('#success-message1').text('');
        $('#error').text('');
        event.preventDefault();
        $('#image-error').text('');

        old = $('#old').val();
        pass = $('#password').val();
        cpass = $('#password_confirmation').val();

        $.ajax({
          url: "{{route('updatePassword')}}",
          type: "POST",
          data:{
              old_password:old,
              password:pass,
              password_confirmation:cpass,
          },
          success:function(response){
            console.log(response);
            if (response.status) {
              $('#success-message1').text(response.message);
              $("#save-password")[0].reset();
              location.reload();
            }else
              $('#error').text(response.message);
          },
          error: function(response) {
              console.log(response.responseJSON)
                $('#error').text(response.responseJSON.errors.password);
          }
         });
        });
      </script>
@endpush
